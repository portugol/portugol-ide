import classNames from 'classnames';
import * as React from 'react';
import './FileNameInput.css';

type FileNameInputProps = {
    value: string,
    onChange: (value: string) => void,
    className?: string,
    refNameInput?: React.Ref<HTMLInputElement>,
};

export default (props: FileNameInputProps) => {
    const fileNameSplit = props.value.split('.');
    const fileExtension = fileNameSplit[fileNameSplit.length - 1];
    const fileNameWithoutExtension = fileNameSplit.slice(0, fileNameSplit.length - 1).join('.');

    const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (!event.target.value.includes('/')) {
            props.onChange(`${event.target.value}.${fileExtension}`);
        }
    };

    return (
        <div className={classNames('file-name-input', props.className)}>
            <input 
                className="name-input"
                type="text"
                value={fileNameWithoutExtension}
                onChange={onChange}
                ref={props.refNameInput}
            />
            <input 
                className="extension-input"
                type="text"
                value={`.${fileExtension}`}
                disabled={true}
            />
        </div>
    );
};