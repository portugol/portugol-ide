export type Promisify<T> = T extends Promise<any> ? T : Promise<T>;

export type RemoteReturn<T, B extends 'local' | 'remote'> = { 
    local: T, 
    remote: Promisify<T>,
}[B];

export type RemoteObject<T, B extends 'local' | 'remote'> = { [K in keyof T]: (...args: any[]) => RemoteReturn<any, B> };