import { isRemoteResponse, RemoteRequest, RemoteResponse } from './shared';
import { RemoteObject } from './types';

export function connectToInterface<T extends RemoteObject<T, 'remote'>>(
    interfaceName: string,
    channel: Worker | Window = self,
): { remote: T, dispose: () => void } {
    type CallHandler = (response: RemoteResponse) => void;

    type PendingCall = {
        messageId: number,
        handler: CallHandler,
    };

    let nextMessageId = 0;
    let stopped = false;
    const pendingCalls: PendingCall[] = [];

    const callRemote = (method: string, args: any[], handler: CallHandler) => {
        const messageId = nextMessageId++;
        pendingCalls.push({
            handler,
            messageId,
        });
        const request: RemoteRequest = {
            type: '@worker-bridge/REMOTE_REQUEST',
            interfaceName,
            messageId,
            args,
            method,
        };
        (channel.postMessage as any)(request);
    };
    
    const remoteObject: Partial<T> = {};
    const proxy = new Proxy<T>({} as any, {
        get: (target, property: string) => {
            if (stopped) {
                throw new Error('It is not possible to get a property of a stopped interface.');
            }

            if (!remoteObject[property]) {
                remoteObject[property] = (...args: any[]) => {
                    return new Promise((resolve, reject) => {
                        callRemote(property, args, (response) => {
                            if (response.error) {
                                reject(response.error);
                            } else {
                                resolve(response.value);
                            }
                        });
                    });
                };
            }
            return remoteObject[property];
        },
    });

    const messageListener = (event: MessageEvent) => {
        const message = event.data;
        if (isRemoteResponse(message) && message.interfaceName === interfaceName) {
            const callIndex = pendingCalls.findIndex(rc => rc.messageId === message.messageId);
            const [pendingCall] = pendingCalls.splice(callIndex, 1);
            pendingCall.handler(message);
        }
    };

    channel.addEventListener('message', messageListener);

    const dispose = () => {
        channel.removeEventListener('message', messageListener);
        stopped = true;
    };

    return {
        dispose,
        remote: proxy,
    };
}