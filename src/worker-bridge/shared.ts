export type RemoteRequest = {
    type: '@worker-bridge/REMOTE_REQUEST',
    interfaceName: string,
    messageId: number,
    method: string,
    args: any[],
};

export type RemoteResponse = {
    type: '@worker-bridge/REMOTE_RESPONSE',
    interfaceName: string,
    messageId: number,
    error?: Error,
    value?: any,
};

export function isRemoteRequest(message: any): message is RemoteRequest {
    return message.type === '@worker-bridge/REMOTE_REQUEST';
}

export function isRemoteResponse(message: any): message is RemoteResponse {
    return message.type === '@worker-bridge/REMOTE_RESPONSE';
}