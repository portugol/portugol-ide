import { isRemoteRequest, RemoteResponse } from './shared';
import { RemoteObject } from './types';

export function provideInterface<T extends RemoteObject<T, 'local'>>(
    interfaceName: string, 
    handler: T,
    channel: Worker | Window = self,
): { dispose: () => void } {
    let stopped = false;

    const respond = (messageId: number, error?: Error, value?: any) => {
        let message: RemoteResponse;

        if (stopped) {
            // If has already stopped, respond with error
            message = {
                type: '@worker-bridge/REMOTE_RESPONSE',
                interfaceName,
                messageId,
                error: new Error('Server has stopped.'),
            };    
        } else {
            message = {
                type: '@worker-bridge/REMOTE_RESPONSE',
                interfaceName,
                messageId,
                error,
                value,
            };
        }

        (channel.postMessage as any)(message);
    };

    const messageListener = async (event: MessageEvent) => {
        const message = event.data;
        
        if (isRemoteRequest(message) && message.interfaceName === interfaceName) {
            if (!handler[message.method]) {
                respond(message.messageId, new Error(`Method named "${message.method}" not implemented in the server.`));
                return;
            }

            try {
                respond(
                    message.messageId, 
                    undefined,
                    await Promise.resolve(handler[message.method](...message.args)),
                );
            } catch (error) {
                respond(message.messageId, error);
            }
        }
    };

    channel.addEventListener('message', messageListener);

    const dispose = () => {
        channel.removeEventListener('message', messageListener);
        stopped = true;
    };

    return { dispose };
}