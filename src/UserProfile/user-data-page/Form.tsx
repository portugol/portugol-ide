import * as React from 'react';
import { UserSelf } from '../../services/applications-server';
import Button from '../../ui/Button';
import Checkbox from '../../ui/Checkbox';
import Input from '../../ui/Input';
import InputWithMessage from '../../ui/InputWithMessage';
import Label from '../../ui/Label';
import TextArea from '../../ui/TextArea';
// tslint:disable-next-line:no-var-requires
const styles = require('./Form.css');

export type UserChanges = {
    name?: string,
    email?: string,
    biography?: string,
    showEmail?: boolean,
};

type FormProps = {
    user: UserSelf,
    isSaving: boolean,
    save: (changes: UserChanges) => void,
};

type FormState = {
    name: string,
    email: string,
    biography: string,
    showEmail: boolean,
    emailError: string | null,
};

export default class Form extends React.Component<FormProps, FormState> {
    state: FormState = {
        name: '',
        email: '',
        biography: '',
        showEmail: false,
        emailError: null,
    };

    createField = (fieldName: 'name' | 'email' | 'biography' | 'showEmail', isCheckBox = false) => {
        const field = {
            onChange: this.updateField(fieldName, isCheckBox),
        } as any;
        if (isCheckBox) {
            field.checked = this.state[fieldName];
        } else {
            field.value = this.state[fieldName];
        }
        return field;
    }

    updateField = (fieldName: 'name' | 'email' | 'biography' | 'showEmail', isCheckBox: boolean) => (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => {
        const target = event.target as any;
        const value = isCheckBox ? target.checked : target.value;
        this.setState({
            [fieldName]: value,
        } as any);
    }

    componentDidMount() {
        const {
            name,
            email,
            biography,
            showEmail,
        } = this.props.user;

        this.setState({
            name,
            email,
            biography,
            showEmail,
        });
    }

    componentDidUpdate(oldProps: FormProps, oldState: FormState) {
        if (oldProps.user !== this.props.user) {
            const {
                name,
                email,
                biography,
                showEmail,
            } = this.props.user;

            this.setState({
                name,
                email,
                biography,
                showEmail,
            });
        }
        if (oldState.email !== this.state.email) {
            this.setState({
                emailError: this.validateEmail(),
            });
        }
    }

    hasChanges() {
        const { name, email, biography, showEmail } = this.props.user;
        return this.state.name !== name
            || this.state.biography !== biography
            || this.state.showEmail !== showEmail
            || this.state.email !== email;
    }

    hasError() {
        return this.state.emailError !== null;
    }

    onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const { name, email, biography, showEmail } = this.props.user;
        const changes: UserChanges = {};
        if (this.state.name !== name) {
            changes.name = this.state.name;
        }
        if (this.state.email !== email) {
            changes.email = this.state.email;
        }
        if (this.state.biography !== biography) {
            changes.biography = this.state.biography;
        }
        if (this.state.showEmail !== showEmail) {
            changes.showEmail = this.state.showEmail;
        }
        this.props.save(changes);
    }

    validateEmail(): string | null {
        if (this.state.email === '') {
            return 'Informe o seu email.';
        }
        if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.state.email)) {
            return 'O email informado está com formato incorreto.';
        }
        return null;
    }

    render() {
        return (
            <form onSubmit={this.onSubmit} className={styles.container}>
                <Label className={styles.label}>
                    Nome
                </Label>
                <Input
                    {...this.createField('name')}
                    placeholder="Informe seu nome"
                    className={styles.textInput}
                    disabled={this.props.isSaving}
                />
                <Label className={styles.label}>
                    Email
                </Label>
                <InputWithMessage 
                    error={this.state.emailError}
                    className={styles.textInput}
                >
                    {({ inputClassName }) => (
                        <Input
                            {...this.createField('email')}
                            placeholder="Insira seu email"
                            className={inputClassName}
                            disabled={this.props.isSaving}
                        />
                    )}
                </InputWithMessage>
                <Label className={styles.label}>
                    Tornar email público
                </Label>
                <Checkbox
                    {...this.createField('showEmail', true)}
                    className={styles.checkbox}
                    disabled={this.props.isSaving}
                />
                <Label className={styles.label}>
                    Biografia
                </Label>
                <TextArea
                    {...this.createField('biography')}
                    placeholder="Insira uma biografia"
                    className={styles.textInput}
                    disabled={this.props.isSaving}
                />
                <Button
                    type="submit"
                    className={styles.button}
                    disabled={this.props.isSaving || this.hasError() || !this.hasChanges()}
                >
                    Salvar
                </Button>
            </form>
        );
    }
}