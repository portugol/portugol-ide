import { css, StyleSheet } from 'aphrodite';
import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { ActionMessageContext, ActionMessageContextType } from '../../ActionMessageController';
import AppContext from '../../AppContext';
import ApplicationsServer, { User } from '../../services/applications-server';
import Heading from '../../ui/Heading';
import Form, { UserChanges } from './Form';
import Values from './Values';

type UserDataPageInjectedProps = ActionMessageContextType & {
    applicationsServer: ApplicationsServer,
};

type UserDataPageProps = RouteComponentProps & {
    username: string,
    isLoading: boolean,
    error: 'NOT_FOUND' | 'OTHER' | null,
    user: User | null;
    updateUser: (changes: Partial<User>) => void,
};

type UserDataPageState = {
    isSaving: boolean,
};

class UserDataPage extends React.Component<UserDataPageProps & UserDataPageInjectedProps, UserDataPageState> {
    state: UserDataPageState = {
        isSaving: false,
    };

    save = async (changes: UserChanges) => {
        this.setState({ isSaving: true });
        const usersEndpoint = this.props.applicationsServer.users;
        try {
            await usersEndpoint.update(this.props.username, changes);
            this.props.updateUser(changes);
            this.props.showMessage('Informações salvas.', 'SUCCESS');
        } catch (error) {
            // tslint:disable-next-line no-console
            console.error(error);
            this.props.showMessage('Ocorreu um erro ao salvar', 'DANGER');
        }
        this.setState({ isSaving: false });
    }

    render() {
        let userInfo: JSX.Element | null = null;
        if (this.props.user) {
            userInfo = this.props.user.isSelf
                ? (<Form
                    isSaving={this.state.isSaving}
                    save={this.save}
                    user={this.props.user}
                />)
                : (<Values user={this.props.user} />);
        }

        let message: string | null = null;
        if (this.props.isLoading) {
            message = 'Carregando...';
        } else if (this.props.error === 'NOT_FOUND') {
            message = 'Usuário não encontrado.';
        } else if (this.props.error === 'OTHER') {
            message = 'Não foi possível carregar os dados do usuário.';
        }

        return (
            <div className={css(styles.container)}>
                <Heading>Dados do usuário</Heading>
                {message
                    ? <div className={css(styles.message)}>{message}</div>
                    : userInfo}
            </div>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        padding: '20px',
        backgroundColor: '#fffffe',
        overflowY: 'auto',
    },
    message: {
        marginTop: '20px',
    },
});

export default React.forwardRef<UserDataPage, UserDataPageProps>((props, ref) => (
    <AppContext.Consumer>
        {({ applicationsServer }) => (
            <ActionMessageContext.Consumer>
                {(actionMessageProps) => (
                    <UserDataPage
                        ref={ref}
                        applicationsServer={applicationsServer}
                        {...actionMessageProps}
                        {...props}
                    />
                )}
            </ActionMessageContext.Consumer>
        )}
    </AppContext.Consumer>
));