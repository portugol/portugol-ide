import * as classNames from 'classnames';
import * as React from 'react';
import { UserOther } from '../../services/applications-server';
import Label from '../../ui/Label';
// tslint:disable-next-line:no-var-requires
const styles = require('./Values.css');

type ValuesProps = {
    user: UserOther,
};

export default class Values extends React.Component<ValuesProps> {
    render() {
        return (
            <div className={styles.container}>
                <Label className={styles.label}>Nome</Label>
                <Value className={styles.value}>{this.props.user.name}</Value>
                {this.props.user.email !== undefined && (
                    <>
                        <Label className={styles.label}>Email</Label>
                        <Value className={styles.value}>{this.props.user.email}</Value>
                    </>
                )}
                <Label className={styles.label}>Biografia</Label>
                <Value className={styles.value}>{this.props.user.biography}</Value>
            </div>
        );
    }
}

type ValueProps = {
    children: string,
    className?: string,
};

function Value(props: ValueProps) {
    return (
        <p className={classNames(
            styles.valueContainer,
            { empty: !props.children },
            props.className,
        )}>
            {props.children || 'Não informado pelo usuário'}
        </p>
    );
}