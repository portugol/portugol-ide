import { css, StyleSheet } from 'aphrodite';
import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import * as theme from '../../theme';
import ApplicationTableRow from './ApplicationTableRow';
import PlaceholderTableRow from './PlaceholderTableRow';
import TableRow from './TableRow';

type ApplicationPagePropsApplication = {
    name: string,
    lastUpdated: Date,
    ownerUsername: string,
};

type ApplicationsPageProps = RouteComponentProps & {
    isSelf: boolean,
    applications: ApplicationPagePropsApplication[] | undefined,
    deleteApplication: (name: string) => void,
};

const Applications = (props: ApplicationsPageProps) => {
    const deleteApplication = (name: string) => {
        if (confirm(`Você tem certeza que deseja remover a aplicação "${name}"?`)) {
            props.deleteApplication(name);
        }
    };

    const onPressDelete = (applicationName: string) => props.isSelf ? () => deleteApplication(applicationName) : undefined;

    const onSelect = (applicationName: string, applicationOwnerUsername: string) => () => {
        const name = encodeURIComponent(applicationName);
        const user = encodeURIComponent(applicationOwnerUsername);
        props.history.push(`/edit/${name}?username=${user}`);
    };

    const createApplicationTableRow = (application: ApplicationPagePropsApplication) => (
        <ApplicationTableRow
            lastUpdated={application.lastUpdated}
            name={application.name}
            onPressDelete={onPressDelete(application.name)}
            onSelect={onSelect(application.name, application.ownerUsername)}
            key={application.name}
        />
    );

    const createTable = () => (
        <div>
            <TableRow
                title={<span className={css(styles.headerColumn)}>Nome</span>}
                description={<span className={css(styles.headerColumn)}>Última alteração</span>}
            />
            <div className={css(styles.divider)} />
            {props.applications
                ? props.applications.map(createApplicationTableRow)
                : [0, 1, 2].map((i) => <PlaceholderTableRow key={i} />)}
        </div>
    );

    return (
        <div className={css(styles.page)}>
            <h2>Aplicações</h2>
            {createTable()}
        </div>
    );
};

const styles = StyleSheet.create({
    page: {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 1,
        padding: '20px',
        backgroundColor: theme.backgroundColor.light,
        overflow: 'auto',
        color: theme.foregroundColor.normal,
    },
    headerColumn: {
        fontWeight: 'bold',
    },
    divider: {
        height: '1px',
        backgroundColor: 'black',
    }
});

export default Applications;
