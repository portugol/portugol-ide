import { css, StyleSheet } from 'aphrodite';
import * as moment from 'moment';
import * as React from 'react';
import * as theme from '../../theme';
import TableRow from './TableRow';

type ApplicationTableRowProps = {
    lastUpdated: Date,
    name: string,
    onSelect: () => void,
    onPressDelete?: () => void,
};

const ApplicationTableRow = (props: ApplicationTableRowProps) => {
    const doDelete = (event: React.SyntheticEvent) => {
        event.stopPropagation();
        props.onPressDelete!();
    };

    return (
        <TableRow
            onSelect={props.onSelect}
            title={props.name}
            description={moment(props.lastUpdated).format('DD/MM/YYYY hh:mm:ss')}
            actionButton={props.onPressDelete && (
                <button
                    className={css(styles.deleteApplicationButton)}
                    onClick={doDelete}
                >
                    <i className={'fas fa-trash ' + css(styles.trashIcon)} />
                </button>
            )}
        />
    );
};

const styles = StyleSheet.create({
    trashIcon: {
        color: theme.foregroundColor.normal,
    },
    deleteApplicationButton: {
        border: `1px solid ${theme.foregroundColor.normal}`,
        cursor: 'pointer',
        borderRadius: '5px',
        width: '30px',
        height: '30px',
        backgroundColor: theme.backgroundColor.light,
        ':hover': {
            backgroundColor: theme.backgroundColor.normal,
        },
    },
});

export default ApplicationTableRow;