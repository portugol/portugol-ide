import { css, StyleSheet } from 'aphrodite';
import * as React from 'react';
import * as theme from '../../theme';

type PlaceholderProps = {
    maxWidth?: number,
};

const Placeholder = (props: PlaceholderProps) => {
    const maxWidth = props.maxWidth || 150;
    const minWidth = maxWidth / 3;
    const width = (Math.floor(Math.random() * (maxWidth - minWidth)) + minWidth);
    return (
        <div
            className={css(styles.placeholder)}
            style={{ width: `${width}px` }}
        >{' '}</div>
    );
};

const pulseKeyframes = {
    '0%': {
        backgroundColor: theme.foregroundColor.light,
    },
    '25%': {
        backgroundColor: theme.foregroundColor.normal,
    },
    '50%': {
        backgroundColor: theme.foregroundColor.dark,
    },
    '75%': {
        backgroundColor: theme.foregroundColor.normal,
    },
    '100%': {
        backgroundColor: theme.foregroundColor.light,
    },
};

const styles = StyleSheet.create({
    placeholder: {
        whiteSpace: 'pre-wrap',
        animationName: [pulseKeyframes],
        animationDuration: '1s',
        animationIterationCount: 'infinite',
    },
});

export default Placeholder;