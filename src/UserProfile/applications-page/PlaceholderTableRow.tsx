import * as React from 'react';
import Placeholder from './Placeholder';
import TableRow from './TableRow';

const PlaceholderTableRow = () => (
    <TableRow
        title={<Placeholder />}
        description={<Placeholder />}
    />
);

export default PlaceholderTableRow;