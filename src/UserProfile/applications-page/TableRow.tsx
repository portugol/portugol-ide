import { css, StyleSheet } from 'aphrodite';
import * as React from 'react';
import * as theme from '../../theme';

type TableRowProps = {
    title: React.ReactNode;
    description: React.ReactNode;
    actionButton?: React.ReactNode;
    onSelect?: () => void,
};

const TableRow = (props: TableRowProps) => {
    return (
        <div
            onClick={props.onSelect}
            className={css(styles.container, props.onSelect && styles.selectableContainer)}
        >
            <div className={css(styles.column)}>
                {props.title}
            </div>
            <div className={css(styles.column)}>
                {props.description}
            </div>
            <div className={css(styles.column, styles.actionColumn)}>
                {props.actionButton || null}
            </div>
        </div>
    );
};

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        height: '40px',
        paddingLeft: '10px',
        paddingRight: '10px',
    },
    selectableContainer: {
        cursor: 'pointer',
        ':hover': {
            backgroundColor: theme.backgroundColor.normal,
        },
    },
    column: {
        alignItems: 'center',
        display: 'flex',
        flex: 1,
    },
    actionColumn: {
        flex: 'none',
        width: '30px',
    },
});

export default TableRow;