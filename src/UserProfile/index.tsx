import * as React from 'react';
import { Redirect, Route, RouteComponentProps, Switch } from 'react-router-dom';
import AppContext from '../AppContext';
import Menu, { MenuItem } from '../Menu';
import NavigationBar from '../NavigationBar';
import NavigationBarButton from '../NavigationBar/Button';
import UserBadge from '../NavigationBar/UserBadge';
import ApplicationsServer, { User } from '../services/applications-server';
import Applications from './applications-page/ApplicationsPage';
import { UserDataPage } from './user-data-page';
import { UserChanges } from './user-data-page/Form';
import UserProfileInformation from './UserProfileInformation';
// tslint:disable-next-line:no-var-requires
const styles = require('./index.css');

type UserProfileInjectedProps = {
    applicationsServer: ApplicationsServer,
};

type UserProfileProps = RouteComponentProps<{ username: string }> & {
    loadApplications: (username: string) => void,
    deleteApplication: (name: string) => void,
    applications: Array<{ name: string, lastUpdated: Date, ownerUsername: string }> | undefined,
    loggedUserUsername: string | undefined,
};

type UserProfileState = {
    isLoading: boolean,
    error: 'NOT_FOUND' | 'OTHER' | null,
    user: User | null;
};

export default React.forwardRef<UserProfile, UserProfileProps>((props, ref) => (
    <AppContext.Consumer>
        {({ applicationsServer }) => (
            <UserProfile
                applicationsServer={applicationsServer}
                ref={ref}
                {...props}
            />
        )}
    </AppContext.Consumer>
));

class UserProfile extends React.Component<UserProfileProps & UserProfileInjectedProps, UserProfileState> {
    state: UserProfileState = {
        user: null,
        error: null,
        isLoading: false,
    };

    loadUser = () => {
        const username = this.props.match.params.username;
        this.setState({ isLoading: true });
        const usersEndpoint = this.props.applicationsServer.users;
        usersEndpoint.getOne(username)
            .then(user => {
                if (!user) {
                    this.setState({ error: 'NOT_FOUND' });
                    return;
                }

                this.setState({ user });
            })
            .catch(() => this.setState({ error: 'OTHER' }))
            .then(() => this.setState({ isLoading: false }));
    }

    loadApplications = () => {
        const username = this.props.match.params.username;
        this.props.loadApplications(username);
    }

    componentDidMount() {
        this.loadUser();
        this.loadApplications();
    }

    componentDidUpdate(oldProps: UserProfileProps) {
        if (this.props.match.params.username !== oldProps.match.params.username) {
            this.loadUser();
            this.loadApplications();
        }
    }

    createNewApplication = () => {
        this.props.history.push('/');
    }

    deleteApplication = (name: string) => {
        if (confirm(`Você tem certeza que deseja remover a aplicação "${name}"?`)) {
            this.props.deleteApplication(name);
        }
    }

    renderApplications = (props: RouteComponentProps) => {
        const isSelf = this.props.match.params.username === this.props.loggedUserUsername;
        return (
            <Applications
                {...props}
                isSelf={isSelf}
                applications={this.props.applications}
                deleteApplication={this.props.deleteApplication}
            />
        );
    }

    renderUserData = (props: RouteComponentProps) => {
        const updateUser = (changes: UserChanges) => {
            this.setState({ user: { ...this.state.user!, ...changes } });
        };

        return (
            <UserDataPage
                {...props}
                username={this.props.match.params.username}
                user={this.state.user}
                error={this.state.error}
                isLoading={this.state.isLoading}
                updateUser={updateUser}
            />
        );
    }

    goToApplications = () => {
        this.props.history.push(`${this.props.match.url}/applications`);
    }

    goToUserData = () => {
        this.props.history.push(`${this.props.match.url}/user-data`);
    }

    isApplicationsActive = () => this.props.location.pathname === `${this.props.match.url}/applications`;
    isUserDataActive = () => this.props.location.pathname === `${this.props.match.url}/user-data`;

    render() {
        const userName = this.state.user ? this.state.user.name : undefined;

        return (
            <div className="user-profile" style={{
                backgroundColor: 'pink',
                position: 'absolute',
                left: 0,
                top: 0,
                right: 0,
                bottom: 0,
                display: 'flex',
                flexDirection: 'column',
            }}>
                <NavigationBar
                    { /* tslint:disable-next-line */ ...{}}
                    renderLeft={() => (
                        <NavigationBarButton
                            iconName="plus"
                            title="Criar nova aplicação"
                            onClick={this.createNewApplication}
                        />
                    )}
                    { /* tslint:disable-next-line */ ...{}}
                    renderRight={() => (<UserBadge history={this.props.history} />)}
                />
                <div style={{ display: 'flex', flexGrow: 1, overflow: 'hidden' }}>
                    <Menu className={styles.sideMenu}>
                        <UserProfileInformation
                            name={userName}
                            username={this.props.match.params.username}
                            isLoading={this.state.isLoading}
                        />
                        <MenuItem
                            title="Aplicações"
                            iconName="code"
                            active={this.isApplicationsActive()}
                            onClick={this.goToApplications}
                        />
                        <MenuItem
                            title="Dados do usuário"
                            iconName="address-card"
                            iconSolid={false}
                            active={this.isUserDataActive()}
                            onClick={this.goToUserData}
                        />
                    </Menu>
                    <Switch>
                        <Route
                            path={`${this.props.match.path}/applications`}
                            render={this.renderApplications}
                        />
                        <Route
                            path={`${this.props.match.path}/user-data`}
                            render={this.renderUserData}
                        />
                        <Redirect to={`${this.props.match.url}/applications`} />
                    </Switch>
                </div>
            </div>
        );
    }
}



