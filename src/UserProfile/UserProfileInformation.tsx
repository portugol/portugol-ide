import { css, StyleSheet } from 'aphrodite';
import * as React from 'react';
import * as theme from '../theme';
import { Placeholder } from './applications-page';

type UserProfileInformationProps = {
    username: string,
    name: string | undefined,
    isLoading: boolean,
};

const UserProfileInformation = (props: UserProfileInformationProps) => {
    const nameUnavailable = !props.isLoading && !props.name;
    return (
        <div className={css(styles.userProfileInformation)}>
            <h2 className={css(
                styles.name,
                nameUnavailable && styles.nameUnavailable,
            )}>
                {props.isLoading && <Placeholder />}
                {!props.isLoading && (props.name || 'Sem nome')}
            </h2>
            <h4 className={css(styles.username)}>@{props.username}</h4>
        </div >
    );
};

const styles = StyleSheet.create({
    userProfileInformation: {
        padding: '20px',
    },
    name: {
        textAlign: 'left',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        margin: 0,
        padding: 0,
        fontWeight: 'bold',
        width: '210px',
        boxSizing: 'border-box',
        border: '1px solid transparent',
        fontSize: '20px',
        marginBottom: '5px',
        height: '24px',
        color: theme.foregroundColor.dark,
    },
    nameUnavailable: {
        color: theme.foregroundColor.light,
    },
    username: {
        fontSize: '16px',
        color: theme.foregroundColor.normal,
        margin: 0,
    },
});

export default UserProfileInformation;