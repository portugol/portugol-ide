import classNames from 'classnames';
import * as download from 'downloadjs';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { ActionMessageContext, ActionMessageContextType } from './ActionMessageController';
import './FileEditor.css';
import FileNameInput from './FileNameInput';
import FilePreview from './FilePreview';
import { PreviewableFile } from './FilePreviewContainer';
import IDEContext, { IDEContextType } from './IDE/Context';

type FileEditorInjectedProps = {
    actionMessage: ActionMessageContextType,
    ide: IDEContextType,
};

type FileEditorProps = {
    file: PreviewableFile | undefined,
    toggle: () => void,
    next: () => void,
    previous: () => void,
    updateName: (newName: string) => void,
    hasNext: boolean,
    hasPrevious: boolean,
    deleteFile: () => void,
};

type FileEditorState = {
    newName: string,
    error: 'DUPLICATE_NAME' | 'EMPTY' | null,
};

class FileEditor extends React.Component<FileEditorProps & FileEditorInjectedProps, FileEditorState> { 
    nameInputRef = React.createRef<HTMLInputElement>();

    state: FileEditorState = {
        newName: '',
        error: null,
    };
    
    onChangeName = (newValue: string) => {
        if (!this.props.file) { return; }

        let error: FileEditorState['error'] = null;

        const newValueSplit = newValue.split('.');
        if (newValueSplit.length === 2 && newValueSplit[0] === '') {
            error = 'EMPTY';
        }

        if (error === null) {
            if (this.props.file.name !== newValue) {
                error = this.props.ide.applicationFiles
                    .findIndex(f => f.name === newValue) !== -1
                    ? 'DUPLICATE_NAME' 
                    : null;
            }
        }
        
        this.setState({ 
            newName: newValue,
            error,
        });
    }

    componentDidUpdate(oldProps: FileEditorProps) {
        if (oldProps.file !== this.props.file && this.props.file) {
            this.setState({ newName: this.props.file.name, error: null });
        }

        // if there was no file and now there is, the input has just been added
        if (!oldProps.file && this.props.file) {
            this.listenNameInputEvents();
        }
    }

    componentDidMount() {
        if (this.props.file) {
            this.setState({ newName: this.props.file.name });
        }

        document.addEventListener('keyup', this.keyUpListener);
        document.addEventListener('copy', this.copyListener);

        this.listenNameInputEvents();
    }

    componentWillUnmount() {
        document.removeEventListener('keyup', this.keyUpListener);
        document.removeEventListener('copy', this.copyListener);
        if (this.nameInputRef.current) {
            this.nameInputRef.current.removeEventListener('copy', this.nameInputCopyListener);
            this.nameInputRef.current.removeEventListener('keyup', this.nameInputKeyUpListener);
        }
    }

    listenNameInputEvents() {
        if (this.nameInputRef.current) {
            this.nameInputRef.current.addEventListener('copy', this.nameInputCopyListener);
            this.nameInputRef.current.addEventListener('keyup', this.nameInputKeyUpListener);
        }
    }

    keyUpListener = (event: KeyboardEvent) => {
        const ESCAPE_KEYCODE = 27;
        const LEFT_ARROW_KEYCODE = 37;
        const RIGHT_ARROW_KEYCODE = 39;
        switch (event.keyCode) {
            case ESCAPE_KEYCODE: this.props.toggle(); break;
            case LEFT_ARROW_KEYCODE: this.props.previous(); break;
            case RIGHT_ARROW_KEYCODE: this.props.next(); break;
        }
    }

    copyListener = (event: ClipboardEvent) => {
        if (this.props.file) {
            if (this.state.error) {
                this.props.actionMessage.showMessage(this.errorMessage()!, 'DANGER');
            } else {
                event.preventDefault();
                const name = this.state.newName;
                event.clipboardData.setData('text/plain', name);
                this.props.updateName(name);
                this.props.actionMessage.showMessage('Nome salvo e copiado');
                this.props.toggle();
            }
        }
    }

    nameInputCopyListener = (event: ClipboardEvent) => {
        event.stopPropagation();
    }

    nameInputKeyUpListener = (event: KeyboardEvent) => {
        const ESCAPE_KEY_CODE = 27;
        if (event.keyCode !== ESCAPE_KEY_CODE) {
            event.stopPropagation();
        }
    }

    stopEditorClickPropagation = (event: React.MouseEvent) => {
        event.stopPropagation();
    }

    errorMessage(): string | null {
        switch(this.state.error) {
            case null: return null;
            case 'DUPLICATE_NAME': return `Nome "${this.state.newName}" já está em uso.`;
            case 'EMPTY': return 'Nome do arquivo não pode ser vazio.';
        }
    }

    applyName = (event: React.FormEvent) => {
        event.preventDefault();
        if (this.state.error) {
            this.props.actionMessage.showMessage(this.errorMessage()!, 'DANGER');
        } else {
            this.props.updateName(this.state.newName);
            this.props.actionMessage.showMessage('Nome atualizado');
            this.props.toggle();
        }
    }

    copyName = () => {
        if (this.props.file) {
            const input = document.createElement('input');
            input.value = this.state.newName;
            input.addEventListener('copy', e => e.stopPropagation());
            document.body.appendChild(input);
            input.select();
            document.execCommand('copy');
            document.body.removeChild(input);
            this.props.actionMessage.showMessage('Nome copiado');
        }
    }

    downloadFile = () => {
        if (this.props.file && this.props.file.raw) {
            download(this.props.file.raw, this.state.newName);
            this.props.actionMessage.showMessage('Download iniciado');
        }
    }

    deleteFile = () => {
        if (this.props.file) {
            this.props.deleteFile();
            this.props.toggle();
        }
    }

    render() {
        return this.props.file
            ? ReactDOM.createPortal((
                <div className="file-editor-backdrop" onClick={this.props.toggle}>
                    <div 
                        className={classNames('file-editor', { 'has-error': this.state.error })} 
                        onClick={this.stopEditorClickPropagation}
                    >
                        <FilePreview file={this.props.file.raw!} className="file-editor-preview">
                            <button className="navigation-button close-button" onClick={this.props.toggle}>
                                <i className="fas fa-times" />
                            </button>
                            {this.props.hasPrevious && (
                                <button className="navigation-button previous-button" onClick={this.props.previous}>
                                    <i className="fas fa-chevron-left" />
                                </button>
                            )}
                            {this.props.hasNext && (
                                <button className="navigation-button next-button" onClick={this.props.next}>
                                    <i className="fas fa-chevron-right" />
                                </button>
                            )}
                        </FilePreview>
                        <form className="file-name-wrapper" onSubmit={this.applyName}>
                            <FileNameInput 
                                value={this.state.newName}
                                onChange={this.onChangeName}
                                refNameInput={this.nameInputRef}
                                className="application-name-input"
                            />
                            <button type="button" title="Copiar o nome" onClick={this.copyName}>
                                <i className="fas fa-clipboard" />
                            </button>
                            <button 
                                type="button"
                                title="Baixar arquivo"
                                disabled={!this.props.file.raw}
                                onClick={this.downloadFile}
                            >
                                <i className="fas fa-download" />
                            </button>
                            <button type="button" title="Apagar arquivo" onClick={this.deleteFile}>
                                <i className="fas fa-trash" />
                            </button>
                            <button type="submit" title="Aplicar nome">
                                <i className="fas fa-check" />
                            </button>
                        </form>
                        <div className="name-error">
                            {this.errorMessage()}
                        </div>
                    </div>
                </div>
            ), document.body)
            : null;
    }
}

export default (props: FileEditorProps) => (
    <ActionMessageContext.Consumer>
        {(actionMessageContext) => (
            <IDEContext.Consumer>
                {(ideContext) => (
                    <FileEditor 
                        actionMessage={actionMessageContext}
                        ide={ideContext}
                        {...props} 
                    />
                )}
            </IDEContext.Consumer>
        )}
    </ActionMessageContext.Consumer>
);