import * as React from 'react';
import * as rxjs from 'rxjs';
import AppContext from '../AppContext';
import { PreviewableFile } from '../FilePreviewContainer';
import ApplicationsServer from '../services/applications-server';

export type ApplicationFile = PreviewableFile & {
    isBusy: boolean,
    oldName?: string,
};

type ApplicationFilesInjectedProps = {
    applicationsServer: ApplicationsServer,
};

export type ApplicationFilesChildrenProps = {
    isLoading: boolean,
    isSaving: boolean,
    files: ApplicationFile[],
    addFiles: (files: File[], tracked?: boolean) => void,
    removeFile: (name: string) => boolean,
    renameFile: (oldName: string, newName: string) => void,
    saveChanges: () => void,
};

type ApplicationFilesProps = {
    application: { name: string, ownerUsername: string } | null,
    children: (props: ApplicationFilesChildrenProps) => JSX.Element,
};

type ApplicationFilesState = {
    isLocked: boolean,
    isSaving: boolean,
    isLoading: boolean,
    files: ApplicationFile[],
    filesNamesToDelete: string[],
};

export class ApplicationFiles extends React.Component<ApplicationFilesProps & ApplicationFilesInjectedProps, ApplicationFilesState> {
    state: ApplicationFilesState = {
        isLocked: false,
        isSaving: false,
        isLoading: false,
        files: [],
        filesNamesToDelete: [],
    };

    private pendingSubscriptions: rxjs.Subscription[] = [];

    componentDidMount() {
        if (this.props.application) {
            this.load();
        }
    }

    componentDidUpdate(oldProps: ApplicationFilesProps) {
        const hasNameChanged = (this.props.application && this.props.application.name) !== (oldProps.application && oldProps.application.name);
        if (hasNameChanged && !this.state.isLocked) {
            this.cancelPendingSubscriptions();
            if (this.props.application) {
                this.load();
            } else {
                this.unload();
            }
        }
    }

    componentWillUnmount() {
        this.cancelPendingSubscriptions();
    }

    render() {
        const { isLoading, isSaving, files } = this.state;
        const { addFiles, removeFile, renameFile, saveChanges } = this;
        return this.props.children({
            isLoading,
            isSaving,
            files,
            addFiles,
            removeFile,
            renameFile,
            saveChanges,
        });
    }

    addFiles = (files: File[], tracked: boolean = true) => {
        const applicationFiles = files.map<ApplicationFile>(file => ({
            tracked,
            raw: file,
            name: file.name,
            dataUrl: URL.createObjectURL(file),
            isBusy: false,
            uploaded: false,
        }));

        const deletedFilesNames: string[] = [];
        let newFiles = [...this.state.files];
        for (const file of applicationFiles) {
            const fileWithSameNameIndex = newFiles.findIndex(f => f.name === file.name);
            if (fileWithSameNameIndex !== -1) {
                const [deletedFile] = newFiles.splice(fileWithSameNameIndex, 1);
                if (deletedFile.uploaded) {
                    deletedFilesNames.push(deletedFile.oldName || deletedFile.name);
                }
            }
        }
        newFiles = [...newFiles, ...applicationFiles];

        this.setState(({ filesNamesToDelete }) => ({
            // need to keep ordered by name because its how it comes from the server
            files: newFiles.sort((a, b) => a.name > b.name ? 1 : -1),
            filesNamesToDelete: [
                ...filesNamesToDelete,
                ...deletedFilesNames,
            ],
        }));
    }

    removeFile = (name: string): boolean => {
        const fileIndex = this.state.files.findIndex(file => file.name === name);
        const toBeRemoved: string[] = [];
        if (fileIndex !== -1) {
            const file = this.state.files[fileIndex];
            if (file.uploaded) {
                // maybe the file has been renamed
                toBeRemoved.push(file.oldName || file.name);
            }
            this.setState(({ files, filesNamesToDelete }) => ({
                files: [
                    ...files.slice(0, fileIndex),
                    ...files.slice(fileIndex + 1),
                ],
                filesNamesToDelete: [
                    ...filesNamesToDelete,
                    ...toBeRemoved,
                ]
            }));
            return true;
        }
        return false;
    }

    renameFile = (oldName: string, newName: string) => {
        this.updateFile(oldName, file => ({
            ...file,
            name: newName,
            // save the server name
            oldName: file.uploaded && !file.oldName ? file.name : file.oldName,
        }));
    }

    saveChanges = () => this.save();

    lock = () => {
        this.setState({ isLocked: true });
    }

    unlock = () => {
        this.setState({ isLocked: false });
    }

    private updateFile(name: string, update: (file: ApplicationFile) => ApplicationFile): boolean {
        const fileIndex = this.state.files.findIndex(file => file.name === name);
        if (fileIndex === -1) return false;
        this.setState(({ files }) => ({
            files: [
                ...files.slice(0, fileIndex),
                update(files[fileIndex]),
                ...files.slice(fileIndex + 1),
            ]
        }));
        return true;
    }

    private downloadFile(name: string): rxjs.Observable<void> {
        const filesEndpoint = this.props.applicationsServer.applications.files(this.props.application!.name);

        return rxjs.defer(() => {
                this.updateFile(name, file => ({ ...file, isBusy: true }));

                return filesEndpoint.download(name);
            })
            .map(raw => {
                this.updateFile(name, file => ({ ...file, isBusy: false, raw: raw!, dataUrl: URL.createObjectURL(raw!) }));
            })
            .catch((error, caught) => {
                this.updateFile(name, file => ({ ...file, isBusy: false }));
                return caught;
            });
    }

    private loadFiles(): rxjs.Observable<void> {
        if (this.state.isLoading) throw new Error('Can not load if already loading.');
        
        const filesEndpoint = this.props.applicationsServer.applications.files(this.props.application!.name);

        return rxjs.defer(() => {
                this.setState({ isLoading: true });

                return filesEndpoint.list(this.props.application!.ownerUsername);
            })
            .map((files) => {
                const applicationFiles = files!.map<ApplicationFile>(file => ({
                    tracked: true,
                    isBusy: false,
                    name: file.name,
                    uploaded: true,
                }));

                this.setState({
                    files: applicationFiles,
                    isLoading: false,
                });
            });
    }

    private load() {
        this.unload(() => {
            if (!this.props.application) return;
            const subscription = this.loadFiles()
                .switchMap(() => {
                    return rxjs
                        .merge(...this.state.files.map(file => this.downloadFile(file.name)));
                })
                .subscribe(undefined, undefined, () => {
                    this.pendingSubscriptions.splice(this.pendingSubscriptions.indexOf(subscription), 1);
                });
            this.pendingSubscriptions.push(subscription);
        });
    }

    private unload(cb?: () => void) {
        this.setState({
            isSaving: false,
            isLoading: false,
            files: [],
            filesNamesToDelete: [],
        }, cb);
    }

    private save() {
        this.setState({ isSaving: true });

        const filesEndpoint = this.props.applicationsServer.applications.files(this.props.application!.name);

        // DELETE THE DELETED FILES
        const subscription = rxjs.from(filesEndpoint.remove(this.state.filesNamesToDelete))
            .map(() => this.setState({ filesNamesToDelete: [] }))
            // RENAME THE RENAMED FILES
            .map(() => {
                // only rename tracked files
                const renamedFiles = this.state.files.filter(af => af.tracked && !!af.oldName);
                const renames = renamedFiles.map<[string, string]>(file => ([file.oldName!, file.name]));
                return filesEndpoint.rename(renames);
            })
            .map(() => {
                this.setState(({ files }) => ({
                    files: files.map(file => {
                        if (!file.oldName) { return file; }
                        return { ...file, oldName: undefined };
                    }),
                    isSaving: false,
                }));
            })
            // UPLOAD THE ADDED FILES - do not need to be waited
            .switchMap(() => {
                // only upload tracked files
                const shouldUpload = (file: ApplicationFile) => file.tracked && !file.uploaded;
 
                const addedFiles = this.state.files.filter(shouldUpload);

                this.setState(({ files }) => ({
                    files: files.map(file => {
                        if (shouldUpload(file)) {
                            return { ...file, isBusy: true };
                        }
                        return file;
                    }),
                }));

                return rxjs.merge(...addedFiles.map(file => {
                    return rxjs.from(filesEndpoint.upload(file.raw!, file.name))
                        .map(() => this.updateFile(file.name, toUpdate => ({ ...toUpdate, isBusy: false, uploaded: true })))
                        .catch((error, caught) => {
                            this.updateFile(file.name, toUpdate => ({ ...toUpdate, isBusy: false, uploaded: false }));
                            return caught;
                        });
                    }));
            })
            .subscribe(undefined, undefined, () => {
                this.pendingSubscriptions.splice(this.pendingSubscriptions.indexOf(subscription), 1);
            });
        this.pendingSubscriptions.push(subscription);
    }

    private cancelPendingSubscriptions() {
        for (const subscription of this.pendingSubscriptions) {
            subscription.unsubscribe();
        }
        this.pendingSubscriptions = [];
    }
}

export default React.forwardRef<ApplicationFiles, ApplicationFilesProps>((props, ref) => (
    <AppContext.Consumer>
        {({ applicationsServer }) => (
            <ApplicationFiles
                ref={ref}
                applicationsServer={applicationsServer}
                {...props}
            />
        )}
    </AppContext.Consumer>
));