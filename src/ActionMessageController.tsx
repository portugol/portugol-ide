import classNames from 'classnames';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './ActionMessageController.css';

type ActionMessageType = 'SUCCESS' | 'WARNING' | 'DANGER';

type ActionMessage = {
    id: number,
    text: string,
    type: ActionMessageType,
};

type ActionMessageControllerProps = React.Props<{}>;

type ActionMessageControllerState = {
    messages: ActionMessage[],
};

export type ActionMessageContextType = {
    showMessage: (text: string, type?: ActionMessageType) => void,
};

export const ActionMessageContext = React.createContext<ActionMessageContextType>({
    showMessage: () => undefined,
});

let lastActionMessageId = 0;

export default class extends React.Component<ActionMessageControllerProps, ActionMessageControllerState> {
    state: ActionMessageControllerState = {
        messages: [],
    };

    addMessage = (text: string, type: ActionMessageType = 'SUCCESS') => {
        const message: ActionMessage = { id: lastActionMessageId++, text, type };
        this.setState(({ messages }) => ({ messages: [...messages, message] }));

        setTimeout(() => {
            this.setState(({ messages }) => {
                const messageIndex = messages.findIndex(m => m.id === message.id);

                return {
                    messages: [
                        ...messages.slice(0, messageIndex),
                        ...messages.slice(messageIndex + 1),
                    ],
                };
            });
        }, 3000);
    }

    render() {
        return (
            <>
                {ReactDOM.createPortal((
                    this.state.messages.map(message => (
                        <div key={message.id} className={classNames('action-message', {
                            'is-danger': message.type === 'DANGER',
                            'is-success': message.type === 'SUCCESS',
                            'is-warning': message.type === 'WARNING',
                        })}>
                            <div className="action-message-text">
                                {message.text}
                            </div>
                            <i className={classNames('fas', {
                                'fa-times-circle': message.type === 'DANGER',
                                'fa-check-circle': message.type === 'SUCCESS',
                                'fa-exclamation-circle': message.type === 'WARNING',
                            })} />
                        </div>
                    ))
                ), document.body)}
                <ActionMessageContext.Provider value={{ showMessage: this.addMessage }}>
                    {this.props.children}
                </ActionMessageContext.Provider>
            </>
        );
    }
}

