import classNames from 'classnames';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import FileNameInput from './FileNameInput';
import FilePreview from './FilePreview';
import './FilesValidator.css';

type FilesValidatorProps = {
    files: File[],
};

type FilesValidatorState = {
    error: 'DUPLICATE_NAME' | 'EMPTY_NAME' | null,
    incomingFiles: File[] | null,
    currentFileIndex: number,
    newFileName: string,
};

export default class extends React.Component<FilesValidatorProps, FilesValidatorState> {
    nameInputRef = React.createRef<HTMLInputElement>();

    state: FilesValidatorState = {
        error: null,
        currentFileIndex: 0,
        incomingFiles: null,
        newFileName: '',
    };

    onValidate: ((files: File[]) => void) | null = null;

    updateNewFileName = (newFileName: string) => {
        this.setState({ 
            newFileName,
            error: this.getErrorForName(newFileName),
        });
    }
    
    validate(files: File[], onValidate: (files: File[]) => void) {
        if (this.state.incomingFiles !== null) {
            throw new Error('Can not validate more while the component is still validating other files.');
        }
        this.onValidate = onValidate;
        this.setState({ currentFileIndex: -1, incomingFiles: files }, () => {
            this.nextFile();
        });
    }

    getErrorForName(newFileName: string, fileIndex = this.state.currentFileIndex): FilesValidatorState['error'] {
        const newFileNameSplit = newFileName.split('.');
        if (newFileNameSplit.length === 2 && newFileNameSplit[0] === '') {
            return 'EMPTY_NAME';
        }

        if (this.props.files.findIndex(f => f.name === newFileName) !== -1) { 
            return 'DUPLICATE_NAME';
        }

        const previousFiles = this.state.incomingFiles!.slice(0, fileIndex);
        if (previousFiles.findIndex(f => f.name === newFileName) !== -1) { 
            return 'DUPLICATE_NAME';
        }

        return null;
    }

    nextFile() {
        if (!this.state.incomingFiles) { return; }

        let index = this.state.currentFileIndex + 1;
        let error: FilesValidatorState['error'] = null;
        while (index < this.state.incomingFiles.length && error === null) {
            const newFileName = this.state.incomingFiles[index].name;
            error = this.getErrorForName(newFileName, index);
            if (error !== null) {
                this.setState({
                    currentFileIndex: index,
                    newFileName,
                    error,
                }, () => {
                    this.nameInputRef.current!.focus();
                });
                return;
            }
            index++;
        }

        this.onValidate!(this.state.incomingFiles);
        this.onValidate = null;
        this.setState({ incomingFiles: null });
    }

    saveCurrent = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        if (!this.state.incomingFiles) { return; }

        const currentFile = this.state.incomingFiles[this.state.currentFileIndex];
        this.setState({
            incomingFiles: [
                ...this.state.incomingFiles.slice(0, this.state.currentFileIndex),
                this.state.newFileName === currentFile.name 
                    ? currentFile
                    : new File([currentFile], this.state.newFileName, { 
                        type: currentFile.type, 
                        lastModified: currentFile.lastModified,
                    }),
                ...this.state.incomingFiles.slice(this.state.currentFileIndex + 1),
            ],
        }, () => {
            this.nextFile();
        });
    }

    replaceCurrent = () => {
        if (!this.state.incomingFiles) { return; }

        // maybe it is replacing incomingFiles
        const currentFile = this.state.incomingFiles[this.state.currentFileIndex];
        const previousFiles = this.state.incomingFiles.slice(0, this.state.currentFileIndex);
        const existingFileIndex = previousFiles.findIndex(f => f.name === this.state.newFileName);

        if (existingFileIndex !== -1) {
            this.setState({
                incomingFiles: [
                    ...this.state.incomingFiles.slice(0, existingFileIndex),
                    ...this.state.incomingFiles.slice(existingFileIndex + 1, this.state.currentFileIndex),
                    currentFile.name === this.state.newFileName
                        ? currentFile
                        : new File([currentFile], this.state.newFileName, { 
                            type: currentFile.type, 
                            lastModified: currentFile.lastModified,
                        }),
                    ...this.state.incomingFiles.slice(this.state.currentFileIndex + 1),
                ],
                currentFileIndex: this.state.currentFileIndex - 1,
            }, () => {
                this.nextFile();
            });
        } else {
            this.setState({
                incomingFiles: [
                    ...this.state.incomingFiles.slice(0, this.state.currentFileIndex),
                    currentFile.name === this.state.newFileName
                        ? currentFile
                        : new File([currentFile], this.state.newFileName, { 
                            type: currentFile.type, 
                            lastModified: currentFile.lastModified,
                        }),
                    ...this.state.incomingFiles.slice(this.state.currentFileIndex + 1),
                ]
            }, () => {
                this.nextFile();
            });
        }

    }

    ignoreCurrent = () => {
        if (!this.state.incomingFiles) { return; }
        this.setState({
            incomingFiles: [
                ...this.state.incomingFiles.slice(0, this.state.currentFileIndex),
                ...this.state.incomingFiles.slice(this.state.currentFileIndex + 1),
            ],
            currentFileIndex: this.state.currentFileIndex - 1,
        }, () => {
            this.nextFile();
        });
    }

    errorMessage(): string | null {
        const error = this.state.error;
        switch (error) {
            case null: return null;
            case 'DUPLICATE_NAME': return `Nome de arquivo "${this.state.newFileName}" já está em uso.`;
            case 'EMPTY_NAME': return `O nome do arquivo não pode ser vazio.`;
        }
    }

    componentDidMount() {
        document.addEventListener('keyup', this.documentKeyboardListener);
    }

    documentKeyboardListener = (event: KeyboardEvent) => {
        if (this.state.incomingFiles === null) { return; }

        const ESCAPE_KEYCODE = 27;

        if (event.keyCode === ESCAPE_KEYCODE) {
            this.ignoreCurrent();
        }
    }

    stopValidatorClickPropagation = (event: React.MouseEvent<HTMLDivElement>) => {
        event.stopPropagation();
    }

    render() {
        if (this.state.currentFileIndex === -1 || !this.state.incomingFiles) return null;
        const currentFile = this.state.incomingFiles[this.state.currentFileIndex];

        return ReactDOM.createPortal((
            <div className="files-validator-backdrop" onClick={this.ignoreCurrent}>
                <div className={classNames('files-validator', {
                    'has-error': this.state.error !== null,
                })} onClick={this.stopValidatorClickPropagation}>
                    <FilePreview file={currentFile} />
                    <div className="error-message">
                        {this.errorMessage()}
                    </div>
                    <form className="validator-body" onSubmit={this.saveCurrent}>
                        <FileNameInput
                            refNameInput={this.nameInputRef}
                            value={this.state.newFileName}
                            onChange={this.updateNewFileName}
                        />
                        <div className="validator-actions">
                            <button 
                                type="button"
                                className="action-button"
                                onClick={this.ignoreCurrent}
                            >
                                Ignorar
                            </button>
                            <button 
                                type="button"
                                className="action-button"
                                disabled={this.state.error !== 'DUPLICATE_NAME'}
                                onClick={this.replaceCurrent}
                            >
                                Substituir
                            </button>
                            <button 
                                type="submit"
                                className="action-button"
                                disabled={this.state.error !== null}
                            >
                                Salvar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        ), document.body);
    }
}