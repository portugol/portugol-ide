import * as React from 'react';
import ModalForm from './ui/ModalForm';
import ActionButton from './ui/ModalForm/ActionButton';
import ButtonGroup from './ui/ModalForm/ButtonGroup';
import SubmitButton from './ui/ModalForm/SubmitButton';
import TextField from './ui/ModalForm/TextField';
// tslint:disable-next-line no-var-requires
const styles = require('./SignInModal.css');

type SignInModalProps = {
    isOpen: boolean,
    toggle: () => void,
    isSigningIn: boolean,
    signIn: (username: string, password: string) => void,
    signUp: () => void,
    signInWithUnivali: () => void,
};

export default class extends React.Component<SignInModalProps> {
    signIn = (values: { [key: string]: string }) => {
        this.props.signIn(values.username, values.password);
    }

    validateUsername = (username: string): string | null => {
        let error: string | null = null;

        if (username === '') {
            error = 'Informe o seu nome de usuário.';
        }

        return error;
    }

    validatePassword = (password: string): string | null => {
        let error: string | null = null;

        if (password === '') {
            error = 'Informe sua senha.';
        }

        return error;
    }

    render() {
        return (
            <ModalForm
                toggle={this.props.toggle}
                isOpen={this.props.isOpen}
                className={styles.signInModal}
                disabled={this.props.isSigningIn}
                title="Entre na sua conta"
                onSubmit={this.signIn}
            >
                <TextField
                    isSecure={false}
                    placeholder="Informe seu nome de usuário"
                    name="username"
                    validate={this.validateUsername}
                />
                <TextField
                    isSecure={true}
                    placeholder="Informe sua senha"
                    name="password"
                    validate={this.validatePassword}
                />
                <ButtonGroup>
                    <ActionButton action={this.props.signUp}>
                        Cadastrar
                    </ActionButton>
                    <SubmitButton>
                        Entrar
                    </SubmitButton>
                </ButtonGroup>
                <ActionButton action={this.props.signInWithUnivali}>
                    Entrar com UNIVALI
                </ActionButton>
            </ModalForm>
        );
    }
}