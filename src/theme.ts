export const backgroundColor = {
    light: '#f3f3ef',
    normal: '#d5d5d0',
    dark: '#a4a49f',
};

export const foregroundColor = {
    light: '#66665A',
    normal: '#444439',
    dark: '#222218',
};