import * as React from 'react';
import ModalForm from './ui/ModalForm';
import ActionButton from './ui/ModalForm/ActionButton';
import ButtonGroup from './ui/ModalForm/ButtonGroup';
import SubmitButton from './ui/ModalForm/SubmitButton';
import TextField from './ui/ModalForm/TextField';
// tslint:disable-next-line no-var-requires
const styles = require('./SignUpModal.css');

type SignUpModalProps = {
    isOpen: boolean,
    toggle: () => void,
    isSigningUp: boolean,
    signIn: () => void,
    signUp: (email: string, username: string, password: string) => void,
};
export default class extends React.Component<SignUpModalProps> {
    signUp = (values: {[key: string]: string}) => {
        this.props.signUp(values.email, values.username, values.password);
    }

    validateEmail(email: string): string | null {
        if (email === '') {
            return 'Informe o seu email.';
        }
        if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)) {
            return 'O email informado está com formato incorreto.';
        }
        return null;
    }

    validateUsername(username: string): string | null {
        if (username === '') {
            return 'Escolha um nome de usuário.';
        }
        if (username.length > 18) {
            return 'O nome de usuário deve conter no máximo 18 caracteres.';
        }
        return null;
    }

    validatePassword(password: string): string | null {
        if (password === '') {
            return 'Escolha uma senha.';
        }
        if (password.length < 6) {
            return 'A senha deve conter pelo menos 6 caracteres.';
        }
        if (password.length > 50) {
            return 'A senha deve conter no máximo 50 caracteres.';
        }
        return null;
    }

    validatePasswordRepeated(passwordRepeated: string, values: { [key: string]: string }): string | null {
        if (passwordRepeated !== values.password) {
            return 'Senha informada não confere.';
        }
        return null;
    }

    render() {
        return (
            <ModalForm
                isOpen={this.props.isOpen}
                toggle={this.props.toggle}
                className={styles.registerModal}
                title="Crie uma conta"
                disabled={this.props.isSigningUp}
                onSubmit={this.signUp}
            >
                <TextField
                    name="email"
                    placeholder="Informe o seu email"
                    validate={this.validateEmail}
                />
                <TextField
                    name="username"
                    placeholder="Escolha um nome de usuário"
                    validate={this.validateUsername}
                />
                <TextField 
                    isSecure={true}
                    name="password"
                    placeholder="Escolha uma senha"
                    validate={this.validatePassword}
                />
                <TextField
                    isSecure={true}
                    name="repeatPassword"
                    placeholder="Repita a senha escolhida"
                    validate={this.validatePasswordRepeated}
                />
                <ButtonGroup>
                    <ActionButton action={this.props.signIn}>
                        Entrar
                    </ActionButton>
                    <SubmitButton>
                        Cadastrar
                    </SubmitButton>
                </ButtonGroup>
            </ModalForm>
        );
    }
}