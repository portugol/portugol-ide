import * as React from 'react';
import ApplicationsServer from './services/applications-server';

type AppContextType = {
    loggedUserUsername: string | undefined,
    logout: () => void,
    requestLogin: () => void,
    requestCreateAccount: () => void,
    applicationsServer: ApplicationsServer,
};

// tslint:disable no-empty
export const AppContext = React.createContext<AppContextType>({
    loggedUserUsername: undefined,
    logout: () => undefined,
    requestLogin: () => undefined,
    requestCreateAccount: () => undefined,
    applicationsServer: new ApplicationsServer(),
});
// tslint:enable no-empty

export default AppContext;