import * as React from 'react';
import './ActionsBar.css';

type ActionBarProps = {
    canPlay?: boolean,
    onPlay?: () => void,
    canDebug?: boolean,
    onDebug?: () => void,
    canStop?: boolean,
    onStop?: () => void,
    canSave?: boolean,
    onSave?: () => void,
    canHelp?: boolean,
    onHelp?: () => void,
};

export default class ActionsBar extends React.Component<ActionBarProps> {
    render() {
        return (
            <div className="actions-bar">
                <button disabled={!this.props.canPlay} onClick={this.props.onPlay}>
                    <i className={'fas fa-play'} />
                </button>
                {/* <button disabled={!this.props.canDebug} onClick={this.props.onDebug}>
                    <i className="fas fa-shoe-prints" style={{ transform: 'rotate(270deg)' }} />
                </button> */}
                <button disabled={!this.props.canStop} onClick={this.props.onStop}>
                    <i className="fas fa-stop-circle" />
                </button>
                <hr />
                <button disabled={!this.props.canSave} onClick={this.props.onSave}>
                    <i className="fas fa-save" />
                </button>
                <hr />
                <button disabled={!this.props.canHelp} onClick={this.props.onHelp}>
                    <i className="fas fa-question" />
                </button>
            </div>
        );
    }
}