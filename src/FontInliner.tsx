import * as React from 'react';

type FontInlinerProps = {
    fontUrl: string | null,
    children: (inject: { fontName: string | null }) => JSX.Element,
};

type FontInlinerState = {
    fontName: string | null,
};

export default class extends React.Component<FontInlinerProps, FontInlinerState> {
    styleElement = document.createElement('style');
    
    state: FontInlinerState = {
        fontName: null,
    };

    createRandomFontName() {
        const length = 20;
        let fontName = '';
        const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

        for (let i = 0; i < length; i++)
            fontName += possible.charAt(Math.floor(Math.random() * possible.length));

        return fontName;
    }

    componentDidMount() {
        document.head.appendChild(this.styleElement);

        if (this.props.fontUrl !== null) {
            this.setFont(this.props.fontUrl);
        }
    }

    componentDidUpdate(oldProps: FontInlinerProps) {
        if (oldProps.fontUrl !== this.props.fontUrl) {
            this.setFont(this.props.fontUrl);
        }
    }

    componentWillUnmount() {
        document.head.removeChild(this.styleElement);
    }

    setFont(url: string | null) {
        if (url === null) {
            this.styleElement.innerHTML = '';
            this.setState({ fontName: null });
        } else {
            const fontName = this.createRandomFontName();
            this.styleElement.innerHTML = `
                @font-face {
                    font-family: ${fontName};
                    src: url(${this.props.fontUrl});
                }
            `;
            this.setState({ fontName });
        }
    }

    render() {
        return this.props.children({ fontName: this.state.fontName });
    }
}