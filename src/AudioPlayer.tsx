import * as React from 'react';

export type AudioPlayerChildrenProps = { isPlaying: boolean, playOrStop: () => void };

type AudioPlayerProps = {
    audioUrl: string | null,
    children: (props: AudioPlayerChildrenProps) => JSX.Element,
};

type AudioPlayerState = {
    audio: HTMLAudioElement | null,
    isPlaying: boolean,
};

export default class extends React.Component<AudioPlayerProps, AudioPlayerState> {
    state: AudioPlayerState = {
        audio: null,
        isPlaying: false,
    };

    componentDidMount() {
        if (this.props.audioUrl !== null) {
            this.setAudio(this.props.audioUrl);
        }
    }

    componentDidUpdate(oldProps: AudioPlayerProps) {
        if (oldProps.audioUrl !== this.props.audioUrl) {
            if (this.props.audioUrl) {
                this.setAudio(this.props.audioUrl);
            } else {
                this.setAudio(null);
            }     
        }
    }

    componentWillUnmount() {
        this.setAudio(null);
    }

    playOrStop = () => {
        if (this.state.audio) {
            if (this.state.isPlaying) {
                this.state.audio.pause();
                this.state.audio.currentTime = 0;
            } else {
                this.state.audio.play();
            }
        }
    }

    setAudio(audioUrl: string | null) {
        if (this.state.audio) {
            if (this.state.isPlaying) {
                this.state.audio.pause();
            }

            delete this.state.audio.onplay;
            delete this.state.audio.onpause;
            delete this.state.audio.src;
            this.state.audio = null;
        }

        if (audioUrl !== null) {
            const audio = new Audio();
            this.setState({ audio, isPlaying: false });
            audio.onplay = this.onAudioPlay(audio);
            audio.onpause = this.onAudioPause(audio);
            audio.src = audioUrl;
        } else {
            this.setState({ audio: null, isPlaying: false });
        }
    }

    onAudioPause = (audio: HTMLAudioElement) => () => {
        if (this.state.audio === audio) {
            this.setState({ isPlaying: false });
        }
    }

    onAudioPlay = (audio: HTMLAudioElement) => () => {
        if (this.state.audio === audio) {
            this.setState({ isPlaying: true });
        }
    }

    render() {
        return this.props.children({ isPlaying: this.state.isPlaying, playOrStop: this.playOrStop });
    }
}