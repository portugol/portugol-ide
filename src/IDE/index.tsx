import * as qs from 'qs';
import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import ActionsBar from '../ActionsBar';
import { ApplicationFile } from '../App';
import Console from '../Console';
import { Editor, FullSize } from '../Editor';
import NavigationBar from '../NavigationBar';
import ApplicationNameInput from '../NavigationBar/ApplicationNameInput';
import NavigationBarButton from '../NavigationBar/Button';
import UserBadge from '../NavigationBar/UserBadge';
import { Program } from '../portugol/ast';
import { interprete } from '../portugol/interpreter/worker-client';
import parser from '../portugol/parser.pegjs';
import { ParseError } from '../portugol/parsing';
import ApplicationSettings from './ApplicationSettings';
import IDEContext from './Context';
import './index.css';

type IDEProps = RouteComponentProps<{ applicationName: string | undefined }> & {
    loadedApplication: { code: string, name: string, ownerUsername: string } | undefined,
    unloadApplication: () => void,
    loadApplication: (name: string, username?: string) => void,
    updateApplication: (name: string, changes: { code?: string, name?: string }) => void,
    createApplication: (application: { code: string, name: string }) => void,
    addFilesToApplication: (files: File[], tracked?: boolean) => void,
    removeFileFromApplication: (fileName: string) => void,
    updateApplicationFileName: (oldName: string, newName: string) => void,
    applicationFiles: ApplicationFile[],
};

type IDEState = {
    applicationName: string,
    applicationCode: string,
    isPlaying: boolean,
    showingApplicationSettings: boolean,
};

export default class IDE extends React.Component<IDEProps, IDEState> {
    console = React.createRef<Console>();
    editor = React.createRef<Editor>();

    state = {
        applicationName: '',
        applicationCode: '',
        isPlaying: false,
        showingApplicationSettings: false,
    };

    windowUnloadListener = () => {
        if (this.stopInterpreting) {
            this.stopInterpreting();
        }
    }

    componentDidMount() {
        if (this.props.loadedApplication) {
            this.editor.current!.editor.setValue(this.props.loadedApplication.code);
            this.setState({ applicationName: this.props.loadedApplication.name, applicationCode: this.props.loadedApplication.code });
        } else {
            this.editor.current!.editor.setValue('');
            this.setState({ applicationName: '', applicationCode: '' });
        }

        const nameFromRoute = this.props.match.params.applicationName;
        if (nameFromRoute) {
            const params = qs.parse(this.props.location.search, { ignoreQueryPrefix: true });
            this.props.loadApplication(nameFromRoute, params.username);
        } else {
            this.props.unloadApplication();
        }

        window.addEventListener('beforeunload', this.windowUnloadListener);
    }

    componentWillUnmount() {
        window.removeEventListener('beforeunload', this.windowUnloadListener);
    }

    componentDidUpdate(oldProps: IDEProps) {
        if (oldProps.loadedApplication !== this.props.loadedApplication) {
            if (this.props.loadedApplication) {
                this.editor.current!.editor.setValue(this.props.loadedApplication.code);
                this.setState({ applicationName: this.props.loadedApplication.name, applicationCode: this.props.loadedApplication.code }, () => {
                    // add the username to the route after loading the application
                    // to facilitate the sharing
                    this.props.history.push({
                        search: qs.stringify(
                            { username: this.props.loadedApplication!.ownerUsername },
                            { addQueryPrefix: true },
                        ),
                    });
                });
            } else {
                this.editor.current!.editor.setValue('');
                this.setState({ applicationName: '', applicationCode: '' });
            }
        }
        
        const nameFromRoute = this.props.match.params.applicationName;

        if (oldProps.match.params.applicationName !== nameFromRoute) {
            if (nameFromRoute) {
                const params = qs.parse(this.props.location.search, { ignoreQueryPrefix: true });
                this.props.loadApplication(nameFromRoute, params.username);
            } else {
                this.props.unloadApplication();
            }
        }

        const oldName = oldProps.loadedApplication && oldProps.loadedApplication.name;
        const newName = this.props.loadedApplication && this.props.loadedApplication.name;

        if (newName && newName !== oldName && newName !== nameFromRoute) {
            this.props.history.replace(`/edit/${encodeURIComponent(newName)}`);
        }
    }

    setApplicationName = (applicationName: string) => {
        this.setState({ applicationName });
    }

    handleEditorContentChange = (applicationCode: string) => {
        this.setState({ applicationCode });
    }

    // tslint:disable-next-line
    stopInterpreting: (() => Promise<void>) | null = null;

    onStop = async () => {
        if (this.stopInterpreting) {
            await this.stopInterpreting();
        }
    }

    onPlay = () => {
        const value = this.editor.current!.getValue();
        this.console.current!.clear();
        try {
            let ast: Program;
            try {
                ast = parser.parse(value);
            } catch (error) {
                throw new ParseError('Houve um erro ao tentar interpretar.', error.location);
            }
            this.setState({ isPlaying: true });
            const { stop } = interprete(
                ast,
                { 
                    write: async (v: string) => this.console.current!.write(v),
                    read: async () => this.console.current!.read(),
                    clear: async () => this.console.current!.clear(),
                },
                {
                    getFileUrl: (path: string) => {
                        const file = this.props.applicationFiles.find(af => af.name === path);
                        if (!file) return null;
                        if (!file.dataUrl) {
                            // the resource has not being downloaded from the server yet
                            // TODO: improve the handling :D
                            throw new Error(`O arquivo "${file.name}" ainda não foi baixado do servidor.`);
                        }
                        return file.dataUrl;
                    },
                    getFile: (path) => {
                        const file = this.props.applicationFiles.find(af => af.name === path);
                        if (file === undefined) { return null; }
                        if (file.raw === undefined) {
                            throw new Error(`O arquivo "${file.name}" ainda não foi baixado do servidor.`);
                        }
                        return file.raw;
                    },
                    deleteFile: (path) => {
                        this.props.removeFileFromApplication(path);
                    },
                    createOrReplaceFile: (file) => {
                        const fileNameSplit = file.name.split('.');
                        const extension = fileNameSplit[fileNameSplit.length - 1];
                        if (!extension) {
                            throw new Error('Para criar um arquivo é necessário definir a extensão.');
                        }
                        if (fileNameSplit[0] === '') {
                            throw new Error('O nome do arquivo não pode ser vazio.');
                        }
                        if (file.name.includes('/')) {
                            throw new Error('O nome do arquivo não pode conter o caracter "/"');
                        }
                        this.props.addFilesToApplication([file], false);
                    },
                    listFiles: (ofTypes) => {
                        let filesNames = this.props.applicationFiles.map(file => {
                            if (!file.raw) {
                                throw new Error(`O arquivo "${file.name}" ainda não foi baixado do servidor.`);
                            }
                            return file.raw.name;
                        });
                        if (ofTypes !== undefined) {
                            filesNames = filesNames.filter(fileName => {
                                const fileNameSplit = fileName.split('.');
                                const extension = fileNameSplit[fileNameSplit.length - 1];
                                return ofTypes.includes(extension);
                            });
                        }
                        return filesNames;
                    },
                    selectFile: (acceptTypes) => {
                        throw new Error('não implementado :D');
                    },
                },
                (error) => {
                    // tslint:disable-next-line no-console
                    console.error(error);
                    this.console.current!.write(error.message, true);
                    if ((error as any).location) {
                        this.console.current!.write(
                            `\\nLINHA ${(error as any).location.start.line}, COLUNA ${(error as any).location.start.column}\\n`,
                            true
                        );
                    }
                },
                () => {
                    this.setState({ isPlaying: false });
                }
            );
            this.stopInterpreting = stop;
        } catch (error) {
            // tslint:disable-next-line no-console
            console.error(error);
            this.console.current!.write(error.message, true);
            // TODO: duplicate
            if ((error as any).location) {
                this.console.current!.write(
                    `\\nLINHA ${(error as any).location.start.line}, COLUNA ${(error as any).location.start.column}\\n`,
                    true
                );
            }
        }
        
    }

    toggleShowingApplicationSettings = () => {
        this.setState(({ showingApplicationSettings }) => ({ showingApplicationSettings: !showingApplicationSettings }));
    }

    onSave = async () => {
        const nameFromRoute = this.props.match.params.applicationName;
        const savedCode = this.props.loadedApplication && this.props.loadedApplication.code || '';
        const name = this.state.applicationName;
        const code = this.state.applicationCode;
        
        if (!name) { 
            alert('é necessário definir o nome da aplicação!');
            return;
        }

        if (nameFromRoute) {
            const changes: { name?: string, code?: string } = {};

            if (nameFromRoute !== name) {
                changes.name = name;
            }
            if (savedCode !== code) {
                changes.code = code;
            }

            this.props.updateApplication(nameFromRoute, changes);
        } else {
            this.props.createApplication({ name, code });
        }
    }

    renderEditor = (params: { width: number, height: number }) => (
        <Editor
            onChange={this.handleEditorContentChange}
            width={params.width}
            height={params.height}
            ref={this.editor}
        />
    )

    render() {
        return (
            <IDEContext.Provider value={{
                applicationSavedName: this.props.match.params.applicationName,
                applicationName: this.state.applicationName,
                setApplicationName: this.setApplicationName,
                applicationFiles: this.props.applicationFiles,
                addFilesToApplication: this.props.addFilesToApplication,
                removeFileFromApplication: this.props.removeFileFromApplication,
                updateApplicationFileName: this.props.updateApplicationFileName,
            }}>
                <div className="ide">
                    <ApplicationSettings showingApplicationSettings={this.state.showingApplicationSettings} />
                    <div style={{ 
                        display: 'flex',
                        flexDirection: 'column',
                        flexGrow: 1,
                    }}>
                        <NavigationBar 
                            { /* tslint:disable-next-line */ ...{} }
                            renderLeft={() => (
                                <>
                                    <NavigationBarButton
                                        iconName="folder"
                                        title="Arquivos"
                                        onClick={this.toggleShowingApplicationSettings}
                                    />
                                    <ApplicationNameInput />
                                </>
                            )}
                            { /* tslint:disable-next-line */ ...{} }
                            renderRight={() => <UserBadge history={this.props.history} />}
                        />
                        <div style={{ display: 'flex', flexGrow: 1, overflow: 'hidden' }}>
                            <ActionsBar
                                canPlay={!this.state.isPlaying}
                                onPlay={this.onPlay}
                                canStop={this.state.isPlaying}
                                onStop={this.onStop}
                                canSave={true}
                                onSave={this.onSave}
                            />
                            <div style={{ flexGrow: 1 }}>
                                <div style={{ height: '70%' }}>
                                    <FullSize render={this.renderEditor} />
                                </div>
                                <div style={{ height: '30%' }}>
                                    <Console ref={this.console} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </IDEContext.Provider>
        );
    }
}