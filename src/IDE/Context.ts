import * as React from 'react';
import { ApplicationFile } from '../App';

export type IDEContextType = {
    applicationSavedName: string | undefined,
    applicationName: string,
    setApplicationName: (applicationName: string) => void,
    addFilesToApplication: (files: File[]) => void,
    removeFileFromApplication: (fileName: string) => void,
    updateApplicationFileName: (oldName: string, newName: string) => void,
    applicationFiles: ApplicationFile[];
};

export const IDEContext = React.createContext<IDEContextType>({
    applicationSavedName: undefined,
    applicationName: '',
    setApplicationName: () => undefined,
    addFilesToApplication: () => undefined,
    removeFileFromApplication: () => undefined,
    updateApplicationFileName: () => undefined,
    applicationFiles: [],
});

export default IDEContext;