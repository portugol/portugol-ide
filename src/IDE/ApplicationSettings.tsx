import * as React from 'react';
import FilesDropzone from '../FilesDropzone';
import './ApplicationSettings.css';

const Header = () => (
    <div className="header">
        <h4>Arquivos</h4>
    </div>
);

type ApplicationSettingsProps = {
    showingApplicationSettings: boolean,
};

export default class extends React.Component<ApplicationSettingsProps> {
    render() {
        return (
            <div className={`ide-application-settings${this.props.showingApplicationSettings ? ' is-open' : ''}`}>
                <div className="ide-application-settings-content">
                    <Header />
                    <FilesDropzone />
                </div>
            </div>
        );
    }
}