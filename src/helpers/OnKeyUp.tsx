import * as React from 'react';

type OnKeyUpProps = {
    do: () => void,
    keyCode: number[] | number,
    children?: React.Props<{}>['children'],
    element?: HTMLElement | null,
};

export default class extends React.Component<OnKeyUpProps> {
    keyUpListener = (event: KeyboardEvent) => {
        const keysCodes = typeof this.props.keyCode === 'number' ? [this.props.keyCode] : this.props.keyCode;
        if (keysCodes.includes(event.keyCode)) {
            this.props.do();
        }
    }

    componentDidUpdate(oldProps: OnKeyUpProps) {
        if (this.props.element !== oldProps.element) {
            this.removeEventListener(oldProps.element);
            this.addEventListener();
        }
    }

    componentDidMount() {
        this.addEventListener();
    }

    componentWillUnmount() {
        this.removeEventListener();
    }

    render() {
        return this.props.children || null;
    }

    private addEventListener() {
        const element = this.props.element || document;
        element.addEventListener('keyup', this.keyUpListener);
    }

    private removeEventListener(from: HTMLElement | undefined | null = this.props.element) {
        const element = from || document;
        element.removeEventListener('keyup', this.keyUpListener);
    }
}