import * as React from 'react';
import { BrowserRouter, Redirect, Route, RouteComponentProps, Switch } from 'react-router-dom';
import ActionMessageController from './ActionMessageController';
import './App.css';
import AppContext from './AppContext';
import ApplicationFiles, { ApplicationFiles as ApplicationFilesComponent, ApplicationFilesChildrenProps } from './containers/ApplicationFiles';
import { PreviewableFile } from './FilePreviewContainer';
import IDE from './IDE';
import ApplicationsServer, { IdentityNotRegisteredError, InvalidCredentialsError, UsernameAlreadyInUseError } from './services/applications-server';
import SignInModal from './SignInModal';
import SignInWithUnivaliModal from './SignInWithUnivaliModal';
import SignUpModal from './SignUpModal';
import UserProfile from './UserProfile';

type AppState = {
    application: { code: string, name: string, ownerUsername: string } | undefined,
    deletedApplicationFilesNames: string[],
    loggedUserUsername: string | undefined,
    userApplications: Array<{ name: string, ownerUsername: string, lastUpdated: Date }> | undefined,
    signUp: {
        isOpen: boolean,
        isSingingUp: boolean,
    },
    signIn: {
        isOpen: boolean,
        isSigningIn: boolean,
    },
    signInWithUnivali: {
        isOpen: boolean,
        isSigningIn: boolean,
        isFirstSignIn: boolean,
    }
};

export type ApplicationFile = PreviewableFile & {
    isBusy: boolean,
    oldName?: string,
    uploaded: boolean,
};

export default class App extends React.Component<{}, AppState> {
    applicationsServer = new ApplicationsServer();
    actionMessageRef = React.createRef<ActionMessageController>();
    signUpModalRef = React.createRef<SignUpModal>();
    applicationFilesRef = React.createRef<ApplicationFilesComponent>();
    
    state: AppState = {
        application: undefined,
        deletedApplicationFilesNames: [],
        loggedUserUsername: undefined,
        userApplications: undefined,
        signUp: {
            isOpen: false,
            isSingingUp: false,
        },
        signIn: {
            isOpen: false,
            isSigningIn: false,
        },
        signInWithUnivali: {
            isOpen: false,
            isSigningIn: false,
            isFirstSignIn: false,
        }
    };

    componentDidMount() {
        if (this.applicationsServer.authentication.isAuthenticated()) {
            this.setState({ loggedUserUsername: this.applicationsServer.authentication.getUsername() });
        }
    }

    loadApplication = async (name: string, username?: string) => {
        const application = await this.applicationsServer.applications.getOne(name, username);
        if (application) {
            this.setState({ 
                application: {
                    name: application.name,
                    code: application.code,
                    ownerUsername: application.ownerUsername,
                },
            });
        }
    }

    unloadApplication = () => {
        this.setState({ application: undefined });
    }

    createApplication = async (application: { name: string, code: string }) => {
        if (!this.applicationsServer.authentication.isAuthenticated()) {
            alert('Não é possível criar uma aplicação sem estar autenticado.');
            return;
        }

        const applicationFiles = this.applicationFilesRef.current!;
        applicationFiles.lock();

        await this.applicationsServer.applications.create(application);
        this.setState({ 
            application: {
                ...application,
                ownerUsername: this.state.loggedUserUsername!,
            },
        });

        applicationFiles.unlock();
        applicationFiles.saveChanges();
    }

    // TODO: if the user trying to save is not the owner, should create a new one and redirect ;)
    // TODO: make the save button disabled while saving
    updateApplication = async (name: string, changes: { name?: string, code?: string }) => {
        if (!this.applicationsServer.authentication.isAuthenticated()) {
            alert('Não é possível atualizar uma aplicação sem estar autenticado.');
            return;
        }

        const applicationFiles = this.applicationFilesRef.current!;
        applicationFiles.lock();

        await this.applicationsServer.applications.update(name, changes);
        this.setState(({ application }) => ({ application: { ...application!, ...changes } }));

        applicationFiles.unlock();
        applicationFiles.saveChanges();
    }

    loadUserApplications = async (username: string) => {
        this.setState({ userApplications: undefined });
        const userApplications = await this.applicationsServer.applications.getAll(username);
        this.setState({ userApplications });
    }

    deleteApplication = async (name: string) => {
        await this.applicationsServer.applications.delete(name);
        if (this.state.userApplications) {
            const applicationIndex = this.state.userApplications
                .findIndex(app => app.name === name && app.ownerUsername === this.state.loggedUserUsername);
            if (applicationIndex !== -1) {
                this.setState(({ userApplications }) => ({
                    userApplications: [
                        ...userApplications!.slice(0, applicationIndex),
                        ...userApplications!.slice(applicationIndex + 1),
                    ],
                }));
            }
        }
    }

    renderIDE = (props: RouteComponentProps<{ applicationName: string | undefined }>) => {
        const renderIDE = (childrenProps: ApplicationFilesChildrenProps) => (
            <IDE 
                {...props}
                unloadApplication={this.unloadApplication}
                loadApplication={this.loadApplication}
                createApplication={this.createApplication}
                updateApplication={this.updateApplication}
                loadedApplication={this.state.application}
                applicationFiles={childrenProps.files}
                addFilesToApplication={childrenProps.addFiles}
                removeFileFromApplication={childrenProps.removeFile}
                updateApplicationFileName={childrenProps.renameFile}
            />
        );

        return (
            <ApplicationFiles 
                application={this.state.application 
                    ? { 
                        name: this.state.application.name,
                        ownerUsername: this.state.application.ownerUsername,
                    } 
                    : null}
                ref={this.applicationFilesRef}
            >
                {renderIDE}
            </ApplicationFiles>
        );   
    }

    renderUserProfile = (props: RouteComponentProps<{ username: string }>) => {
        return (
            <UserProfile
                {...props}
                loadApplications={this.loadUserApplications}
                applications={this.state.userApplications}
                loggedUserUsername={this.state.loggedUserUsername}
                deleteApplication={this.deleteApplication}
            />
        );
    }

    logout = () => {
        this.applicationsServer.account.deauthenticate()
            .then(() => this.setState({ loggedUserUsername: undefined }))
            .catch(() => alert('não foi possível sair'));
    }

    toggleSignIn = () => {
        this.setState(({ signIn, signUp, signInWithUnivali }) => ({
            signInWithUnivali: { ...signInWithUnivali, isOpen: false },
            signUp: { ...signUp, isOpen: false },
            signIn: { ...signIn, isOpen: !signIn.isOpen },
        }));
    }

    toggleSignUp = () => {
        this.setState(({ signIn, signUp, signInWithUnivali }) => ({
            signIn: { ...signIn, isOpen: false },
            signInWithUnivali: { ...signInWithUnivali, isOpen: false },
            signUp: { ...signUp, isOpen: !signUp.isOpen },
        }));
    }

    toggleSignInWithUnivali = () => {
        this.setState(({ signIn, signUp, signInWithUnivali }) => ({
            signIn: { ...signIn, isOpen: false },
            signUp: { ...signUp, isOpen: false },
            signInWithUnivali: { ...signInWithUnivali, isOpen: !signInWithUnivali.isOpen },
        }));
    }

    signIn = (username: string, password: string, showSuccessMessage = true) => {
        this.setState({ signIn: { ...this.state.signIn, isSigningIn: true } });
        this.applicationsServer.account.authenticate({ username, password })
            .then(() => {
                this.setState({ 
                    loggedUserUsername: this.applicationsServer.authentication.getUsername(),
                    signIn: {
                        ...this.state.signIn,
                        isSigningIn: false,
                        isOpen: false,
                    }
                });
                if (showSuccessMessage) {
                    this.actionMessageRef.current!.addMessage('Conectado a sua conta.');
                }
            })
            .catch((error) => {
                if (error instanceof InvalidCredentialsError) {
                    this.actionMessageRef.current!.addMessage('Nome de usuário ou senha incorretos.', 'DANGER');
                } else {
                    this.actionMessageRef.current!.addMessage('Algo deu errado.', 'DANGER');
                }

                this.setState({ signIn: { ...this.state.signIn, isSigningIn: false, isOpen: true } });
            });
    }

    signInWithUnivali = (personCode: string, password: string, username?: string) => {
        this.setState({ signInWithUnivali: { ...this.state.signInWithUnivali, isSigningIn: true } });
        this.applicationsServer.account.authenticateWithUnivali({ personCode, password, username })
            .then(() => {
                this.setState(({ signInWithUnivali }) => ({ 
                    loggedUserUsername: this.applicationsServer.authentication.getUsername(),
                    signInWithUnivali: {
                        ...signInWithUnivali,
                        isSigningIn: false,
                        isOpen: false,
                        isFirstSignIn: false,
                    }
                }));
                this.actionMessageRef.current!.addMessage('Conectado a sua conta.');
            })
            .catch((error) => {
                if (error instanceof InvalidCredentialsError) {
                    this.actionMessageRef.current!.addMessage('Nome de usuário ou senha incorretos.', 'DANGER');
                } else if (error instanceof IdentityNotRegisteredError) {
                    this.actionMessageRef.current!.addMessage('Mais informações serão necessárias', 'WARNING');
                    this.setState(({ signInWithUnivali }) => ({ signInWithUnivali: { ...signInWithUnivali, isFirstSignIn: true } }));
                } else {
                    this.actionMessageRef.current!.addMessage('Algo deu errado.', 'DANGER');
                }

                this.setState(({ signInWithUnivali }) => ({ signInWithUnivali: { ...signInWithUnivali, isSigningIn: false, isOpen: true } }));
            });
    }

    signUp = (email: string, username: string, password: string) => {
        this.setState({ signUp: { ...this.state.signUp, isSingingUp: true } });
        this.applicationsServer.account.create({ email, username, password })
            .then(() => {
                this.actionMessageRef.current!.addMessage('Cadastrado com sucesso.');
                this.signIn(username, password, false);
            })
            .then(() => this.setState({ signUp: { ...this.state.signUp, isOpen: false, isSingingUp: false } }))
            .catch((error) => {
                if (error instanceof UsernameAlreadyInUseError) {
                //     this.signUpModalRef.current!.addError('username', 'Nome de usuário já está sendo utilizado.');
                    this.actionMessageRef.current!.addMessage('Nome de usuário já está sendo utilizado.', 'DANGER');
                } else {
                    this.actionMessageRef.current!.addMessage('Algo deu errado.', 'DANGER');
                }
                
                this.setState({ signUp: { ...this.state.signUp, isOpen: true, isSingingUp: false } });
            });
    }

    render() {
        return (
            <>
                <SignInWithUnivaliModal
                    isOpen={this.state.signInWithUnivali.isOpen}
                    isSigningIn={this.state.signInWithUnivali.isSigningIn}
                    isFirstSignIn={this.state.signInWithUnivali.isFirstSignIn}
                    toggle={this.toggleSignInWithUnivali}
                    signInWithUnivali={this.signInWithUnivali}
                    signIn={this.toggleSignIn}
                />
                <SignInModal
                    isOpen={this.state.signIn.isOpen}
                    isSigningIn={this.state.signIn.isSigningIn}
                    signIn={this.signIn}
                    signUp={this.toggleSignUp}
                    toggle={this.toggleSignIn}
                    signInWithUnivali={this.toggleSignInWithUnivali}
                />
                <SignUpModal
                    ref={this.signUpModalRef}
                    isOpen={this.state.signUp.isOpen}
                    isSigningUp={this.state.signUp.isSingingUp}
                    toggle={this.toggleSignUp}
                    signUp={this.signUp}
                    signIn={this.toggleSignIn}
                />
                <AppContext.Provider 
                    value={{ 
                        loggedUserUsername: this.state.loggedUserUsername, 
                        logout: this.logout,
                        requestLogin: this.toggleSignIn,
                        requestCreateAccount: this.toggleSignUp,
                        applicationsServer: this.applicationsServer,
                    }}
                >
                    <ActionMessageController ref={this.actionMessageRef}>
                        <BrowserRouter>
                            <Switch>
                                <Route 
                                    path="/edit/:applicationName?"
                                    render={this.renderIDE}
                                />
                                <Route
                                    path="/user/:username"
                                    render={this.renderUserProfile}
                                />
                                <Redirect to="/edit" />
                            </Switch>
                        </BrowserRouter>
                    </ActionMessageController>
                </AppContext.Provider>
            </>
        );
    }
}