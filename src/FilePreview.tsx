import classNames from 'classnames';
import * as React from 'react';
import AudioPlayer from './AudioPlayer';
import './FilePreview.css';
import { isFileAudio, isFileFont, isFileImage } from './FilePreviewContainer';
import FontInliner from './FontInliner';

type FilePreviewProps = React.Props<{}> & {
    file: File,
    className?: string,
};

type FilePreviewTypeProps = FilePreviewProps & {
    dataUrl: string,
};

const FilePreviewDefault = (props: FilePreviewTypeProps) => {
    const fileNameSplit = props.file.name.split('.');
    const fileExtension = fileNameSplit[fileNameSplit.length - 1];

    return (
        <div className={classNames(`file-preview`, props.className)}>
            <div className="file-extension">
                .{fileExtension}
            </div>
            {props.children}
        </div>
    );
};

const FilePreviewImage = (props: FilePreviewTypeProps) => (
    <div className={classNames(`file-preview`, props.className)}>
        <img src={props.dataUrl} className="image-preview" />
        {props.children}
    </div>
);

const FilePreviewAudio = (props: FilePreviewTypeProps) => (
    <AudioPlayer audioUrl={props.file && props.dataUrl || null}>
        {({ isPlaying, playOrStop }) => (
            <div className={classNames(`file-preview`, props.className)}>
                <button onClick={playOrStop} className="play-button">
                    {isPlaying 
                        ? <i className="fas fa-stop" /> 
                        : <i className="fas fa-play" />}
                </button>
                {props.children}
            </div>
        )}
    </AudioPlayer>
);

const FilePreviewFont = (props: FilePreviewTypeProps) => (
    <FontInliner fontUrl={props.file && props.dataUrl || null}>
        {({ fontName }) =>  ( 
            <div className={classNames(`file-preview`, props.className)} style={{ fontFamily: fontName === null ? '' : fontName }}>
                <div className="font-preview">
                    0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz
                </div>
                {props.children}
            </div>
        )}
    </FontInliner>
);

type FilePreviewState = {
    dataUrl: string,
};

export default class extends React.Component<FilePreviewProps, FilePreviewState> {
    state: FilePreviewState = {
        dataUrl: '',
    };

    componentDidMount() {
        this.setState({ dataUrl: URL.createObjectURL(this.props.file) });
    }

    componentDidUpdate(oldProps: FilePreviewProps) {
        if (oldProps.file !== this.props.file) {
            const urlToRevoke = this.state.dataUrl;
            setImmediate(() => {
                URL.revokeObjectURL(urlToRevoke);
            });
            this.setState({ dataUrl: URL.createObjectURL(this.props.file) });
        }
    }

    componentWillUnmount() {
        const urlToRevoke = this.state.dataUrl;
        setImmediate(() => {
            URL.revokeObjectURL(urlToRevoke);
        });
    }

    render() {
        if (this.props.file) {
            if (isFileImage(this.props.file)) {
                return <FilePreviewImage {...this.props}  dataUrl={this.state.dataUrl} />;
            }
            if (isFileAudio(this.props.file)) {
                return <FilePreviewAudio {...this.props} dataUrl={this.state.dataUrl} />;
            }
            if (isFileFont(this.props.file)) {
                return <FilePreviewFont {...this.props}  dataUrl={this.state.dataUrl} />;
            }
        }
        return <FilePreviewDefault {...this.props} dataUrl={this.state.dataUrl} />;
    }
}