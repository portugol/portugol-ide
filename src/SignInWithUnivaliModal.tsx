import classNames from 'classnames';
import * as React from 'react';
import ModalForm from './ui/ModalForm';
import ActionButton from './ui/ModalForm/ActionButton';
import ButtonGroup from './ui/ModalForm/ButtonGroup';
import SubmitButton from './ui/ModalForm/SubmitButton';
import TextField from './ui/ModalForm/TextField';
// tslint:disable-next-line no-var-requires
const styles = require('./SignInWithUnivaliModal.css');

type SignInWithUnivaliModalProps = {
    isOpen: boolean,
    toggle: () => void,
    isSigningIn: boolean,
    signInWithUnivali: (personCode: string, password: string, username?: string) => void,
    signIn: () => void,
    isFirstSignIn: boolean,
};

export default class extends React.Component<SignInWithUnivaliModalProps> {
    signInWithUnivali = (values: { [key: string]: string }) => {
        this.props.signInWithUnivali(values.personCode, values.password, values.username);
    }

    validateUsername = (username: string): string | null => {
        if (username === '') {
            return 'Escolha um nome de usuário.';
        }
        if (username.length > 18) {
            return 'O nome de usuário deve conter no máximo 18 caracteres.';
        }
        return null;
    }

    validatePersonCode = (username: string): string | null => {
        let error: string | null = null;

        if (username === '') {
            error = 'Informe seu código de pessoa.';
        }

        return error;
    }

    validatePassword = (password: string): string | null => {
        let error: string | null = null;

        if (password === '') {
            error = 'Informe sua senha.';
        }

        return error;
    }

    render() {
        return (
            <ModalForm
                toggle={this.props.toggle}
                isOpen={this.props.isOpen}
                className={classNames(styles.modal, { 'is-signup': this.props.isFirstSignIn })}
                disabled={this.props.isSigningIn}
                title={this.props.isFirstSignIn ? 'Cadastrar com UNIVALI' : 'Entrar com UNIVALI'}
                onSubmit={this.signInWithUnivali}
            >
                <TextField
                    isSecure={false}
                    placeholder="Informe seu código de pessoa"
                    name="personCode"
                    validate={this.validatePersonCode}
                />
                <TextField
                    isSecure={true}
                    placeholder="Informe sua senha"
                    name="password"
                    validate={this.validatePassword}
                />
                {this.props.isFirstSignIn && (
                    <TextField
                        isSecure={false}
                        placeholder="Escolha um nome de usuário"
                        name="username"
                        validate={this.validateUsername}
                    />
                )}
                <ButtonGroup>
                    <ActionButton action={this.props.signIn}>
                        Voltar
                    </ActionButton>
                    <SubmitButton>
                        {this.props.isFirstSignIn ? 'Cadastrar' : 'Entrar'}
                    </SubmitButton>
                </ButtonGroup>
            </ModalForm>
        );
    }
}