import { css, StyleSheet } from 'aphrodite';
import * as React from 'react';
import * as theme from '../theme';

import IDEContext from '../IDE/Context';

type ApplicationNameProps = {
    value: string,
    onChange: (value: string) => void,
};

const ApplicationNameInput = (props: ApplicationNameProps) => {
    const onChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        props.onChange(event.target.value);
    };
    return (
        <input
            className={css(styles.applicationNameInput)}
            type="text"
            placeholder="Sem nome definido"
            value={props.value}
            onChange={onChangeHandler}
        />
    );
};

const styles = StyleSheet.create({
    applicationNameInput: {
        height: '44px',
        width: '400px',
        margin: '5px',
        padding: '2px 10px',
        fontSize: '32px',
        backgroundColor: 'transparent',
        border: '1px solid transparent',
        color: theme.foregroundColor.normal,

        ':hover,:focus': {
            outline: 0,
            border: `1px solid ${theme.foregroundColor.normal}`,
            borderRadius: '5px',
            backgroundColor: theme.backgroundColor.light,
        },
    },
});

export default () => (
    <IDEContext.Consumer>
        {({ applicationName, setApplicationName }) => (
            <ApplicationNameInput
                value={applicationName}
                onChange={setApplicationName}
            />
        )}
    </IDEContext.Consumer>
);