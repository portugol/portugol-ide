import { css, StyleSheet } from 'aphrodite';
import { History } from 'history';
import * as React from 'react';
import { Collapse } from 'react-collapse';
import { Link } from 'react-router-dom';
import AppContext from '../AppContext';
import Menu, { MenuItem } from '../Menu';
import * as theme from '../theme';
import Button from './Button';

type UserBadgeContextProps = {
    loggedUserUsername: string | undefined,
    logout: () => void,
    requestLogin: () => void,
    requestCreateAccount: () => void,
};

type UserBadgeProps = {
    history: History,
};

type UserBadgeState = {
    isDropdownOpen: boolean,
};

class UserBadge extends React.Component<UserBadgeContextProps & UserBadgeProps, UserBadgeState> {
    state = {
        isDropdownOpen: false,
    };

    componentDidMount() {
        document.addEventListener('click', this.closeDropdownIfOpen);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.closeDropdownIfOpen);
    }

    toggleDropdown = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.nativeEvent.stopImmediatePropagation();
        this.setState(({ isDropdownOpen }) => ({ isDropdownOpen: !isDropdownOpen }));
    }

    closeDropdownIfOpen = () => {
        if (this.state.isDropdownOpen) {
            this.setState({ isDropdownOpen: false });
        }
    }

    goToMyData = () => {
        this.props.history.push(`/user/${this.props.loggedUserUsername}/user-data`);
    }

    renderDropdownItems: () => JSX.Element = () => {
        const isLoggedIn = !!this.props.loggedUserUsername;

        return isLoggedIn
            ? (
                <>
                    <MenuItem title="Meus dados" onClick={this.goToMyData} iconName="address-card" iconSolid={false} iconRight={true} />
                    <MenuItem title="Sair" onClick={this.props.logout} iconName="sign-out-alt" iconRight={true} />
                </>
            )
            : (
                <>
                    <MenuItem title="Entrar" onClick={this.props.requestLogin} iconName="sign-in-alt" iconRight={true} />
                    <MenuItem title="Cadastrar" onClick={this.props.requestCreateAccount} iconName="user-plus" iconRight={true} />
                </>
            );
    }

    createUsernameWrapper = () => (
        <div className={css(styles.usernameWrapper)} >
            {this.props.loggedUserUsername
                ? (
                    <Link
                        className={css(styles.username)}
                        to={`/user/${encodeURIComponent(this.props.loggedUserUsername)}`}
                    >
                        {this.props.loggedUserUsername}
                    </Link>
                )
                : 'Não conectado'}
        </div>
    )

    render() {
        return (
            <div className={css(styles.userBadge)}>
                {this.createUsernameWrapper()}
                <Button
                    onClick={this.toggleDropdown}
                    iconName={this.state.isDropdownOpen ? 'caret-up' : 'caret-down'}
                    title="Seu perfil"
                />
                <div className={css(styles.dropdownWrapper)}>
                    <Collapse isOpened={this.state.isDropdownOpen}>
                        <Menu>
                            {this.renderDropdownItems()}
                        </Menu>
                    </Collapse>
                </div>
            </div>
        );
    }
}

const styles = StyleSheet.create({
    userBadge: {
        boxSizing: 'border-box',
        height: '60px',
        display: 'flex',
    },
    username: {
        color: theme.foregroundColor.normal,
        textDecoration: 'none',

        ':hover': {
            textDecoration: 'underline',
        },
    },
    usernameWrapper: {
        width: '190px',
        padding: '5px',
        boxSizing: 'border-box',
        lineHeight: '50px',
        flexGrow: 1,
        textAlign: 'right',
        fontSize: '20px',
        color: theme.foregroundColor.light,
    },
    dropdownWrapper: {
        position: 'absolute',
        right: 0,
        top: '60px',
        zIndex: 1,
    },
});

export default React.forwardRef((props: UserBadgeProps, ref: React.Ref<UserBadge>) => (
    <AppContext.Consumer>
        {({ loggedUserUsername, logout, requestLogin, requestCreateAccount }) => (
            <UserBadge
                {...props}
                ref={ref}
                loggedUserUsername={loggedUserUsername}
                logout={logout}
                requestLogin={requestLogin}
                requestCreateAccount={requestCreateAccount}
            />
        )}
    </AppContext.Consumer>
));