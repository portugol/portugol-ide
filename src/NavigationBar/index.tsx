import { css, StyleSheet } from 'aphrodite';
import * as React from 'react';
import * as theme from '../theme';

type NavigationBarProps = {
    renderLeft?: () => React.ReactElement,
    renderRight?: () => React.ReactElement,
};

const NavigationBar = (props: NavigationBarProps) => (
    <div className={css(styles.navigationBar)}>
        {props.renderLeft && (
            <div style={{ float: 'left' }}>
                {props.renderLeft()}
            </div>
        )}
        {props.renderRight && (
            <div style={{ float: 'right' }}>
                {props.renderRight()}
            </div>
        )}
    </div>
);

const styles = StyleSheet.create({
    navigationBar: {
        height: '60px',
        backgroundColor: theme.backgroundColor.normal,
    },
});

export default NavigationBar;
