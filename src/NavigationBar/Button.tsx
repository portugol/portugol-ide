import { css, StyleSheet } from 'aphrodite';
import * as React from 'react';
import * as theme from '../theme';

type ButtonProps = {
    title?: string,
    onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void,
    iconName: string,
};

const Button = (props: ButtonProps) => (
    <button title={props.title} className={css(styles.button)} onClick={props.onClick} >
        <i className={`fas fa-${props.iconName} ${css(styles.icon)}`} />
    </button>
);

const styles = StyleSheet.create({
    icon: {
        color: theme.foregroundColor.normal,
    },
    button: {
        width: '50px',
        height: '50px',
        fontSize: '32px',
        borderRadius: '5px',
        margin: '5px',
        padding: 0,
        border: 0,
        backgroundColor: 'transparent',
        boxSizing: 'border-box',

        ':not(:disabled)': {
            cursor: 'pointer',
        },

        ':not(:disabled):hover': {
            backgroundColor: theme.backgroundColor.dark,
        },

        ':focus': {
            outline: 0,
        },
    },
});

export default Button;