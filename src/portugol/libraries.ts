import { LibrarySymbol } from './context';
import { ControllableInterface } from './interpreter/interfaces';
import { createLib as createLibArquivos } from './libs/Arquivos';
import { createLib as createLibCalendario } from './libs/Calendario';
import { createLib as createLibGraficos } from './libs/Graficos';
import { createLib as createLibMatematica } from './libs/Matematica';
import { createLib as createLibMouse } from './libs/Mouse';
import { createLib as createLibObjetos } from './libs/Objetos';
import { createLib as createLibSons } from './libs/Sons';
import { createLib as createLibTeclado } from './libs/Teclado';
import { createLib as createLibTexto } from './libs/Texto';
import { createLib as createLibTipos } from './libs/Tipos';
import { createLib as createLibUtil } from './libs/Util';

const libraries: { [key: string]: () => { 
    lib: LibrarySymbol, 
    start: (i?: ControllableInterface) => Promise<void>,
    stop: () => Promise<void>,
}  } = {
    Arquivos: createLibArquivos,
    Calendario: createLibCalendario,
    Graficos: createLibGraficos,
    Matematica: createLibMatematica,
    Objetos: createLibObjetos,
    Sons: createLibSons,
    Teclado: createLibTeclado,
    Texto: createLibTexto,
    Tipos: createLibTipos,
    Util: createLibUtil,
    Mouse: createLibMouse,
};

export default libraries;