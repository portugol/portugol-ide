import { Expression } from './ast';
import { ExpressionResult } from './context';

export type Visitor = { [key in (Expression["type"])]: ((instruction: Expression) => Promise<ExpressionResult>) };

export async function visit(visitObject: Visitor, expression: Expression): Promise<ExpressionResult> {
    const toVisit = visitObject[expression.type];
    if (toVisit === undefined) throw new Error(`VISITOR NÃO IMPLEMENTADO PARA INSTRUÇÃO DE TIPO "${expression.type}".`);
    return toVisit(expression);
}