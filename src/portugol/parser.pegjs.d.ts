import * as ast from './ast';

type Parser = { parse: (program: string) => ast.Program };

declare const parser: Parser;
export default parser;