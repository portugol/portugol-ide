import { ExpressionResult, FunctionSymbolOverload, LibrarySymbol } from '../context';
import { Type } from '../types';

const PRIMITIVE_TYPES: Type[] = ['CADEIA', 'CARACTER', 'INTEIRO', 'LOGICO', 'REAL', 'VAZIO'];

export function createLib(): { 
    lib: LibrarySymbol,
    start: () => Promise<void>,
    stop: () => Promise<void>,
} {
    let sleepTimeout: NodeJS.Timeout | null = null;

    let startedAt: number = 0;

    const lib: LibrarySymbol = {
        type: 'LIBRARY_SYMBOL',
        symbols: {
            aguarde: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Pausa a execução do programa durante um intervalo de tempo especificado',
                        returnType: 'VAZIO',
                        parameters: [{ parameterType: 'INTEIRO', name: 'intervalo', description: 'Intervalo de tempo (em millisegundos) durante o qual o programa ficará pausado' }],
                        instructions: async (args) => {
                            const intervalo: number = args[0].value;
    
                            return new Promise<ExpressionResult>(resolve => {
                                sleepTimeout = setTimeout(() => {
                                    sleepTimeout = null;
                                    resolve({ resultType: 'VAZIO', value: undefined });
                                }, intervalo);
                            });
                        },
                    }
                ],
            },
            sorteia: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Sorteia um número entre o mínimo e o máximo especificados',
                        returnDescription: 'Resultado do sorteio',
                        returnType: 'INTEIRO',
                        parameters: [
                            { parameterType: 'INTEIRO', name: 'minimo', description: 'O menor número que pode ser sorteado' }, 
                            { parameterType: 'INTEIRO', name: 'maximo', description: 'O maior número que pode ser sorteado' }], 
                        instructions: (args) => {
                            const minimo: number = args[0].value;
                            const maximo: number = args[1].value;
                            
                            return {resultType: 'INTEIRO', value: Math.round(Math.random() * (maximo - minimo) + minimo)};
                        },
                    }
                ],
            },
            obter_diretorio_usuario: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        returnType: 'CADEIA',
                        parameters: [], 
                        instructions: () => {
                            return {resultType: 'CADEIA', value: ''};
                        },
                    }
                ],
            },
            tempo_decorrido: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém o tempo decorrido (em milissegundos) desde que a biblioteca foi utilizada pela primeira vez',
                        returnType: 'INTEIRO',
                        parameters: [],
                        instructions: () => {
                            return { resultType: 'INTEIRO', value: new Date().getTime() - startedAt };
                        },
                    },
                ],
            },
            numero_colunas: {
                type: 'FUNCTION_SYMBOL',
                overloads: PRIMITIVE_TYPES.map<FunctionSymbolOverload>(type => ({
                    description: 'Descobre o número de colunas existentes em uma matriz',
                    returnType: 'INTEIRO',
                    returnDescription: 'O número de colunas existentes na matriz',
                    parameters: [
                        {
                            parameterType: { 
                                type: 'ARRAY', 
                                size: null,
                                valueType: { 
                                    type: 'ARRAY', 
                                    valueType: type,
                                    size: null,
                                } 
                            },
                            name: 'matriz',
                            description: 'a matriz em questão',
                        },
                    ],
                    instructions: (args) => {
                        const matriz: number[][] = args[0].value;
                        return { resultType: 'INTEIRO', value: matriz[0].length };
                    },
                })),
            },
            numero_linhas: {
                type: 'FUNCTION_SYMBOL',
                overloads: PRIMITIVE_TYPES.map<FunctionSymbolOverload>(type => ({
                    description: 'Descobre o número de linhas existentes em uma matriz',
                    returnType: 'INTEIRO',
                    returnDescription: 'O número de linhas existentes na matriz',
                    parameters: [
                        {
                            parameterType: { 
                                type: 'ARRAY', 
                                size: null,
                                valueType: { 
                                    type: 'ARRAY', 
                                    valueType: type,
                                    size: null,
                                } 
                            },
                            name: 'matriz',
                            description: 'a matriz em questão',
                        },
                    ],
                    instructions: (args) => {
                        const matriz: any[][] = args[0].value;
                        return { resultType: 'INTEIRO', value: matriz.length };
                    },
                })),
            },
            numero_elementos: {
                type: 'FUNCTION_SYMBOL',
                overloads: PRIMITIVE_TYPES.map<FunctionSymbolOverload>(type => ({
                    description: 'Descobre o número de elementos existentes em um vetor',
                    returnType: 'INTEIRO',
                    returnDescription: 'O número de elementos existentes no vetor',
                    parameters: [
                        {
                            parameterType: { 
                                type: 'ARRAY', 
                                size: null,
                                valueType: type,
                            },
                            name: 'vetor',
                            description: 'o vetor em questão',
                        },
                    ],
                    instructions: (args) => {
                        const vetor: any[] = args[0].value;
                        return { resultType: 'INTEIRO', value: vetor.length };
                    },
                })),
            },
        },
    };

    return {
        lib,
        start: async () => {
            startedAt = new Date().getTime();
        },
        stop: async () => {
            if (sleepTimeout) clearTimeout(sleepTimeout);
        },
    };
}
