import { ExpressionResult, LibrarySymbol } from '../context';
import { GraphicInterface } from '../interpreter/interfaces';

export function createLib(): { 
    lib: LibrarySymbol, 
    start: (graphicInterface: GraphicInterface) => Promise<void>,
    stop: () => Promise<void>,
} {
    let startedGraphicInterface: GraphicInterface | undefined;
    const graphicInterface = new Proxy<GraphicInterface>({} as any, {
        get: (target, property) => {
            if (!startedGraphicInterface) {
                throw new Error('TENTATIVA DE CHAMADA À BIBLIOTECA NÃO INICIADA. grafico');
            }
            return startedGraphicInterface[property];
        },
    });

    const lib: LibrarySymbol = {
        type: 'LIBRARY_SYMBOL',
        symbols: {
            COR_AMARELO: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 0xFFFF00,
                isConstant: true,
            },
            COR_AZUL: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 0x0000FF,
                isConstant: true,
            },
            COR_BRANCO: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 0xFFFFFF,
                isConstant: true,
            },
            COR_PRETO: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 0x000000,
                isConstant: true,
            },
            COR_VERDE: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 0x00FF00,
                isConstant: true,
            },
            COR_VERMELHO: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 0xFF0000,
                isConstant: true,
            },
            criar_cor: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Cria uma nova cor a partir de uma combinação de tons de vermelho, verde e azul',
                        returnDescription: 'A nova cor criada pela combinação dos tons de vermelho, verde e azul',
                        returnType: 'CADEIA',
                        parameters: [
                            { parameterType: 'INTEIRO', name: 'vermelho', description: 'O tom de vermelho (0 a 255)' },
                            { parameterType: 'INTEIRO', name: 'verde', description: 'O tom de verde (0 a 255)' },
                            { parameterType: 'INTEIRO', name: 'azul', description: 'O tom de azul (0 a 255)' },
                        ],
                        instructions: (args) => {
                            const vermelho: number = args[0].value;
                            const verde: number = args[1].value;
                            const azul: number = args[2].value;
    
                            return { resultType: 'CADEIA', value: `rgb(${vermelho}, ${verde}, ${azul})` };
                        }
                    }
                ],
            },
            largura_janela: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém a largura atual da janela do ambiente gráfico',
                        returnType: 'REAL',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            return { resultType: 'REAL', value: await graphicInterface.windowWidth() };
                        },
                    },
                ],
            },
            altura_janela: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém a altura atual da janela do ambiente gráfico',
                        returnType: 'REAL',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            return { resultType: 'REAL', value: await graphicInterface.windowHeight() };
                        },
                    }
                ]
            },
            definir_dimensoes_janela: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Altera as dimensões da janela do ambiente gráfico',
                        returnType: 'VAZIO',
                        parameters: [
                            { parameterType: 'INTEIRO', name: 'largura', description: 'A nova largura da janela' }, 
                            { parameterType: 'INTEIRO', name: 'altura', description: 'A nova altura da janela' }
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const largura: number = args[0].value;
                            const altura: number = args[1].value;

                            await graphicInterface.setWindowDimensions(largura, altura);
    
                            return { resultType: 'VAZIO', value: undefined };
                        }
                    },
                ],
            },
            definir_estilo_texto: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Define o estilo da fonte que será utilizada no ambiente gráfico',
                        returnType: 'VAZIO',
                        parameters: [
                            { parameterType: 'LOGICO', name: 'italico', description: 'Define se a fonte terá o estilo itálico' },
                            { parameterType: 'LOGICO', name: 'negrito', description: 'Define se a fonte terá o estilo negrito' },
                            { parameterType: 'LOGICO', name: 'sublinhado', description: 'Define se a fonte terá o estilo sublinhado' },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const italico: boolean = args[0].value;
                            const negrito: boolean = args[1].value;
                            const sublinhado: boolean = args[2].value;

                            await graphicInterface.setTextStyle(italico, negrito, sublinhado);
    
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            definir_fonte_texto: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Define a fonte que será utilizada no ambiente gráfico',
                        returnType: 'VAZIO',
                        parameters: [{ parameterType: 'CADEIA', name: 'nome', description: 'O nome da fonte a ser utilizada (Ex.: Arial, Times New Roman, Tahoma). Se a fonte informada não existir no sistema operacional do computador, será utilizada a fonte padrão' }],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const nome: string = args[0].value;

                            await graphicInterface.setTextFont(nome);
    
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            definir_tamanho_texto: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Define o tamanho da fonte que será utilizada para desenhar um texto no ambiente gráfico',
                        returnDescription: '',
                        returnType: 'VAZIO',
                        parameters: [{ parameterType: 'REAL', name: 'tamanho', description: 'O tamanho da fonte a ser utilizada'}],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const tamanho: number = args[0].value;

                            await graphicInterface.setTextSize(tamanho);
    
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            iniciar_modo_grafico: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Inicia modo gráfico e exibe uma tela com as configurações padrão (tamanho 640x480 e fundo preto), se o modo gráfico já estiver iniciado, nada acontecerá',
                        returnType: 'VAZIO',
                        parameters: [{ parameterType: 'LOGICO', name: 'manter_visivel', description: 'Define se a janela do ambiente gráfico deve permanecer sepre visível sobre outras janelas (útil durante depuração)' }],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const manterVisivel: boolean = args[0].value;
    
                            await graphicInterface.startGraphicMode(manterVisivel);
    
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            encerrar_modo_grafico: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Encerra o modo gráfico e fecha a janela criada com a função "iniciar_modo_gráfico"',
                        returnType: 'VAZIO',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            await graphicInterface.stopGraphicMode();
    
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    }
                ]
            },
            definir_cor: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Define a cor atual do ambiente gráfico. Esta cor será utilizada para desenhar e preencher as primitivas gráficas (ponto, linha, retângulo, etc.) e, como cor de fundo ao*limpar o ambiente gráfico ou desenhar um texto',
                        returnType: 'VAZIO',
                        parameters: [{ parameterType: 'INTEIRO', name: 'cor', description: 'A nova cor do ambiente gráfico' }],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const cor: number = args[0].value;

                            await graphicInterface.setColor(cor);
    
                            return { resultType: 'VAZIO', value: undefined };
                        }
                    }
                ]
            },
            desenhar_elipse: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Desenha uma elipse na posição definida pelos parâmetros x e y e com as dimensões especificadas pelos parâmetros largura e altura, .A elipse é desenhada na tela a partir do seu canto superior esquerdo',
                        returnType: 'VAZIO',
                        parameters: [
                            { parameterType: 'REAL', name: 'x', description: 'A posição (distância) do círculo no eixo horizontal, em relação ao lado esquerdo da janela' },
                            { parameterType: 'REAL', name: 'y', description: 'A posição (distância) do círculo no eixo vertical, em relação ao topo da janela' },
                            { parameterType: 'REAL', name: 'largura', description: 'A largura da elipse em pixels' },
                            { parameterType: 'REAL', name: 'altura', description: 'A altura da elipse em pixels'},
                            { parameterType: 'LOGICO', name: 'preencher', description: 'Define se a elipse será preenchida com a cor do ambiente gráfico. Se o valor for *verdadeiro*, a elipse será preenchida. Se o valor for *falso*, somente o contorno da elipse será desenhado' },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const x: number = args[0].value;
                            const y: number = args[1].value;
                            const largura: number = args[2].value;
                            const altura: number = args[3].value;
                            const preencher: boolean = args[4].value;

                            await graphicInterface.drawEllipse(x, y, largura, altura, preencher);
    
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            desenhar_linha: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Desenha uma linha de um ponto "A" (definido pelos parâmetros x1 e y1) até um ponto "B" (definido pelos parâmetros x2 e y2)',
                        returnType: 'VAZIO',
                        parameters: [
                            { parameterType: 'REAL', name: 'x1', description: 'A coordenada (distância) do ponto "A" no eixo horizontal, em relação ao lado esquerdo da janela' },
                            { parameterType: 'REAL', name: 'y1', description: 'A coordenada (distância) do ponto "A" no eixo vertical, em relação ao topo da janela' },
                            { parameterType: 'REAL', name: 'x2', description: 'A coordenada (distância) do ponto "B" no eixo horizontal, em relação ao lado esquerdo da janela' },
                            { parameterType: 'REAL', name: 'y2', description: 'A coordenada (distância) do ponto "B" no eixo vertical, em relação ao topo da janela' },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const x1: number = args[0].value;
                            const y1: number = args[1].value;
                            const x2: number = args[2].value;
                            const y2: number = args[3].value;
    
                            await graphicInterface.drawLine(x1, y1, x2, y2);
    
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            desenhar_ponto: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Desenha um ponto na posição definida pelos parâmetros x e y. O ponto desenhado ocupa um único pixel na tela',
                        returnType: 'VAZIO',
                        parameters: [
                            { parameterType: 'REAL', name: 'x', description: 'A coordenada (distância) do ponto no eixo horizontal, em relação ao lado esquerdo da janela' },
                            { parameterType: 'REAL', name: 'y', description: 'A coordenada (distância) do ponto no eixo vertical, em relação ao topo da janela' },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const x: number = args[0].value;
                            const y: number = args[1].value;

                            await graphicInterface.drawPoint(x, y);
    
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            desenhar_retangulo: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Desenha um retângulo na posição definida pelos parâmetros x e y e com as dimensões especificadas pelos parâmetros largura e altura. O retângulo é desenhado na tela a partir do seu canto superior esquerdo',
                        returnType: 'VAZIO',
                        parameters: [
                            { parameterType: 'REAL', name: 'x', description: 'A posição (distância) do retângulo no eixo horizontal, em relação ao lado esquerdo da janela' },
                            { parameterType: 'REAL', name: 'y', description: 'A posição (distância) do retângulo no eixo vertical, em relação ao topo da janela' },
                            { parameterType: 'REAL', name: 'largura', description: 'A largura do retângulo em pixels' },
                            { parameterType: 'REAL', name: 'altura', description: 'A altura do retângulo em pixels' },
                            { parameterType: 'LOGICO', name: 'arredondar_cantos', description: 'Define se o retângulo deverá ter cantos arredondados' },
                            { parameterType: 'LOGICO', name: 'preencher', description: 'Define se o retângulo será preenchido com a cor do ambiente gráfico. Se o valor for *verdadeiro*, o retângulo será preenchido. Se o valor for *falso*, somente o contorno do retângulo será desenhado' }
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const x: number = args[0].value;
                            const y: number = args[1].value;
                            const largura: number = args[2].value;
                            const altura: number = args[3].value;
                            const arredondarCantos: boolean = args[4].value;
                            const preencher: boolean = args[5].value;
                            
                            await graphicInterface.drawRectangle(x, y, largura, altura, arredondarCantos, preencher);
    
                            return { resultType: 'VAZIO', value: undefined };
                        }
                    }
                ],
            },
            desenhar_texto: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Desenha um texto (*cadeia*) na posição especificada pelos parâmetros x e y',
                        returnType: 'VAZIO',
                        parameters: [
                            { parameterType: 'INTEIRO', name: 'x', description: 'A posição (distância) do texto no eixo horizontal, em relação ao lado esquerdo da janela' },
                            { parameterType: 'INTEIRO', name: 'y', description: 'A posição (distância) do ponto no eixo vertical, em relação ao topo da janela' },
                            { parameterType: 'CADEIA', name: 'texto', description: 'O texto (*cadeia*) a ser desenhado' },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const x: number = args[0].value;
                            const y: number = args[1].value;
                            const texto: string = args[2].value;

                            await graphicInterface.drawText(x, y, texto);
    
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            largura_texto: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém a largura em pixels que um texto ocupa para ser desenhado na tela',
                        returnType: 'INTEIRO',
                        returnDescription: 'a largura do texto',
                        parameters: [{ parameterType: 'CADEIA', name: 'texto', description: 'o texto que será mensurado' }],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const texto: string = args[0].value;
    
                            return { resultType: 'INTEIRO', value: Math.floor(await graphicInterface.textWidth(texto)) };
                        }
                    }
                ],
            },
            altura_texto: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém a altura em pixels que um texto ocupa para ser desenhado na tela',
                        returnType: 'INTEIRO',
                        returnDescription: 'a altura do texto',
                        parameters: [{ parameterType: 'CADEIA', name: 'texto', description: 'o texto que será mensurado' }],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const texto: string = args[0].value;
    
                            return { resultType: 'INTEIRO', value: Math.floor(await graphicInterface.textHeight(texto)) };
                        }
                    }
                ],
            },
            limpar: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Limpa o desenho do ambiente e gráfico e preenche o fundo com a cor atual',
                        returnType: 'VAZIO',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            await graphicInterface.clear();
    
                            return { resultType: 'VAZIO', value: undefined };
                        }
                    }
                ],
            },
            renderizar: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Quando uma função de desenho da biblioteca é chamada, o desenho não é realizado imediatamente na tela, mas sim, em uma área reservada da memória. Isto é feito com o objetivo de aumentar o desempenho do programa e minimizar outros problemas. Esta técnica é chamada de Back Buffer ou Double Buffer. A função renderizar, faz com que os desenhos existentes no Back Buffer sejam desenhados na tela. Esta função deve ser chamada sempre após todas as outras funções de desenho, para garantir que todos os desenhos sejam exibidos',
                        returnType: 'VAZIO',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            await graphicInterface.render();
    
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            definir_titulo_janela: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Define o texto da janela do ambiente gráfico',
                        returnType: 'VAZIO',
                        parameters: [{ parameterType: 'CADEIA', name: 'titulo' }],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const titulo: string = args[0].value;
                            
                            await graphicInterface.setWindowTitle(titulo);
    
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            carregar_imagem: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Carrega uma imagem na memória para ser utilizada mais tarde. Os formatos de imagem suportados são: JPEG, PNG, BITMAP e GIF',
                        returnType: 'INTEIRO',
                        returnDescription: 'o endereço de memória no qual a imagem foi carregada',
                        parameters: [
                            { 
                                parameterType: 'CADEIA', 
                                name: 'caminho', 
                                description: 'o caminho do arquivo de imagem na IDE',
                            }
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const caminho: string = args[0].value;

                            const address = await graphicInterface.loadImage(caminho);

                            return { resultType: 'INTEIRO', value: address };
                        },
                    },
                ],
            },
            desenhar_imagem: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Desenha uma imagem previamente carregada, na posição especificada pelos parâmetros *x* e *y*',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                parameterType: 'INTEIRO',
                                name: 'x',
                                description: 'a posição (distância) da imagem no eixo horizontal, em relação ao lado esquerdo da janela',
                            },
                            {
                                parameterType: 'INTEIRO',
                                name: 'y',
                                description: 'a posição (distância) da imagem no eixo vertical, em relação ao topo da janela',
                            },
                            {
                                parameterType: 'INTEIRO',
                                name: 'endereco',
                                description: 'o endereço de memória da imagem a ser desenhada',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const x: number = args[0].value;
                            const y: number = args[1].value;
                            const endereco: number = args[2].value;

                            await graphicInterface.drawImage(x, y, endereco);

                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            liberar_imagem: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Libera a memória utilizada por uma imagem que tenha sido previamente carregada',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                parameterType: 'INTEIRO',
                                name: 'endereco',
                                description: 'o endereço de memória da imagem',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const endereco: number = args[0].value;

                            await graphicInterface.freeImage(endereco);

                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            desenhar_porcao_imagem: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Desenha uma porção de uma imagem previamente carregada, na posição especificada pelos parâmetros *x* e *y*',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                parameterType: 'INTEIRO',
                                name: 'x',
                                description: 'a posição (distância) da imagem no eixo horizontal, em relação ao lado esquerdo da janela',
                            },
                            {
                                parameterType: 'INTEIRO',
                                name: 'y',
                                description: 'a posição (distância) da imagem no eixo vertical, em relação ao topo da janela',
                            },
                            {
                                parameterType: 'INTEIRO',
                                name: 'xi',
                                description: 'a posição (distância) no eixo horizontal a partir da qual a imagem começará a ser desenhada',
                            },
                            {
                                parameterType: 'INTEIRO',
                                name: 'yi',
                                description: 'a posição (distância) no eixo vertical a partir da qual a imagem começará a ser desenhad',
                            },
                            {
                                parameterType: 'INTEIRO',
                                name: 'largura',
                                description: 'a largura da porção da imagem a ser desenhada',
                            },
                            {
                                parameterType: 'INTEIRO',
                                name: 'altura',
                                description: 'a altura da porção da imagem a ser desenhada',
                            },
                            {
                                parameterType: 'INTEIRO',
                                name: 'endereco',
                                description: 'o endereço de memória da imagem a ser desenhada',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const x: number = args[0].value;
                            const y: number = args[1].value;
                            const xi: number = args[2].value;
                            const yi: number = args[3].value;
                            const largura: number = args[4].value;
                            const altura: number = args[5].value;
                            const endereco: number = args[6].value;

                            await graphicInterface.drawImageSlice(x, y, xi, yi, largura, altura, endereco);

                            return { resultType: 'VAZIO', value: undefined };
                        }
                    },
                ],
            },
            transformar_imagem: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: ' Esta função permite transformar uma imagem previamente carregada no ambiente gráfico com a função carregar_imagem(). As transformações possíveis*são: espelhamento, rotação e remoção de cor.O espelhamento permite inverter a imagem tanto na direção horizontal quanto na direção vertical.A rotação, permite girar e inclinar a imagem em um ângulo de 360 graus.A remoção de cor, permite escolher uma cor da imagem e torná-la transparente.Esta função cria uma cópia da imagem original antes de aplicar as transformações, portanto, a imagem original não é perdida ao realizar a transformação e a novaimagem é alocada em outro endereço de memória',
                        returnType: 'INTEIRO',
                        returnDescription: 'o endereço de memória da nova imagem',
                        parameters: [
                            {
                                parameterType: 'INTEIRO',
                                name: 'endereco',
                                description: 'o endereço de memória da imagem que será transformada',
                            },
                            {
                                parameterType: 'LOGICO',
                                name: 'espelhamento_horizontal',
                                description: '*define se a imagem será invertida (espelhada) na direção horizontal',
                            },
                            {
                                parameterType: 'LOGICO',
                                name: 'espelhamento_vertical',
                                description: 'define se a imagem será invertida (espelhada) na direção horizontal',
                            },
                            {
                                parameterType: 'INTEIRO',
                                name: 'rotacao',
                                description: 'define em quantos graus a imagem será rotacionada. Se o valor 0 for informado, a imagem não será rotacionada. É importante notar que, ao rotacionar*a imagem, as suas dimensões (largura e altura) poderão se alterar',
                            },
                            {
                                parameterType: 'INTEIRO',
                                name: 'cor_transparente',
                                description: '*define a cor que será removida da imagem, ou seja, que irá se tornar transparente. Se o valor 0 for informado, nenhuma cor será removida',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const endereco: number = args[0].value;
                            const espelhamentoHorizontal: boolean = args[1].value;
                            const espelhamentoVertical: boolean = args[2].value;
                            const rotacao: number = args[3].value;
                            const corTransparente: number = args[4].value;

                            const address = await graphicInterface.transformImage(
                                endereco,
                                espelhamentoHorizontal,
                                espelhamentoVertical,
                                rotacao,
                                corTransparente,
                            );

                            return { resultType: 'INTEIRO', value: address };
                        },
                    },
                ],
            },
            definir_opacidade: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Esta função define o nível de opacidade dos desenhos no ambiente gráfico. Quanto menor for a opacidade, mais transparente será o desenho e quanto*maior for a opacidade mais opaco será o desenho. Com esta função, é possível desenhar imagens, textos e primitivas gráficas semi-transparentes, o que permite*"enxergar" através dos desenhos.É importante notar que, após ser chamada, esta função afeta todos os desenhos realizados. Isto significa que se foram desenhados um retângulo e uma elipse após achamada desta função, ambos terão seu nível de opacidade alterados.Caso fosse desejável modificar apenas a opacidade do retângulo, então seria necessário chamar novamente esta função definido a opacidade para o valor máximoantes de desenhar a elipse',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                parameterType: 'INTEIRO',
                                name: 'opacidade',
                                description: 'o nível de opacidade dos desenhos. O valor deve estar entre 0 e 255, sendo que, 0 indica um desenho totalmente transparente e 255 indica um*desenho totalmente opaco',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const opacidade: number = args[0].value;

                            await graphicInterface.setOpacity(opacidade);

                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            altura_imagem: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém a altura de uma imagem previamente carregada no ambiente gráfico',
                        returnType: 'INTEIRO',
                        returnDescription: 'a altura da imagem',
                        parameters: [
                            {
                                parameterType: 'INTEIRO',
                                name: 'endereco',
                                description: 'o endereço da imagem para a qual se quer obter a altura',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const endereco: number = args[0].value;

                            const height = await graphicInterface.imageHeight(endereco);

                            return { resultType: 'INTEIRO', value: height };
                        },
                    },
                ],
            },
            largura_imagem: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém a largura de uma imagem previamente carregada no ambiente gráfico',
                        returnType: 'INTEIRO',
                        returnDescription: 'a largura da imagem',
                        parameters: [
                            {
                                parameterType: 'INTEIRO',
                                name: 'endereco',
                                description: 'o endereço da imagem para a qual se quer obter a largura',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const endereco: number = args[0].value;

                            const width = await graphicInterface.imageWidth(endereco);

                            return { resultType: 'INTEIRO', value: width };
                        },
                    },
                ],
            },
        },
    };

    return {
        lib,
        start: async (gi) => {
            startedGraphicInterface = gi;
            await gi._controlStart();
        },
        stop: () => graphicInterface._controlStop(),
    };
}