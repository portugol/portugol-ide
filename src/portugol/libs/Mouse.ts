import { ExpressionResult, LibrarySymbol } from '../context';
import { MouseInterface } from '../interpreter/interfaces';

export function createLib(): { 
    lib: LibrarySymbol, 
    start: (mouseInterface: MouseInterface<'remote'>) => Promise<void>,
    stop: () => Promise<void>,
} {
    let startedMouseInterface: MouseInterface<'remote'> | undefined;
    const mouseInterface = new Proxy<MouseInterface<'remote'>>({} as any, {
        get: (target, property) => {
            if (!startedMouseInterface) {
                throw new Error('TENTATIVA DE CHAMADA À BIBLIOTECA NÃO INICIADA. (Mouse)');
            }
            return startedMouseInterface[property];
        },
    });

    const lib: LibrarySymbol = {
        type: 'LIBRARY_SYMBOL',
        version: '1.1',
        description: 'Esta biblioteca contém um conjunto de funções para manipular a entrada de dados através do mouse do computador *IMPORTANTE:* Esta biblioteca não*funciona no console de entrada e saída de dados do Portugol Studio, ela só funciona com a biblioteca Graficos, se o modo gráfico estiver iniciado.',
        symbols: {
            BOTAO_DIREITO: {
                type: 'VALUE_SYMBOL',
                description: 'Código numérico do botão direito do mouse',
                isConstant: true,
                valueType: 'INTEIRO',
                value: 1,
            },
            BOTAO_ESQUERDO: {
                type: 'VALUE_SYMBOL',
                description: 'Código numérico do botão esquerdo do mouse',
                isConstant: true,
                valueType: 'INTEIRO',
                value: 0,
            },
            BOTAO_MEIO: {
                type: 'VALUE_SYMBOL',
                description: 'Código numérico do botão do meio do mouse',
                isConstant: true,
                valueType: 'INTEIRO',
                value: 2,
            },
            algum_botao_pressionado: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Testa se existe algum botão do mouse pressionado neste instante',
                        returnType: 'LOGICO',
                        returnDescription: 'o resultado do teste. *Verdadeiro* se houver um botão do mouse pressionado no momento do teste. Caso contrário, retorna *falso*',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            return {
                                resultType: 'LOGICO',
                                value: await mouseInterface.anyButtonDown(),
                            };
                        },
                    },
                ],
            },
            botao_pressionado: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Testa se um determinado *botão* do mouse está pressionado neste instante',
                        returnType: 'LOGICO',
                        returnDescription: 'o resultado do teste. *Verdadeiro* se o *botão* estiver pressionado no momento do teste. Caso contrário, retorna *falso*',
                        parameters: [
                            {
                                description: 'o código do botão que será testado',
                                parameterType: 'INTEIRO',
                                name: 'botao',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const botao: number = args[0].value;
                            return {
                                resultType: 'LOGICO',
                                value: await mouseInterface.isButtonDown(botao),
                            };
                        },
                    }
                ],
            },
            exibir_cursor: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Exibe novamente o cursor do mouse caso ele esteja oculto',
                        returnType: 'VAZIO',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            await mouseInterface.showCursor();
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            ocultar_cursor: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Oculta o cursor do mouse',
                        returnType: 'VAZIO',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            await mouseInterface.hideCursor();
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            ler_botao: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Aguarda até que um botão seja clicado (isto é, foi pressionado e depois solto), e captura o seu código',
                        returnType: 'INTEIRO',
                        returnDescription: 'o código do botão lido',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            return {
                                resultType: 'INTEIRO',
                                value: await mouseInterface.readButton(),
                            };
                        },
                    }
                ],
            },
            posicao_x: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém a coordenada X do mouse neste instante',
                        returnType: 'INTEIRO',
                        returnDescription: 'a coordenada X do mouse neste instante',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            return {
                                resultType: 'INTEIRO',
                                value: await mouseInterface.getXPosition(),
                            };
                        },
                    }
                ],
            },
            posicao_y: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém a coordenada Y do mouse neste instante',
                        returnType: 'INTEIRO',
                        returnDescription: 'a coordenada Y do mouse neste instante',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            return {
                                resultType: 'INTEIRO',
                                value: await mouseInterface.getYPosition(),
                            };
                        },
                    }
                ],
            },
        },
    };

    return {
        lib,
        start: async (mi) => {
            startedMouseInterface = mi;
            await mi._controlStart();
        },
        stop: () => mouseInterface._controlStop(),
    };
}