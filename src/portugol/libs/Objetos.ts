import * as convert from 'xml-js';
import { ExpressionResult, FunctionSymbol, LibrarySymbol, ValueSymbolUnionValue } from '../context';

export function createLib(): {
    lib: LibrarySymbol,
    start: () => Promise<void>,
    stop: () => Promise<void>,
} {
    enum TYPES {
        VOID = 0,
        STRING = 2,
        CHARACTER = 3,
        INTEGER = 1,
        BOOLEAN = 5,
        OBJECT = 6,
        FLOAT = 4,
        VECTOR = 7,
    }

    type JSONValue
        = { type: TYPES.VOID, value: void }
        | { type: TYPES.STRING, value: string }
        | { type: TYPES.CHARACTER, value: string }
        | { type: TYPES.INTEGER, value: number }
        | { type: TYPES.BOOLEAN, value: boolean }
        | { type: TYPES.OBJECT, value: number }
        | { type: TYPES.FLOAT, value: number }
        | { type: TYPES.VECTOR, value: JSONValue[] };

    type JSONObject = { [attribute: string]: JSONValue };

    const objects: Array<JSONObject | null> = [];

    function libTypeFROMJSType(jsValue: any): TYPES {
        switch (typeof jsValue) {
            case 'bigint':
            case 'number':
                return TYPES.FLOAT;
            case 'boolean':
                return TYPES.BOOLEAN;
            case 'object':
                return TYPES.OBJECT;
            case 'string':
                return TYPES.STRING;
        }

        return TYPES.VOID;
    }

    function typeValue(value: any): JSONValue {
        const type = libTypeFROMJSType(value);
        if (type === TYPES.OBJECT) {
            const object = {};
            for (const key of Object.keys(value)) {
                object[key] = typeValue(value[key]);
            }
            objects.push(object);
            value = objects.length - 1;
        } else if (type === TYPES.VECTOR) {
            value = value.map(typeValue);
        }

        return { value, type } as JSONValue;
    }

    function untypeValue(value: JSONValue): any {
        let untypedValue: typeof value.value | {} = value.value;
        if (value.type === TYPES.OBJECT) {
            const object = {};
            for (const key of Object.keys(value)) {
                object[key] = untypeValue(untypedValue[key]);
            }
            untypedValue = object;
        } else if (value.type === TYPES.VECTOR) {
            untypedValue = (untypedValue as []).map(untypeValue);
        }
        return untypedValue;
    }

    function defaultLibValue(type: TYPES): any {
        switch (type) {
            case TYPES.BOOLEAN: return false;
            case TYPES.CHARACTER: return '0';
            case TYPES.FLOAT: return 0.0;
            case TYPES.INTEGER: return 0;
            case TYPES.OBJECT: return -1;
            case TYPES.STRING: return '';
            case TYPES.VECTOR: return [];
            case TYPES.VOID: return undefined;
        }
    }

    function readObjectProperty(
        objectAddress: number,
        propertyName: string,
        type: TYPES,
        index?: number,
    ): any {
        const object = objects[objectAddress];
        let value = defaultLibValue(type);

        if (object) {
            let property = object[propertyName];

            if (property !== undefined) {
                if (index !== undefined && property.type === TYPES.VECTOR) {
                    property  = property.value[index];
                    index = undefined;
                }

                if (index === undefined && property.type === type) {
                    value = property.value;
                }
            }
        }

        return value;
    }

    const lib: LibrarySymbol = {
        type: 'LIBRARY_SYMBOL',
        version: '1.0',
        description: 'Esta biblioteca contém uma série de funções para criar e trabalhar com objetos',
        symbols: {
            TIPO_VAZIO: {
                type: 'VALUE_SYMBOL',
                description: 'Constante para definir o tipo vazio',
                valueType: 'INTEIRO',
                value: TYPES.VOID,
                isConstant: true,
            },
            TIPO_CADEIA: {
                type: 'VALUE_SYMBOL',
                description: 'Constante para definir o tipo cadeia',
                valueType: 'INTEIRO',
                value: TYPES.STRING,
                isConstant: true,
            },
            TIPO_CARACTER: {
                type: 'VALUE_SYMBOL',
                description: 'Constante para definir o tipo caracter',
                valueType: 'INTEIRO',
                value: TYPES.CHARACTER,
                isConstant: true,
            },
            TIPO_INTEIRO: {
                type: 'VALUE_SYMBOL',
                description: 'Constante para definir o tipo inteiro',
                valueType: 'INTEIRO',
                value: TYPES.INTEGER,
                isConstant: true,
            },
            TIPO_LOGICO: {
                type: 'VALUE_SYMBOL',
                description: 'Constante para definir o tipo logico',
                valueType: 'INTEIRO',
                value: TYPES.BOOLEAN,
                isConstant: true,
            },
            TIPO_OBJETO: {
                type: 'VALUE_SYMBOL',
                description: 'Constante para definir o tipo objeto',
                valueType: 'INTEIRO',
                value: TYPES.OBJECT,
                isConstant: true,
            },
            TIPO_REAL: {
                type: 'VALUE_SYMBOL',
                description: 'Constante para definir o tipo real',
                valueType: 'INTEIRO',
                value: TYPES.FLOAT,
                isConstant: true,
            },
            TIPO_VETOR: {
                type: 'VALUE_SYMBOL',
                description: 'Constante para definir o tipo vetor',
                valueType: 'INTEIRO',
                value: TYPES.VECTOR,
                isConstant: true,
            },
            atribuir_propriedade: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Realiza a atribuição de uma propriedade do objeto no endereço informado',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                description: 'o endereço onde o objeto foi armazenado',
                                name: 'endereco',
                                parameterType: 'INTEIRO',
                            },
                            {
                                description: 'a descrição da propriedade que terá o valor alterado',
                                name: 'propriedade',
                                parameterType: 'CADEIA',
                            },
                            {
                                description: 'o valor que será atribuido para a propriedade',
                                name: 'valor',
                                parameterType: {
                                    type: 'UNION', valueTypes: [
                                        'CADEIA', 'CARACTER', 'INTEIRO', 'LOGICO', 'REAL',
                                    ]
                                },
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const endereco: number = args[0].value;
                            const propriedade: string = args[1].value;
                            const valor: ValueSymbolUnionValue = args[2].value;

                            const object = objects[endereco];

                            if (object) {
                                let type = TYPES.VOID;

                                switch (valor.originalType) {
                                    case 'CADEIA':
                                        type = TYPES.STRING;
                                        break;
                                    case 'CARACTER':
                                        type = TYPES.CHARACTER;
                                        break;
                                    case 'INTEIRO':
                                        type = TYPES.INTEGER;
                                        break;
                                    case 'LOGICO':
                                        type = TYPES.BOOLEAN;
                                        break;
                                    case 'REAL':
                                        type = TYPES.FLOAT;
                                        break;
                                    case 'VAZIO':
                                        type = TYPES.VOID;
                                        break;
                                }

                                object[propriedade] = { type, value: valor.value } as JSONValue;
                            }

                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            contem_propriedade: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Identifica se o objeto contém ou não a propriedade informada',
                        returnDescription: 'Retorna *verdadeiro* se o objeto conter a propridade, senão retorna *falso*',
                        returnType: 'LOGICO',
                        parameters: [
                            {
                                description: 'o endereço onde o objeto foi armazenado',
                                name: 'endereco',
                                parameterType: 'INTEIRO',
                            },
                            {
                                description: '*propriedade utilizada para verificaçã',
                                name: 'propriedade',
                                parameterType: 'CADEIA',
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const endereco: number = args[0].value;
                            const propriedade: string = args[1].value;

                            const object = objects[endereco];
                            let containsProperty = false;

                            if (object) {
                                containsProperty = Object.keys(object).includes(propriedade);
                            }

                            return { resultType: 'LOGICO', value: containsProperty };
                        },
                    },
                ],
            },
            criar_objeto: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Realiza a criação de um objeto vazio em memória',
                        returnDescription: 'O endereço de memória no qual o objeto foi carregada',
                        returnType: 'INTEIRO',
                        parameters: [],
                        instructions: (): ExpressionResult => {
                            objects.push({});
                            return { resultType: 'INTEIRO', value: objects.length - 1 };
                        },
                    },
                ],
            },
            criar_objeto_via_json: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Realiza a criação de um objeto a partir de uma cadeia no formato JSON (JavaScript Object Notation)',
                        returnDescription: 'Caso o JSON seja um objeto válido, o endereço de memória no qual o objeto foi carregado é retornado, senão é retornado -1.',
                        returnType: 'INTEIRO',
                        parameters: [
                            {
                                description: 'texto no formato JSON para criar o objeto',
                                name: 'json',
                                parameterType: 'CADEIA',
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const json: string = args[0].value;

                            let address = -1;

                            try {
                                const object = JSON.parse(json);
                                if (typeof object === 'object') {
                                    address = typeValue(object).value as number;
                                }
                            } catch (error) { /* ignore the error */ }

                            return { resultType: 'INTEIRO', value: address };
                        },
                    },
                ],
            },
            criar_objeto_via_xml: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Realiza a criação de um objeto a partir de uma cadeia no formato XML (eXtensible Markup Language)',
                        returnDescription: 'Caso seja um xml válido, o endereço do objeto é retornado, senão -1 é retornado.',
                        returnType: 'INTEIRO',
                        parameters: [
                            {
                                description: 'texto no formato XML para criar o objeto',
                                name: 'xml',
                                parameterType: 'CADEIA',
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const xml: string = args[0].value;

                            function setUnderscoreTextAsValue(object: {}, key: string) {
                                const child: { _text: string } = object[key];

                                if (child._text) {
                                    object[key] = child._text;
                                    return;
                                }

                                for (const childKey of Object.keys(child)) {
                                    if (typeof child[childKey] === 'object') {
                                        setUnderscoreTextAsValue(child, childKey);
                                    }
                                }
                            }

                            let address = -1;

                            try {
                                const js = convert.xml2js(xml, { 
                                    ignoreAttributes: true,
                                    compact: true,
                                    ignoreDeclaration: true,
                                    ignoreDoctype: true,
                                    ignoreCdata: true,
                                    ignoreInstruction: true,
                                });
                                const rootKey = Object.keys(js)[0];
                                setUnderscoreTextAsValue(js, rootKey);
                                const json = JSON.stringify(js[rootKey]);
                                const loadObjectFromJSON = (lib.symbols.criar_objeto_via_json as FunctionSymbol)
                                    .overloads[0].instructions as (args: ExpressionResult[]) => ExpressionResult;
                                address = loadObjectFromJSON([{ resultType: 'CADEIA', value: json }]).value;
                            } catch (error) { /* ignore the error */ }

                            return { resultType: 'INTEIRO', value: address };
                        },
                    },  
                ],
            },
            liberar: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Libera todos os objetos previamente armazenados na memória',
                        returnType: 'VAZIO',
                        parameters: [],
                        instructions: (): ExpressionResult => {
                            objects.length = 0;
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            liberar_objeto: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Libera o objeto do endereço informado da memória',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                description: 'o endereço onde o objeto foi armazenado',
                                name: 'endereco',
                                parameterType: 'INTEIRO',
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const endereco: number = args[0].value;

                            if (endereco >= 0 && endereco < objects.length) {
                                objects[endereco] = null;
                            }

                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            obter_json: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém a cadeia que representa o objeto inteiro no formato JSON (JavaScript Object Notation)',
                        returnDescription: 'Representação JSON do objeto',
                        returnType: 'CADEIA',
                        parameters: [
                            {
                                description: 'o endereço onde o objeto foi armazenado',
                                name: 'endereco',
                                parameterType: 'INTEIRO',
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const endereco: number = args[0].value;

                            let json = '';

                            if (objects[endereco]) {
                                json = JSON.stringify(untypeValue({ 
                                    value: endereco,
                                    type: TYPES.OBJECT,
                                }));
                            }

                            return { resultType: 'CADEIA', value: json };
                        },
                    },
                ],
            },
            obter_propriedade_tipo_cadeia: { 
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém o valor de uma propriedade do tipo cadeia no objeto do endereço informado',
                        returnDescription: 'caso a propriedade exista, o valor da propriedade informada, senão o valor padrão',
                        returnType: 'CADEIA',
                        parameters: [
                            {
                                description: 'o endereço onde o objeto foi armazenado',
                                name: 'endereco',
                                parameterType: 'INTEIRO',
                            },
                            {
                                description: 'a descrição da propriedade que terá o valor obtido',
                                name: 'propriedade',
                                parameterType: 'CADEIA',
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const endereco: number = args[0].value;
                            const propriedade: string = args[1].value;

                            const value = readObjectProperty(endereco, propriedade, TYPES.STRING);

                            return { resultType: 'CADEIA', value };
                        },
                    },
                ],
            },
            obter_propriedade_tipo_cadeia_em_vetor: { 
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém o valor de um vetor armazenado no objeto do endereço informado',
                        returnDescription: 'caso a propriedade exista, o valor da propriedade informada, senão o valor padrão',
                        returnType: 'CADEIA',
                        parameters: [
                            {
                                description: 'o endereço onde o objeto foi armazenado',
                                name: 'endereco',
                                parameterType: 'INTEIRO',
                            },
                            {
                                description: 'a descrição da propriedade que terá o valor obtido',
                                name: 'propriedade',
                                parameterType: 'CADEIA',
                            },
                            {
                                description: 'o índice do elemento do vetor que será obtido',
                                name: 'indice',
                                parameterType: 'INTEIRO',
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const endereco: number = args[0].value;
                            const propriedade: string = args[1].value;
                            const indice: number = args[2].value;

                            const value = readObjectProperty(endereco, propriedade, TYPES.STRING, indice);

                            return { resultType: 'CADEIA', value };
                        },
                    },
                ],
            },
            obter_propriedade_tipo_caracter: { 
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém o valor de uma propriedade do tipo caracter no objeto do endereço informado',
                        returnDescription: 'caso a propriedade exista, o valor da propriedade informada, senão o valor padrão',
                        returnType: 'CARACTER',
                        parameters: [
                            {
                                description: 'o endereço onde o objeto foi armazenado',
                                name: 'endereco',
                                parameterType: 'INTEIRO',
                            },
                            {
                                description: 'a descrição da propriedade que terá o valor obtido',
                                name: 'propriedade',
                                parameterType: 'CADEIA',
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const endereco: number = args[0].value;
                            const propriedade: string = args[1].value;

                            const value = readObjectProperty(endereco, propriedade, TYPES.CHARACTER);

                            return { resultType: 'CARACTER', value };
                        },
                    },
                ],
            },
            obter_propriedade_tipo_caracter_em_vetor: { 
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém o valor de um vetor armazenado no objeto do endereço informado',
                        returnDescription: 'caso a propriedade exista, o valor da propriedade informada, senão o valor padrão',
                        returnType: 'CARACTER',
                        parameters: [
                            {
                                description: 'o endereço onde o objeto foi armazenado',
                                name: 'endereco',
                                parameterType: 'INTEIRO',
                            },
                            {
                                description: 'a descrição da propriedade que terá o valor obtido',
                                name: 'propriedade',
                                parameterType: 'CADEIA',
                            },
                            {
                                description: 'o índice do elemento do vetor que será obtido',
                                name: 'indice',
                                parameterType: 'INTEIRO',
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const endereco: number = args[0].value;
                            const propriedade: string = args[1].value;
                            const indice: number = args[2].value;

                            const value = readObjectProperty(endereco, propriedade, TYPES.CHARACTER, indice);

                            return { resultType: 'CARACTER', value };
                        },
                    },
                ],
            },
            obter_propriedade_tipo_inteiro: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém o valor de uma propriedade do tipo inteiro no objeto do endereço informado',
                        returnDescription: 'caso a propriedade exista, o valor da propriedade informada, senão o valor padrão',
                        returnType: 'INTEIRO',
                        parameters: [
                            {
                                description: 'o endereço onde o objeto foi armazenado',
                                name: 'endereco',
                                parameterType: 'INTEIRO',
                            },
                            {
                                description: 'a descrição da propriedade que terá o valor obtido',
                                name: 'propriedade',
                                parameterType: 'CADEIA',
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const endereco: number = args[0].value;
                            const propriedade: string = args[1].value;

                            const value = readObjectProperty(endereco, propriedade, TYPES.INTEGER);

                            return { resultType: 'INTEIRO', value };
                        },
                    },
                ],
            },
            obter_propriedade_tipo_inteiro_em_vetor: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém o valor de um vetor armazenado no objeto do endereço informado',
                        returnDescription: 'caso a propriedade exista, o valor da propriedade informada, senão o valor padrão',
                        returnType: 'INTEIRO',
                        parameters: [
                            {
                                description: 'o endereço onde o objeto foi armazenado',
                                name: 'endereco',
                                parameterType: 'INTEIRO',
                            },
                            {
                                description: 'a descrição da propriedade que terá o valor obtido',
                                name: 'propriedade',
                                parameterType: 'CADEIA',
                            },
                            {
                                description: 'o índice do elemento do vetor que será obtido',
                                name: 'indice',
                                parameterType: 'INTEIRO',
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const endereco: number = args[0].value;
                            const propriedade: string = args[1].value;
                            const indice: number = args[2].value;

                            const value = readObjectProperty(endereco, propriedade, TYPES.INTEGER, indice);

                            return { resultType: 'INTEIRO', value };
                        },
                    },
                ],
            },
            obter_propriedade_tipo_logico: { 
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém o valor de uma propriedade do tipo logico no objeto do endereço informado',
                        returnDescription: 'caso a propriedade exista, o valor da propriedade informada, senão o valor padrão',
                        returnType: 'LOGICO',
                        parameters: [
                            {
                                description: 'o endereço onde o objeto foi armazenado',
                                name: 'endereco',
                                parameterType: 'INTEIRO',
                            },
                            {
                                description: 'a descrição da propriedade que terá o valor obtido',
                                name: 'propriedade',
                                parameterType: 'CADEIA',
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const endereco: number = args[0].value;
                            const propriedade: string = args[1].value;

                            const value = readObjectProperty(endereco, propriedade, TYPES.BOOLEAN);

                            return { resultType: 'LOGICO', value };
                        },
                    },
                ],
            },
            obter_propriedade_tipo_logico_em_vetor: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém o valor de um vetor armazenado no objeto do endereço informado',
                        returnDescription: 'caso a propriedade exista, o valor da propriedade informada, senão o valor padrão',
                        returnType: 'LOGICO',
                        parameters: [
                            {
                                description: 'o endereço onde o objeto foi armazenado',
                                name: 'endereco',
                                parameterType: 'INTEIRO',
                            },
                            {
                                description: 'a descrição da propriedade que terá o valor obtido',
                                name: 'propriedade',
                                parameterType: 'CADEIA',
                            },
                            {
                                description: 'o índice do elemento do vetor que será obtido',
                                name: 'indice',
                                parameterType: 'INTEIRO',
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const endereco: number = args[0].value;
                            const propriedade: string = args[1].value;
                            const indice: number = args[2].value;

                            const value = readObjectProperty(endereco, propriedade, TYPES.BOOLEAN, indice);

                            return { resultType: 'LOGICO', value };
                        },
                    },
                ],
            },
            obter_propriedade_tipo_objeto: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém o valor de uma propriedade do tipo objeto no objeto do endereço informado',
                        returnDescription: 'caso a propriedade exista, o valor da propriedade informada, senão o valor padrão',
                        returnType: 'INTEIRO',
                        parameters: [
                            {
                                description: 'o endereço onde o objeto foi armazenado',
                                name: 'endereco',
                                parameterType: 'INTEIRO',
                            },
                            {
                                description: 'a descrição da propriedade que terá o valor obtido',
                                name: 'propriedade',
                                parameterType: 'CADEIA',
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const endereco: number = args[0].value;
                            const propriedade: string = args[1].value;

                            const value = readObjectProperty(endereco, propriedade, TYPES.OBJECT);

                            return { resultType: 'INTEIRO', value };
                        },
                    },
                ],
            },
            obter_propriedade_tipo_objeto_em_vetor: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém o valor de um vetor armazenado no objeto do endereço informado',
                        returnDescription: 'caso a propriedade exista, o valor da propriedade informada, senão o valor padrão',
                        returnType: 'INTEIRO',
                        parameters: [
                            {
                                description: 'o endereço onde o objeto foi armazenado',
                                name: 'endereco',
                                parameterType: 'INTEIRO',
                            },
                            {
                                description: 'a descrição da propriedade que terá o valor obtido',
                                name: 'propriedade',
                                parameterType: 'CADEIA',
                            },
                            {
                                description: 'o índice do elemento do vetor que será obtido',
                                name: 'indice',
                                parameterType: 'INTEIRO',
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const endereco: number = args[0].value;
                            const propriedade: string = args[1].value;
                            const indice: number = args[2].value;

                            const value = readObjectProperty(endereco, propriedade, TYPES.OBJECT, indice);

                            return { resultType: 'INTEIRO', value };
                        },
                    },
                ],
            },
            obter_propriedade_tipo_real: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém o valor de uma propriedade do tipo real no objeto do endereço informado',
                        returnDescription: 'caso a propriedade exista, o valor da propriedade informada, senão o valor padrão',
                        returnType: 'REAL',
                        parameters: [
                            {
                                description: 'o endereço onde o objeto foi armazenado',
                                name: 'endereco',
                                parameterType: 'INTEIRO',
                            },
                            {
                                description: 'a descrição da propriedade que terá o valor obtido',
                                name: 'propriedade',
                                parameterType: 'CADEIA',
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const endereco: number = args[0].value;
                            const propriedade: string = args[1].value;

                            const value = readObjectProperty(endereco, propriedade, TYPES.FLOAT);

                            return { resultType: 'REAL', value };
                        },
                    },
                ],
            },
            obter_propriedade_tipo_real_em_vetor: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém o valor de um vetor armazenado no objeto do endereço informado',
                        returnDescription: 'caso a propriedade exista, o valor da propriedade informada, senão o valor padrão',
                        returnType: 'REAL',
                        parameters: [
                            {
                                description: 'o endereço onde o objeto foi armazenado',
                                name: 'endereco',
                                parameterType: 'INTEIRO',
                            },
                            {
                                description: 'a descrição da propriedade que terá o valor obtido',
                                name: 'propriedade',
                                parameterType: 'CADEIA',
                            },
                            {
                                description: 'o índice do elemento do vetor que será obtido',
                                name: 'indice',
                                parameterType: 'INTEIRO',
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const endereco: number = args[0].value;
                            const propriedade: string = args[1].value;
                            const indice: number = args[2].value;

                            const value = readObjectProperty(endereco, propriedade, TYPES.FLOAT, indice);

                            return { resultType: 'REAL', value };
                        },
                    },
                ],
            },
            obter_tamanho_vetor_propriedade: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém o tamanho de um vetor armazenado no objeto do endereço informado',
                        returnDescription: 'caso a propriedade seja um vetor o tamanho é retornado, senão é retornado 0',
                        returnType: 'INTEIRO',
                        parameters: [
                            {
                                description: 'o endereço onde o objeto foi armazenado',
                                name: 'endereco',
                                parameterType: 'INTEIRO',
                            },
                            {
                                description: 'a descrição da propriedade que terá o valor obtido',
                                name: 'propriedade',
                                parameterType: 'CADEIA',
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const endereco: number = args[0].value;
                            const propriedade: string = args[1].value;

                            const value = readObjectProperty(endereco, propriedade, TYPES.VECTOR);

                            return { resultType: 'INTEIRO', value: value.length };
                        },
                    },
                ],
            },
            tipo_propriedade: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Identifica o tipo da propriedade informada',
                        returnDescription: 'Retorna o tipo da propriedade do objeto informado',
                        returnType: 'INTEIRO',
                        parameters: [
                            {
                                description: 'o endereço onde o objeto foi armazenado',
                                name: 'endereco',
                                parameterType: 'INTEIRO',
                            },
                            {
                                description: 'propriedade utilizada para verificação',
                                name: 'propriedade',
                                parameterType: 'CADEIA',
                            },
                        ],
                        instructions: (args: ExpressionResult[]): ExpressionResult => {
                            const endereco: number = args[0].value;
                            const propriedade: string = args[1].value;

                            const object = objects[endereco];
                            let type = TYPES.VOID;

                            if (object) {
                                const propertyValue = object[propriedade];

                                if (propertyValue) {
                                    type = propertyValue.type;
                                }
                            }

                            return { resultType: 'INTEIRO', value: type };
                        },
                    },
                ],
            },
        },
    };
    return { lib, start: async () => undefined, stop: async () => undefined };
}
