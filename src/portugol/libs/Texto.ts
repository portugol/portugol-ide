import { LibrarySymbol } from '../context';

export function createLib(): { 
    lib: LibrarySymbol, 
    start: () => Promise<void>,
    stop: () => Promise<void>,
} {
    const lib: LibrarySymbol = {
        type: 'LIBRARY_SYMBOL',
        symbols: {
            caixa_alta: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Transforma os caracteres de uma *cadeia* em caracteres maiúsculos',
                        returnDescription: 'A *cadeia* com os caracteres transformados',
                        returnType: 'CADEIA',
                        parameters: [{ parameterType: 'CADEIA', name: 'cad', description: 'Um valor qualquer do tipo *cadeia*' }], 
                        instructions: (args) => {

                            return { resultType: 'CADEIA', value: args[0].value.toUpperCase() };
                        },
                    }
                ],
            },
            caixa_baixa: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Transforma os caracteres de uma *cadeia* em caracteres maiúsculos',
                        returnDescription: 'A *cadeia* com os caracteres transformados',
                        returnType: 'CADEIA',
                        parameters: [{ parameterType: 'CADEIA', name: 'cad', description: 'Um valor qualquer do tipo *cadeia*' }], 
                        instructions: (args) => {

                            return { resultType: 'CADEIA', value: args[0].value.toLowerCase() };
                        },
                    }
                ],
            },
            extrair_subtexto: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Extrai uma parte da cadeia delimitada pela posição inicial e final',
                        returnDescription: '',
                        returnType: 'CADEIA',
                        parameters: [
                            { parameterType: 'CADEIA', name: 'cad', description: 'A cadeia a partir da qual será extraido o subtexto' }, 
                            { parameterType: 'INTEIRO', name: 'posicaoInicial', description: 'A posição dentro da cadeia onde começará o subtexto' }, 
                            { parameterType: 'INTEIRO', name: 'posicaoFinal', description: 'A posição dentro da cadeia onde terminará o subtexto' }
                        ], 
                        instructions: (args) => {
                            const cad: string = args[0].value;
                            const posicaoInicial: number = args[1].value;
                            const posicaoFinal: number = args[2].value;

                            return { resultType: 'CADEIA', value: cad.substring(posicaoInicial, posicaoFinal) };
                        },
                    }
                ],
            },
            numero_caracteres: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Conta o número de caracteres existentes em uma *cadeia*',
                        returnDescription: 'O número de caracteres na *cadeia*',
                        returnType: 'INTEIRO',
                        parameters: [{ parameterType: 'CADEIA', name: 'cad', description:'Um valor qualquer do tipo *cadeia*' }], 
                        instructions: (args) => {

                            return { resultType: 'INTEIRO', value: args[0].value.length};
                        },
                    }
                ],
            },
            obter_caracter: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém um *caracter* da *cadeia* a partir de seu *índice*. O *índice* deve estar entre 0 e o número de caracteres da *cadeia*',
                        returnType: 'CADEIA',
                        parameters: [{ parameterType: 'CADEIA', name: 'cad', description: 'A *cadeia* da qual será obtido o caracater' }, { parameterType: 'INTEIRO', name: 'indice', description: 'o indice do caracter que se deseja obter' }], 
                        instructions: (args) => {
                            const cad: string = args[0].value;
                            const indice: number = args[1].value;

                            return { resultType: 'CADEIA', value: cad.substr(indice, 1)};
                        },
                    }
                ],
            },
            posicao_texto: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Procura por um texto dentro de uma cadeia e, caso encontrado, retorna a posição da primeira ocorrência',
                        returnDescription: 'A posição da primeira ocorrência do texto, caso ele seja encontrado. Caso o texto não seja encontrado na cadeia o valor retornado é -1',
                        returnType: 'CADEIA',
                        parameters: [
                            { parameterType: 'CADEIA', name: 'texto', description: 'O texto que será procurado na cadeia' },
                            { parameterType: 'CADEIA', name: 'cad', description: 'A cadeia dentro da qual o texto será procurado' },
                            { parameterType: 'INTEIRO', name: 'posicao_inicial', description: 'A posição inicial a partir da qual o texto será procurado. Para procurar a partir do início da cadeia deve-se informar a posição 0' },
                        ], 
                        instructions: (args) => {
                            const cad: string = args[0].value;
                            const indice: number = args[1].value;

                            return { resultType: 'CADEIA', value: cad.substr(indice, 1)};
                        },
                    }
                ],
            },
            preencher_a_esquerda: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Concatena o *caracter* informado, à esquerda da *cadeia*, até que a *cadeia* fique do *tamanho* indicado. Se o tamanho da *cadeia* for maior ou igual ao *tamanho* informado, nada é feito',
                        returnDescription: 'A *cadeia* transformada',
                        returnType: 'CADEIA',
                        parameters: [
                            { parameterType: 'CARACTER', name: 'car', description: 'O *caracter* que será concatenado á esquerda da *cadeia' },
                            { parameterType: 'INTEIRO', name: 'tamanho', description: 'O tamanho final da *cadeia*' },
                            { parameterType: 'CADEIA', name: 'cad', description: 'A *cadeia* que será transformada' },
                        ], 
                        instructions: (args) => {
                            const car: string = args[0].value;
                            const tamanho:number = args[1].value;
                            let cad: string = args[2].value;

                            for(let i = cad.length; i < tamanho; i++) {
                                cad = car + cad;
                            }            

                            return { resultType: 'CADEIA', value: cad};
                        },
                    }
                ],
            },
            substituir: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Pesquisa por um determinado texto em uma *cadeia* e substitui todas as ocorrências por um texto alternativo',
                        returnDescription: 'A *cadeia* resultante da substituição',
                        returnType: 'CADEIA',
                        parameters: [
                            { parameterType: 'CADEIA', name: 'cad', description: 'A *cadeia* que será pesquisada' },
                            { parameterType: 'CADEIA', name: 'texto_pesquisa', description: 'O texto que será pesquisado na *cadeia' },
                            { parameterType: 'CADEIA', name: 'texto_substituto', description: 'O texto pelo qual as ocorrências serão substituídas' },
                        ], 
                        instructions: (args) => {
                            let cad: string = args[0].value;
                            const textoPesquisa: string = args[1].value;
                            const textoSubstituto: string = args[2].value; 
                            let index = cad.indexOf(textoPesquisa);

                            while (index !== -1) {
                                cad = (cad.slice(0, index) + textoSubstituto + cad.slice(index + textoSubstituto.length -1));
                                index = cad.indexOf(textoPesquisa);
                            }

                            return { resultType: 'CADEIA', value: cad };
                        },
                    }
                ],
            },
        },
    };
    return { lib, start: async () => undefined, stop: async () => undefined };
}
