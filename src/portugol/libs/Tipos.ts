import { LibrarySymbol } from '../context';

export function createLib(): { 
    lib: LibrarySymbol, 
    start: () => Promise<void>,
    stop: () => Promise<void>,
} {
    const lib: LibrarySymbol = {
        type: 'LIBRARY_SYMBOL',
        symbols: {
            cadeia_e_caracter: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Verifica se a *cadeia* passada por parâmetro representa um valor do tipo *caracter*',
                        returnDescription: '*verdadeiro* caso a cadeia represente um valor do tipo *caracter*, *falso* caso contrário',
                        returnType: 'LOGICO',
                        parameters: [{ parameterType: 'CADEIA', name: 'cad', description: '*cadeia* a ser verificada' }], 
                        instructions: (args) => {
                            return { resultType: 'LOGICO', value: args[0].value.length === 1 };
                        },
                    }
                ],
            },
            cadeia_e_inteiro: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Verifica se a *cadeia* passada por parâmetro representa um valor do tipo *inteiro* escrito na notação definida pela base',
                        returnDescription: '*verdadeiro* se a *cadeia* representar um valor do tipo *inteiro*, caso contrário, retorna *falso*',
                        returnType: 'LOGICO',
                        parameters: [
                            { parameterType: 'CADEIA', name: 'cad', description: 'A *cadeia* a ser verificada' }, 
                            { parameterType: 'INTEIRO', name: 'base', description: 'A base que deverá ser assumida ao realizar a verificação, a base deve se um *inteiro* entre: 2, 10, 16' }], 
                        instructions: (args) => {
                            const cad: string = args[0].value;
                            const base: number = args[1].value;

                            if (base === 2) {
                                const re2 = /^[-]{0,1}[0-1]*$/;
                                return { resultType: 'LOGICO', value: re2.test(cad) };
                            } else if (base === 10) {
                                const re10 = /^[-]{0,1}[0-9]*$/;
                                return { resultType: 'LOGICO', value: re10.test(cad) };
                            } else if (base === 16) {
                                const re16 = /^[-]{0,1}[0-9A-Fa-f]*$/;
                                return { resultType: 'LOGICO', value: re16.test(cad) };
                            } else {
                                throw new Error('A base informada (' +  base + ') é inválida, a base deve ser um dos seguintes valores: 2, 10, 16');
                            }                 
                            
                        },
                    }
                ],
            },
            cadeia_e_logico: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Verifica se a *cadeia* passada por parâmetro representa um valor do tipo *logico*',
                        returnDescription: '*verdadeiro* se a *cadeia* representar um valor *logico*, *falso*, caso contrário',
                        returnType: 'LOGICO',
                        parameters: [{ parameterType: 'CADEIA', name: 'cad', description: 'A *cadeia* a ser verificada' }], 
                        instructions: (args) => {
                                const cad: string = args[0].value.toUpperCase();
                                const cadtrue: boolean = cad === 'VERDADEIRO';
                                const cadfalse: boolean = cad === 'FALSO';

                                return { resultType: 'LOGICO', value: cadtrue?cadtrue:cadfalse };
                        },
                    }
                ],
            },
            cadeia_e_real: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Verifica se a *cadeia* passada por parâmetro representa um valor do tipo *real*',
                        returnDescription: '*verdadeiro* caso a *cadeia* represente um valor do tipo *real*, *falso*, caso contrário',
                        returnType: 'LOGICO',
                        parameters: [{ parameterType: 'CADEIA', name: 'cad', description: 'A *cadeia* a ser verificada' }], 
                        instructions: (args) => {
                                const cad: string = args[0].value;
                                const re = /^[-]{0,1}[0-9]{1,}\.{1}[0-9]{1,}$/;

                                return { resultType: 'LOGICO', value: re.test(cad) };
                        },
                    }
                ],
            },
            cadeia_para_caracter: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Converte um valor do tipo *cadeia* em um valor do tipo *caracter*',
                        returnDescription: 'Um valor do tipo *caracter*',
                        returnType: 'CARACTER',
                        parameters: [{ parameterType: 'CADEIA', name: 'valor', description: 'Valor a ser convertido' }], 
                        instructions: (args) => {
                            if (args[0].value.length === 1) { 
                                const valor: string = args[0].value;
                                return { resultType: 'CARACTER', value: valor };
                            } else {
                                throw new Error('O valor ('+args[0].value+') não é um caracter válido');
                            }
                        },
                    }
                ],
            },
            cadeia_para_inteiro: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Converte um valor do tipo *cadeia* em um valor do tipo *inteiro* utilizando a base informada',
                        returnDescription: 'Um valor do tipo *inteiro*',
                        returnType: 'INTEIRO',
                        parameters: [
                            { parameterType: 'CADEIA', name: 'valor', description: 'Valor a ser convertido' }, 
                            { parameterType: 'INTEIRO', name: 'base', description: 'Notação em que o *inteiro* será representado, base deverá estar entre os números: 2, 10, 16' }
                        ], 
                        instructions: (args) => {
                            const valor: string = args[0].value;
                            const base: number = args[1].value;

                            if (base === 2) {
                                const re2 = /^[-]{0,1}[0-1]*$/;
                                if (re2.test(valor)) {
                                    return { resultType: 'INTEIRO', value: parseInt(valor, base) };
                                } else {
                                    throw new Error('O valor "'+valor+'" não é um número inteiro válido');
                                }
                            } else if (base === 10) {
                                const re10 = /^[-]{0,1}[0-9]*$/;
                                if (re10.test(valor)) {
                                    return { resultType: 'INTEIRO', value: parseInt(valor, base) };
                                } else {
                                    throw new Error('O valor "'+valor+'" não é um número inteiro válido');
                                }
                            } else if (base === 16) {
                                const re16 = /^[-]{0,1}[0-9A-Fa-f]*$/;
                                if (re16.test(valor)) {
                                    return { resultType: 'INTEIRO', value: parseInt(valor, base) };
                                } else {
                                    throw new Error('O valor "'+valor+'" não é um número inteiro válido');
                                }
                            } else {
                                throw new Error('A base informada (' +  base + ') é inválida, a base deve ser um dos seguintes valores: 2, 10, 16');
                            }
                            
                        },
                    }
                ],
            },
            cadeia_para_logico: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Converte um valor do tipo *cadeia* em um valor do tipo *logico*',
                        returnDescription: 'Um valor do tipo *logico*',
                        returnType: 'LOGICO',
                        parameters: [{ parameterType: 'CADEIA', name: 'valor', description: 'Valor a ser convertido' }], 
                        instructions: (args) => {
                                const valor: string = args[0].value.toUpperCase();
                                if (valor === 'VERDADEIRO') {
                                    return { resultType: 'LOGICO', value: true };
                                } else if(valor === 'FALSO') {
                                    return { resultType: 'LOGICO', value: false };
                                } else {
                                    throw new Error('O valor "'+ args[0].value +'" não é um valor lógico válido');
                                }                      
                        },
                    }
                ],
            },
            cadeia_para_real: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Transforma um valor do tipo *cadeia* em um valor do tipo *real*',
                        returnDescription: 'Um valor do tipo *real*',
                        returnType: 'REAL',
                        parameters: [{ parameterType: 'CADEIA', name: 'valor', description: 'Valor a ser convertido' }], 
                        instructions: (args) => {
                                const cad: string = args[0].value;
                                const re = /^[-]{0,1}[0-9]{1,}\.{1}[0-9]{1,}$/;
                                const re2 = /^[-]{0,1}[0-9]*$/;
                                if (re.test(cad)) {
                                    return { resultType: 'REAL', value: parseFloat(cad) };            
                                } else if (re2.test(cad)) {
                                    return { resultType: 'REAL', value: parseFloat(cad) };            
                                } else {
                                    throw new Error('O valor "'+ args[0].value +'" não é um número real válido');
                                }
                        },
                    }
                ],
            },
            caracter_e_inteiro: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Verifica se o *caracter* passado por parâmetro representa um valor do tipo *inteiro*',
                        returnDescription: '*verdadeiro* caso o *caracter* represente um valor do tipo inteiro, *falso*, caso contrário',
                        returnType: 'LOGICO',
                        parameters: [{ parameterType: 'CARACTER', name: 'car', description: 'O *caracter* a ser verificado' }], 
                        instructions: (args) => {
                            const car: string = args[0].value;
                            const re = /^[0-9]$/;

                            if (re.test(car)) {
                                return { resultType: 'LOGICO', value: true };
                            } else {
                                return { resultType: 'LOGICO', value: false };
                            }

                        },
                    }
                ],
            },
            caracter_e_logico: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Verifica se o *caracter* passado por parâmetro representa um valor do tipo *logico*',
                        returnDescription: '*verdadeiro* caso o *caracter* seja "s" ou "S" *falso*, caso seja "n" ou "N"',
                        returnType: 'LOGICO',
                        parameters: [{ parameterType: 'CARACTER', name: 'car', description: 'O *caracter* a ser verificado' }], 
                        instructions: (args) => {
                            const car: string = args[0].value.toUpperCase();
                            if (car === 'S' || car === 'N') {
                                return { resultType: 'LOGICO', value: true };
                            } else {
                                return { resultType: 'LOGICO', value: false };
                            }

                        },
                    }
                ],
            },
            caracter_para_cadeia: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Transforma um valor do tipo *caracter* em um valor do tipo *cadeia*',
                        returnDescription: 'Um valor do tipo *cadeia*',
                        returnType: 'CADEIA',
                        parameters: [{ parameterType: 'CARACTER', name: 'valor', description: 'O *caracter* a ser transformado' }], 
                        instructions: (args) => {

                            return { resultType: 'CADEIA', value: args[0].value };
                        },
                    }
                ],
            },
            caracter_para_inteiro: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Transforma um valor do tipo *caracter* em um valor do tipo *inteiro*',
                        returnDescription: 'Um valor do tipo *inteiro*',
                        returnType: 'INTEIRO',
                        parameters: [{ parameterType: 'CARACTER', name: 'valor', description: 'O *caracter* a ser transformado' }], 
                        instructions: (args) => {
                            const valor: string = args[0].value;
                            const re = /^[0-9]*$/;
                            if (re.test(valor)){
                                return { resultType: 'INTEIRO', value: valor };
                            } else {
                                throw new Error('O caracter "'+ valor +'" não é um inteiro válido');
                            }
                        },
                    }
                ],
            },
            caracter_para_logico: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Transforma um valor do tipo *caracter* em um valor do tipo *logico*',
                        returnDescription: '*verdadeiro* caso o *caracter* seja "s" ou "S" *falso* caso o *caracter* seja "n" ou "N"',
                        returnType: 'LOGICO',
                        parameters: [{ parameterType: 'CARACTER', name: 'valor', description: 'O *caracter* a ser transformado' }], 
                        instructions: (args) => {
                            const valor: string = args[0].value.toUpperCase();
                            if (valor === 'S') {
                                return { resultType: 'LOGICO', value: true };
                            } else if(valor === 'N') {
                                return { resultType: 'LOGICO', value: false };
                            } else {
                                throw new Error('O caracter "'+ args[0].value +'" não é um caracter lógico válido');
                            }                      
                        },
                    }
                ],
            },
            inteiro_e_caracter: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Verifica se um valor do tipo *inteiro* representa um *caracter*',
                        returnDescription: '*verdadeiro*, caso o *inteiro* represente um *caracter*, *falso*, caso contrário',
                        returnType: 'LOGICO',
                        parameters: [{ parameterType: 'INTEIRO', name: 'valor', description: 'O *inteiro* a ser verificado' }], 
                        instructions: (args) => {
                            const valor: string = args[0].value.tostring();
                            if (valor.length === 1) {
                                return { resultType: 'LOGICO', value: true };
                            } else {
                                return { resultType: 'LOGICO', value: false };
                            }                                        
                        },
                    }
                ],
            },
            inteiro_para_cadeia: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Transforma um valor do tipo *inteiro* em um valor do tipo *cadeia* utilizando a base informada',
                        returnDescription: 'Um valor do tipo *cadeia*',
                        returnType: 'CADEIA',
                        parameters: [
                            { parameterType: 'INTEIRO', name: 'valor', description: 'Um valor do tipo *inteiro*' }, 
                            { parameterType: 'INTEIRO', name: 'base', description: 'Base que será utilizada na notação do número, deve estar entre: 2, 10, 16' }], 
                        instructions: (args) => {
                            const valor: number = args[0].value;
                            const base: number = args[1].value;
                            if (base === 2) {
                                return { resultType: 'CADEIA', value: valor.toString(base) };
                            } else if (base === 10) {
                                return { resultType: 'CADEIA', value: valor.toString(base) };
                            } else if (base === 16) {
                                return { resultType: 'CADEIA', value: valor.toString(base) };
                            } else {
                                throw new Error('A base informada (' +  base + ') é inválida, a base deve ser um dos seguintes valores: 2, 10, 16');
                            }                        
                        },
                    }
                ],
            },
            inteiro_para_caracter: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Transforma um valor do tipo *inteiro* em um valor do tipo *caracter*',
                        returnDescription: 'Um valor do tipo *caracter*',
                        returnType: 'CARACTER',
                        parameters: [{ parameterType: 'INTEIRO', name: 'valor', description: '*inteiro* a ser transformado' }], 
                        instructions: (args) => {
                            const valor: string = args[0].value.tostring();
                            if (valor.length === 1){
                                return { resultType: 'CARACTER', value: valor };
                            } else {
                                throw new Error('O inteiro "'+ valor +'" não é um caracter válido');
                            }
                        },
                    }
                ],
            },
            inteiro_para_logico: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Transforma um valor do tipo *inteiro* em um valor do tipo *logico*',
                        returnDescription: '*verdadeiro* caso o *inteiro* seja igual a 1, *falso* caso seja igual a 0',
                        returnType: 'LOGICO',
                        parameters: [{ parameterType: 'INTEIRO', name: 'valor', description: '*inteiro* a ser transformado' }], 
                        instructions: (args) => {
                            const valor: number = args[0].value;
                            if (valor === 1){
                                return { resultType: 'LOGICO', value: true };
                            } else if (valor === 0) {
                                return { resultType: 'LOGICO', value: false };
                            } else {
                                throw new Error('O inteiro "'+ valor +'" não representa um valor lógico válido');
                            }
                        },
                    }
                ],
            },
            inteiro_para_real: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Transforma um valor do tipo *inteiro* em um valor do tipo *real*',
                        returnDescription: 'Um valor do tipo *real*',
                        returnType: 'REAL',
                        parameters: [{ parameterType: 'INTEIRO', name: 'valor', description: '*inteiro* a ser transformado' }], 
                        instructions: (args) => {
                            const valor: number = args[0].value;
                            return { resultType:'REAL', value: valor };
                        },
                    }
                ],
            },
            logico_para_cadeia: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Transforma um valor do tipo *logico* em um valor do tipo *cadeia*',
                        returnDescription: 'Um valor do tipo *cadeia*',
                        returnType: 'CADEIA',
                        parameters: [{ parameterType: 'LOGICO', name: 'valor', description: 'Valor *logico* a ser transformado' }], 
                        instructions: (args) => {
                            const valor: boolean = args[0].value;
                            if(valor) {
                                return { resultType:'CADEIA', value: 'verdadeiro' };
                            } else {
                                return { resultType:'CADEIA', value: 'falso' };
                            }
                        },
                    }
                ],
            },
            logico_para_caracter: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Transforma um valor do tipo *logico* em um valor do tipo *caracter*',
                        returnDescription: 'O *caracter* que representa o valor *logico*',
                        returnType: 'CARACTER',
                        parameters: [{ parameterType: 'LOGICO', name: 'valor', description: 'O valor *logico* a ser transformado' }], 
                        instructions: (args) => {
                            const valor: boolean = args[0].value;
                            if(valor) {
                                return { resultType:'CARACTER', value: 'S' };
                            } else {
                                return { resultType:'CARACTER', value: 'N' };
                            }
                        },
                    }
                ],
            },
            logico_para_inteiro: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Transforma um valor do tipo *logico* em um valor do tipo *inteiro*',
                        returnDescription: 'O valor do tipo *inteiro* que representa o valor *logico*',
                        returnType: 'INTEIRO',
                        parameters: [{ parameterType: 'LOGICO', name: 'valor', description: 'O valor *logico* a ser transformado' }], 
                        instructions: (args) => {
                            const valor: boolean = args[0].value;
                            if(valor) {
                                return { resultType:'INTEIRO', value: '1' };
                            } else {
                                return { resultType:'INTEIRO', value: '0' };
                            }
                        },
                    }
                ],
            },
            real_para_cadeia: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Transforma um valor do tipo *real* em um valor do tipo *cadeia*',
                        returnDescription: 'O valor do tipo *cadeia* que representa o valor *real*',
                        returnType: 'CADEIA',
                        parameters: [{ parameterType: 'REAL', name: 'valor', description: 'O valor *real* a ser transformado' }], 
                        instructions: (args) => {
                            return { resultType:'CADEIA', value: args[0].value.toString() };                        
                        },
                    }
                ],
            },
            real_para_inteiro: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Transforma um valor do tipo *real* em um valor do tipo *inteiro*',
                        returnDescription: 'O valor transformado em *inteiro*',
                        returnType: 'INTEIRO',
                        parameters: [{ parameterType: 'REAL', name: 'valor', description: 'O valor do tipo *real* a ser transformado' }], 
                        instructions: (args) => {
                            return { resultType:'INTEIRO', value: parseInt(args[0].value, 10) };                        
                        },
                    }
                ],
            },
        },
    };
    return { lib, start: async () => undefined, stop: async () => undefined };
} 