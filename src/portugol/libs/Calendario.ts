import { ExpressionResult, LibrarySymbol } from '../context';

export function createLib(): { 
    lib: LibrarySymbol,
    start: () => Promise<void>,
    stop: () => Promise<void>,
} {
    const lib: LibrarySymbol = {
        type: 'LIBRARY_SYMBOL',
        symbols: {
            DIA_DOMINGO: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 1,
                isConstant: true,
            },
            DIA_QUARTA_FEIRA: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 4,
                isConstant: true,
            },
            DIA_QUINTA_FEIRA: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 5,
                isConstant: true,
            },
            DIA_SABADO: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 7,
                isConstant: true,
            },
            DIA_SEGUNDA_FEIRA: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 2,
                isConstant: true,
            },
            DIA_SEXTA_FEIRA: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 6,
                isConstant: true,
            },
            DIA_TERCA_FEIRA: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 3,
                isConstant: true,
            },
            MES_ABRIL: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 4,
                isConstant: true,
            },
            MES_AGOSTO: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 8,
                isConstant: true,
            },
            MES_DEZEMBRO: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 12,
                isConstant: true,
            },
            MES_FEVEREIRO: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 2,
                isConstant: true,
            },
            MES_JANEIRO: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 1,
                isConstant: true,
            },
            MES_JULHO: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 7,
                isConstant: true,
            },
            MES_JUNHO: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 6,
                isConstant: true,
            },
            MES_MAIO: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 5,
                isConstant: true,
            },
            MES_MARCO: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 3,
                isConstant: true,
            },
            MES_NOVEMBRO: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 11,
                isConstant: true,
            },
            MES_OUTUBRO: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 10,
                isConstant: true,
            },
            MES_SETEMBRO: {
                type: 'VALUE_SYMBOL',
                valueType: 'INTEIRO',
                value: 9,
                isConstant: true,
            },
            ano_atual: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Recupera o ano atual do computador',
                        returnDescription: 'Um *inteiro* atual com o ano. Ex: 2012',
                        returnType: 'INTEIRO',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            const now = new Date;
                            return { resultType: 'INTEIRO', value: await now.getFullYear()  };
                        },
                    },
                ],
            },
            dia_mes_atual: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Recupera o dia no mês atual do computador',
                        returnDescription: 'Um *inteiro* com o dia no mês com dois digitos, se forem menores que 10 apenas um digito. Ex: 26',
                        returnType: 'INTEIRO',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            const now = new Date;
                            return { resultType: 'INTEIRO', value: await now.getDate()  };
                        },
                    },
                ],
            }, 
            dia_semana_abreviado: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'De acordo com o valor de 1 a 7 informado retornará um dia da semana abreviado' ,
                        returnDescription: 'Uma *cadeia* com o dia da semana abreviado. Ex: Seg, para Segunda-Feira',
                        returnType: 'REAL',
                        parameters: [
                            { parameterType: 'INTEIRO', name: 'numero_dia', description: 'Um *inteiro* referente a um dia da semana' },
                            { parameterType: 'LOGICO', name: 'caixa_alta', description: '*logico* para retorno em em caracteres maiúsculos' },
                            { parameterType: 'LOGICO', name: 'caixa_baixa', description: '*logico* para retorno em em caracteres munúsculos' }
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const numeroDia: number = args[0].value;
                            const caixaAlta: boolean = args[1].value;
                            const caixaBaixa: boolean = args[2].value;
                            let diaSemana: string = '';

                            switch (numeroDia) {
                                case 1:
                                    diaSemana = 'Dom';
                                    break;
                                case 2:
                                   diaSemana = 'Seg';
                                    break;
                                case 3:
                                   diaSemana = 'Ter';
                                    break;
                                case 4:
                                   diaSemana = 'Qua';
                                    break;
                                case 5:
                                   diaSemana = 'Qui';
                                    break;
                                case 6:
                                   diaSemana = 'Sex';
                                    break;
                                case 7:
                                   diaSemana = 'Sab';
                                    break;
                                default:
                                    throw new Error('numero_dia deve estar entre 1 e 7');
                            }
                            if (caixaAlta) {
                                diaSemana = diaSemana.toUpperCase();
                            } else if (caixaBaixa) {
                                diaSemana = diaSemana.toLowerCase();
                            }
                            return { resultType: 'REAL', value: await diaSemana };
                        },
                    },
                ],
            },
            dia_semana_atual: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Recupera o dia da semana de 1 a 7',
                        returnDescription: 'Um *inteiro* com o dia da semana',
                        returnType: 'INTEIRO',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            const now = new Date;
                            return { resultType: 'INTEIRO', value: await now.getDay() + 1 };
                        },
                    },
                ],
            },
            dia_semana_completo: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'De acordo com o valor de 1 a 7 informado retornará um dia da semana completo' ,
                        returnDescription: 'Uma *cadeia* com o dia da semana completo. Ex: Segunda-Feira',
                        returnType: 'REAL',
                        parameters: [
                            { parameterType: 'INTEIRO', name: 'numero_dia', description: 'Um *inteiro* referente a um dia da semana' },
                            { parameterType: 'LOGICO', name: 'caixa_alta', description: '*logico* para retorno em em caracteres maiúsculos' },
                            { parameterType: 'LOGICO', name: 'caixa_baixa', description: '*logico* para retorno em em caracteres minúsculos' }
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const numeroDia: number = args[0].value;
                            const caixaAlta: boolean = args[1].value;
                            const caixaBaixa: boolean = args[2].value;
                            let diaSemana: string = '';

                            switch (numeroDia) {
                                case 1:
                                    diaSemana = 'Domingo';
                                    break;
                                case 2:
                                   diaSemana = 'Segunda-Feira';
                                    break;
                                case 3:
                                   diaSemana = 'Terça-Feira';
                                    break;
                                case 4:
                                   diaSemana = 'Quarta-Feira';
                                    break;
                                case 5:
                                   diaSemana = 'Quinta-Feira';
                                    break;
                                case 6:
                                   diaSemana = 'Sexta-Feira';
                                    break;
                                case 7:
                                   diaSemana = 'Sábado';
                                    break;
                                default:
                                    throw new Error('numero_dia deve estar entre 1 e 7');
                            }
                            if (caixaAlta) {
                                diaSemana = diaSemana.toUpperCase();
                            } else if (caixaBaixa) {
                                diaSemana = diaSemana.toLowerCase();
                            }
                            return { resultType: 'REAL', value: await diaSemana };
                        },
                    },
                ],
            }, 
            dia_semana_curto: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'De acordo com o valor de 1 a 7 informado retornará um dia da semana curto' ,
                        returnDescription: 'Uma *cadeia* com o dia da semana curto. Ex: Segunda',
                        returnType: 'REAL',
                        parameters: [
                            { parameterType: 'INTEIRO', name: 'numero_dia', description: 'Um *inteiro* referente a um dia da semana' },
                            { parameterType: 'LOGICO', name: 'caixa_alta', description: '*logico* para retorno em em caracteres maiúsculos' },
                            { parameterType: 'LOGICO', name: 'caixa_baixa', description: '*logico* para retorno em em caracteres minúsculos' }
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const numeroDia: number = args[0].value;
                            const caixaAlta: boolean = args[1].value;
                            const caixaBaixa: boolean = args[2].value;
                            let diaSemana: string = '';

                            switch (numeroDia) {
                                case 1:
                                    diaSemana = 'Domingo';
                                    break;
                                case 2:
                                   diaSemana = 'Segunda';
                                    break;
                                case 3:
                                   diaSemana = 'Terça';
                                    break;
                                case 4:
                                   diaSemana = 'Quarta';
                                    break;
                                case 5:
                                   diaSemana = 'Quinta';
                                    break;
                                case 6:
                                   diaSemana = 'Sexta';
                                    break;
                                case 7:
                                   diaSemana = 'Sábado';
                                    break;
                                default:
                                    throw new Error('numero_dia deve estar entre 1 e 7');
                            }
                            if (caixaAlta) {
                                diaSemana = diaSemana.toUpperCase();
                            } else if (caixaBaixa) {
                                diaSemana = diaSemana.toLowerCase();
                            }
                            return { resultType: 'REAL', value: await diaSemana };
                        },
                    },
                ],
            }, 
            hora_atual: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Recupera os dígitos da hora atual do computador',
                        returnDescription: 'Um *inteiro* com a hora atual no formato 12h ou 24h com dois digitos, se forem menores que 10 apenas com um digito. Ex: 22 para 24h, se o parâmetro for falso ou 10 para 12h, se o parâmetro for verdadeiro.',
                        returnType: 'INTEIRO',
                        parameters: [
                            { parameterType: 'LOGICO', name: 'formato', description: 'Um *logico* que se verdadeiro o retorno será no formato 12h se falso será em 24h' }
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const formato: boolean = args[0].value;
                            let hora = (new Date).getHours();
                            if (formato) {
                                if (hora > 12) {
                                    hora -= 12;
                                } else if (hora === 0) {
                                   hora = 12;
                                }
                            }
                            return { resultType: 'INTEIRO', value: await hora };
                        },
                    },
                ],
            },
            mes_atual: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Recupera o mês atual do computador de 1 a 12',
                        returnDescription: 'Um *inteiro* com o mês com dois digitos, se forem menores que 10 apenas com um digito. Ex: 10',
                        returnType: 'INTEIRO',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            const now = new Date;
                            return { resultType: 'INTEIRO', value: await now.getMonth() + 1 };
                        },
                    },
                ],
            },
            millisegundo_atual: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Recupera os digitos dos milisegundos atuais do computador',
                        returnDescription: 'Um *inteiro* com os milisegundos atuais, com um, dois ou três digitos. Ex: 426',
                        returnType: 'INTEIRO',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            const now = new Date;
                            return { resultType: 'INTEIRO', value: await now.getMilliseconds() };
                        },
                    },
                ],
            },
            minuto_atual: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Recupera os digitos dos munitos atuais do computador',
                        returnDescription: 'Um *inteiro* com os minutos atuais, com um, dois ou três digitos. Ex: 28',
                        returnType: 'INTEIRO',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            const now = new Date;
                            return { resultType: 'INTEIRO', value: await now.getMinutes() };
                        },
                    },
                ],
            }, 
            segundo_atual: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Recupera os digitos dos segundos atuais do computador',
                        returnDescription: 'Um *inteiro* com os segundos atuais, com um, dois ou três digitos. Ex: 28',
                        returnType: 'INTEIRO',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            const now = new Date;
                            return { resultType: 'INTEIRO', value: await now.getSeconds() };
                        },
                    },
                ],
            },       
        },
    };
    return { lib, start: async () => undefined, stop: async () => undefined };
}
