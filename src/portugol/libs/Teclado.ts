import { ExpressionResult, LibrarySymbol, ValueSymbol } from '../context';
import { KeyboardInterface } from '../interpreter/interfaces';

export function createLib(): { 
    lib: LibrarySymbol,
    start: (keyboardInterface: KeyboardInterface) => Promise<void>,
    stop: () => Promise<void>,
} {
    let startedKeyboardInterface: KeyboardInterface | undefined;
    const keyboardInterface = new Proxy<KeyboardInterface>({} as any, {
        get: (target, property) => {
            if (!startedKeyboardInterface) {
                throw new Error('TENTATIVA DE CHAMADA À BIBLIOTECA NÃO INICIADA. teclado');
            }
            return startedKeyboardInterface[property];
        },
    });

    function createKeyConstant(value: number): ValueSymbol {
        return { type: 'VALUE_SYMBOL', valueType: 'INTEIRO', value, isConstant: true };
    }

    const lib: LibrarySymbol = {
        type: 'LIBRARY_SYMBOL',
        symbols: {
            TECLA_0: createKeyConstant(48),
            TECLA_1: createKeyConstant(49),
            TECLA_2: createKeyConstant(50),
            TECLA_3: createKeyConstant(51),
            TECLA_4: createKeyConstant(52),
            TECLA_5: createKeyConstant(53),
            TECLA_6: createKeyConstant(54),
            TECLA_7: createKeyConstant(55),
            TECLA_8: createKeyConstant(56),
            TECLA_9: createKeyConstant(57),
            TECLA_0_NUM: createKeyConstant(96),
            TECLA_1_NUM: createKeyConstant(97),
            TECLA_2_NUM: createKeyConstant(98),
            TECLA_3_NUM: createKeyConstant(99),
            TECLA_4_NUM: createKeyConstant(100),
            TECLA_5_NUM: createKeyConstant(101),
            TECLA_6_NUM: createKeyConstant(102),
            TECLA_7_NUM: createKeyConstant(103),
            TECLA_8_NUM: createKeyConstant(104),
            TECLA_9_NUM: createKeyConstant(105),
            TECLA_A: createKeyConstant(65),
            TECLA_B: createKeyConstant(66),
            TECLA_C: createKeyConstant(67),
            TECLA_D: createKeyConstant(68),
            TECLA_E: createKeyConstant(69),
            TECLA_F: createKeyConstant(70),
            TECLA_G: createKeyConstant(71),
            TECLA_H: createKeyConstant(72),
            TECLA_I: createKeyConstant(73),
            TECLA_J: createKeyConstant(74),
            TECLA_K: createKeyConstant(75),
            TECLA_L: createKeyConstant(76),
            TECLA_M: createKeyConstant(77),
            TECLA_N: createKeyConstant(78),
            TECLA_O: createKeyConstant(79),
            TECLA_P: createKeyConstant(80),
            TECLA_Q: createKeyConstant(81),
            TECLA_R: createKeyConstant(82),
            TECLA_S: createKeyConstant(83),
            TECLA_T: createKeyConstant(84),
            TECLA_U: createKeyConstant(85),
            TECLA_V: createKeyConstant(86),
            TECLA_W: createKeyConstant(87),
            TECLA_X: createKeyConstant(88),
            TECLA_Y: createKeyConstant(89),
            TECLA_Z: createKeyConstant(90),
            TECLA_ABRE_COLCHETES: createKeyConstant(91),
            TECLA_ADICAO: createKeyConstant(97),
            TECLA_AJUDA: createKeyConstant(156),
            TECLA_ALT: createKeyConstant(18),
            TECLA_BACKSPACE: createKeyConstant(8),
            TECLA_BARRA: createKeyConstant(47),
            TECLA_BARRA_INVERTIDA: createKeyConstant(92),
            TECLA_CANCELAR: createKeyConstant(3),
            TECLA_CAPS_LOCK: createKeyConstant(20),
            TECLA_CONTROL: createKeyConstant(17),
            TECLA_DECIMAL: createKeyConstant(110),
            TECLA_DELETAR: createKeyConstant(127),
            TECLA_DIVISAO: createKeyConstant(111),
            TECLA_END: createKeyConstant(35),
            TECLA_ENTER: createKeyConstant(13),
            TECLA_ESC: createKeyConstant(27),
            TECLA_ESPACO: createKeyConstant(32),
            TECLA_F1: createKeyConstant(112),
            TECLA_F2: createKeyConstant(113),
            TECLA_F3: createKeyConstant(114),
            TECLA_F4: createKeyConstant(115),
            TECLA_F5: createKeyConstant(116),
            TECLA_F6: createKeyConstant(117),
            TECLA_F7: createKeyConstant(118),
            TECLA_F8: createKeyConstant(119),
            TECLA_F9: createKeyConstant(120),
            TECLA_F10: createKeyConstant(121),
            TECLA_F11: createKeyConstant(122),
            TECLA_F12: createKeyConstant(123),
            TECLA_F13: createKeyConstant(61440),
            TECLA_F14: createKeyConstant(61441),
            TECLA_F15: createKeyConstant(61442),
            TECLA_F16: createKeyConstant(61443),
            TECLA_F17: createKeyConstant(61444),
            TECLA_F18: createKeyConstant(61445),
            TECLA_F19: createKeyConstant(61446),
            TECLA_F20: createKeyConstant(61447),
            TECLA_F21: createKeyConstant(61448),
            TECLA_F22: createKeyConstant(61449),
            TECLA_F23: createKeyConstant(61450),
            TECLA_F24: createKeyConstant(61451),
            TECLA_FECHA_COLCHETES: createKeyConstant(93),
            TECLA_HOME: createKeyConstant(36),
            TECLA_IGUAL: createKeyConstant(61),
            TECLA_INSERT: createKeyConstant(155),
            TECLA_LIMPAR: createKeyConstant(12),
            TECLA_MENOS: createKeyConstant(45),
            TECLA_MENU_CONTEXTO: createKeyConstant(525),
            TECLA_MULTIPLICACAO: createKeyConstant(106),
            TECLA_NUM_LOCK: createKeyConstant(144),
            TECLA_NUM_SEPARADOR_DECIMAL: createKeyConstant(108),
            TECLA_PAGE_DOWN: createKeyConstant(34),
            TECLA_PAGE_UP: createKeyConstant(33),
            TECLA_PAUSE: createKeyConstant(19),
            TECLA_PONTO_FINAL: createKeyConstant(46),
            TECLA_PONTO_VIRGULA: createKeyConstant(59),
            TECLA_PRINTSCREEN: createKeyConstant(154),
            TECLA_SCROLL_LOCK: createKeyConstant(145),
            TECLA_SETA_ABAIXO: createKeyConstant(40),
            TECLA_SETA_ACIMA: createKeyConstant(38),
            TECLA_SETA_DIREITA: createKeyConstant(39),
            TECLA_SETA_ESQUERDA: createKeyConstant(37),
            TECLA_SHIFT: createKeyConstant(16),
            TECLA_SUBTRACAO: createKeyConstant(109),
            TECLA_TAB: createKeyConstant(9),
            TECLA_VIRGULA: createKeyConstant(44),
            TECLA_WINDOWS: createKeyConstant(524),
            alguma_tecla_pressionada: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Testa se existe alguma tecla pressionada neste instante',
                        returnDescription: 'O resultado do teste. *Verdadeiro* se houver alguma tecla pressionada, *falso* caso contrário',
                        returnType: 'LOGICO',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            return { resultType: 'LOGICO', value: await keyboardInterface.isSomeKeyDown() };
                        },
                    },
                ],
            },
            caracter_tecla: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtém o caracter correspondente a determinada tecla',
                        returnDescription: 'O caracter correspondente a tecla',
                        returnType: 'CARACTER',
                        parameters: [{ parameterType: 'INTEIRO', name: 'tecla', description: 'A tecla a qual se quer obter o caracter' }],
                        instructions: async (args) => {
                            // const tecla: number = args[0].value;
    
                            throw new Error('Não implementado.');
                        },
                    },
                ],
            },
            ler_tecla: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Aguarda até que uma tecla seja digitada, isto é, foi pressionada e depois solta, e captura o seu código',
                        returnDescription: 'O código da tecla lida',
                        returnType: 'INTEIRO',
                        parameters: [],
                        instructions: async () => {
                            const key = await keyboardInterface.readKey();
                            const result: ExpressionResult = { resultType: 'INTEIRO', value: key };
                            return result;
                        },
                    },
                ],
            },
            tecla_pressionada: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Testa se uma determinada tecla está sendo pressionada neste instante',
                        returnDescription: 'O resultado do teste, *verdadeiro* se a tecla estiver sendo pressionada, *falso*, caso contrário',
                        returnType: 'LOGICO',
                        parameters: [{ parameterType: 'INTEIRO', name: 'tecla', description: 'O código da tecla a ser testada' }],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const tecla: number = args[0].value;
    
                            return { resultType: 'LOGICO', value: await keyboardInterface.isKeyDown(tecla) };
                        },
                    },
                ],
            },
        },
    };

    return {
        lib,
        start: async (ki) => {
            startedKeyboardInterface = ki;
            await keyboardInterface._controlStart();
        },
        stop: () => keyboardInterface._controlStop(),
    };
}