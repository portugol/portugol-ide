import { ExpressionResult, LibrarySymbol } from '../context';
import { Type } from '../types';

export function createLib(): { 
    lib: LibrarySymbol,
    start: () => Promise<void>,
    stop: () => Promise<void>,
} {
    const lib: LibrarySymbol = {
        type: 'LIBRARY_SYMBOL',
        symbols: {
            PI: {
                type: 'VALUE_SYMBOL',
                valueType: 'REAL',
                value: 3.141592653589793,
                isConstant: true,
            },
            arredondar: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Arredonda um número *real* para o número de casas decimais informadas. Quando o último dígito for maior ou igual a 5, o número será arredondado para cima, quando o último dígito for menor que 5, o número será arredondado para baixo',
                        returnDescription: 'O número arredondado',
                        returnType: 'REAL',
                        parameters: [
                            { parameterType: 'REAL', name: 'titulo', description: 'O número que será arredondado' },
                            { parameterType: 'INTEIRO', name: 'titulo', description: 'O número de casas decimais após o arredondamento' }
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const numero: number = args[0].value;
                            const casas: number = args[1].value;
                            const power = Math.pow(10, casas);
                            const value = Math.round(numero * power) / power;
                            return { resultType: 'REAL', value };
                        },
                    },
                ],
            }, 
            cosseno: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Calcula o cosseno e um ângulo informado',
                        returnDescription: 'O cosseno do ângulo informado',
                        returnType: 'REAL',
                        parameters: [
                            { parameterType: 'REAL', name: 'angulo', description: 'O ângulo que será calculado o cosseno' },
                        ], 
                        instructions: async (args): Promise<ExpressionResult> => {
                            const angulo: number = args[0].value;
    
                            return { resultType: 'REAL', value: Math.cos(angulo) };
                        },
                    }
                ],
            }, 
            logaritmo: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Calcula um logaritmo de um número para uma determinada base',
                        returnDescription: 'O logaritmo',
                        returnType: 'REAL',
                        parameters: [
                            { parameterType: 'REAL', name: 'numero', description: 'O numero resultante de exponenciação' },
                            { parameterType: 'REAL', name: 'base', description: 'A base da exponenciação' }
                        ], 
                        instructions: async (args): Promise<ExpressionResult> => {
                            const numero: number = args[0].value;
                            const base: number = args[1].value;
    
                            return { resultType: 'REAL', value: await (Math.log(numero) / Math.log(base)) };
                        },
                    }
                ],
            },
            maior_numero: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Identifica o maior número entre dois números informados',
                        returnDescription: 'O maior número',
                        returnType: 'REAL',
                        parameters: [
                            { parameterType: 'REAL', name: 'numeroA', description: 'Um número qualquer' },
                            { parameterType: 'REAL', name: 'numeroB', description: 'Um número qualquer' },
                        ], 
                        instructions: async (args): Promise<ExpressionResult> => {
                            const numeroA: number = args[0].value;
                            const numeroB: number = args[1].value;

                            return { resultType: 'REAL', value: Math.max(numeroA, numeroB) };
                                    
                        },
                    }
                ],
            },
            menor_numero: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Identifica o menor número entre dois números informados',
                        returnDescription: 'O menor número',
                        returnType: 'REAL',
                        parameters: [
                            { parameterType: 'REAL', name: 'numeroA', description: 'Um número qualquer' },
                            { parameterType: 'REAL', name: 'numeroB', description: 'Um número qualquer' },
                        ], 
                        instructions: async (args): Promise<ExpressionResult> => {
                            const numeroA: number = args[0].value;
                            const numeroB: number = args[1].value;
                             
                            return { resultType: 'REAL', value: Math.min(numeroA, numeroB) };
                        },
                    }
                ],
            },
            potencia: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Realiza uma exponenciação através da multiplicação da base por ela mesma tantas vezes quanto indicar o expoente',
                        returnDescription: 'A exponenciação da base pelo expoente',
                        returnType: 'REAL',
                        parameters: [
                            { parameterType: 'REAL', name: 'base', description: 'O número que será multiplicado' },
                            { parameterType: 'REAL', name: 'expoente', description: 'O número de vezes em que a base será multiplicada' },
                        ], 
                        instructions: async (args): Promise<ExpressionResult> => {
                            const base: number = args[0].value;
                            const expoente: number = args[1].value;

                                return { resultType: 'REAL', value: Math.pow(base, expoente) };
                        },
                    }
                ],
            },
            raiz: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Realiza a radiciação (extrai a raíz) de um número por um determinado índice',
                        returnDescription: 'A raiz do número informado',
                        returnType: 'REAL',
                        parameters: [
                            { parameterType: 'REAL', name: 'radicando', description: 'O número do qual será extraído a raiz' },
                            { parameterType: 'REAL', name: 'indice', description: 'Indica o grau de radiciação. Quando o índice é 2 a raíz é quadrada, quando o índice é 3 a raiz é cúbica e assim por diante' },
                        ], 
                        instructions: async (args): Promise<ExpressionResult> => {
                            const radicando: number = args[0].value;
                            const indice: number = args[1].value;

                                return { resultType: 'REAL', value: Math.pow(radicando, 1 / indice) };
                        },
                    }
                ],
            },
            seno: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Calcula o seno e um ângulo informado',
                        returnDescription: 'O seno do ângulo informado',
                        returnType: 'REAL',
                        parameters: [
                            { parameterType: 'REAL', name: 'angulo', description: 'O ângulo que será calculado o seno' },
                        ], 
                        instructions: async (args): Promise<ExpressionResult> => {
                            const angulo: number = args[0].value;
    
                            return { resultType: 'REAL', value: Math.sin(angulo) };
                        },
                    }
                ],
            },
            tangente: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Calcula o tangente e um ângulo informado',
                        returnDescription: 'O tangente do ângulo informado',
                        returnType: 'REAL',
                        parameters: [
                            { parameterType: 'REAL', name: 'angulo', description: 'O ângulo que será calculado o tangente' },
                        ], 
                        instructions: async (args): Promise<ExpressionResult> => {
                            const angulo: number = args[0].value;
    
                            return { resultType: 'REAL', value: Math.tan(angulo) };
                        },
                    }
                ],
            },
            valor_absoluto: {
                type: 'FUNCTION_SYMBOL',
                overloads: (['INTEIRO', 'REAL'] as Type[]).map((tipo) => ({
                    description: 'Calcula o valor absoluto de um número informado',
                    returnDescription: 'O valor absoluto do número informado',
                    returnType: tipo,
                    parameters: [
                        { parameterType: tipo, name: 'numero', description: 'O número que será calculado o valor absoluto' },
                    ], 
                    instructions: async (args: ExpressionResult[]): Promise<ExpressionResult> => {
                        const numero: number = args[0].value;

                        return { resultType: tipo, value: Math.abs(numero) };
                    },
                })),
            },          
        },
    };
    return { lib, start: async () => undefined, stop: async () => undefined };
}
