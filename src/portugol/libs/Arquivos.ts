import { ExpressionResult, LibrarySymbol } from '../context';
import { FilesInterface } from '../interpreter/interfaces';

export function createLib(): { 
    lib: LibrarySymbol,
    start: (filesInterface: FilesInterface) => Promise<void>,
    stop: () => Promise<void>,
} {
    let startedFilesInterface: FilesInterface | undefined;
    const filesInterface = new Proxy<FilesInterface>({} as any, {
        get: (target, property) => {
            if (!startedFilesInterface) {
                throw new Error('TENTATIVA DE CHAMADA À BIBLIOTECA NÃO INICIADA.');
            }
            return startedFilesInterface[property];
        },
    });

    const lib: LibrarySymbol = {
        type: 'LIBRARY_SYMBOL',
        symbols: {
            MODO_ACRESCENTAR: {
                type: 'VALUE_SYMBOL',
                description: 'indica à biblioteca que o arquivo deve ser aberto apenas para escrita que acrescenta ao final do arquivo',
                valueType: 'INTEIRO',
                value: 2,
                isConstant: true,
            },
            MODO_ESCRITA: {
                type: 'VALUE_SYMBOL',
                description: 'indica à biblioteca que o arquivo deve ser aberto apenas para escrita',
                valueType: 'INTEIRO',
                value: 1,
                isConstant: true,
            },
            MODO_LEITURA: {
                type: 'VALUE_SYMBOL',
                description: 'indica à biblioteca que o arquivo deve ser aberto apenas para leitura',
                valueType: 'INTEIRO',
                value: 0,
                isConstant: true,
            },
            abrir_arquivo: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Abre um arquivo para leitura ou escrita. No modo leitura, caso o arquivo informado não exista, será gerado um erro. No modo escrita, caso o*arquivo informado não exista, ele tentará ser criado, se a criação do arquivo falhar, então será gerado um erro.*IMPORTANTE:* ao abrir o arquivo no modo de escrita, o conteúdo do arquivo é apagado para que o novo conteúdo seja escrito. Caso seja necessário*manter o conteúdo atual do arquivo, deve-se armazená-lo em uma variável e depois escrevê-lo novamente no arquivo.',
                        returnType: 'INTEIRO',
                        returnDescription: 'o endereço de memória onde o arquivo foi carregado',
                        parameters: [
                            {
                                description: 'o nome do arquivo que se quer abrir',
                                parameterType: 'CADEIA',
                                name: 'caminho_arquivo',
                            },
                            {
                                description: 'determina se o arquivo será aberto para leitura ou para escrita. Constantes aceitas: MODO_LEITURA | MODO_ESCRITA | MODO_ACRESCENTAR',
                                parameterType: 'INTEIRO',
                                name: 'modo_acesso',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const caminhoArquivo: string = args[0].value;
                            const modoAcesso: number = args[1].value;

                            return {
                                resultType: 'INTEIRO',
                                value: await filesInterface.openFile(caminhoArquivo, modoAcesso),
                            };
                        },
                    },
                ],
            },
            apagar_arquivo: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Remove um arquivo do sistema de arquivos',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                description: 'o caminho do arquivo que ser quer apagar',
                                parameterType: 'CADEIA',
                                name: 'caminho_arquivo',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const caminhoArquivo: string = args[0].value;

                            await filesInterface.deleteFile(caminhoArquivo);

                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            arquivo_existe: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Verifica se um determinado arquivo existe no sistema de arquivos',
                        returnType: 'LOGICO',
                        returnDescription: '*verdadeiro* se o arquivo existir',
                        parameters: [
                            {
                                description: 'o caminho do arquivo que se quer verificar',
                                parameterType: 'CADEIA',
                                name: 'caminho_arquivo',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const caminhoArquivo: string = args[0].value;

                            return {
                                resultType: 'LOGICO',
                                value: await filesInterface.fileExists(caminhoArquivo),
                            };
                        },
                    },
                ],
            },
            criar_pasta: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Cria pastas no caminho informado caso elas não existam',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                description: 'Caminho onde as pastas serão criadas',
                                parameterType: 'CADEIA',
                                name: 'caminho',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const caminho: string = args[0].value;
                            await filesInterface.createFolder(caminho);
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            escrever_linha: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Escreve uma linha no arquivo. Esta função só é executada se o arquivo estiver aberto em modo de escrita. Se o arquivo estiver em modo de*leitura, será gerado um erro.',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                description: 'a linha a ser escrita no arquivo',
                                parameterType: 'CADEIA',
                                name: 'linha',
                            },
                            {
                                description: 'o endereço de memória do arquivo',
                                parameterType: 'INTEIRO',
                                name: 'endereco',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const linha: string = args[0].value;
                            const endereco: number = args[1].value;

                            await filesInterface.writeLine(linha, endereco);

                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            fechar_arquivo: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Fecha um arquivo aberto anteriormente',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                description: 'o endereço de memória do arquivo',
                                parameterType: 'INTEIRO',
                                name: 'endereco',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const endereco: number = args[0].value;
                            await filesInterface.closeFile(endereco);
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            fim_arquivo: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Verifica se o arquivo chegou ao fim, isto é, se todas as linhas já foram lidas. Esta função só é executada se o arquivo estiver aberto em modo de*leitura. Se o arquivo estiver em modo de escrita, será gerado um erro.',
                        returnType: 'LOGICO',
                        returnDescription: '*verdadeiro* se o arquivo tiver chegado ao fim. Caso contrário retorna *falso*',
                        parameters: [
                            {
                                description: 'o endereço de memória do arquivo',
                                parameterType: 'INTEIRO',
                                name: 'endereco'
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const endereco: number = args[0].value;
                            return { 
                                resultType: 'LOGICO',
                                value: await filesInterface.isFileEnd(endereco),
                            };
                        },
                    },
                ],
            },
            ler_linha: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Lê a próxima linha do arquivo. Esta função só é executada se o arquivo estiver aberto em modo de leitura. Se o arquivo estiver em modo de*escrita, será gerado um erro.',
                        returnType: 'CADEIA',
                        returnDescription: 'Uma *cadeia* contendo o conteudo da linha lida',
                        parameters: [
                            {
                                description: 'o endereço de memória do arquivo',
                                parameterType: 'INTEIRO',
                                name: 'endereco',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const endereco: number = args[0].value;
                            return {
                                resultType: 'CADEIA',
                                value: await filesInterface.readLine(endereco),
                            };
                        },
                    },
                ],
            },
            listar_arquivos: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Altera um vetor para que ele represente os arquivos existentes em um diretório',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                description: 'Define o diretório cujas pastas serão listadas',
                                parameterType: 'CADEIA',
                                name: 'caminho_pai',
                            },
                            {
                                description: 'Vetor destino que contará com as pastas encontradas',
                                parameterType: { type: 'ARRAY', valueType: 'CADEIA', size: null },
                                isReference: true,
                                name: 'vetor_arquivos',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const caminhoPai: string = args[0].value;
                            const vetorArquivos: string[] = args[1].value;
                            vetorArquivos.length = 0;
                            vetorArquivos.push(
                                ...(await filesInterface.listFiles(caminhoPai)),
                            );
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            listar_arquivos_por_tipo: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Altera um vetor para que ele represente os arquivos existentes em um diretório',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                description: 'Altera um vetor para que ele represente os arquivos existentes em um diretório',
                                parameterType: 'CADEIA',
                                name: 'caminho_pai',
                            },
                            {
                                description: 'Vetor destino que contará com as pastas encontradas',
                                parameterType: { type: 'ARRAY', valueType: 'CADEIA', size: null },
                                isReference: true,
                                name: 'vetor_arquivos',
                            },
                            {
                                description: 'Vetor contendo tipos de arquivos que serão listados',
                                parameterType: { type: 'ARRAY', valueType: 'CADEIA', size: null },
                                name: 'vetor_tipos',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const caminhoPai: string = args[0].value;
                            const vetorArquivos: string[] = args[1].value;
                            const vetorTipos: string[] = args[2].value;
                            vetorArquivos.length = 0;
                            vetorArquivos.push(
                                ...(await filesInterface.listFilesByType(caminhoPai, vetorTipos)),
                            );
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            listar_pastas: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Altera um vetor para que ele represente as pastas existentes em um diretório',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                description: 'Define o diretório cujas pastas serão listadas',
                                parameterType: 'CADEIA',
                                name: 'caminho_pai',
                            }, 
                            {
                                description: 'Vetor destino que contará com as pastas encontradas',
                                parameterType: { type: 'ARRAY', valueType: 'CADEIA', size: null },
                                isReference: true,
                                name: 'vetor_pastas',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const caminhoPai: string = args[0].value;
                            const vetorPastas: string[] = args[1].value;
                            vetorPastas.length = 0;
                            vetorPastas.push(
                                ...(await filesInterface.listFolders(caminhoPai)),
                            );
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            selecionar_arquivo: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Abre um janela que permite ao usuário navegar nos diretórios do computador e selecionar um arquivo',
                        returnType: 'CADEIA',
                        returnDescription: 'O arquivo_selecionado ou uma string vazia caso o usuário tenha cancelado.',
                        parameters: [
                            {
                                description: "Define os formatos de arquivos que poderão ser selecionados. Um formato de arquivo é formado por uma descrição e uma*lista de extensões válidas. A descrição deve estar separada da lista de extensões pelo caracter '|' e cada extensão deverá estar separada da outra*pelo caracter ','. Ex.: 'Arquivos de texto|txt', 'Arquivos de imagem|png,jpg,jpeg,bmp'",
                                parameterType: { type: 'ARRAY', valueType: 'CADEIA', size: null },
                                name: 'formatos_suportados',
                            },
                            {
                                description: "Quando verdadeiro, inclui automaticamente um formato que permite selecionar qualquer arquivo. Este formato também*será incluído se nenhum outro formato for informado no parâmetro 'formatos_suportados'",
                                parameterType: 'LOGICO',
                                name: 'aceitar_todos_arquivos',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const formatosSuportados: string[] = args[0].value;
                            const aceitarTodosArquivos: boolean = args[1].value;
                            return {
                                resultType: 'CADEIA',
                                value: await filesInterface.selectFile(formatosSuportados, aceitarTodosArquivos)
                            };
                        },
                    },
                ],
            },
            substituir_texto: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Pesquisa por um determinado texto no arquivo e substitui todas as ocorrências por um texto alternativo',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                description: 'o endereço do arquivo',
                                parameterType: 'CADEIA',
                                name: 'endereco',
                            },
                            {
                                description: 'o texto que será pesquisado no arquivo',
                                parameterType: 'CADEIA',
                                name: 'texto_pesquisa',
                            },
                            {
                                description: 'o texto pelo qual as ocorrências serão substituídas',
                                parameterType: 'CADEIA',
                                name: 'texto_substituto',
                            },
                            {
                                description: 'confirma se substituirá apenas a primeira ocorrência no texto, caso contrário, substituirá todas',
                                parameterType: 'LOGICO',
                                name: 'primeira_ocorrencia',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const endereco: string = args[0].value;
                            const textoPesquisa: string = args[1].value;
                            const textoSubstituto: string = args[2].value;
                            const primeiraOcorrencia: boolean = args[3].value;
                            await filesInterface.replaceText(endereco, textoPesquisa, textoSubstituto, primeiraOcorrencia);
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
        },
    };

    return {
        lib,
        start: async (fi) => {
            startedFilesInterface = fi;
            await fi._controlStart();
        },
        stop: () => filesInterface._controlStop(),
    };
}