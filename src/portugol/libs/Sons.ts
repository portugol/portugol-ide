import { ExpressionResult, LibrarySymbol } from '../context';
import { SoundInterface } from '../interpreter/interfaces';

export function createLib(): { 
    lib: LibrarySymbol,
    start: (soundInterface: SoundInterface) => Promise<void>,
    stop: () => Promise<void>,
} {
    let startedSoundInterface: SoundInterface | undefined;
    const soundInterface = new Proxy<SoundInterface>({} as any, {
        get: (target, property) => {
            if (!startedSoundInterface) {
                throw new Error('TENTATIVA DE CHAMADA À BIBLIOTECA NÃO INICIADA. som');
            }
            return startedSoundInterface[property];
        },
    });

    const lib: LibrarySymbol = {
        type: 'LIBRARY_SYMBOL',
        symbols: {
            carregar_som: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Carrega um som na memória para ser reproduzido mais tarde',
                        returnType: 'INTEIRO',
                        returnDescription: 'o endereço de memória no qual o som foi carregado',
                        parameters: [
                            {
                                parameterType: 'CADEIA',
                                name: 'caminho_som',
                                description: 'o caminho para o arquivo de som no sistema de arquivos',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const caminhoSom: string = args[0].value;

                            return {
                                resultType: 'INTEIRO',
                                value: await soundInterface.loadSound(caminhoSom),
                            };
                        },
                    },
                ],
            },
            definir_posicao_atual_musica: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Define a posição atual da música em milisegundos',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                parameterType: 'INTEIRO',
                                name: 'endereco',
                                description: 'o endereço de memória da reprodução que se quer alterar o volume',
                            },
                            {
                                parameterType: 'INTEIRO',
                                name: 'milissegundos',
                                description: 'o tempo em milissegundos que deseja colocar a musica',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const endereco: number = args[0].value;
                            const milissegundos: number = args[1].value;

                            await soundInterface.setCurrentPositionMusic(endereco, milissegundos);

                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            definir_volume: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Define o volume geral',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                parameterType: 'INTEIRO',
                                name: 'volume',
                                description: 'O novo volume geral (entre 0 e 100)',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const volume: number = args[0].value;

                            await soundInterface.setVolume(volume);

                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            definir_volume_reproducao: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Define um novo volume para um som que já está sendo executado',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                parameterType: 'INTEIRO',
                                name: 'endereco',
                                description: 'o endereço de memória da reprodução que se quer alterar o volume',
                            },
                            {
                                parameterType: 'INTEIRO',
                                name: 'volume',
                                description: 'o novo volume entre 0 e 100',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const endereco: number = args[0].value;
                            const volume: number = args[0].value;

                            await soundInterface.setPlayingVolume(endereco, volume);

                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            interromper_som: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Interrompe uma reprodução específica de um som que está sendo executada no momento',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                parameterType: 'INTEIRO',
                                name: 'endereco',
                                description: 'o endereço de memória da reprodução que se quer interromper',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const endereco: number = args[0].value;

                            await soundInterface.stopSound(endereco);

                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            liberar_som: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Libera a memória utilizada por um som que tenha sido previamente carregado. Se o som estiver sendo reproduzido, todas as reproduções deste som serão*interrompidas',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                parameterType: 'INTEIRO',
                                name: 'endereco',
                                description: 'o endereço de memória do som',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const endereco: number = args[0].value;

                            await soundInterface.freeSound(endereco);

                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            obter_posicao_atual_musica: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtêm a posição atual da música em milisegundos',
                        returnType: 'INTEIRO',
                        returnDescription: 'o tempo atual em milissegundos do som',
                        parameters: [
                            {
                                parameterType: 'INTEIRO',
                                name: 'endereco',
                                description: 'o endereço de memória da reprodução que se quer alterar o volume',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const endereco: number = args[0].value;

                            return {
                                resultType: 'INTEIRO',
                                value: await soundInterface.getCurrentPositionMusic(endereco),
                            };
                        },
                    },
                ],
            },
            obter_tamanho_musica: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Obtêm o tamanho da música em milisegundos',
                        returnType: 'INTEIRO',
                        returnDescription: 'tempo total em milissegundos do som',
                        parameters: [
                            {
                                parameterType: 'INTEIRO',
                                name: 'endereco',
                                description: 'o endereço de memória da música que se quer saber o tamanho',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const endereco: number = args[0].value;

                            return { 
                                resultType: 'INTEIRO',
                                value: await soundInterface.getMusicSize(endereco),
                            };
                        },
                    },
                ],
            },
            obter_volume: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Retorna o volume geral',
                        returnType: 'INTEIRO',
                        returnDescription: 'Um valor do tipo inteiro entre 0 e 100 representando o volume geral atual.',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            return {
                                resultType: 'INTEIRO',
                                value: await soundInterface.getVolume(),
                            };
                        },
                    },
                ],
            },
            obter_volume_reproducao: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Retornar o volume de uma reprodução de som',
                        returnType: 'INTEIRO',
                        returnDescription: 'Um valor do tipo inteiro entre 0 e 100 representando o volume atual da reprodução. Caso a reprodução não exista ou já tenha sido finalizada o valor -1 será *retornado*',
                        parameters: [
                            {
                                parameterType: 'INTEIRO',
                                name: 'endereco',
                                description: 'O endereço de memória da reprodução que se quer obter o volume',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const endereco: number = args[0].value;

                            return {
                                resultType: 'INTEIRO',
                                value: await soundInterface.getPlayingVolume(endereco),
                            };
                        },
                    },
                ],
            },
            pausar_som: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Pausa uma reprodução específica de um som que está sendo executada no momento',
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                parameterType: 'INTEIRO',
                                name: 'endereco',
                                description: 'o endereço de memória da reprodução que se quer interromper',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const endereco: number = args[0].value;

                            await soundInterface.pauseSound(endereco);

                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            reproduzir_som: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        description: 'Reproduz um som previamente carregado na memória. O som é reproduzido de forma assíncrona, ou seja, o som ficará reproduzindo no fundo, enquanto*o programa executa as próximas instruções normalmente. Um mesmo som pode ser reproduzido várias vezes e pode se sobrepor a outros sons.',
                        returnType: 'INTEIRO',
                        returnDescription: 'o endereço de memória da reprodução de som. Este endereço é utilizado quando se deseja interromper esta reprodução de som especificamente.',
                        parameters: [
                            {
                                parameterType: 'INTEIRO',
                                name: 'endereco',
                                description: 'o endereço de memória do som a ser reproduzido',
                            },
                            {
                                parameterType: 'LOGICO',
                                name: 'repetir',
                                description: 'determina se o som deve ficar repetindo até ser manualmente interrompido',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const endereco: number = args[0].value;
                            const repetir: boolean = args[1].value;

                            return {
                                resultType: 'INTEIRO',
                                value: await soundInterface.playSound(endereco, repetir),
                            };
                        },
                    },
                ],
            },
        },
    };

    
    return {
        lib,
        start: async (si) => {
            startedSoundInterface = si;
            await soundInterface._controlStart();
        },
        stop: () => soundInterface._controlStop(),
    };
}