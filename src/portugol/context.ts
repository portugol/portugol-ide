import { Code, DeclareFunctionParameter, DeclareSymbolValueType, Expression, Program } from './ast';
import { ControllableInterface } from './interpreter/interfaces';
import { createVisitor } from './interpreter/visitor';
import libraries from './libraries';
import { ParseError } from './parsing';
import { 
    isTypeArray,
    isTypeFunction,
    isTypePrimitive,
    isTypeUnion,
    PrimitiveType,
    Type,
    TypeArray,
    TypeCadeia,
    TypeCaracter,
    TypeFunction,
    TypeInteiro,
    TypeLogico,
    TypeReal,
    TypeUnion,
    TypeVazio,
} from './types';
import { visit } from './visitor';

export interface ValueSymbolArrayValue extends Array<ValueSymbol['value']> { }
export interface ValueSymbolUnionValue { 
    originalType: PrimitiveType;
    value: boolean | string | number | undefined;
}
export type ValueSymbol = { type: 'VALUE_SYMBOL', isConstant: boolean, description?: string, location?: Code['location'] } & 
    ( { valueType: TypeInteiro, value: number | null }
    | { valueType: TypeReal, value: number | null }
    | { valueType: TypeCaracter, value: string | null }
    | { valueType: TypeCadeia, value: string | null }
    | { valueType: TypeLogico, value: boolean | null }
    | { valueType: TypeVazio, value: undefined | null } 
    | { valueType: TypeFunction, value: FunctionSymbol | null }
    | { valueType: TypeArray, value: ValueSymbolArrayValue | null }
    | { valueType: TypeUnion, value: ValueSymbolUnionValue | null }
    );

export type ExpressionResult = { 
    resultType: Type,
    value: any,
};

export type FunctionSymbolParameter = { 
    location?: Code['location'],
    description?: string,
    parameterType: Type,
    name: string,
    isReference?: boolean,
    isVariadic?: boolean,
};
export type FunctionSymbolOverloadInstructionsNativeUpdateReferenceValue = ((newValue: any) => Promise<void>) | undefined;
export type FunctionSymbolOverloadInstructionsNative = (
    args: ExpressionResult[],
    updateReferenceValue: FunctionSymbolOverloadInstructionsNativeUpdateReferenceValue[] | FunctionSymbolOverloadInstructionsNativeUpdateReferenceValue[][],
) => (ExpressionResult | Promise<ExpressionResult>);
export type FunctionSymbolOverload = { 
    description?: string,
    returnDescription?: string,
    returnType: Type,
    parameters: FunctionSymbolParameter[],
    instructions: Expression[] | FunctionSymbolOverloadInstructionsNative,
    location?: Code['location'],
};
export type FunctionSymbol = {
    type: 'FUNCTION_SYMBOL',
    overloads: FunctionSymbolOverload[],
    parentContext?: Context,
};
export type LibrarySymbol = {
    version?: string,
    description?: string,
    type: 'LIBRARY_SYMBOL',
    symbols: { [key: string]: ValueSymbol | FunctionSymbol },
    location?: Code['location'],
    libraryName?: string,
};
export type ContextSymbol = ValueSymbol | FunctionSymbol | LibrarySymbol;
export type Context = { 
    returnType?: Type,
    parentContext?: Context,
    symbols: { [key: string]: ContextSymbol },
};

export type DescendibleContext = Context & {
    childrenContext: DescendibleContext[],
    location: Code['location'],
};

type Problem = {
    description: string,
    location: Code['location'],
};

export function getType(type: DeclareSymbolValueType, location?: Code['location']): Type {
    if (typeof type === 'string') {
        switch (type) {
            case 'inteiro': return 'INTEIRO';
            case 'real': return 'REAL';
            case 'cadeia': return 'CADEIA';
            case 'logico': return 'LOGICO';
            case 'caracter': return 'CARACTER';
            case 'vazio': return 'VAZIO';
            default: throw new ParseError(`Tipo inválido "${type}".`, location);
        }
    }
    
    if (type.type === 'ARRAY') {
        let size: number | null = null;

        if (type.size) {
            if (type.size.type !== 'CONSTANT_INTEGER') {
                throw new ParseError(`O tamanho de um vetor deve ser uma constante do tipo "${formatType("INTEIRO")}".`, type.size.location);
            }

            size = type.size.value;
        }

        return {
            type: 'ARRAY',
            valueType: getType(type.valueType, location),
            size,
        };
    }

    throw new ParseError(`TIPO NÃO RECONHECIDO ${JSON.stringify(type)}.`, location);
}

export function formatType(type: Type): string {
    if (typeof type === 'string') {
        return type.toLowerCase();
    }

    if (type.type === 'FUNCTION') {
        return `funcao ${formatType(type.returnType)} (${
            type.parameters.map(p => {
                let formattedType = formatType(p.parameterType);
                if (p.isReference) {
                    formattedType = `&${formattedType}`;
                }
                if (p.isVariadic) {
                    formattedType = `${formattedType}...`;
                }
                return formattedType;
            }).join(', ')
        })`;
    }

    if (type.type === 'ARRAY') {
        const size: Array<number | null> = [type.size];
        let childType = type.valueType;
        while (typeof childType !== 'string' && childType.type === 'ARRAY') {
            size.push(childType.size);
            childType = childType.valueType;
        }
        const sizeDescription = size.map(s => `[${s === null ? '' : s}]`).join('');
        return `${formatType(childType)}${sizeDescription}`;
    }

    if (type.type === 'UNION') {
        return `(${type.valueTypes.map(vt => formatType(vt)).join(' | ')})`;
    }

    throw new Error(`TIPO NÃO IMPLEMENTADO ${JSON.stringify(type)}`);
}

async function captureProblem(fn: () => Promise<void>, onProblem: (problem: Problem) => void) {
    try {
        await fn();
    } catch (error) {
        if (error instanceof ParseError) {
            onProblem({
                description: error.message,
                location: error.location,
            });
        } else {
            throw error;
        }
    }
}

export function declareValueSymbol(
    context: Context,
    symbolInfo: {
        name: string,
        valueType: Type,
        value: ExpressionResult | null,
        isConstant: boolean,
    },
    location: Code['location'],
) {
    const existingSymbol = context.symbols[symbolInfo.name];
    if (existingSymbol) {
        throw new ParseError(`Não é possível redeclarar símbolo com nome "${symbolInfo.name}".`, location);
    }

    const valueType: Type = symbolInfo.valueType;

    const symbol = {
        type: 'VALUE_SYMBOL',
        isConstant: symbolInfo.isConstant,
        value: null,
        valueType,
        location,
    } as ValueSymbol;

    context.symbols[symbolInfo.name] = symbol;

    if (symbolInfo.value !== null) {
        const convertedResult = convertResult(symbolInfo.value, valueType);
        if (!convertedResult) {
            throw new ParseError(`Não é possível atribuir um valor de tipo "${formatType(symbolInfo.value.resultType)}" para tipo "${formatType(valueType)}".`, location);
        }
        symbol.value = convertedResult.value;
    } else if (isTypeArray(valueType)) {
        if (valueType.size !== null) { // Automatically initialize array with declared size
            symbol.value = defaultValue(valueType);
        }
    }

    if (symbol.value === null && symbolInfo.isConstant) {
        throw new ParseError('Uma constante deve ser inicializada na declaração.', location);
    }
}

export function declareFunctionSymbol(
    context: Context,
    symbolInfo: {
        name: string,
        parameters: DeclareFunctionParameter[],
        returnType: string | undefined,
        instructions: Expression[],
    },
    location: Code['location'],
): {
    returnType: Type,
    parameters: FunctionSymbolParameter[], 
    symbol: FunctionSymbol,
} {
    const existingSymbol = context.symbols[symbolInfo.name];
    
    const parameters: FunctionSymbolParameter[] = [];
    for (const parameter of symbolInfo.parameters) {
        const parameterType = getType(parameter.valueType, location);

        parameters.push({
            parameterType,
            name: parameter.name,
            isReference: parameter.isReference,
            location: parameter.location,
        });
    }

    if (existingSymbol !== undefined) {
        if (existingSymbol.type !== 'FUNCTION_SYMBOL') {
            throw new ParseError(`Não é possível redeclarar símbolo com nome "${symbolInfo.name}".`, location);
        } else {
            const args = parameters.map<ExpressionResult>(p => ({ resultType: p.parameterType, value: null }));
            const overload = findOverload(existingSymbol, args, true);
            if (overload !== null) {
                const typesFormatted = parameters.map(p => formatType(p.parameterType)).join(', '); // duplicate
                throw new ParseError(`Sobrecarga para função de nome "${symbolInfo.name}" já existente com parâmetros de tipos (${typesFormatted}).`, location);
            }
        }
    }
    
    const returnType = getType(symbolInfo.returnType || 'vazio', location);
    
    if (returnType === null) {
        throw new ParseError(`Tipo inválido ${symbolInfo.returnType}.`, location);
    }

    const symbol = existingSymbol || {
        type: 'FUNCTION_SYMBOL',
        overloads: [],
        parentContext: context,
    };

    symbol.overloads.push({
        returnType,
        parameters,
        instructions: symbolInfo.instructions,
        location,
    });

    if (existingSymbol === undefined) {
        context.symbols[symbolInfo.name] = symbol;
    }

    return { returnType, parameters, symbol };
}

export function declareLibrarySymbol(
    context: Context,
    symbolInfo: {
        libraryName: string,
        giveName: string | undefined,
    },
    location: Code['location'],
): {
    start: (i?: ControllableInterface) => Promise<void>,
    stop: () => Promise<void>,
} {
    const symbolName = symbolInfo.giveName || symbolInfo.libraryName;
    
    if (!libraries[symbolInfo.libraryName]) {
        throw new ParseError(`Não há nenhuma biblioteca com nome "${symbolInfo.libraryName}".`, location);
    }

    const existingSymbol = findSymbol(symbolName, context);

    if (existingSymbol) {
        throw new ParseError(`Não é possível importar biblioteca com nome "${symbolName}" pois já há um símbolo com esse nome no contexto atual.`, location);
    }

    const { lib, start, stop } = libraries[symbolInfo.libraryName]();
    context.symbols[symbolName] = { ...lib, location, libraryName: symbolInfo.libraryName };

    return { start, stop };
}

export async function fillFromAST(
    ast: Program,
    context: DescendibleContext,
): Promise<{ problems: Problem[] }> {
    const problems: Problem[] = [];

    const visitor = createVisitor({ type: 'STATIC', context });

    for (const key of Object.keys(visitor)) {
        const oldVisitorAtKey = visitor[key];
        visitor[key] = async (instruction: Expression) => {
            let result: ExpressionResult = { resultType: 'VAZIO', value: undefined };
            await captureProblem(async () => {
                result = await oldVisitorAtKey(instruction);
            }, problem => problems.push(problem));
            return result;
        };
    }

    await visit(visitor, ast);

    return { problems };
}

export function findContext(
    context: DescendibleContext,
    lineNumber: number,
    column: number,
): Context {
    if (context.location) {
        if (containsPosition(context.location, { lineNumber, column })) {
            for (const child of context.childrenContext) {
                const foundContext = findContext(child, lineNumber, column);
                if (foundContext !== context) {
                    return foundContext;
                }
            }

            return context;
        }
    }
    return context.parentContext || context;
}

export function findOverload(fn: FunctionSymbol, args: ExpressionResult[], exact = false): { overload: FunctionSymbolOverload, args: ExpressionResult[] } | null {
    function findMatch(convertFn: (value: ExpressionResult, toType: Type) => ExpressionResult | null): { overload: FunctionSymbolOverload, args: ExpressionResult[] } | null {
        overloadLoop: for (const overload of fn.overloads) {
            const callArgs: ExpressionResult[] = [];
            let i = 0;
            while (i < overload.parameters.length) {
                const parameter = overload.parameters[i];
    
                if (i >= args.length) continue overloadLoop;
    
                if (parameter.isVariadic) {
                    if (i !== overload.parameters.length - 1) continue overloadLoop;
    
                    const argumentValue: any[] = [];
    
                    while (i < args.length) {
                        const argument = args[i];
                        i++;
    
                        const convertedValue = convertFn(argument, parameter.parameterType);
    
                        if (convertedValue !== null) {
                            argumentValue.push(convertedValue.value);
                        } else {
                            continue overloadLoop;
                        }
                    }
    
                    callArgs.push({
                        resultType: { type: 'ARRAY', valueType: parameter.parameterType, size: null },
                        value: argumentValue,
                    });
                } else {
                    const argument = args[i];
                    i++;
    
                    const convertedValue = convertFn(argument, parameter.parameterType);
    
                    if (convertedValue !== null) {
                        callArgs.push(convertedValue);
                    } else {
                        continue overloadLoop;
                    }
                }
            }
    
            if (i !== args.length) continue;
    
            return { overload, args: callArgs };
        }

        return null;
    }

    const perfectMatch = findMatch(ifAssigned);
    if (perfectMatch !== null) return perfectMatch;

    if (!exact) {
        const convertibleMatch = findMatch(convertResult);
        if (convertibleMatch !== null) return convertibleMatch;
    }

    return null;
}

export function ifAssigned(value: ExpressionResult, toType: Type): ExpressionResult | null {
    const type = value.resultType;
    if (isTypeUnion(toType) && isTypePrimitive(type)) {
        if (!toType.valueTypes.includes(type)) return null;
        return {
            resultType: toType,
            value: {
                originalType: type,
                value: value.value,
            },
        };
    }
    if (isTypeUnion(type) && isTypePrimitive(toType)) {
        const unionValue: ValueSymbolUnionValue = value.value;
        if (unionValue.originalType !== toType) return null;
        return { resultType: toType, value: unionValue.value };
    }
    if (isTypePrimitive(type) && isTypePrimitive(toType)) {
        if (type !== toType) return null;
        return value;
    }
    if (isTypeArray(type) && isTypeArray(toType)) {
        const sizeAssignable = toType.size === null || type.size === toType.size;
        if (!sizeAssignable) return null;
        const ifAssignedValueType = ifAssigned({ resultType: type.valueType, value: null }, toType.valueType);
        if (ifAssignedValueType === null) return null;
        return value;
    }
    if (isTypeFunction(type) && isTypeFunction(toType)) {
        const returnAssignable = ifAssigned({ resultType: toType, value: null }, type) !== null;
        if (!returnAssignable) return null;
        for (let i = 0; i < type.parameters.length; i++) {
            const parameter = type.parameters[i];
            const toParameter = toType.parameters[i];
            if (!!parameter.isReference !== !!toParameter.isReference) return null;
            if (!!parameter.isVariadic !== !!toParameter.isVariadic) return null;
            const parameterAssignable = ifAssigned({ resultType: parameter.parameterType, value: null }, toParameter.parameterType) !== null;
            if (!parameterAssignable) return null;
        }
        return value;
    }

    return null;
}

export function convertResult(result: ExpressionResult, toType: Type): ExpressionResult | null {
    const ifAssignedValue = ifAssigned(result, toType);
    if (ifAssignedValue !== null) return ifAssignedValue;
    if (result.resultType === 'INTEIRO' && toType === 'REAL') return { resultType: 'REAL', value: result.value };
    if (result.resultType === 'REAL' && toType === 'INTEIRO') return { resultType: 'INTEIRO', value: Math.floor(result.value) };
    if (result.resultType === 'INTEIRO' && toType === 'CADEIA') return { resultType: 'CADEIA', value: String(result.value) };
    if (result.resultType === 'REAL' && toType === 'CADEIA') return { resultType: 'CADEIA', value: result.value.toFixed(2) };
    return null;
}

export function findSymbol(name: string, context: Context): ContextSymbol | undefined {
    let symbol: ContextSymbol | undefined = context.symbols[name];
    while (symbol === undefined && context.parentContext !== undefined) {
        context = context.parentContext;
        symbol = context.symbols[name];
    }
    return symbol;
}

export function defaultValue(type: Type): ValueSymbol['value'] {
    switch (type) {
        case 'CADEIA': return '';
        case 'CARACTER': return 'a';
        case 'INTEIRO': return 0;
        case 'REAL': return 0;
        case 'LOGICO': return false;
        case 'VAZIO': return undefined;
    }

    if (type.type === 'ARRAY' && type.size !== null) {
        return Array.from(new Array(type.size)).map(v => defaultValue(type.valueType));
    }

    throw new Error(`NÃO HÁ VALOR PADRÃO PARA TIPO "${formatType(type)}".`);
}

export function containsPosition(location: Code['location'], position: { lineNumber: number, column: number }): boolean {
    if (!location) return false;
    if (position.lineNumber < location.start.line || position.lineNumber > location.end.line) {
        return false;
    }
    if (position.lineNumber === location.start.line && position.column < location.start.column) {
        return false;
    }
    if (position.lineNumber === location.end.line && position.column > location.end.column) {
        return false;
    }
    return true;
}