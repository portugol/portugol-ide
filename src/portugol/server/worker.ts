import { provideInterface } from '../../worker-bridge/server';
import StaticAnalyser, { IStaticAnalyser } from './static-analyser';

provideInterface<IStaticAnalyser<'local'>>('language-server', new StaticAnalyser());