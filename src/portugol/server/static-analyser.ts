import { RemoteObject, RemoteReturn } from '../../worker-bridge/types';
import { Code, Expression, Program } from '../ast';
import { containsPosition, Context, ContextSymbol, DescendibleContext, fillFromAST, findContext, findSymbol, formatType, LibrarySymbol } from '../context';
import { createDefaultContext } from '../globals';
import libraries from '../libraries';
import parser from '../parser.pegjs';

type Diagnostic = {
    severity: 'ERROR' | 'WARNING',
    description: string,
    location: Code['location'],
};

type CompletionItem = {
    triggerSuggestion?: boolean,
    description?: string,
    snippet: string,
    documentation: string | undefined,
    label: string,
    type: 'FUNCTION' | 'LIBRARY' | 'VALUE' | 'SNIPPET',
    replaceLocation?: Code['location'],
};

type SymbolsInformation = {
    descriptions: string[],
    location?: Code['location'],
};

type SymbolsDefinitions = { 
    selectionLocation: Code['location'],
    definitions: Array<Code['location']>,
};

export interface IStaticAnalyser<B extends 'local' | 'remote'> extends RemoteObject<IStaticAnalyser<B>, B> {
    update(code: string): RemoteReturn<void, B>;
    getDiagnostics(): RemoteReturn<Diagnostic[], B>;
    getCompletion(line: string, lineNumber: number, column: number): RemoteReturn<CompletionItem[], B>;
    getSymbolsInformation(lineNumber: number, column: number): RemoteReturn<SymbolsInformation | null, B>;
    getSymbolsDefinitions(
        lineNumber: number,
        column: number,
    ): RemoteReturn<SymbolsDefinitions | null, B>;
}

export default class StaticAnalyser implements IStaticAnalyser<'local'> {
    private ast: Program;
    private context: DescendibleContext | undefined;
    private diagnostics: Diagnostic[] = [];

    private nextUpdateId = 0;
    private latestUpdatedId = 0;
    update(code: string): void {
        const program = parser.parse(code);
        // we are not going to call the methods, so we can fake the interface
        const context: DescendibleContext = { 
            ...(createDefaultContext({} as any)), 
            childrenContext: [], 
            location: program.location,
        };
        const updateId = this.nextUpdateId++;
        fillFromAST(program, context).then(({ problems }) => {
            // take only the latest and greatest
            if (updateId < this.latestUpdatedId) return;
            this.latestUpdatedId = updateId;
            this.ast = program;
            this.context = context;
            this.diagnostics = problems.map<Diagnostic>(({ description, location }) => ({ 
                severity: 'ERROR', description, location 
            }));
        });
    }

    getDiagnostics(): Diagnostic[] {
        return this.diagnostics;
    }

    getCompletion(
        line: string,
        lineNumber: number,
        column: number,
    ): CompletionItem[] {
        if (!this.context) {
            throw new Error('Trying to call method before updating.');
        }

        let searchNamespace = '';
        let searchWord = '';
        const stillInWordRegex = /[a-zA-Z0-9_]/;
        let isDotAccess = false;
        let isIncludeLibrary = false;
        let scanningColumn = column - 2;
        while (scanningColumn >= 0 && stillInWordRegex.test(line[scanningColumn])) {
            searchWord = line[scanningColumn] + searchWord;
            scanningColumn--;
        }
        if (line[scanningColumn] === '.') {
            isDotAccess = true;
            scanningColumn--;
            while (scanningColumn >= 0 && stillInWordRegex.test(line[scanningColumn])) {
                searchNamespace = line[scanningColumn] + searchNamespace;
                scanningColumn--;
            }
        }
        if (!isDotAccess 
            && line.slice(scanningColumn - 17, scanningColumn) === 'inclua biblioteca'
        ) {
            isIncludeLibrary = true;
        }

        let foundContext: Context;

        if (isIncludeLibrary) {
            const librariesSymbols: { [key: string]: LibrarySymbol } = {};
            for (const libraryName of Object.keys(libraries)) {
                librariesSymbols[libraryName] = libraries[libraryName]().lib;
            }
            foundContext = { symbols: librariesSymbols };
        } else {
            foundContext = findContext(this.context, lineNumber, column);
        }

        let librarySymbol: LibrarySymbol | null = null;
        if (isDotAccess) {
            const possibleLibrarySymbol = findSymbol(searchNamespace, foundContext);

            if (!possibleLibrarySymbol || possibleLibrarySymbol.type !== 'LIBRARY_SYMBOL') {
                return [];
            }

            librarySymbol = possibleLibrarySymbol;
        }

        const foundSymbols: Array<[string, ContextSymbol]> = [];
        let searchContext: Context | undefined = isDotAccess
            ? { symbols: librarySymbol!.symbols }
            : foundContext;
        while (searchContext !== undefined) {
            const symbolsNames = Object.keys(searchContext.symbols);
            const possibleNames = symbolsNames.filter(name => name.startsWith(searchWord));

            const namesSymbolsPairs = possibleNames.map<[string, ContextSymbol]>(name => [name, searchContext!.symbols[name]]);

            foundSymbols.push(...namesSymbolsPairs);

            searchContext = searchContext.parentContext;
        }

        const snippets: CompletionItem[] = [];

        return foundSymbols.map<CompletionItem[]>(([name, symbol]) => {
            if (symbol.type === 'VALUE_SYMBOL') {
                const description = this.describeSymbol(name, symbol);
                return [{
                    type: 'VALUE',
                    description,
                    label: name,
                    snippet: name,
                    documentation: symbol.description,
                }];
            } else if (symbol.type === 'LIBRARY_SYMBOL') {
                const description = this.describeSymbol(name, symbol);
                return [{
                    type: 'LIBRARY',
                    description,
                    label: name,
                    snippet: isIncludeLibrary ? name : name + '.',
                    documentation: undefined,
                    triggerSuggestion: !isIncludeLibrary,
                }];
            } else {
                return symbol.overloads.map<CompletionItem>((overload, index) => {
                    const description = this.describeSymbol(name, symbol, index);
                    let snippetIndex = 1;
                    let snippet = name + '(';
                    snippet += overload.parameters
                        .map(parameter => {
                            let parameterName = parameter.name;
                            if (parameter.isVariadic) {
                                parameterName = parameterName + '...';
                            }
                            if (parameter.isReference) {
                                parameterName = '&' + parameterName;
                            }
                            return '${' + snippetIndex++ + ':' + parameterName + '}';
                        })
                        .join(', ');
                    snippet += ')';

                    return {
                        type: 'FUNCTION',
                        description,
                        label: name,
                        snippet,
                        documentation: overload.description,
                    };
                });
            }
        }).reduce((c, a) => c.concat(a), []).concat(snippets);
    }

    getSymbolsInformation(
        lineNumber: number,
        column: number,
    ): SymbolsInformation | null {
        const locationAndSymbols = this.getSymbols(lineNumber, column);

        if (!locationAndSymbols) return null;

        const { location, symbols } = locationAndSymbols;

        const descriptions: string[] = [];

        for (const { symbol, name } of symbols ) {
            if (symbol.type === 'FUNCTION_SYMBOL') {
                descriptions.push(...symbol.overloads.map((o, i) => this.describeSymbol(name, symbol, i)));
                continue;
            }
            descriptions.push(this.describeSymbol(name, symbol));
        }

        return {
            descriptions,
            location,
        };
    }

    getSymbolsDefinitions(
        lineNumber: number,
        column: number,
    ): SymbolsDefinitions | null {
        const symbolsAndLocation = this.getSymbols(lineNumber, column);

        if (!symbolsAndLocation) return null;

        const { symbols, location } = symbolsAndLocation;

        const symbolsDefinitions: SymbolsDefinitions = {
            selectionLocation: location,
            definitions: [],
        };

        for (const { symbol } of symbols) {
            if (symbol.type === 'FUNCTION_SYMBOL') {
                symbolsDefinitions.definitions.push(...symbol.overloads.map(o => o.location));
            } else {
                symbolsDefinitions.definitions.push(symbol.location);
            }
        }

        return symbolsDefinitions;
    }

    private getSymbols(
        lineNumber: number,
        column: number,
    ): { location: Code['location'], symbols: Array<{ name: string, symbol: ContextSymbol }> } | null {
        if (!this.context) {
            throw new Error('Trying to call method before updating.');
        }

        const instructionAndParentInstruction = this.findInstruction(lineNumber, column);

        if (!instructionAndParentInstruction) return null;

        const { instruction, parentInstruction } = instructionAndParentInstruction;

        const namesAndContext = this.getSymbolsNames(instruction, parentInstruction);

        if (!namesAndContext) return null;

        const { names, context: contextName } = namesAndContext;

        let context: Context | undefined;

        if (contextName) {
            const library = findSymbol(contextName, this.context);

            if (!library || library.type !== 'LIBRARY_SYMBOL') {
                return { location: instruction.location, symbols: [] };
            }

            context = { symbols: library.symbols };
        } else {
            context = findContext(this.context, lineNumber, column);
        }

        if (!context) return null;

        const symbols = names
            .map(name => ({ name, symbol: findSymbol(name, context!) }))
            .filter(s => !!s.symbol) as Array<{ name: string, symbol: ContextSymbol}>;

        return {
            location: instruction.location,
            symbols,
        };
    }

    private describeSymbol(name: string, symbol: ContextSymbol, overloadIndex = 0): string {
        if (symbol.type === 'VALUE_SYMBOL') {
            return (symbol.isConstant ? 'const ' : '') + formatType(symbol.valueType) + ' ' + name;
        } else if (symbol.type === 'LIBRARY_SYMBOL') {
            let description = 'biblioteca ';
            if (name !== symbol.libraryName && symbol.libraryName !== undefined) {
                description += `(${symbol.libraryName}) `;
            }
            description += name;
            return description;
        } else {
            const overload = symbol.overloads[overloadIndex!];
            let description = 'funcao ' + formatType(overload.returnType) + ' ' + name + '(';
            description += overload.parameters
                .map(parameter => {
                    let parameterDescription = formatType(parameter.parameterType) + ' ' + parameter.name;
                    if (parameter.isVariadic) {
                        parameterDescription = parameterDescription + '...';
                    }
                    if (parameter.isReference) {
                        parameterDescription = '&' + parameterDescription;
                    }
                    return parameterDescription;
                })
                .join(', ');
            description += ')';

            return description;
        }
    }

    private findInstruction(
        lineNumber: number,
        column: number,
        instruction: Expression = this.ast,
    ): { instruction: Expression, parentInstruction: Expression | undefined } | null {
        const position = { lineNumber, column };

        if (!instruction.location || !containsPosition(instruction.location, position)) return null;

        let subInstructions: Expression[] = [];

        switch (instruction.type) {
            case 'PROGRAM':
            case 'DECLARE_FUNCTION': 
                subInstructions = instruction.instructions; 
                break;
            case 'DECLARE_SYMBOLS':
                subInstructions = instruction.declarations
                    .filter(i => i.value !== null)
                    .map(i => i.value!);
                break;
            case 'IF_STATEMENT': 
                subInstructions = [
                    instruction.condition,
                    ...instruction.consequent,
                    ...(instruction.alternate || []),
                ];
                break;
            case 'SWITCH_STATEMENT':
                subInstructions = [
                    instruction.value,
                    ...instruction.cases
                        .map(c => [
                            ...c.instructions, 
                            ...(c.value !== undefined ? [c.value] : [])
                        ])
                        .reduce((c, a) => [...c, ...a], []),
                ];     
                break;
            case 'WHILE_STATEMENT':
            case 'DO_WHILE_STATEMENT':
                subInstructions = [
                    instruction.test,
                    ...instruction.instructions
                ];
                break;
            case 'FOR_STATEMENT':
                subInstructions = [
                    instruction.init,
                    instruction.test,
                    instruction.update,
                    ...instruction.instructions,
                ];
                break;
            case 'ADDITION':
            case 'MULTIPLICATION':
            case 'SUBTRACTION':
            case 'DIVISION':
            case 'MODULUS':
            case 'EQUALS':
            case 'NOT_EQUALS':
            case 'LOWER_THAN':
            case 'GREATER_THAN':
            case 'LOWER_THAN_OR_EQUAL_TO':
            case 'GREATER_THAN_OR_EQUAL_TO':
            case 'LOGICAL_AND':
            case 'LOGICAL_OR':
                subInstructions = [instruction.lhs, instruction.rhs];
                break;
            case 'MINUS_UNARY':
            case 'LOGICAL_NOT':
                subInstructions = [instruction.operand];
                break;
            case 'CALL_FUNCTION':
                subInstructions = instruction.arguments;
                break;
            case 'ARRAY_LITERAL':
                subInstructions = instruction.value;
                break;
            case 'RETURN_VALUE':
                subInstructions = [instruction.value];
                break;
            case 'ACCESS_OBJECT_ATTRIBUTE':
                subInstructions = [instruction.attribute];
                break;
        }

        for (const subInstruction of subInstructions) {
            if (containsPosition(subInstruction.location, position)) {
                const foundSubInstruction = this.findInstruction(lineNumber, column, subInstruction);
                if (foundSubInstruction) {
                    return foundSubInstruction;
                }
                return { instruction: subInstruction, parentInstruction: instruction };
            }
        }

        return null;
    }

    private getSymbolsNames(
        instruction: Expression, 
        parentInstruction: Expression | undefined,
    ): { names: string[], context?: string } | undefined {
        const context = parentInstruction && parentInstruction.type === 'ACCESS_OBJECT_ATTRIBUTE'
            ? parentInstruction.name
            : undefined;
        let names: string[];

        switch (instruction.type) {
            case 'ADDITION_AND_WRITE_TO_SYMBOL':
            case 'DIVISION_AND_WRITE_TO_SYMBOL':
            case 'MULTIPLICATION_AND_WRITE_TO_SYMBOL':
            case 'SUBTRACTION_AND_WRITE_TO_SYMBOL':
            case 'WRITE_TO_SYMBOL':
            case 'READ_SYMBOL':
            case 'CALL_FUNCTION': 
            case 'DECLARE_FUNCTION':
            case 'ACCESS_OBJECT_ATTRIBUTE':
                names = [instruction.name];
                break;
            case 'INCLUDE_LIBRARY':
                names = [instruction.giveName || instruction.libraryName];
                break;
            case 'DECLARE_SYMBOLS':
                names = instruction.declarations.map(d => d.name);
                break;
            default:
                return undefined;
        }

        return { context, names };
    }
}