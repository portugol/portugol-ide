/* --------------------------------------------------------------------------------------------
 * Copyright (c) 2018 TypeFox GmbH (http://www.typefox.io). All rights reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 * ------------------------------------------------------------------------------------------ */
import * as monaco from 'monaco-editor';
import StaticAnalyserWorker from 'worker-loader!./worker';
import portugolMonarch from '../../portugol.monarch';
import { connectToInterface } from '../../worker-bridge/client';
import { IStaticAnalyser } from './static-analyser';

export function install(editor: monaco.editor.IStandaloneCodeEditor): monaco.IDisposable {
    const disposables: monaco.IDisposable[] = [];
    let disposable: monaco.IDisposable;

    const LANGUAGE_ID = 'portugol';
    
    if (!monaco.languages.getLanguages().find(l => l.id === LANGUAGE_ID)) {
        monaco.languages.register({
            id: LANGUAGE_ID,
            extensions: ['.por'],
            aliases: ['PORTUGOL', 'portugol'],
            mimetypes: ['application/portugol'],
        });
    }
    
    disposable = monaco.languages.setMonarchTokensProvider(LANGUAGE_ID, portugolMonarch as any);
    disposables.push(disposable);
    monaco.editor.setModelLanguage(editor.getModel()!, LANGUAGE_ID);

    const interfaceConnection = connectToInterface<IStaticAnalyser<'remote'>>('language-server', new StaticAnalyserWorker());
    const analyser = interfaceConnection.remote;
    disposables.push(interfaceConnection);

    disposable = monaco.languages.registerCompletionItemProvider(LANGUAGE_ID, {
        triggerCharacters: ['.'],

        provideCompletionItems: async (model, position, token) => {
            const line = model.getLineContent(position.lineNumber);
            const completion = await analyser.getCompletion(line, position.lineNumber, position.column);
            return completion.map<monaco.languages.CompletionItem>(item => ({
                command: item.triggerSuggestion 
                    ? { id: 'editor.action.triggerSuggest', title: 'Trigger suggestion' } 
                    : undefined,
                detail: item.description,
                label: item.label,
                insertText: { value: item.snippet },
                kind: item.type === 'FUNCTION' 
                    ? monaco.languages.CompletionItemKind.Function 
                    : item.type === 'LIBRARY'
                    ? monaco.languages.CompletionItemKind.Module
                    : item.type === 'SNIPPET'
                    ? monaco.languages.CompletionItemKind.Snippet
                    : monaco.languages.CompletionItemKind.Value,    
                documentation: item.documentation,
                range: item.replaceLocation && new monaco.Range(
                    item.replaceLocation.start.line,
                    item.replaceLocation.start.column,
                    item.replaceLocation.end.line,
                    item.replaceLocation.end.column,
                ),
            }));
        },
    });
    disposables.push(disposable);

    disposable = monaco.languages.registerHoverProvider(LANGUAGE_ID, {
        provideHover: async (model, position, token) => {
            const symbolsInformation = await analyser.getSymbolsInformation(position.lineNumber, position.column);

            if (!symbolsInformation) return { contents: [] };

            const range: monaco.IRange | undefined = symbolsInformation.location
                && {
                    startLineNumber: symbolsInformation.location.start.line,
                    endLineNumber: symbolsInformation.location.end.line,
                    startColumn: symbolsInformation.location.start.column,
                    endColumn: symbolsInformation.location.end.column,
                };

            return {
                range,
                contents: symbolsInformation.descriptions.map(d => ({ value: d })),
            };
        }
    });
    disposables.push(disposable);

    disposable = monaco.languages.registerDefinitionProvider(LANGUAGE_ID, {
        provideDefinition: async (model, position, token) => {
            const selectionLocationAndSymbolsLocations = await analyser.getSymbolsDefinitions(position.lineNumber, position.column);

            if (!selectionLocationAndSymbolsLocations) return [];

            const { definitions, selectionLocation } = selectionLocationAndSymbolsLocations;

            const origin = selectionLocation && new monaco.Range(
                selectionLocation!.start.line,
                selectionLocation!.start.column,
                selectionLocation!.end.line,
                selectionLocation!.end.column,
            );

            const providedDefinitions: monaco.languages.DefinitionLink[] = [];

            for (const definition of definitions) {
                if (!definition) continue;

                const range = new monaco.Range(
                    definition.start.line,
                    definition.start.column,
                    definition.end.line,
                    definition.end.column,
                );

                if (origin && origin.containsRange(range)) {
                    // remove the ones that are itself
                    continue;
                }

                providedDefinitions.push({
                    uri: model.uri,
                    range,
                    origin,
                });
            }

            return providedDefinitions;
        },
    });
    disposables.push(disposable);

    disposable = editor.getModel().onDidChangeContent((event) => {
        validate();
    });
    disposables.push(disposable);

    let pendingValidation: NodeJS.Immediate | null = null;

    async function validate() {
        if (pendingValidation !== null) {
            clearImmediate(pendingValidation);
            pendingValidation = null;
        }

        // batching
        pendingValidation = setImmediate(async () => {
            const editorText = editor.getModel().getValue();
            try {
                await analyser.update(editorText);

                const diagnostics = await analyser.getDiagnostics();
                monaco.editor.setModelMarkers(
                    editor.getModel(), 
                    'default',
                    diagnostics.map(diagnostic => ({
                        startLineNumber: diagnostic.location!.start.line,
                        endLineNumber: diagnostic.location!.end.line,
                        startColumn: diagnostic.location!.start.column,
                        endColumn: diagnostic.location!.end.column,
                        message: diagnostic.description,
                        severity: diagnostic.severity === 'ERROR' 
                            ? monaco.MarkerSeverity.Error
                            : monaco.MarkerSeverity.Warning
                    })),
                );
            } catch (error) { /* ignore errors :D */ }
        });
    }

    return { dispose: () => disposables.forEach(d => d.dispose()) };
}
