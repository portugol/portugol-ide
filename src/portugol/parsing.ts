import { Code } from './ast';

export class ParseError {
    constructor(
        public message: string,
        public location: Code['location'],
    ) { }
}