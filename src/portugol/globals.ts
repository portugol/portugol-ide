import { Context, ExpressionResult, ValueSymbolUnionValue } from './context';
import { ConsoleInterface } from './interpreter/interfaces';

export function createDefaultContext(consoleInterface: ConsoleInterface): Context {
    return { 
        symbols: {
            escreva: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        returnType: 'VAZIO',
                        parameters: [
                            {
                                parameterType: {
                                    type: 'UNION',
                                    valueTypes: ['INTEIRO', 'REAL', 'CADEIA', 'CARACTER', 'LOGICO'],
                                },
                                isVariadic: true,
                                name: 'valor',
                            },
                        ],
                        instructions: async (args): Promise<ExpressionResult> => {
                            const valores: ValueSymbolUnionValue[] = args[0].value;
                            for (const valor of valores) {
                                let printValue = '';
                                switch (valor.originalType) {
                                    case 'CADEIA': printValue = valor.value as string; break;
                                    case 'CARACTER': printValue = valor.value as string; break;
                                    case 'LOGICO':
                                        printValue = valor.value as boolean === true
                                            ? 'verdadeiro'
                                            : 'falso';
                                        break;
                                    case 'INTEIRO': printValue = String(valor.value as number); break;
                                    case 'REAL':
                                        let stringValue = String(valor.value);
                                        const shouldAddPoint = stringValue.indexOf('.') === -1;
                                        if (shouldAddPoint) {
                                            stringValue = `${stringValue}.0`;
                                        }
                                        printValue = stringValue;
                                        break;
                                }
                                await consoleInterface.write(printValue);
                            }
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            leia: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        returnType: 'VAZIO',
                        parameters: [{
                            isReference: true,
                            isVariadic: true,
                            parameterType: { type: 'UNION', valueTypes: ['INTEIRO', 'CADEIA', 'CARACTER', 'REAL', 'LOGICO'] },
                            name: 'valor',
                        }],
                        instructions: async (args, updateReferenceValue): Promise<ExpressionResult> => {
                            const valores: ValueSymbolUnionValue[] = args[0].value;
                            for (let i = 0; i < valores.length; i++) {
                                const valor = valores[i];
                                const expectedType = valor.originalType;
                                const read = await consoleInterface.read();
                                
                                let newValue: any;

                                switch (expectedType) {
                                    case 'CADEIA': 
                                        newValue = read;
                                        break;
                                    case 'CARACTER': 
                                        newValue = read[0] || '0';
                                        break;
                                    case 'INTEIRO': 
                                    case 'REAL': 
                                        newValue = Number(read) || 0;
                                        break;
                                    case 'LOGICO': 
                                        newValue = ['y', 'Y', 'v', 'V', 's', 'S', '1', 1, 'verdadeiro'].includes(read);
                                        break;
                                }

                                await updateReferenceValue[0]![i]({
                                    resultType: expectedType,
                                    value: newValue,
                                });
                            }

                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
            limpa: {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        returnType: 'VAZIO',
                        parameters: [],
                        instructions: async (): Promise<ExpressionResult> => {
                            await consoleInterface.clear();
                            return { resultType: 'VAZIO', value: undefined };
                        },
                    },
                ],
            },
        } 
    };
}