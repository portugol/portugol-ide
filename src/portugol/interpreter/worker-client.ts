import InterpreterWorker from 'worker-loader!./worker';
import { connectToInterface } from '../../worker-bridge/client';
import { provideInterface } from '../../worker-bridge/server';
import { Program } from '../ast';
import { IInterpreter } from './';
import { ConsoleInterface, createGraphicInterface, createSoundInterface, keyboardInterface, mouseInterface } from './interfaces';
import { createFilesInterface } from './interfaces/files';

export function interprete(
    program: Program,
    consoleInterface: ConsoleInterface,
    resourcesAccessor: {
        getFileUrl: (path: string) => string | null,
        getFile: (path: string) => File | null,
        deleteFile: (path: string) => void,
        createOrReplaceFile: (file: File) => void,
        listFiles: (ofTypes?: string[]) => string[],
        selectFile: (acceptTypes?: string[]) => string,
    },
    onError: (error: Error) => void,
    onFinish: () => void,
): { stop: () => Promise<void> } {
    const worker = new InterpreterWorker();

    const graphicInterface = createGraphicInterface(resourcesAccessor.getFileUrl);
    const soundInterface = createSoundInterface(resourcesAccessor.getFileUrl);
    const filesInterface = createFilesInterface({
        createOrReplaceFile: resourcesAccessor.createOrReplaceFile,
        deleteFile: resourcesAccessor.deleteFile,
        getFile: resourcesAccessor.getFile,
        listFiles: resourcesAccessor.listFiles,
        selectFile: resourcesAccessor.selectFile,
    });

    const disposables = [
        provideInterface('console-interface', consoleInterface, worker),
        provideInterface('graphic-interface', graphicInterface, worker),
        provideInterface('keyboard-interface', keyboardInterface, worker),
        provideInterface('sound-interface', soundInterface, worker),
        provideInterface('files-interface', filesInterface, worker),
        provideInterface('mouse-interface', mouseInterface, worker),
    ];

    const { remote: interpreter, dispose: disposeInterpreter } = connectToInterface<IInterpreter<'remote'>>('interpreter', worker);
    disposables.push({ dispose: disposeInterpreter });

    interpreter.interprete(program)
        .then(() => {
            stop();
        })
        .catch((error) => {
            onError(error);
            stop();
        });

    async function stop() {
        worker.terminate();
        await graphicInterface._controlStop();
        await keyboardInterface._controlStop();
        await soundInterface._controlStop();
        await filesInterface._controlStop();
        await mouseInterface._controlStop();
        disposables.forEach(d => d.dispose());
        onFinish();
    }

    return { stop };
}