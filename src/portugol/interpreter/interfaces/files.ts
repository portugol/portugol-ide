import { FilesInterface } from '.';

type AccessMode = { READ: 0, WRITE: 1, APPEND: 2 };

const ACCESS_MODE: AccessMode = {
    READ: 0,
    WRITE: 1,
    APPEND: 2,
};

type OpenFile = {
    file: EditableFile,
    currentLine: number,
    accessMode: AccessMode['READ'] | AccessMode['WRITE'] | AccessMode['APPEND'],
};

class EditableFile {
    static async create(copyContentFrom: File): Promise<EditableFile> {
        return new Promise(resolve => {
            const reader = new FileReader();
            reader.onload = () => {
                const file = new EditableFile(copyContentFrom.name, reader.result as string, {
                    type: copyContentFrom.type,
                    lastModified: copyContentFrom.lastModified,
                });
                resolve(file);
            };
            reader.readAsText(copyContentFrom);
        });
    }

    lastModified: number = new Date().getTime();
    type: string = '';
    name: string;
    private lines: string[] = [];

    constructor(name: string, content?: string, options?: { type?: string, lastModified?: number }) {
        this.name = name;
        if (options) {
            this.type = options.type || this.type;
            this.lastModified = options.lastModified || this.lastModified;
        }
        if (content) {
            this.lines = content.split('\n');
        }
    }

    append(content: string) {
        const lines = content.split('\n');
        this.lines[this.lines.length - 1] = this.lines[this.lines.length - 1] + lines[0];
        this.lines.push(...lines.slice(1));
    }

    write(content: string) {
        this.lines = content.split('\n');
    }

    read(line?: number): string {
        if (line !== undefined) {
            if (line > 0 && line < this.lines.length) {
                return this.lines[line];
            } else {
                return '';
            }
        }
        return this.lines.join('\n');
    }

    replace(search: string, replaceWith: string, firstOccurrence: boolean) {
        let text = this.read();
        if (firstOccurrence) {
            text = text.replace(search, replaceWith);
        } else {
            text = text.split(search).join(replaceWith);
        }
        this.write(text);
    }

    isEnd(line: number) {
        return line === this.lines.length;
    }

    toFile(): File {
        return new File([this.lines.join('\n')], this.name, {
            type: this.type,
            lastModified: this.lastModified,
        });
    }
}

export const createFilesInterface = (
    fileSystem: {
        getFile: (name: string) => File | null,
        deleteFile: (name: string) => void,
        listFiles: (ofTypes?: string[]) => string[],
        createOrReplaceFile: (file: File) => void,
        selectFile: (acceptTypes?: string[]) => string,
    },
): FilesInterface => {
    const editableFiles: { [name: string]: EditableFile } = {};
    const openFiles: Array<OpenFile | null> = [];

    async function getEditableFile(name: string): Promise<EditableFile | null> {
        let editableFile = editableFiles[name];
        if (editableFile) return editableFile;
        const file = fileSystem.getFile(name);
        if (!file) return null;
        editableFile = await EditableFile.create(file);
        editableFiles[name] = editableFile;
        return editableFile;
    }

    function getOpenFile(address: number): OpenFile {
        const file = openFiles[address];
        if (!file) {
            throw new Error('Não foi possível encontrar um arquivo no endereço específicado.');
        }
        return file;
    }

    const filesInterface: FilesInterface = {
        _controlStart: async () => undefined,
        _controlStop: async () => undefined,
        closeFile: async (address) => {
            const openFile = openFiles[address];
            if (openFile !== null) {
                const file = openFile.file.toFile();
                fileSystem.createOrReplaceFile(file);
                openFiles[address] = null;

                // if the file is not being used, remove it from memory
                let stillBeingUsed = false;
                for (const oFile of openFiles) {
                    if (oFile !== null && oFile.file.name === openFile.file.name) {
                        stillBeingUsed = true;
                        break;
                    }
                }
                if (!stillBeingUsed) {
                    delete editableFiles[openFile.file.name];
                }
            }
        },
        createFolder: async (path) => {
            throw new Error('Não implementado.');
        },
        deleteFile: async (path) => {
            fileSystem.deleteFile(path);
        },
        fileExists: async (path) => {
            return fileSystem.getFile(path) !== null;
        },
        isFileEnd: async (address) => {
            const openFile = getOpenFile(address);
            return openFile.file.isEnd(openFile.currentLine);
        },
        listFiles: async (path) => {
            if (path !== '' && path !== '/') {
                throw new Error('É possível apenas listar arquivos da raíz');
            }
            return fileSystem.listFiles();
        },
        listFilesByType: async (path, types) => {
            if (path !== '' && path !== '/') {
                throw new Error('É possível apenas listar arquivos da raíz');
            }
            return fileSystem.listFiles(types);
        },
        listFolders: async (path) => {
            throw new Error('Não implementado.');
        },
        openFile: async (path, accessMode) => {
            if (accessMode !== ACCESS_MODE.READ
                && accessMode !== ACCESS_MODE.WRITE
                && accessMode !== ACCESS_MODE.APPEND 
            ) {
                throw new Error('Tipo de acesso informado é inválido.');
            }

            let file = await getEditableFile(path);
            if (file === null) {
                if (accessMode !== ACCESS_MODE.WRITE) {
                    throw new Error(`Não foi possível encontrar o arquivo com nome "${path}".`);
                }
                file = new EditableFile(path, '');
                editableFiles[path] = file;
            }
            if (accessMode === ACCESS_MODE.WRITE) {
                file.write('');
            }
            openFiles.push({
                currentLine: 0,
                file,
                accessMode,
            });
            return openFiles.length - 1;
        },
        readLine: async (address) => {
            const openFile = getOpenFile(address);
            if (openFile.accessMode !== ACCESS_MODE.READ) {
                throw new Error('Não é possível ler pois o arquivo não foi aberto em modo de leitura.');
            }
            return openFile.file.read(openFile.currentLine++);
        },
        replaceText: async (address, searchText, replacementText, firstOccurrence) => {
            const fileAddress = await filesInterface.openFile(address, ACCESS_MODE.APPEND);
            const openFile = getOpenFile(fileAddress);
            openFile.file.replace(searchText, replacementText, firstOccurrence);
            await filesInterface.closeFile(fileAddress);
        },
        selectFile: async (supportedFormats, acceptAllFiles) => {
            const acceptedTypes: string[] | undefined = acceptAllFiles
                ? undefined
                : supportedFormats;
            return fileSystem.selectFile(acceptedTypes);
        },
        writeLine: async (line, address) => {
            const openFile = getOpenFile(address);
            if (openFile.accessMode === ACCESS_MODE.READ) {
                throw new Error('Não é possível escrever pois o arquivo foi aberto em modo de leitura.');
            }
            openFile.file.append(line + '\n');
        },
    };

    return filesInterface;
};