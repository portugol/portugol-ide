import { RemoteObject, RemoteReturn } from '../../../worker-bridge/types';

export interface ControllableInterface<B extends 'local' | 'remote' = 'remote'> extends RemoteObject<ControllableInterface<B>, B> {
    _controlStart: () => RemoteReturn<void, B>;
    _controlStop: () => RemoteReturn<void, B>;
}

export type ConsoleInterface = {
    write: (value: string) => Promise<void>,
    read: () => Promise<string>,
    clear: () => Promise<void>,
};

export type GraphicInterface = ControllableInterface & {
    windowWidth: () => Promise<number>,
    windowHeight: () => Promise<number>,
    setWindowDimensions: (width: number, height: number) => Promise<void>,
    setTextStyle: (italic: boolean, bold: boolean, undelined: boolean) => Promise<void>,
    setTextFont: (name: string) => Promise<void>,
    setTextSize: (size: number) => Promise<void>,
    startGraphicMode: (keepVisible: boolean) => Promise<void>,
    stopGraphicMode: () => Promise<void>,
    setColor: (color: number) => Promise<void>,
    drawEllipse: (x: number, y: number, width: number, height: number, fill: boolean) => Promise<void>,
    drawLine: (x1: number, y1: number, x2: number, y2: number) => Promise<void>,
    drawPoint: (x: number, y: number) => Promise<void>,
    drawRectangle: (x: number, y: number, width: number, height: number, roundCorners: boolean, fill: boolean) => Promise<void>,
    drawText: (x: number, y: number, text: string) => Promise<void>,
    textWidth: (text: string) => Promise<number>,
    textHeight: (text: string) => Promise<number>,
    clear: () => Promise<void>,
    render: () => Promise<void>,
    setWindowTitle: (title: string) => Promise<void>,
    loadImage: (path: string) => Promise<number>,
    freeImage: (address: number) => Promise<void>,
    drawImage: (x: number, y: number, address: number) => Promise<void>,
    drawImageSlice: (x: number, y: number, xi: number, yi: number, width: number, height: number, address: number) => Promise<void>,
    setOpacity: (opacity: number) => Promise<void>,
    imageWidth: (address: number) => Promise<number>,
    imageHeight: (address: number) => Promise<number>,
    transformImage: (address: number, flipHorizontally: boolean, flipVertically: boolean, rotation: number, transparentColor: number) => Promise<number>,
};

export type SoundInterface = ControllableInterface & {
    loadSound: (path: string) => Promise<number>,
    setCurrentPositionMusic: (address: number, ms: number) => Promise<void>,
    setVolume: (volume: number) => Promise<void>,
    setPlayingVolume: (address: number, volume: number) => Promise<void>,
    stopSound: (address: number) => Promise<void>,
    freeSound: (address: number) => Promise<void>,
    getCurrentPositionMusic: (address: number) => Promise<number>,
    getMusicSize: (address: number) => Promise<number>,
    getVolume: () => Promise<number>,
    getPlayingVolume: (address: number) => Promise<number>,
    pauseSound: (address: number) => Promise<void>,
    playSound: (address: number, repeat: boolean) => Promise<number>,
};

export type KeyboardInterface = ControllableInterface & {
    isSomeKeyDown: () => Promise<boolean>,
    readKey: () => Promise<number>,
    isKeyDown: (key: number) => Promise<boolean>,
};

export type FilesInterface = ControllableInterface & {
    openFile: (path: string, accessMode: number) => Promise<number>,
    deleteFile: (path: string) => Promise<void>,
    fileExists: (path: string) => Promise<boolean>,
    createFolder: (path: string) => Promise<void>,
    writeLine: (line: string, address: number) => Promise<void>,
    closeFile: (address: number) => Promise<void>,
    isFileEnd: (address: number) => Promise<boolean>,
    readLine: (address: number) => Promise<string>,
    listFiles: (path: string) => Promise<string[]>,
    listFilesByType: (path: string, types: string[]) => Promise<string[]>,
    listFolders: (path: string) => Promise<string[]>,
    selectFile: (supportedFormats: string[], acceptAllFiles: boolean) => Promise<string>,
    replaceText: (address: string, searchText: string, replacementText: string, firstOccurrence: boolean) => Promise<void>,
};

export interface MouseInterface<B extends 'local' | 'remote'> extends RemoteObject<ControllableInterface<B>, B> {
    anyButtonDown: () => RemoteReturn<boolean, B>;
    isButtonDown: (button: number) => RemoteReturn<boolean, B>;
    showCursor: () => RemoteReturn<void, B>;
    readButton: () => RemoteReturn<Promise<number>, B>;
    hideCursor: () => RemoteReturn<void, B>;
    getXPosition: () => RemoteReturn<number, B>;
    getYPosition: () => RemoteReturn<number, B>;
}

type LoadedImage = {
    transform: {
        flipHorizontally: boolean,
        flipVertically: boolean,
        rotation: number,
        transparentColor: number,
    },
    raw: HTMLImageElement,
};

let windowWidth = 0;
let windowHeight = 0;
let windowTitle = 'Título';
let drawingColor = 0x000000;
let drawingOpacity = 1;
const textStyle = { bold: false, italic: false, size: 15, name: 'Arial' };
const loadedImages: Array<LoadedImage | null> = [];
let libWindow: Window | null = null;
let tempCanvas: HTMLCanvasElement | null = null;
let tempContext: CanvasRenderingContext2D | null = null;
let canvas: HTMLCanvasElement | null = null;
let context: CanvasRenderingContext2D | null = null;
let listeningKeys = false;

function createFontFromTextStyle(): string {
    let font = `${textStyle.size}px ${textStyle.name}`;
    if (textStyle.bold) {
        font = `bold ${font}`;
    }
    if (textStyle.italic) {
        font = `italic ${font}`;
    }
    return font;
}

let keysDown: { [key: number]: true } = {};
const waitingForKey: Array<(key: number) => void> = [];

function keyDownListener(event: KeyboardEvent) {
    event.preventDefault();
    keysDown[event.keyCode] = true;
    for (const fn of waitingForKey) {
        fn(event.keyCode);
    }
    waitingForKey.length = 0;
}

function keyUpListener(event: KeyboardEvent) {
    event.preventDefault();
    delete keysDown[event.keyCode];
}

export const createGraphicInterface = (
    getFileUrl: (path: string) => string | null,
): GraphicInterface => {
    const graphicInterface: GraphicInterface = {
        _controlStart: async () => { /* */ },
        _controlStop: async () => graphicInterface.stopGraphicMode(),
        windowWidth: async () => windowWidth,
        windowHeight: async () => windowHeight,
        setWindowDimensions: async (width, height) => {
            const addressBarHeight = 40;
            windowWidth = width;
            windowHeight = height + addressBarHeight;
    
            if (libWindow) {
                libWindow.resizeTo(windowWidth, windowHeight);
                canvas!.width = windowWidth;
                canvas!.height = windowHeight;
                tempCanvas!.width = windowWidth;
                tempCanvas!.height = windowHeight;
            }
        },
        setTextStyle: async (italic, bold, undelined) => {
            textStyle.bold = bold;
            textStyle.italic = italic;
            // textStyle.undelined = undelined;
    
            if (libWindow) {
                tempContext!.font = createFontFromTextStyle();
            }
        },
        setTextFont: async (name) => {
            textStyle.name = name;
    
            if (libWindow) {
                tempContext!.font = createFontFromTextStyle();
            }
        },
        setTextSize: async (size) => {
            textStyle.size = size;
    
            if (libWindow) {
                tempContext!.font = createFontFromTextStyle();
            }
        },
        startGraphicMode: async (keepVisible) => {
            if (libWindow) {
                graphicInterface.stopGraphicMode();
            }
    
            libWindow = window.open(undefined, undefined, `width=${windowWidth},height=${windowHeight + 20}`);
    
            if (!libWindow) { throw new Error('Não foi possível abrir uma nova janela.'); }
            if (listeningKeys) {
                libWindow.addEventListener('keydown', keyDownListener);
                libWindow.addEventListener('keyup', keyUpListener);
            }
    
            const doc = libWindow.document;
            doc.body.style.margin = '0';
            doc.body.style.overflow = 'hidden';
            doc.title = windowTitle;
            canvas = doc.createElement('canvas');
            context = canvas.getContext('2d');
            tempCanvas = doc.createElement('canvas');
            tempContext = tempCanvas.getContext('2d');
            tempContext!.font = createFontFromTextStyle();
            tempContext!.fillStyle = `#${drawingColor.toString(16)}`;
            tempContext!.strokeStyle = `#${drawingColor.toString(16)}`;
            tempContext!.globalAlpha = drawingOpacity;
    
            canvas.width = windowWidth;
            canvas.height = windowHeight;
            tempCanvas.width = windowWidth;
            tempCanvas.height = windowHeight;
    
            doc.body.appendChild(canvas);

            if (listeningMouse) {
                canvas.addEventListener('mousedown', mouseDownListener);
                canvas.addEventListener('mouseup', mouseUpListener);
                canvas.addEventListener('contextmenu', contextMenuListener);
                if (cursorHidden) {
                    canvas.style.cursor = 'hidden';
                }
            }
        },
        stopGraphicMode: async () => {
            loadedImages.length = 0;
            windowWidth = 0;
            windowHeight = 0;
            windowTitle = 'Título';
            drawingColor = 0x000000;
            drawingOpacity = 1;
            textStyle.bold = false;
            textStyle.italic = false;
            textStyle.size = 15;
            textStyle.name = 'Arial';

            if (libWindow) {
                if (listeningKeys) {
                    libWindow.removeEventListener('keydown', keyDownListener);
                    libWindow.removeEventListener('keyup', keyUpListener);
                }
                if (listeningMouse) {
                    canvas!.removeEventListener('mousedown', mouseDownListener);
                    canvas!.removeEventListener('mouseup', mouseUpListener);
                    canvas!.removeEventListener('contextmenu', contextMenuListener);
                    if (cursorHidden) {
                        delete canvas!.style.cursor;
                    }
                }
                context = null;
                canvas = null;
                libWindow.close();
                libWindow = null;
            }
        },
        setColor: async (color) => {
            drawingColor = color;
    
            if (libWindow) {
                tempContext!.fillStyle = `#${drawingColor.toString(16)}`;
                tempContext!.strokeStyle = `#${drawingColor.toString(16)}`;
            }
        },
        drawEllipse: async (x, y, width, height, fill) => {
            if (libWindow) {
                tempContext!.beginPath();
                if (width < 0) {
                    x -= width;
                    width *= -1;
                }
                if (height < 0) {
                    y -= height;
                    height *= -1;
                }
                tempContext!.ellipse(x + width / 2, y + height / 2, width, height, 0, 0, 2 * Math.PI);
                if (fill) {
                    tempContext!.fill();
                } else {
                    tempContext!.stroke();
                }
            }
        },
        drawLine: async (x1, y1, x2, y2) => {
            if (libWindow) {
                tempContext!.beginPath();
                tempContext!.moveTo(x1, y1);
                tempContext!.lineTo(x2, y2);
                tempContext!.stroke();
            }
        },
        drawPoint: async (x, y) => {
            if (libWindow) {
                tempContext!.beginPath();
                tempContext!.rect(x, y, 1, 1);
                tempContext!.stroke();
            }
        },
        drawRectangle: async (x, y, width, height, roundCorners, fill) => {
            if (libWindow) {
                tempContext!.beginPath();
                tempContext!.rect(x, y, width, height);
                if (fill) {
                    tempContext!.fill();
                } else {
                    tempContext!.stroke();
                }
            }
        },
        drawText: async (x, y, text) => {
            if (libWindow) {
                tempContext!.fillText(text, x, y);
            }
        },
        textWidth: async (text) => {
            let textWidth = 0;
    
            if (libWindow) {
                textWidth = tempContext!.measureText(text).width;
            }
    
            return textWidth;
        },
        textHeight: async () => {
            // imprecise but works
            return textStyle.size;
        },
        clear: async () => {
            if (libWindow) {
                tempContext!.fillRect(0, 0, windowWidth, windowHeight);
            }
        },
        render: async () => {
            if (libWindow) {
                context!.drawImage(tempCanvas!, 0, 0);
            }
        },
        setWindowTitle: async (title) => {
            windowTitle = title;
    
            if (libWindow) {
                libWindow.document.title = windowTitle;
            }
        },
        loadImage: async (path) => {
            const image = new Image();
    
            const fileUrl = getFileUrl(path);

            if (!fileUrl) {
                throw new Error(`Não há nenhuma imagem com caminho "${path}".`);
            }

            // TODO: what to do if the file is not an image? D:
            image.src = fileUrl;
            
            const loadedImage: LoadedImage = {
                raw: image,
                transform: {
                    flipHorizontally: false,
                    flipVertically: false,
                    rotation: 0,
                    transparentColor: 0x00000000,
                },
            };

            loadedImages.push(loadedImage);
    
            return loadedImages.length - 1;
        },
        drawImage: async (x, y, address) => {
            if (libWindow) {
                const image = loadedImages[address];
                if (!image) {
                    throw new Error(`Imagem com endereço "${address}" não carregada.`);
                }
                // TODO: image.transform.transparentColor
                tempContext!.save();
                tempContext!.translate(x, y);
                tempContext!.rotate(Math.PI / 180 * image.transform.rotation);
                tempContext!.scale(
                    image.transform.flipHorizontally ? -1 : 1, 
                    image.transform.flipVertically ? -1 : 1,
                );
                tempContext!.drawImage(
                    image.raw,
                    0,
                    0,
                );
                tempContext!.restore();
            }
        },
        drawImageSlice: async (x, y, xi, yi, width, height, address) => {
            if (libWindow) {
                const image = loadedImages[address];
                if (!image) {
                    throw new Error(`Imagem com endereço "${address}" não carregada.`);
                }
                // TODO: DUPLICATE TRANSFORMATION
                // TODO: image.transform.transparentColor
                tempContext!.save();
                tempContext!.translate(x, y);
                tempContext!.rotate(Math.PI / 180 * image.transform.rotation);
                tempContext!.scale(
                    image.transform.flipHorizontally ? -1 : 1, 
                    image.transform.flipVertically ? -1 : 1,
                );
                tempContext!.drawImage(
                    image.raw,
                    xi,
                    yi,
                    width,
                    height,
                    0,
                    0,
                    width,
                    height,
                );
                tempContext!.restore();
            }
        },
        freeImage: async (address) => {
            const image = loadedImages[address];
            if (!image) {
                return;
            }
            delete image.raw.src;
            loadedImages[address] = null;
        },
        transformImage: async (address, flipHorizontally, flipVertically, rotation, transparentColor) => {
            const image = loadedImages[address];
            if (!image) {
                throw new Error(`Imagem com endereço "${address}" não carregada.`);
            }
            const transformedImage: LoadedImage = {
                ...image,
                transform: { 
                    flipHorizontally,
                    flipVertically,
                    rotation: image.transform.rotation + rotation,
                    transparentColor,
                },
            };
            loadedImages.push(transformedImage);
            return loadedImages.length - 1;
        },
        setOpacity: async (opacity) => {
            drawingOpacity = opacity / 255;

            if (libWindow) {
                tempContext!.globalAlpha = drawingOpacity;
            }
        },
        imageHeight: async (address) => {
            const image = loadedImages[address];
            if (!image) {
                throw new Error(`Imagem com endereço "${address}" não carregada.`);
            }
            return image.raw.height;
        },
        imageWidth: async (address) => {
            const image = loadedImages[address];
            if (!image) {
                throw new Error(`Imagem com endereço "${address}" não carregada.`);
            }
            return image.raw.width;
        },
    };

    return graphicInterface;
};

export const keyboardInterface: KeyboardInterface = {
    _controlStart: async () => {
        window.addEventListener('keydown', keyDownListener);
        window.addEventListener('keyup', keyUpListener);

        if (libWindow) {
            libWindow.addEventListener('keydown', keyDownListener);
            libWindow.addEventListener('keyup', keyUpListener);
        }

        listeningKeys = true;
    },
    _controlStop: async () => {
        window.removeEventListener('keydown', keyDownListener);
        window.removeEventListener('keyup', keyUpListener);

        if (libWindow) {
            libWindow.removeEventListener('keydown', keyDownListener);
            libWindow.removeEventListener('keyup', keyUpListener);
        }

        listeningKeys = false;
        keysDown = {};
    },
    isKeyDown: async (key) => !!keysDown[key],
    isSomeKeyDown: async () => Object.keys(keysDown).length > 0,
    readKey: async () => new Promise<number>(resolve => waitingForKey.push(resolve)),
};

let cursorHidden = false;
let listeningMouse = false;
const mousePosition = { x: 0, y: 0 };
let mouseButtonsDown: { [button: number]: true } = {};
const waitingForMouseButton: Array<(button: number) => void> = [];

function mouseDownListener(event: MouseEvent) {
    event.preventDefault();
    mouseButtonsDown[event.button] = true;
    mousePosition.x = event.clientX;
    mousePosition.y = event.clientY;
    for (const fn of waitingForMouseButton) {
        fn(event.button);
    }
    waitingForMouseButton.length = 0;
}

function mouseUpListener(event: MouseEvent) {
    event.preventDefault();
    delete mouseButtonsDown[event.button];
}

function contextMenuListener(event: Event) {
    event.preventDefault();
}

export const mouseInterface: MouseInterface<'local'> = {
    _controlStart: () => {
        listeningMouse = true;
        if (libWindow) {
            canvas!.addEventListener('mousedown', mouseDownListener);
            canvas!.addEventListener('mouseup', mouseUpListener);
            canvas!.addEventListener('contextmenu', contextMenuListener);
        }
    },
    _controlStop: () => {
        if (libWindow) {
            canvas!.removeEventListener('mousedown', mouseDownListener);
            canvas!.removeEventListener('mouseup', mouseUpListener);
            canvas!.removeEventListener('contextmenu', contextMenuListener);
            if (cursorHidden) {
                delete canvas!.style.cursor;
            }
        }
        cursorHidden = false;
        listeningMouse = false;
        mousePosition.x = 0;
        mousePosition.y = 0;
        mouseButtonsDown = {};
        waitingForMouseButton.length = 0;
    },
    anyButtonDown: () => Object.keys(mouseButtonsDown).length > 0,
    getXPosition: () => mousePosition.x,
    getYPosition: () => mousePosition.y,
    showCursor: () => {
        cursorHidden = false;
        if (libWindow) {
            delete canvas!.style.cursor;
        }
    },
    hideCursor: () => {
        cursorHidden = true;
        if (libWindow) {
            canvas!.style.cursor = 'hidden';
        }
    },
    isButtonDown: (button: number) => !!mouseButtonsDown[button],
    readButton: () => new Promise<number>(resolve => waitingForMouseButton.push(resolve)),
};

export const createSoundInterface = (
    getFileUrl: (path: string) => string | null,
): SoundInterface => {
    type PlayingAudio = {
        raw: HTMLAudioElement,
        volume: number,
    };

    let globalVolume = 1;
    const loadedAudios: Array<HTMLAudioElement | null> = [];
    const playingAudios: Array<PlayingAudio | null> = [];

    function getLoadedAudio(address: number): HTMLAudioElement {
        const audio = loadedAudios[address];

        if (!audio) {
            throw new Error(`Não há áudio carregado no dado endereço.`);
        }
        
        return audio;
    }

    function getPlayingAudio(address: number): PlayingAudio {
        const audio = playingAudios[address];

        if (!audio) {
            throw new Error(`Não há áudio em reprodução no dado endereço.`);
        }

        return audio;
    }

    const soundInterface: SoundInterface = {
        _controlStart: async () => undefined,
        _controlStop: async () => {
            for (const audio of playingAudios) {
                if (audio !== null) {
                    audio.raw.pause();
                    delete audio.raw.onended;
                    delete audio.raw.src;
                }
            }
            for (const audio of loadedAudios) {
                if (audio !== null) {
                    delete audio.onended;
                    delete audio.src;
                }
            }
            playingAudios.length = 0;
            loadedAudios.length = 0;
        },
        freeSound: async (address) => {
            const audio = loadedAudios[address];

            if (!audio) { return; }

            delete audio.onended;
            delete audio.src;
            loadedAudios[address] = null;
        },
        getCurrentPositionMusic: async (address) => {
            return Math.floor(getPlayingAudio(address).raw.currentTime * 1000);
        },
        getMusicSize: async (address) => {
            return Math.floor(getLoadedAudio(address).duration * 1000);
        },
        getPlayingVolume: async (address) => {
            return Math.floor(getPlayingAudio(address).volume * 100);
        },
        getVolume: async () => {
            return Math.floor(globalVolume * 100);
        },
        loadSound: async (path) => {
            const filePath = getFileUrl(path);

            if (filePath === null) {
                throw new Error(`Não foi encontrado áudio com nome "${path}".`);
            }

            const audio = new Audio(filePath);
            loadedAudios.push(audio);
            return loadedAudios.length - 1;
        },
        pauseSound: async (address) => {
            soundInterface.stopSound(address);
        },
        playSound: async (address, repeat) => {
            const audio = getLoadedAudio(address).cloneNode() as HTMLAudioElement;
            audio.play();
            if (repeat) {
                audio.onended = () => {
                    audio.currentTime = 0;
                    audio.play();
                };
            }
            playingAudios.push({
                raw: audio,
                volume: 1,
            });
            return playingAudios.length - 1;
        },
        setCurrentPositionMusic: async (address, ms) => {
            const audio = getPlayingAudio(address);
            audio.raw.currentTime = ms / 1000;
        },
        setPlayingVolume: async (address, volume) => {
            const audio = getPlayingAudio(address);
            audio.volume = volume / 100;
            audio.raw.volume = audio.volume * globalVolume;
        },
        setVolume: async (volume) => {
            globalVolume = volume / 100;
            for (const audio of playingAudios) {
                if (audio !== null) {
                    audio.raw.volume = audio.volume * globalVolume;
                }
            }
        },
        stopSound: async (address) => {
            const audio = getPlayingAudio(address);
            audio.raw.pause();
            delete audio.raw.onended;
            delete audio.raw.src;
            playingAudios[address] = null;
        },
    };

    return soundInterface;
};