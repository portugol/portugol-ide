import { connectToInterface } from '../../worker-bridge/client';
import { RemoteObject, RemoteReturn } from '../../worker-bridge/types';
import { Program } from '../ast';
import { visit } from '../visitor';
import { ConsoleInterface, FilesInterface, GraphicInterface, KeyboardInterface, MouseInterface, SoundInterface } from './interfaces';
import { createVisitor } from './visitor';

export interface IInterpreter<B extends 'local' | 'remote'> extends RemoteObject<IInterpreter<B>, B> {
    interprete(program: Program): RemoteReturn<Promise<void>, B>;
}

export default class Interpreter implements IInterpreter<'local'> {
    async interprete(program: Program): Promise<void> {
        const { remote: console, dispose: disposeConsole } = connectToInterface<ConsoleInterface>('console-interface');
        const { remote: files, dispose: disposeFiles } = connectToInterface<FilesInterface>('files-interface');
        const { remote: graphic, dispose: disposeGraphic } = connectToInterface<GraphicInterface>('graphic-interface');
        const { remote: keyboard, dispose: disposeKeyboard } = connectToInterface<KeyboardInterface>('keyboard-interface');
        const { remote: sound, dispose: disposeSound } = connectToInterface<SoundInterface>('sound-interface');
        const { remote: mouse, dispose: disposeMouse } = connectToInterface<MouseInterface<'remote'>>('mouse-interface');

        const interfaces = {
            console,
            files,
            graphic,
            keyboard,
            sound,
            mouse,
        };

        const visitor = createVisitor({ type: 'DYNAMIC', interfaces });
        await visit(visitor, program);

        disposeConsole();
        disposeFiles();
        disposeGraphic();
        disposeKeyboard();
        disposeSound();
        disposeMouse();
    }
}