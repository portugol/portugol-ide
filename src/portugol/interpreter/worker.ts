import { provideInterface } from '../../worker-bridge/server';
import Interpreter, { IInterpreter } from './';

provideInterface<IInterpreter<'local'>>('interpreter', new Interpreter());