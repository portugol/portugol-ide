import {
    BreakStatement,
    CallFunction,
    Code,
    ContinueStatement,
    Expression,
    SwitchStatement,
} from '../ast';
import { 
    Context,
    convertResult,
    declareFunctionSymbol,
    declareLibrarySymbol,
    declareValueSymbol,
    defaultValue,
    DescendibleContext,
    ExpressionResult,
    findOverload,
    findSymbol,
    formatType,
    FunctionSymbol,
    FunctionSymbolOverload,
    FunctionSymbolOverloadInstructionsNativeUpdateReferenceValue,
    getType,
    ifAssigned,
} from '../context';
import { createDefaultContext } from '../globals';
import { ParseError } from '../parsing';
import { isTypeUnion, Type, TypeArray } from '../types';
import { visit, Visitor } from '../visitor';
import { ConsoleInterface, ControllableInterface, FilesInterface, GraphicInterface, KeyboardInterface, MouseInterface, SoundInterface } from './interfaces';

const COMPARABLE_TYPES: Type[] = ['REAL', 'INTEIRO'];
const EQUATABLE_TYPES: Type[] = ['INTEIRO', 'REAL', 'CADEIA', 'LOGICO', 'CARACTER'];

export type Interfaces = {
    console: ConsoleInterface,
    graphic: GraphicInterface,
    keyboard: KeyboardInterface,
    sound: SoundInterface,
    files: FilesInterface,
    mouse: MouseInterface<'remote'>,
};

type VisitorParamsStatic = {
    type: 'STATIC',
    context: DescendibleContext,
};

type VisitorParamsDynamic = {
    type: 'DYNAMIC',
    interfaces: Interfaces,
};

type VisitorParams = VisitorParamsStatic | VisitorParamsDynamic;

export function createVisitor(params: VisitorParams): Visitor {
    let stopLibraries: Array<() => Promise<void>>;
    let contextManager: ContextManager;
    let flowControl: FlowControl;

    async function withSymbolAtIndexPath(name: string, indexPath: Expression[], code: Code) {
        const symbol = findSymbol(name, contextManager.get());

        if (symbol === undefined) {
            throw new ParseError(`Não foi possível encontrar símbolo com nome "${name}".`, code.location);
        }
        if (symbol.type !== 'VALUE_SYMBOL') {
            throw new ParseError(`Não é possível ler valor de símbolo que não seja de valor.`, code.location);
        }

        let reference: { value: any } | any[] = symbol;
        let value = symbol.value;
        let resultType = symbol.valueType;
        let lastIndex: number | undefined;

        for (const index of indexPath) {
            if (typeof resultType === 'string' || resultType.type !== 'ARRAY') {
                throw new ParseError(`"${name}" não é um vetor.`, code.location);
            }
            const indexResult = await visit(visitor, index);

            if (indexResult.resultType !== 'INTEIRO') {
                throw new ParseError(`O acesso ao índice de um vetor deve ser feito utilizando um valor do tipo "${formatType("INTEIRO")}".`, index.location);
            }

            if (indexResult.value < 0 || indexResult.value >= (value as any[]).length) {
                throw new ParseError(`O índice "${indexResult.value}" está fora dos limites do vetor.`, index.location);
            }

            reference = value as any[];
            value = (value as any[])[indexResult.value];
            lastIndex = indexResult.value;
            resultType = resultType.valueType;
            name = `${name}[${indexResult.value}]`;
        }

        return {
            setValue: (newValue: ExpressionResult) => {
                if (symbol.isConstant) {
                    throw new ParseError('Não é possível atribuir a uma constante.', code.location);
                }

                const convertedNewValue = convertResult(newValue, resultType);
                if (!convertedNewValue) {
                    throw new ParseError(`Não é possível atribuir um valor de tipo "${formatType(newValue.resultType)}" para tipo "${formatType(resultType)}".`, code.location);
                }
                if (lastIndex !== undefined) {
                    (reference as any[])[lastIndex] = convertedNewValue.value;
                } else {
                    (reference as { value: any }).value = convertedNewValue.value;
                }     
            },
            getValue: (allowNotInitialized: boolean = false): ExpressionResult => {
                const result = { value, resultType };
                if (!allowNotInitialized) {
                    ensureHasValue(result, name, code.location);
                }
                return result;
            },
        };
    }

    async function runInstructions(instructions: Expression[], isRoot: boolean = false) {
        // STRICT MODE IS A RETROCOMPATIBLE EXECUTION WAY, WHERE THE FUNCTION "INICIO"
        // IS CALLED IN THE BEGGINING AND ON THE TOP LEVEL OF THE PROGRAM ONLY
        // DECLARATIONS CAN EXIST
        let isStrictMode = false;
        if (isRoot) {
            isStrictMode = instructions.findIndex(instruction => {
                return instruction.type === 'DECLARE_FUNCTION' 
                    && instruction.name === 'inicio'
                    && (!instruction.returnType || instruction.returnType === 'vazio')
                    && instruction.parameters.length === 0;
            }) !== -1;
        }

        for (const instruction of instructions) {
            if (instruction.type !== 'DECLARE_FUNCTION') continue;

            await visit(visitor, instruction);
        }

        for (const instruction of instructions) {
            if (instruction.type === 'DECLARE_FUNCTION') continue;

            if ((flowControl.returned() !== undefined && params.type === 'DYNAMIC') 
                || flowControl.broke() !== undefined 
                || flowControl.continued() !== undefined) {
                if (isRoot) {
                    throw new ParseError(`Não é possível executar ação na raíz do programa.`, instruction.location);
                }
                break;
            }

            if (isStrictMode) {
                const allowedInstructions: Array<Expression['type']> = [
                    'DECLARE_SYMBOLS', 'INCLUDE_LIBRARY'
                ];
                if (allowedInstructions.indexOf(instruction.type) === -1) {
                    throw new ParseError(`Não é possível executar a instrução na raíz do programa.`, instruction.location);
                }
            }

            await visit(visitor, instruction);
        }

        if (params.type === 'STATIC') {
            for (const instruction of instructions) {
                if (instruction.type !== 'DECLARE_FUNCTION') continue;

                const symbol = contextManager.get().symbols[instruction.name] as FunctionSymbol;
                const args = instruction.parameters.map<Expression>(p => {
                    const parameterType = getType(p.valueType, p.location);
                    return {
                        type: '_RESOLVED',
                        result: { resultType: parameterType, value: parameterType },
                    };
                });

                await callFunction(symbol, {
                    type: 'CALL_FUNCTION',
                    arguments: args,
                    location: instruction.location,
                    name: instruction.name,
                }, true);
            }
        }

        if (isStrictMode && params.type === 'DYNAMIC') {
            const symbol = contextManager.get().symbols.inicio as FunctionSymbol;
            await callFunction(symbol, { type: 'CALL_FUNCTION', name: 'inicio', arguments: [] });
        }
    }

    async function callFunction(symbol: FunctionSymbol, call: CallFunction, staticCall = false): Promise<ExpressionResult> {
        function outsideCallContext(run: () => void, location: Code['location']): void;
        function outsideCallContext(run: () => Promise<void>, location: Code['location']): Promise<void>;
        async function outsideCallContext(run: () => Promise<void> | void, location: Code['location']) {
            const fnContext = contextManager.get();
            contextManager.popContext(contextManager.popCall());
            await run();
            contextManager.pushCall();
            contextManager.pushContext(location, fnContext);
        }

        function createUpdateReferenceValue(argument: Expression) {
            if (argument.type !== 'READ_SYMBOL') {
                throw new ParseError(`Esperado referência a um símbolo, porém recebido uma expressão.`, argument.location);
            }

            return async (newValue: ExpressionResult) => {
                await outsideCallContext(async () => {
                    const withSymbol = await withSymbolAtIndexPath(argument.name, [], argument);
                    withSymbol.setValue(newValue);
                }, argument.location);
            };
        }

        let argumentsResults: ExpressionResult[] = [];

        // BEFORE KNOWING IF ANY OF THE ARGUMENTS SHOULD BE A REFERENCE, ITS NECESSARY TO KNOW THE
        // OVERLOAD THAT WILL BE USED, BUT TO KNOW THE OVERLOAD THE GIVEN ARGUMENTS TYPES MUST BE KNOWN
        // BUT NOT THEIR VALUES, SO, IT SHOULD BE GOOD IF AN EXPRESSION COULD BE RESOLVED WITHOUT VALUE
        // JUST TO KNOW ITS TYPE, IN A SYNCHRONOUS WAY, SO THAT IT IS LESS COMPUTATIONALLY INTENSE.
        // BECAUSE WE MAY NOT USE THE VALUES IF THE PARAMETER IS A REFERENCE AND THE GIVEN ARGUMENT
        // IS NOT A SYMBOL ;D
        for (const argument of call.arguments) {
            // READ SYMBOL ALLOWING IT TO BE NOT INITIALIZED, SO WE CAN PASS IT AS REFERENCE PARAMETER
            if (argument.type === 'READ_SYMBOL') {
                const withSymbol = await withSymbolAtIndexPath(argument.name, argument.indexPath, argument);
                argumentsResults.push(withSymbol.getValue(true));
            } else {
                argumentsResults.push(await visit(visitor, argument));
            }
        }

        if (params.type === 'DYNAMIC' || staticCall) {
            contextManager.pushCall();
            contextManager.pushContext(undefined, symbol.parentContext);
        }

        const foundOverload = findOverload(symbol, argumentsResults); // check if is convertible somehow D:
        
        if (foundOverload === null) {
            const typesFormatted = argumentsResults.map(ar => formatType(ar.resultType)).join(', ');
            throw new ParseError(`Não foi possível encontrar sobrecarga para função de nome "${call.name || '[SEM NOME]'}" que contivesse parâmetros de tipos (${typesFormatted}).`, call.location);
        }

        const overload = foundOverload.overload;
        argumentsResults = foundOverload.args;

        if (staticCall) {
            (contextManager.get() as DescendibleContext).location = overload.location;
        }

        let result: ExpressionResult | null = null;

        if (typeof overload.instructions === 'function') {
            if (params.type === 'STATIC') {
                result = { resultType: overload.returnType, value: defaultValue(overload.returnType) };
            } else {
                const updateReferenceValue
                    : FunctionSymbolOverloadInstructionsNativeUpdateReferenceValue[] 
                    | FunctionSymbolOverloadInstructionsNativeUpdateReferenceValue[][] 
                    = new Array(overload.parameters.length);
                for (let i = 0; i < overload.parameters.length; i++) {
                    const parameter = overload.parameters[i];
                    if (parameter.isReference) {
                        if (parameter.isVariadic) {
                            if (!updateReferenceValue[i]) { updateReferenceValue[i] = []; }

                            for (let j = 0; j < argumentsResults[i].value.length; j++) {
                                updateReferenceValue[i]![j] = createUpdateReferenceValue(call.arguments[i + j]);
                            }
                        } else {
                            updateReferenceValue[i] = createUpdateReferenceValue(call.arguments[i]);
                        }
                    } else {
                        // SINCE WE DID NOT VERIFY IF THE VALUE WAS INITIALIZED BEFORE WE NEED TO VERIFY NOW
                        if (parameter.isVariadic) {
                            for (let j = 0; j < argumentsResults[i].value.length; j++) {
                                const argument = call.arguments[i + j];
                                if (argument.type === 'READ_SYMBOL') {
                                    // unwrap variadic value
                                    const argumentResult = argumentsResults[i];
                                    const resultType = (argumentResult.resultType as TypeArray).valueType;
                                    const value = argumentResult.value[j];
                                    ensureHasValue({ resultType, value }, argument.name, argument.location);
                                }
                            }
                        } else {
                            const argument = call.arguments[i];
                            if (argument.type === 'READ_SYMBOL') {
                                ensureHasValue(argumentsResults[i], argument.name, argument.location);
                            }
                        }
                    }
                }
                result = await Promise.resolve(overload.instructions(argumentsResults, updateReferenceValue));   
            }
        } else {
            for (let i = 0; i < argumentsResults.length; i++) {
                const parameter = overload.parameters[i];

                if (parameter.isReference) {
                    const argument = call.arguments[i];
                    if (argument.type !== 'READ_SYMBOL') {
                        throw new ParseError(`Esperado referência a um símbolo, porém recebido uma expressão.`, argument.location);
                    }
                    if (params.type === 'DYNAMIC' || staticCall) {
                        const fnContext = contextManager.get();
                        const location = params.type === 'STATIC'
                            ? (fnContext as DescendibleContext).location
                            : undefined;
                        outsideCallContext(() => {
                            // We know the symbol exists because we have to resolve the expression before finding the function overload
                            fnContext.symbols[parameter.name] = findSymbol(argument.name, contextManager.get())!;
                        }, location);
                    }
                } else {
                    if (params.type === 'DYNAMIC' || staticCall) {
                        const argument = argumentsResults[i];
                        declareValueSymbol(
                            contextManager.get(),
                            {
                                isConstant: false,
                                value: argument,
                                name: parameter.name,
                                valueType: parameter.parameterType,
                            },
                            parameter.location,
                        );
                    }
                }
            }

            if (params.type === 'DYNAMIC' || staticCall) {
                contextManager.get().returnType = overload.returnType;

                await runInstructions(overload.instructions);
    
                const returned = flowControl.returned();
                if (returned) {
                    result = returned;
                    flowControl.reset();
                }
            } else {
                result = { 
                    resultType: overload.returnType,
                    value: defaultValue(overload.returnType)
                };
            }
        }

        if (params.type === 'DYNAMIC' || staticCall) {
            contextManager.popContext(contextManager.popCall());
        }

        if (!result) {
            if (overload.returnType === 'VAZIO') {
                result = { resultType: 'VAZIO', value: undefined };
            } else {
                throw new ParseError(`Função com retorno diferente de "${formatType('VAZIO')}" deve retornar.`, overload.location);
            }
        }

        return result;
    }

    const visitor: Visitor = {
        '_RESOLVED': async (instruction) => {
            if (instruction.type !== '_RESOLVED') throw new Error();
            return instruction.result;
        },
        'PROGRAM': async (instruction) => {
            if (instruction.type !== 'PROGRAM') throw new Error();

            // startup
            stopLibraries = [];
            if (params.type === 'STATIC') {
                contextManager = createStaticContextManager(params.context);    
            } else {
                contextManager = createDynamicContextManager(createDefaultContext(params.interfaces.console));
            }
            flowControl = createFlowControl();

            await runInstructions(instruction.instructions, true);

            // cleanup
            for (const stopLibrary of stopLibraries) {
                await stopLibrary();
            }

            return { resultType: 'VAZIO', value: undefined };
        },
        'CONSTANT_BOOLEAN': async (instruction) => {
            if (instruction.type !== 'CONSTANT_BOOLEAN') throw new Error();
            return { resultType: 'LOGICO', value: instruction.value };
        },
        'CONSTANT_CHARACTER': async (instruction) => {
            if (instruction.type !== 'CONSTANT_CHARACTER') throw new Error();
            return { resultType: 'CARACTER', value: instruction.value };
        },
        'CONSTANT_INTEGER': async (instruction) => {
            if (instruction.type !== 'CONSTANT_INTEGER') throw new Error();
            return { resultType: 'INTEIRO', value: instruction.value };
        },
        'CONSTANT_FLOAT': async (instruction) => {
            if (instruction.type !== 'CONSTANT_FLOAT') throw new Error();
            return { resultType: 'REAL', value: instruction.value };
        },
        'CONSTANT_STRING': async (instruction) => {
            if (instruction.type !== 'CONSTANT_STRING') throw new Error();
            return { resultType: 'CADEIA', value: instruction.value };
        },
        'ARRAY_LITERAL': async (instruction) => {
            if (instruction.type !== 'ARRAY_LITERAL') throw new Error();

            const value: any[] = [];

            // TODO: what to do if the array literal is empty? D:
            const firstElement = await visit(visitor, instruction.value[0]);
            value.push(firstElement.value);

            for (let i = 1; i < instruction.value.length; i++) {
                const element = await visit(visitor, instruction.value[i]);

                const ifAssignedValue = ifAssigned(element, firstElement.resultType);
                if (ifAssignedValue === null) {
                    throw new ParseError(`Não é possível converter tipo "${formatType(element.resultType)}" para "${formatType(firstElement.resultType)}".`, instruction.value[i].location);
                }

                value.push(ifAssignedValue.value);
            }

            return {
                resultType: {
                    type: 'ARRAY',
                    valueType: firstElement.resultType,
                    size: instruction.value.length,
                },
                value,
            };
        },
        'MINUS_UNARY': async (instruction) => {
            if (instruction.type !== 'MINUS_UNARY') throw new Error();

            const types: Type[] = ['INTEIRO', 'REAL'];

            const fn: FunctionSymbol = {
                type: 'FUNCTION_SYMBOL',
                overloads: types.map<FunctionSymbolOverload>(type => ({
                    returnType: type,
                    parameters: [{ parameterType: type, name: 'operand' }],
                    instructions: ([operand]) => ({ resultType: type, value: -operand.value }),
                })),
            };

            return callFunction(fn, {
                location: instruction.location,
                type: 'CALL_FUNCTION',
                name: '-',
                arguments: [instruction.operand],
            });
        },
        'DIVISION': async (instruction) => {
            if (instruction.type !== 'DIVISION') throw new Error();

            const fn: FunctionSymbol = {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        returnType: 'INTEIRO',
                        parameters: [{ parameterType: 'INTEIRO', name: 'lhs' }, { parameterType: 'INTEIRO', name: 'rhs' }],
                        instructions: ([lhs, rhs]) => ({ resultType: 'INTEIRO', value: Math.floor(lhs.value / rhs.value) }),
                    },
                    {
                        returnType: 'REAL',
                        parameters: [{ parameterType: 'REAL', name: 'lhs' }, { parameterType: 'REAL', name: 'rhs' }],
                        instructions: ([lhs, rhs]) => ({ resultType: 'REAL', value: lhs.value / rhs.value }),
                    },
                    {
                        returnType: 'REAL',
                        parameters: [{ parameterType: 'INTEIRO', name: 'lhs' }, { parameterType: 'REAL', name: 'rhs' }],
                        instructions: ([lhs, rhs]) => ({ resultType: 'REAL', value: lhs.value / rhs.value }),
                    },
                    {
                        returnType: 'REAL',
                        parameters: [{ parameterType: 'REAL', name: 'lhs' }, { parameterType: 'INTEIRO', name: 'rhs' }],
                        instructions: ([lhs, rhs]) => ({ resultType: 'REAL', value: lhs.value / rhs.value }),
                    },
                ],
            };

            return callFunction(fn, {
                location: instruction.location,
                type: 'CALL_FUNCTION',
                name: '/',
                arguments: [instruction.lhs, instruction.rhs],
            });
        },
        'MULTIPLICATION': async (instruction) => {
            if (instruction.type !== 'MULTIPLICATION') throw new Error();

            const fn: FunctionSymbol = {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        returnType: 'REAL',
                        parameters: [{ parameterType: 'REAL', name: 'lhs' }, { parameterType: 'REAL', name: 'rhs' }],
                        instructions: ([lhs, rhs]) => ({ resultType: 'REAL', value: lhs.value * rhs.value }),
                    },
                    {
                        returnType: 'INTEIRO',
                        parameters: [{ parameterType: 'INTEIRO', name: 'lhs' }, { parameterType: 'INTEIRO', name: 'rhs' }],
                        instructions: ([lhs, rhs]) => ({ resultType: 'INTEIRO', value: lhs.value * rhs.value }),
                    },
                ],
            };

            return callFunction(fn, {
                location: instruction.location,
                type: 'CALL_FUNCTION',
                name: '*',
                arguments: [instruction.lhs, instruction.rhs],
            });
        },
        'MODULUS': async (instruction) => {
            if (instruction.type !== 'MODULUS') throw new Error();

            const fn: FunctionSymbol = {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        returnType: 'INTEIRO',
                        parameters: [{ parameterType: 'INTEIRO', name: 'lhs' }, { parameterType: 'INTEIRO', name: 'rhs' }],
                        instructions: ([lhs, rhs]) => ({ resultType: 'INTEIRO', value: lhs.value % rhs.value }),
                    },
                    {
                        returnType: 'REAL',
                        parameters: [{ parameterType: 'REAL', name: 'lhs' }, { parameterType: 'REAL', name: 'rhs' }],
                        instructions: ([lhs, rhs]) => ({ resultType: 'REAL', value: lhs.value % rhs.value }),
                    },
                ],
            };

            return callFunction(fn, {
                location: instruction.location,
                type: 'CALL_FUNCTION',
                name: '%',
                arguments: [instruction.lhs, instruction.rhs],
            });
        },
        'SUBTRACTION': async (instruction) => {
            if (instruction.type !== 'SUBTRACTION') throw new Error();

            const fn: FunctionSymbol = {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        returnType: 'INTEIRO',
                        parameters: [{ parameterType: 'INTEIRO', name: 'lhs' }, { parameterType: 'INTEIRO', name: 'rhs' }],
                        instructions: ([lhs, rhs]) => ({ resultType: 'INTEIRO', value: lhs.value - rhs.value }),
                    },
                    {
                        returnType: 'REAL',
                        parameters: [{ parameterType: 'REAL', name: 'lhs' }, { parameterType: 'REAL', name: 'rhs' }],
                        instructions: ([lhs, rhs]) => ({ resultType: 'REAL', value: lhs.value - rhs.value }),
                    },
                ],
            };

            return callFunction(fn, {
                location: instruction.location,
                type: 'CALL_FUNCTION',
                name: '-',
                arguments: [instruction.lhs, instruction.rhs],
            });
        },
        'ADDITION': async (instruction) => {
            if (instruction.type !== 'ADDITION') throw new Error();

            const fn: FunctionSymbol = {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        returnType: 'INTEIRO',
                        parameters: [{ parameterType: 'INTEIRO', name: 'lhs' }, { parameterType: 'INTEIRO', name: 'rhs' }],
                        instructions: ([lhs, rhs]) => ({ resultType: 'INTEIRO', value: lhs.value + rhs.value }),
                    },
                    {
                        returnType: 'REAL',
                        parameters: [{ parameterType: 'REAL', name: 'lhs' }, { parameterType: 'REAL', name: 'rhs' }],
                        instructions: ([lhs, rhs]) => ({ resultType: 'REAL', value: lhs.value + rhs.value }),
                    },
                    {
                        returnType: 'CADEIA',
                        parameters: [{ parameterType: 'CADEIA', name: 'lhs' }, { parameterType: 'CADEIA', name: 'rhs' }],
                        instructions: ([lhs, rhs]) => ({ resultType: 'CADEIA', value: lhs.value + rhs.value }),
                    },
                ],
            };

            return callFunction(fn, {
                location: instruction.location,
                type: 'CALL_FUNCTION',
                name: '+',
                arguments: [instruction.lhs, instruction.rhs],
            });
        },
        'LOGICAL_AND': async (instruction) => {
            if (instruction.type !== 'LOGICAL_AND') throw new Error();

            const fn: FunctionSymbol = {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        returnType: 'LOGICO',
                        parameters: [{ parameterType: 'LOGICO', name: 'lhs' }, { parameterType: 'LOGICO', name: 'rhs' }],
                        instructions: ([lhs, rhs]) => ({ resultType: 'LOGICO', value: lhs.value && rhs.value }),
                    },
                ],
            };

            return callFunction(fn, {
                location: instruction.location,
                type: 'CALL_FUNCTION',
                name: 'e',
                arguments: [instruction.lhs, instruction.rhs],
            });
        },
        'LOGICAL_OR': async (instruction) => {
            if (instruction.type !== 'LOGICAL_OR') throw new Error();

            const fn: FunctionSymbol = {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        returnType: 'LOGICO',
                        parameters: [{ parameterType: 'LOGICO', name: 'lhs' }, { parameterType: 'LOGICO', name: 'rhs' }],
                        instructions: ([lhs, rhs]) => ({ resultType: 'LOGICO', value: lhs.value || rhs.value }),
                    },
                ],
            };

            return callFunction(fn, {
                location: instruction.location,
                type: 'CALL_FUNCTION',
                name: 'ou',
                arguments: [instruction.lhs, instruction.rhs],
            });
        },
        'LOGICAL_NOT': async (instruction) => {
            if (instruction.type !== 'LOGICAL_NOT') throw new Error();

            const fn: FunctionSymbol = {
                type: 'FUNCTION_SYMBOL',
                overloads: [
                    {
                        returnType: 'LOGICO',
                        parameters: [{ parameterType: 'LOGICO', name: 'operand' }],
                        instructions: ([operand]) => ({ resultType: 'LOGICO', value: !operand.value }),
                    },
                ],
            };

            return callFunction(fn, {
                location: instruction.location,
                type: 'CALL_FUNCTION',
                name: 'nao',
                arguments: [instruction.operand],
            });
        },
        'NOT_EQUALS': async (instruction) => {
            if (instruction.type !== 'NOT_EQUALS') throw new Error();

            const fn: FunctionSymbol = {
                type: 'FUNCTION_SYMBOL',
                overloads: EQUATABLE_TYPES.map((type): FunctionSymbolOverload => ({ 
                    returnType: 'LOGICO', 
                    parameters: [{ parameterType: type, name: 'lhs' }, { parameterType: type, name: 'rhs' }], 
                    instructions: ([lhs, rhs]: ExpressionResult[]) => ({ resultType: 'LOGICO', value: lhs.value !== rhs.value })
                })),
            };

            return callFunction(fn, {
                location: instruction.location,
                type: 'CALL_FUNCTION',
                name: '!=',
                arguments: [instruction.lhs, instruction.rhs],
            });
        },
        'EQUALS': async (instruction) => {
            if (instruction.type !== 'EQUALS') throw new Error();

            const fn: FunctionSymbol = {
                type: 'FUNCTION_SYMBOL',
                overloads: EQUATABLE_TYPES.map((type): FunctionSymbolOverload => ({ 
                    returnType: 'LOGICO', 
                    parameters: [{ parameterType: type, name: 'lhs' }, { parameterType: type, name: 'rhs' }], 
                    instructions: ([lhs, rhs]: ExpressionResult[]) => ({ resultType: 'LOGICO', value: lhs.value === rhs.value })
                })),
            };

            return callFunction(fn, {
                location: instruction.location,
                type: 'CALL_FUNCTION',
                name: '==',
                arguments: [instruction.lhs, instruction.rhs],
            });
        },
        'LOWER_THAN': async (instruction) => {
            if (instruction.type !== 'LOWER_THAN') throw new Error();

            const fn: FunctionSymbol = {
                type: 'FUNCTION_SYMBOL',
                overloads: COMPARABLE_TYPES.map<FunctionSymbolOverload>(parameterType => ({ 
                    returnType: 'LOGICO', 
                    parameters: [{ parameterType, name: 'lhs' }, { parameterType, name: 'rhs' }], 
                    instructions: ([lhs, rhs]: ExpressionResult[]) => ({ resultType: 'LOGICO', value: lhs.value < rhs.value })
                })),
            };

            return callFunction(fn, {
                location: instruction.location,
                type: 'CALL_FUNCTION',
                name: '<',
                arguments: [instruction.lhs, instruction.rhs],
            });
        },
        'GREATER_THAN': async (instruction) => {
            if (instruction.type !== 'GREATER_THAN') throw new Error();

            const fn: FunctionSymbol = {
                type: 'FUNCTION_SYMBOL',
                overloads: COMPARABLE_TYPES.map<FunctionSymbolOverload>(parameterType => ({ 
                    returnType: 'LOGICO', 
                    parameters: [{ parameterType, name: 'lhs' }, { parameterType, name: 'rhs' }], 
                    instructions: ([lhs, rhs]: ExpressionResult[]) => ({ resultType: 'LOGICO', value: lhs.value > rhs.value })
                })),
            };

            return callFunction(fn, {
                location: instruction.location,
                type: 'CALL_FUNCTION',
                name: '>',
                arguments: [instruction.lhs, instruction.rhs],
            });
        },
        'LOWER_THAN_OR_EQUAL_TO': async (instruction) => {
            if (instruction.type !== 'LOWER_THAN_OR_EQUAL_TO') throw new Error();

            const fn: FunctionSymbol = {
                type: 'FUNCTION_SYMBOL',
                overloads: COMPARABLE_TYPES.map<FunctionSymbolOverload>(parameterType => ({ 
                    returnType: 'LOGICO', 
                    parameters: [{ parameterType, name: 'lhs' }, { parameterType, name: 'rhs' }], 
                    instructions: ([lhs, rhs]: ExpressionResult[]) => ({ resultType: 'LOGICO', value: lhs.value <= rhs.value })
                })),
            };

            return callFunction(fn, {
                location: instruction.location,
                type: 'CALL_FUNCTION',
                name: '<=',
                arguments: [instruction.lhs, instruction.rhs],
            });
        },
        'GREATER_THAN_OR_EQUAL_TO': async (instruction) => {
            if (instruction.type !== 'GREATER_THAN_OR_EQUAL_TO') throw new Error();

            const fn: FunctionSymbol = {
                type: 'FUNCTION_SYMBOL',
                overloads: COMPARABLE_TYPES.map<FunctionSymbolOverload>(parameterType => ({ 
                    returnType: 'LOGICO', 
                    parameters: [{ parameterType, name: 'lhs' }, { parameterType, name: 'rhs' }], 
                    instructions: ([lhs, rhs]: ExpressionResult[]) => ({ resultType: 'LOGICO', value: lhs.value >= rhs.value })
                })),
            };

            return callFunction(fn, {
                location: instruction.location,
                type: 'CALL_FUNCTION',
                name: '>=',
                arguments: [instruction.lhs, instruction.rhs],
            });
        },
        'RETURN_VALUE': async (instruction) => {
            if (instruction.type !== 'RETURN_VALUE') throw new Error();

            const result = await visit(visitor, instruction.value);

            let context = contextManager.get();

            while (context.returnType === undefined && context.parentContext !== undefined) {
                context = context.parentContext;
            }

            if (context.returnType === undefined) {
                throw new ParseError('Não é possível retornar no escopo atual.', instruction.location);
            }

            const convertedResult = convertResult(result, context.returnType);

            if (!convertedResult) {
                flowControl.return({ resultType: 'VAZIO', value: undefined });
                throw new ParseError(`Não é possível converter o tipo "${formatType(result.resultType)}" para "${formatType(context.returnType)}".`, instruction.location);
            }

            flowControl.return(convertedResult);

            return { resultType: 'VAZIO', value: undefined };
        },
        'READ_SYMBOL': async (instruction) => {
            if (instruction.type !== 'READ_SYMBOL') throw new Error();

            const symbolAccessor = await withSymbolAtIndexPath(instruction.name, instruction.indexPath, instruction);

            return symbolAccessor.getValue();
        },
        'CALL_FUNCTION': async (instruction) => {
            if (instruction.type !== 'CALL_FUNCTION') throw new Error();

            const fn = findSymbol(instruction.name, contextManager.get());

            if (fn === undefined) {
                throw new ParseError(`Nenhum símbolo declarado com o nome "${instruction.name}".`, instruction.location);
            }

            if (fn.type !== 'FUNCTION_SYMBOL') {
                throw new ParseError(`Símbolo de nome "${instruction.name}" não é uma função.`, instruction.location);
            }

            return callFunction(fn, instruction);
        },
        'DECLARE_SYMBOLS': async (instruction) => {
            if (instruction.type !== 'DECLARE_SYMBOLS') throw new Error();

            for (const declaration of instruction.declarations) {
                declareValueSymbol(
                    contextManager.get(),
                    {
                        isConstant: instruction.isConstant,
                        valueType: getType(declaration.valueType, declaration.location),
                        name: declaration.name,
                        value: declaration.value && await visit(visitor, declaration.value),
                    },
                    declaration.location,
                );
            }

            return { resultType: 'VAZIO', value: undefined };
        },
        'DECLARE_FUNCTION': async (instruction) => {
            if (instruction.type !== 'DECLARE_FUNCTION') throw new Error();

            const { returnType, parameters, symbol } = declareFunctionSymbol(
                contextManager.get(),
                {
                    instructions: instruction.instructions,
                    name: instruction.name,
                    parameters: instruction.parameters,
                    returnType: instruction.returnType,
                },
                instruction.location,
            );

            return {
                resultType: {
                    type: 'FUNCTION',
                    parameters: parameters.map(p => ({
                        parameterType: p.parameterType,
                        isReference: !!p.isReference,
                        isVariadic: !!p.isVariadic,
                    })),
                    returnType,
                },
                value: symbol,
            };
        },
        'INCREMENT': async (instruction) => {
            if (instruction.type !== 'INCREMENT') throw new Error();

            const value = await visit(visitor, {
                location: instruction.location,
                type: 'READ_SYMBOL',
                name: instruction.name,
                indexPath: instruction.indexPath,
            });

            await visit(visitor, {
                location: instruction.location,
                type: 'ADDITION_AND_WRITE_TO_SYMBOL',
                name: instruction.name,
                indexPath: instruction.indexPath,
                value: { type: 'CONSTANT_INTEGER', value: 1 },
            });
            
            return value;
        },
        'DECREMENT': async (instruction) => {
            if (instruction.type !== 'DECREMENT') throw new Error();

            const value = await visit(visitor, {
                location: instruction.location,
                type: 'READ_SYMBOL',
                name: instruction.name,
                indexPath: instruction.indexPath,
            });

            await visit(visitor, {
                location: instruction.location,
                type: 'SUBTRACTION_AND_WRITE_TO_SYMBOL',
                name: instruction.name,
                indexPath: instruction.indexPath,
                value: { type: 'CONSTANT_INTEGER', value: 1 },
            });
            
            return value;
        },
        'ADDITION_AND_WRITE_TO_SYMBOL': async (instruction) => {
            if (instruction.type !== 'ADDITION_AND_WRITE_TO_SYMBOL') throw new Error();
            return visit(visitor, {
                location: instruction.location,
                type: 'WRITE_TO_SYMBOL',
                name: instruction.name,
                indexPath: instruction.indexPath,
                value: {
                    location: instruction.location,
                    type: 'ADDITION',
                    lhs: {
                        location: instruction.location,
                        type: 'READ_SYMBOL',
                        name: instruction.name,
                        indexPath: instruction.indexPath,
                    },
                    rhs: instruction.value,
                },
            });
        },
        'SUBTRACTION_AND_WRITE_TO_SYMBOL': async (instruction) => {
            if (instruction.type !== 'SUBTRACTION_AND_WRITE_TO_SYMBOL') throw new Error();
            return visit(visitor, {
                location: instruction.location,
                type: 'WRITE_TO_SYMBOL',
                name: instruction.name,
                indexPath: instruction.indexPath,
                value: {
                    location: instruction.location,
                    type: 'SUBTRACTION',
                    lhs: {
                        location: instruction.location,
                        type: 'READ_SYMBOL',
                        name: instruction.name,
                        indexPath: instruction.indexPath,
                    },
                    rhs: instruction.value,
                },
            });
        },
        'MULTIPLICATION_AND_WRITE_TO_SYMBOL': async (instruction) => {
            if (instruction.type !== 'MULTIPLICATION_AND_WRITE_TO_SYMBOL') throw new Error();
            return visit(visitor, {
                location: instruction.location,
                type: 'WRITE_TO_SYMBOL',
                name: instruction.name,
                indexPath: instruction.indexPath,
                value: {
                    location: instruction.location,
                    type: 'MULTIPLICATION',
                    lhs: {
                        location: instruction.location,
                        type: 'READ_SYMBOL',
                        name: instruction.name,
                        indexPath: instruction.indexPath,
                    },
                    rhs: instruction.value,
                },
            });
        },
        'DIVISION_AND_WRITE_TO_SYMBOL': async (instruction) => {
            if (instruction.type !== 'DIVISION_AND_WRITE_TO_SYMBOL') throw new Error();
            return visit(visitor, {
                location: instruction.location,
                type: 'WRITE_TO_SYMBOL',
                name: instruction.name,
                indexPath: instruction.indexPath,
                value: {
                    location: instruction.location,
                    type: 'DIVISION',
                    lhs: {
                        location: instruction.location,
                        type: 'READ_SYMBOL',
                        name: instruction.name,
                        indexPath: instruction.indexPath,
                    },
                    rhs: instruction.value,
                },
            });
        },
        'WRITE_TO_SYMBOL': async (instruction) => {
            if (instruction.type !== 'WRITE_TO_SYMBOL') throw new Error();

            const result = await visit(visitor, instruction.value);

            const symbolAccessor = await withSymbolAtIndexPath(instruction.name, instruction.indexPath, instruction);
            symbolAccessor.setValue(result);
        
            return { resultType: 'VAZIO', value: undefined };
        },
        'IF_STATEMENT': async (instruction) => {
            if (instruction.type !== 'IF_STATEMENT') throw new Error();

            const conditionResult = await visit(visitor, instruction.condition);

            if (conditionResult.resultType !== 'LOGICO') {
                throw new ParseError(`A condição de um bloco "se" deve ser do tipo "${formatType("LOGICO")}".`, instruction.condition.location);
            }

            if (params.type === 'DYNAMIC') {
                if (conditionResult.value) {
                    contextManager.pushContext(instruction.location);
                    await runInstructions(instruction.consequent);
                    contextManager.popContext();
                } else if (instruction.alternate !== null) {
                    contextManager.pushContext(instruction.location);
                    await runInstructions(instruction.alternate);
                    contextManager.popContext();
                }
            } else {
                contextManager.pushContext(instruction.location);
                await runInstructions(instruction.consequent);
                contextManager.popContext();
                if (instruction.alternate !== null) {
                    contextManager.pushContext(instruction.location);
                    await runInstructions(instruction.alternate);
                    contextManager.popContext();
                }
            }

            return { resultType: 'VAZIO', value: undefined };
        },
        'SWITCH_STATEMENT': async (instruction: SwitchStatement) => {
            const resolvedValue: Expression = {
                location: instruction.value.location,
                type: '_RESOLVED',
                result: await visit(visitor, instruction.value),
            };

            async function matchesSwitchingValue(value: Expression | undefined, code: Code): Promise<boolean> {
                if (!value) return true;
                return (await visit(visitor, {
                    location: code.location,
                    type: 'EQUALS',
                    lhs: resolvedValue,
                    rhs: value,
                })).value;
            }

            if (params.type === 'DYNAMIC') {
                let fallingThrough = false;

                for (const switchCase of instruction.cases) {
                    if (flowControl.broke() !== undefined) {
                        break;
                    }
    
                    if (fallingThrough || await matchesSwitchingValue(switchCase.value, switchCase)) {
                        await runInstructions(switchCase.instructions);
                        fallingThrough = true;
                    }
                }
    
                if (flowControl.broke() !== undefined) {
                    flowControl.reset();
                }
            } else {
                for (const switchCase of instruction.cases) {
                    await matchesSwitchingValue(switchCase.value, switchCase);
                    await runInstructions(switchCase.instructions);
                }
            }

            return { resultType: 'VAZIO', value: undefined };
        },
        'BREAK_STATEMENT': async (instruction: BreakStatement) => {
            if (params.type === 'DYNAMIC') {
                flowControl.break(instruction.label);
            }
            return { resultType: 'VAZIO', value: undefined };
        },
        'CONTINUE_STATEMENT': async (instruction: ContinueStatement) => {
            if (params.type === 'DYNAMIC') {
                flowControl.continue(instruction.label);
            }
            return { resultType: 'VAZIO', value: undefined };
        },
        'FOR_STATEMENT': async (instruction) => {
            if (instruction.type !== 'FOR_STATEMENT') throw new Error();

            contextManager.pushContext(instruction.location);

            await visit(visitor, instruction.init);

            let testResult = await visit(visitor, instruction.test);

            if (testResult.resultType !== 'LOGICO') {
                throw new ParseError(`Condição de um loop "para" deve ser do tipo "${formatType('LOGICO')}".`, instruction.test.location);
            }

            if (params.type === 'DYNAMIC') {
                while (testResult.value) {
                    if (flowControl.broke() !== undefined) {
                        break;
                    }
                    if (flowControl.continued() !== undefined) {
                        flowControl.reset();
                    }
    
                    contextManager.pushContext(instruction.location);
                    await runInstructions(instruction.instructions);
                    contextManager.popContext();
    
                    await visit(visitor, instruction.update);
                    testResult = await visit(visitor, instruction.test);
                }
    
                if (flowControl.broke() !== undefined || flowControl.continued() !== undefined) {
                    flowControl.reset();
                }   
            } else {
                contextManager.pushContext(instruction.location);
                await runInstructions(instruction.instructions);
                contextManager.popContext();
            }

            contextManager.popContext();

            return { resultType: 'VAZIO', value: undefined };
        },
        'WHILE_STATEMENT': async (instruction) => {
            if (instruction.type !== 'WHILE_STATEMENT') throw new Error();

            let testResult = await visit(visitor, instruction.test);

            if (testResult.resultType !== 'LOGICO') {
                throw new ParseError(`Condição de um loop "enquanto" deve ser do tipo "${formatType('LOGICO')}".`, instruction.test.location);
            }

            if (params.type === 'DYNAMIC') {
                while (testResult.value) {
                    if (flowControl.broke() !== undefined) {
                        break;
                    }
                    if (flowControl.continued() !== undefined) {
                        flowControl.reset();
                    }
    
                    contextManager.pushContext(instruction.location);
                    await runInstructions(instruction.instructions);
                    contextManager.popContext();
    
                    testResult = await visit(visitor, instruction.test);
                }

                if (flowControl.broke() !== undefined || flowControl.continued() !== undefined) {
                    flowControl.reset();
                }
            } else {
                contextManager.pushContext(instruction.location);
                await runInstructions(instruction.instructions);
                contextManager.popContext();
            }

            return { resultType: 'VAZIO', value: undefined };
        },
        'DO_WHILE_STATEMENT': async (instruction) => {
            if (instruction.type !== 'DO_WHILE_STATEMENT') throw new Error();

            contextManager.pushContext(instruction.location);
            await runInstructions(instruction.instructions);
            contextManager.popContext();

            let testResult = await visit(visitor, instruction.test);

            if (testResult.resultType !== 'LOGICO') {
                throw new ParseError(`Condição de um loop "faca enquanto" deve ser do tipo "${formatType('LOGICO')}".`, instruction.test.location);
            }

            if (params.type === 'DYNAMIC') {
                while (testResult.value) {
                    if (flowControl.broke() !== undefined) {
                        break;
                    }
                    if (flowControl.continued() !== undefined) {
                        flowControl.reset();
                    }
    
                    contextManager.pushContext(instruction.location);
                    await runInstructions(instruction.instructions);
                    contextManager.popContext();
    
                    testResult = await visit(visitor, instruction.test);
                }
    
                if (flowControl.broke() !== undefined || flowControl.continued() !== undefined) {
                    flowControl.reset();
                }
            }

            return { resultType: 'VAZIO', value: undefined };
        },
        'INCLUDE_LIBRARY': async (instruction) => {
            if (instruction.type !== 'INCLUDE_LIBRARY') throw new Error();

            const { start, stop } = declareLibrarySymbol(
                contextManager.get(),
                {
                    giveName: instruction.giveName,
                    libraryName: instruction.libraryName,
                },
                instruction.location,
            );

            // only start libraries if running dynamic
            if (params.type === 'DYNAMIC') {
                const libsInterfaces: { [name: string]: ControllableInterface | undefined } = {
                    Graficos: params.interfaces.graphic,
                    Arquivos: params.interfaces.files,
                    Sons: params.interfaces.sound,
                    Teclado: params.interfaces.keyboard,
                    Mouse: params.interfaces.mouse,
                };

                await start(libsInterfaces[instruction.libraryName]);
                stopLibraries.push(stop);
            }

            return { resultType: 'VAZIO', value: undefined };
        },
        'ACCESS_OBJECT_ATTRIBUTE': async (instruction) => {
            if (instruction.type !== 'ACCESS_OBJECT_ATTRIBUTE') throw new Error();

            const symbol = findSymbol(instruction.name, contextManager.get());
            if (symbol === undefined) {
                throw new ParseError(`Nenhum símbolo declarado com o nome "${instruction.name}".`, instruction.location);
            }

            if (symbol.type !== 'LIBRARY_SYMBOL') {
                throw new ParseError(`Símbolo de nome "${instruction.name}" não é uma biblioteca.`, instruction.location);
            }

            const attribute = symbol.symbols[instruction.attribute.name];

            if (!attribute) {
                throw new ParseError(`Biblioteca de nome "${instruction.name}" não contem símbolo de nome "${instruction.attribute.name}".`, instruction.location);
            }

            if (instruction.attribute.type === 'CALL_FUNCTION') {
                if (attribute.type === 'FUNCTION_SYMBOL') {
                    return callFunction(attribute, instruction.attribute);
                } else {
                    throw new ParseError(`Símbolo de nome "${instruction.attribute.name}" da biblioteca "${instruction.name}" não é uma função.`, instruction.location);
                }
            } else {
                if (attribute.type === 'VALUE_SYMBOL') {
                    if (attribute.value === null) {
                        // attribute not initialized
                    }

                    return { resultType: attribute.valueType, value: attribute.value };
                } else {
                    throw new ParseError(`Símbolo de nome "${instruction.attribute.name}" da biblioteca "${instruction.name}" não é um valor.`, instruction.location);
                }
            }
        },
    };

    return visitor;
}

type FlowControl = {
    reset: () => void,
    return: (value: ExpressionResult) => void,
    returned: () => ExpressionResult | undefined,
    break: (label: string | null) => void,
    broke: () => string | null | undefined,
    continue: (label: string | null) => void,
    continued: () => string | null | undefined,
};

function createFlowControl(): FlowControl {
    let returned: ExpressionResult | undefined;
    let broke: string | null | undefined;
    let continued: string | null | undefined;

    return {
        reset: () => {
            returned = undefined;
            broke = undefined;
            continued = undefined;
        },
        returned: () => returned,
        broke: () => broke,
        continued: () => continued,
        return: (value) => returned = value,
        break: (label) => broke = label,
        continue: (label) => continued = label,
    };
}

type ContextManager<ContextType extends Context = Context> = {
    get: () => ContextType,
    pushCall: () => void,
    popCall: () => ContextType | undefined,
    pushContext: (location: Code['location'], context?: ContextType) => void,
    popContext: (context?: ContextType) => void,
};

function createStaticContextManager(context: DescendibleContext): ContextManager<DescendibleContext> {
    const callStack: DescendibleContext[] = [];

    return {
        get: () => context,
        pushCall: () => {
            callStack.push(context);
        },
        popCall: () => {
            return callStack.pop();
        },
        pushContext: (location, ctx = context) => {
            const newContext: DescendibleContext = { 
                parentContext: ctx,
                symbols: {},
                childrenContext: [],
                location
            };
            context.childrenContext.push(newContext);
            context = newContext;
        },
        popContext: (ctx = context.parentContext as DescendibleContext) => {
            if (ctx === undefined) {
                throw new Error(`NÃO FOI POSSÍVEL DESEMPILHAR O CONTEXTO POIS O MESMO É O RAÍZ.`);
            }
            context = ctx;
        }
    };
}

function createDynamicContextManager(context: Context): ContextManager {
    const callStack: Context[] = [];

    return {
        get: () => context,
        pushCall: () => {
            callStack.push(context);
        },
        popCall: () => {
            return callStack.pop();
        },
        pushContext: (location, ctx = context) => {
            context = { parentContext: ctx, symbols: {} };
        },
        popContext: (ctx = context.parentContext) => {
            if (ctx === undefined) {
                throw new Error(`NÃO FOI POSSÍVEL DESEMPILHAR O CONTEXTO POIS O MESMO É O RAÍZ.`);
            }
            context = ctx;
        }
    };
}

function ensureHasValue(result: ExpressionResult, name: string, location: Code['location']) {
    let hasValue = false;

    if (isTypeUnion(result.resultType)) {
        hasValue = result.value.value !== null;
    } else {
        hasValue = result.value !== null;
    }

    if (!hasValue) {
        throw new ParseError(`Não é possível ler de variável não inicializada de nome "${name}".`, location);
    }
}