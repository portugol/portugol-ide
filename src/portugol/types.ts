export type TypeInteiro = 'INTEIRO';
export type TypeReal = 'REAL';
export type TypeCaracter = 'CARACTER';
export type TypeCadeia = 'CADEIA';
export type TypeLogico = 'LOGICO';
export type TypeVazio = 'VAZIO';

export type TypeFunctionParameter = {
    parameterType: Type,
    isReference: boolean,
    isVariadic: boolean,
};

export type TypeFunction = {
    type: 'FUNCTION',
    parameters: TypeFunctionParameter[],
    returnType: Type,
};

export type TypeArray = {
    type: 'ARRAY',
    valueType: Type,
    size: number | null,
};

export type TypeUnion = {
    type: 'UNION',
    valueTypes: PrimitiveType[],
};

export type PrimitiveType
    = TypeInteiro
    | TypeReal
    | TypeCaracter
    | TypeCadeia
    | TypeLogico
    | TypeVazio;

export type Type 
    = PrimitiveType
    | TypeFunction
    | TypeArray
    | TypeUnion;

export function isTypeArray(type: Type): type is TypeArray {
    return (type as any).type === 'ARRAY';
}

export function isTypeFunction(type: Type): type is TypeFunction {
    return (type as any).type === 'FUNCTION';
}

export function isTypeUnion(type: Type): type is TypeUnion {
    return (type as any).type === 'UNION';
}

export function isTypePrimitive(type: Type): type is PrimitiveType {
    return typeof type === 'string';
}