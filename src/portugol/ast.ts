import { ExpressionResult } from './context';


export type Code = { 
    location?: {
        start: { offset: number, line: number, column: number },
        end: { offset: number, line: number, column: number },
    }
};

export type Program 
    = Code & { type: 'PROGRAM', instructions: Expression[] };

export type DeclareSymbolValueType = string | {
    type: 'ARRAY',
    valueType: DeclareSymbolValueType,
    size: Expression | null,
};
export type DeclareSymbolsDeclaration = Code & { 
    name: string,
    valueType: DeclareSymbolValueType,
    value: Expression | null,
};
export type DeclareSymbols = Code & { 
    type: 'DECLARE_SYMBOLS',
    isConstant: boolean, 
    declarations: DeclareSymbolsDeclaration[]
};
export type ReadSymbol = Code & { type: 'READ_SYMBOL', name: string, indexPath: Expression[] };
export type CallFunction = Code & { type: 'CALL_FUNCTION', name: string, arguments: Expression[] };
type AccessObjectAttribute = Code & { type: 'ACCESS_OBJECT_ATTRIBUTE', name: string, attribute: ReadSymbol | CallFunction };

export type DeclareFunctionParameter = Code & { name: string, valueType: DeclareSymbolValueType, isReference: boolean };
export type DeclareFunction = Code & { type: 'DECLARE_FUNCTION', name: string, returnType?: string, parameters: DeclareFunctionParameter[], instructions: Expression[] };

export type SwitchStatement = Code & { type: 'SWITCH_STATEMENT', value: Expression, cases: SwitchStatementCase[], label: string | null };
export type SwitchStatementCase = Code & { value?: Expression, instructions: Expression[] };
export type BreakStatement = Code & { type: 'BREAK_STATEMENT', label: string | null };
export type ContinueStatement = Code & { type: 'CONTINUE_STATEMENT', label: string | null };
    
export type Expression
    = Program
    | Code & { type: 'INCLUDE_LIBRARY', libraryName: string, giveName?: string }
    | AccessObjectAttribute
    | CallFunction
    | ReadSymbol
    | DeclareSymbols
    | DeclareFunction
    | Code & { type: 'INCREMENT', name: string, indexPath: Expression[] }
    | Code & { type: 'DECREMENT', name: string, indexPath: Expression[] }
    | Code & { type: 'ADDITION_AND_WRITE_TO_SYMBOL', name: string, value: Expression, indexPath: Expression[] }
    | Code & { type: 'SUBTRACTION_AND_WRITE_TO_SYMBOL', name: string, value: Expression, indexPath: Expression[] }
    | Code & { type: 'MULTIPLICATION_AND_WRITE_TO_SYMBOL', name: string, value: Expression, indexPath: Expression[] }
    | Code & { type: 'DIVISION_AND_WRITE_TO_SYMBOL', name: string, value: Expression, indexPath: Expression[] }
    | Code & { type: 'WRITE_TO_SYMBOL', name: string, value: Expression, indexPath: Expression[] }
    | Code & { type: 'RETURN_VALUE', value: Expression }
    | Code & { type: 'CONSTANT_STRING', value: string }
    | Code & { type: 'CONSTANT_CHARACTER', value: string }
    | Code & { type: 'IF_STATEMENT', condition: Expression, consequent: Expression[], alternate: Expression[] | null }
    | SwitchStatement
    | BreakStatement
    | ContinueStatement
    | Code & { type: 'FOR_STATEMENT', init: Expression, test: Expression, update: Expression, instructions: Expression[], label: string | null }
    | Code & { type: 'WHILE_STATEMENT', test: Expression, instructions: Expression[], label: string | null }
    | Code & { type: 'DO_WHILE_STATEMENT', instructions: Expression[], test: Expression, label: string | null }
    | Code & { type: 'ARRAY_LITERAL', value: Expression[] }
    | Code & { type: 'MINUS_UNARY', operand: Expression }
    | Code & { type: 'ADDITION', lhs: Expression, rhs: Expression }
    | Code & { type: 'SUBTRACTION', lhs: Expression, rhs: Expression }
    | Code & { type: 'MULTIPLICATION', lhs: Expression, rhs: Expression }
    | Code & { type: 'DIVISION', lhs: Expression, rhs: Expression }
    | Code & { type: 'MODULUS', lhs: Expression, rhs: Expression }
    | Code & { type: 'CONSTANT_INTEGER', value: number }
    | Code & { type: 'CONSTANT_FLOAT', value: number }
    | Code & { type: 'LOGICAL_AND', lhs: Expression, rhs: Expression }
    | Code & { type: 'LOGICAL_OR', lhs: Expression, rhs: Expression }
    | Code & { type: 'LOGICAL_NOT', operand: Expression }
    | Code & { type: 'CONSTANT_BOOLEAN', value: boolean }
    | Code & { type: 'EQUALS', lhs: Expression, rhs: Expression }
    | Code & { type: 'NOT_EQUALS', lhs: Expression, rhs: Expression }
    | Code & { type: 'GREATER_THAN', lhs: Expression, rhs: Expression }
    | Code & { type: 'LOWER_THAN', lhs: Expression, rhs: Expression }
    | Code & { type: 'GREATER_THAN_OR_EQUAL_TO', lhs: Expression, rhs: Expression }
    | Code & { type: 'LOWER_THAN_OR_EQUAL_TO', lhs: Expression, rhs: Expression }
    | Code & { type: '_RESOLVED', result: ExpressionResult };