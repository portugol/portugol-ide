{
    function createLeftPrecedence(head, tail, createOperation) {
        return tail.reduce((result, element) => {
            const operator = element[1];
            const lhs = result;
            const rhs = element[3];
            return createOperation(operator, lhs, rhs);
        }, head);
    }
}

Program
    = _ 'programa' _ '{' _ instructions:Expressions _ '}' _ { return { location: location(), type: 'PROGRAM', instructions }; }
    / _ instructions:Expressions _ { return { location: location(), type: 'PROGRAM', instructions }; }

DeclareFunction
    = 'funcao' returnType:(__ PrimitiveType)? __ name:Word _ '(' _ parameters:FunctionParameters _ ')' _ '{' _ instructions:Expressions _ '}' {
        returnType = returnType ? returnType[1] : undefined
        return { location: location(), type: 'DECLARE_FUNCTION', name, returnType, parameters, instructions };
    }

FunctionParameters
    = valueType:PrimitiveType _ isReference:'&'? _ name:Word _ arraySize:(_ '[' _ Expression? _ ']')* nextParameters:(_ ',' _ FunctionParameters)? {
        let type = valueType;

        arraySize = arraySize.reverse()
        for (let i = 0; i < arraySize.length; i++) {
            type = {
                type: 'ARRAY',
                valueType: type,
                size: arraySize[i][3],
            }
        }

        return [{
            location: location(),
            name,
            valueType: type,
            isReference: !!isReference,
        }, ...(nextParameters ? nextParameters[3] : [])];
    }
    / '' { return [] }
 
DeclareSymbols
    = isConstant:('const' __)? valueType:PrimitiveType __ 
        declaration:DeclareSymbolDeclaration 
        otherDeclarations:(_ ',' _ DeclareSymbolDeclaration)*
    { 
        const declarations = [
            declaration,
            ...otherDeclarations.map(d => d[3]),
        ];

        return {
            location: location(),
            type: 'DECLARE_SYMBOLS',
            isConstant: !!isConstant,
            declarations: declarations.map(({ name, arraySize, value, location }) => {
                let type = valueType;

                arraySize = arraySize.reverse();
                for (let i = 0; i < arraySize.length; i++) {
                    type = {
                        type: 'ARRAY',
                        valueType: type,
                        size: arraySize[i][3],
                    };
                }

                return { 
                    location,
                    name,
                    valueType: type,
                    value: value ? value[3] : null,
                };
            }),
        }
    }

DeclareSymbolDeclaration 
    = name:Word arraySize:(_ '[' _ Expression? _ ']')* value:(_ '=' _ Expression)? {
        return { location: location(), name, arraySize, value };
    }

AdditionAndWriteToSymbol
    = name:Word _ indexPath:(_ '[' _ Expression _ ']')* _ '+=' _ value:Expression {
        indexPath = indexPath.map(ip => ip[3]);
        return { location: location(), type: 'ADDITION_AND_WRITE_TO_SYMBOL', name, value, indexPath }; 
    }

Comment
    = '//' [^\n]*
    / '/*' (!'*/' .)* '*/'

SubtractionAndWriteToSymbol
    = name:Word _ indexPath:(_ '[' _ Expression _ ']')* _ '-=' _ value:Expression {
        indexPath = indexPath.map(ip => ip[3]);
        return { location: location(), type: 'SUBTRACTION_AND_WRITE_TO_SYMBOL', name, value, indexPath }; 
    }

MultiplicationAndWriteToSymbol
    = name:Word _ indexPath:(_ '[' _ Expression _ ']')* _ '*=' _ value:Expression {
        indexPath = indexPath.map(ip => ip[3]);
        return { location: location(), type: 'MULTIPLICATION_AND_WRITE_TO_SYMBOL', name, value, indexPath }; 
    }

DivisionAndWriteToSymbol
    = name:Word _ indexPath:(_ '[' _ Expression _ ']')* _ '/=' _ value:Expression { 
        indexPath = indexPath.map(ip => ip[3]);
        return { location: location(), type: 'DIVISION_AND_WRITE_TO_SYMBOL', name, value, indexPath }; 
    }

WriteToSymbol 
    = name:Word _ indexPath:(_ '[' _ Expression _ ']')* _ '=' _ value:Expression { 
        indexPath = indexPath.map(ip => ip[3]);
        return { location: location(), type: 'WRITE_TO_SYMBOL', name, value, indexPath }; 
    }
 
Expressions
    = exp:Expression (Comment / [ \t\r])* (';'/'\n') _ exps:Expressions { return [exp, ...exps]; }
    / exp:Expression { return [exp]; }
    / _ { return []; }

ReturnValue
    = 'retorne' _ expression:Expression { return { location: location(), type: 'RETURN_VALUE', value: expression }; }

IfStatement
    = 'se' _ '(' _ condition:Expression _ ')' _ consequent:IfStatementBody !(_ 'senao' _ 'se') alternate:(_ 'senao' _ IfStatementBody)? {
        alternate = alternate && alternate[3];
        return { location: location(), type: 'IF_STATEMENT', condition, consequent, alternate };
    }
    / 'se' _ '(' _ condition:Expression _ ')' _ consequent:IfStatementBody alternate:(_ 'senao' _ IfStatement)? {
        alternate = alternate && [alternate[3]];
        return { location: location(), type: 'IF_STATEMENT', condition, consequent, alternate };
    }

IfStatementBody
    = '{' _ body:Expressions _ '}' { return body; }
    / body:Expression { return [body] }

BreakStatement
    = 'pare' label:(_ Word)? {
        return {
            location: location(),
            type: 'BREAK_STATEMENT',
            label: label && label[1],
        };
    }

ContinueStatement
    = 'continue' label:(_ Word)? {
        return {
            location: location(),
            type: 'CONTINUE_STATEMENT',
            label: label && label[1],
        };
    }

SwitchStatement
    = label:(Word _ ':' _)? 'escolha' _ '(' _ value:Expression _ ')' _ '{' _ cases:(_ SwitchStatementCase)* _  '}' {
        return {
            location: location(),
            type: 'SWITCH_STATEMENT',
            value,
            cases: cases.map(c => c[1]),
            label: label && label[0],
        };
    }

SwitchStatementCase
    = 'caso' _ 'contrario' _ ':' _ instructions:SwitchStatementCaseInstructions {
        return {
            location: location(),
            instructions,
        };
    }
    / 'caso' _ value:SwitchStatementCaseValue _ ':' _ instructions:SwitchStatementCaseInstructions {
        return {
            location: location(),
            instructions,
            value,
        };
    }

SwitchStatementCaseValue
    = '(' _ value:Expression _ ')' { return value }
    / value:Expression { return value }

SwitchStatementCaseInstructions
    = '{' _ instructions:Expressions _ '}' { return instructions }
    / instructions:Expressions { return instructions }

IterationStatement
    = label:(Word _ ':' _)? 'faca' _ '{' _ instructions:Expressions _ '}' _ 'enquanto' _ '(' _ test:Expression _ ';'? _ ')' {
        return { location: location(), type: 'DO_WHILE_STATEMENT', instructions, test, label: label && label[0] };
    }
    / label:(Word _ ':' _)? 'enquanto' _ '(' _ test:Expression _ ';'? _ ')' _ '{' _ instructions:Expressions _ '}' {
        return { location: location(), type: 'WHILE_STATEMENT', test, instructions, label: label && label[0] };
    }
    / label:(Word _ ':' _)? 'para' _ '(' _ init:Expression [ \t\r]* (';'/'\n') _ test:Expression [ \t\r]* (';'/'\n') _ update:Expression _ ';'? _ ')' _ '{' _ instructions:Expressions _ '}' {
        return { location: location(), type: 'FOR_STATEMENT', init, test, update, instructions, label: label && label[0] };
    }

IncludeLibrary 
    = 'inclua' __ 'biblioteca' __ libraryName:Word giveName:(_ '-->' _ Word)? {
        const nameToGive = giveName && giveName[3];
        const instruction = { location: location(), type: 'INCLUDE_LIBRARY', libraryName };
        if (nameToGive) {
            instruction.giveName = nameToGive;
        }
        return instruction;
    }

Expression
    = IncludeLibrary
    / DeclareFunction
    / ReturnValue
    / DeclareSymbols
    / AdditionAndWriteToSymbol / SubtractionAndWriteToSymbol / MultiplicationAndWriteToSymbol / DivisionAndWriteToSymbol / WriteToSymbol
    / IfStatement
    / SwitchStatement
    / BreakStatement
    / ContinueStatement
    / IterationStatement
    / LogicalDisjunctionOperation
 
CallFunction
    = name:Word _ '(' _ args:FunctionArguments _ ')' {
        return { location: location(), type: 'CALL_FUNCTION', name, arguments: args };
    }

FunctionArguments
    = expression:Expression _ ',' _ args:FunctionArguments { 
        return [expression, ...args]; }
    / expression:Expression { 
        return [expression]; }
    / _ { 
        return []; }

    

LogicalDisjunctionOperation
    = head:LogicalConjuctionOperation tail:(_ 'ou' __ LogicalConjuctionOperation)* {
        return createLeftPrecedence(head, tail, (operator, lhs, rhs) => {
            return { location: location(), type: 'LOGICAL_OR', lhs, rhs };
        });
    }

LogicalConjuctionOperation
    = head:ComparisonOperation tail:(_ 'e' __ ComparisonOperation)* {
        return createLeftPrecedence(head, tail, (operator, lhs, rhs) => {
            return { location: location(), type: 'LOGICAL_AND', lhs, rhs };
        });
    }

ComparisonOperation
    = head:AdditionOperation tail:(_ ('!='/'=='/'>='/'<='/'>'/'<') _ AdditionOperation)* {
        return createLeftPrecedence(head, tail, (operator, lhs, rhs) => {
            switch (operator) {
                case '!=': return { location: location(), type: 'NOT_EQUALS', lhs, rhs };
                case '==': return { location: location(), type: 'EQUALS', lhs, rhs };
                case '>=': return { location: location(), type: 'GREATER_THAN_OR_EQUAL_TO', lhs, rhs };
                case '<=': return { location: location(), type: 'LOWER_THAN_OR_EQUAL_TO', lhs, rhs };
                case '>': return { location: location(), type: 'GREATER_THAN', lhs, rhs };
                case '<': return { location: location(), type: 'LOWER_THAN', lhs, rhs };
            }
        });
    }

AdditionOperation
    = head:MultiplicationOperation tail:(_ ('+'/'-') _ MultiplicationOperation)* {
        return createLeftPrecedence(head, tail, (operator, lhs, rhs) => {
            switch (operator) {
                case '+': return { location: location(), type: 'ADDITION', lhs, rhs };
                case '-': return { location: location(), type: 'SUBTRACTION', lhs, rhs };
            }
        });
    }

MultiplicationOperation
    = head:UnaryOperation tail:(_ ('*'/'/'/'%') _ UnaryOperation)* {
        return createLeftPrecedence(head, tail, (operator, lhs, rhs) => {
            switch (operator) {
                case '*': return { location: location(), type: 'MULTIPLICATION', lhs, rhs };
                case '/': return { location: location(), type: 'DIVISION', lhs, rhs };
                case '%': return { location: location(), type: 'MODULUS', lhs, rhs };
            }
        });
    }

UnaryOperation 
    = LogicalNotOperation
    / MinusUnaryOperation
    / IncrementOperation
    / DecrementOperation
    / Value

IncrementOperation
    = name:Word _ indexPath:(_ '[' _ Expression _ ']')* _ '++' {
        return { location: location(), type: 'INCREMENT', name, indexPath };
    }

DecrementOperation
    = name:Word _ indexPath:(_ '[' _ Expression _ ']')* _ '--' {
        return { location: location(), type: 'DECREMENT', name, indexPath };
    }

LogicalNotOperation
    = 'nao ' _ operand:Value {
        return { location: location(), type: 'LOGICAL_NOT', operand };
    }

MinusUnaryOperation
    = '-' _ operand:Value {
        return { location: location(), type: 'MINUS_UNARY', operand };
    }


Value 
    = '(' _ instruction:Expression _ ')' { return instruction; }
    / ArrayLiteral
    / ConstantBoolean
    / AccessObjectAttribute
    / CallFunction
    / ReadSymbol
    / ConstantFloat
    / ConstantInteger
    / ConstantString
    / ConstantCharacter

ArrayLiteral 
    = '{' _ head:Expression tail:(_ ',' _ Expression)* (_ ',')? _ '}' {
        const value = [head, ...tail.map(e => e[3])];
        return { location: location(), type: 'ARRAY_LITERAL', value };
    }
    / '{' _ '}' {
        return { location: location(), type: 'ARRAY_LITERAL', value: [] };
    }

ConstantBoolean
    = value:('verdadeiro'/'falso') {
        return { location: location(), type: 'CONSTANT_BOOLEAN', value: value === 'verdadeiro' ? true : false }
    }

ConstantInteger
    = '0x' hexValue:[0-9A-F]+ {
        return { location: location(), type: 'CONSTANT_INTEGER', value: parseInt(hexValue.join(''), 16) };
    }
    / value:[0-9]+ { 
        return { location: location(), type: 'CONSTANT_INTEGER', value: Number(text()) };
    }

ConstantString
    = '"' value:StringCharacter* '"' {
        return { location: location(), type: 'CONSTANT_STRING', value: value.join('') };
    }

StringCharacter 
    = '\\"' { return '"' }
    / [^"] { return text() }

ConstantCharacter
    = '\'' value:CharacterCharacter '\'' {
        return { location: location(), type: 'CONSTANT_CHARACTER', value };
    }

CharacterCharacter
    = '\\\'' { return '\'' }
    / [^'] { return text() }

ConstantFloat
    = value:[0-9]+'.'[0-9]+ {
        return { location: location(), type: 'CONSTANT_FLOAT', value: Number(text()) }
    }
 
ReadSymbol
    = name:Word indexPath:(_ '[' _ Expression _ ']')* {
        indexPath = indexPath.map(ip => ip[3])

        return { location: location(), type: 'READ_SYMBOL', name, indexPath };
    }

AccessObjectAttribute
    = name:Word _ '.' _ attribute:(CallFunction / ReadSymbol) {
        return { location: location(), type: 'ACCESS_OBJECT_ATTRIBUTE', name, attribute };
    }

PrimitiveType
    = 'vazio'
    / 'inteiro'
    / 'real'
    / 'cadeia'
    / 'caracter'
    / 'logico'

ReservedWord
    = 'se'
    / 'senao'
    / 'funcao'
    / 'para'
    / 'enquanto'
    / 'faca'
    / 'escolha'
    / 'caso'
    / 'contrario'
    / 'pare'
    / 'programa'
    / 'retorne'
    / 'verdadeiro'
    / 'falso'
    / 'inclua'
    / 'biblioteca'
    / 'nao'
    / 'e'
    / 'ou'
    / 'const'
    / PrimitiveType

Word
 = !(ReservedWord ![a-zA-Z_0-9]) [a-zA-Z_][a-zA-Z_0-9]* {
    return text()
}

_ "whitespace"
    = (Comment / [ \t\n\r])*

__ "whitespace"
    = (Comment / [ \t\n\r])+