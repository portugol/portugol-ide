import * as React from 'react';
import './AccordionGroup.css';

type AccordionInjectedProps = {
    isOpen: boolean,
    toggle: () => void,
};

type AccordionProps = React.Props<{}> & {
    title: string,
};

export const Accordion: React.FunctionComponent<AccordionProps> = (props: AccordionProps & AccordionInjectedProps) => (
    <>
        <div className="accordion-title" onClick={props.toggle}>
            <h5>
                <i className={`fas${props.isOpen ? ' fa-chevron-up' : ' fa-chevron-down'}`} style={{ marginRight: '5px' }} />
                {props.title}
            </h5>
        </div>
        <div className={`accordion-content${props.isOpen ? ' is-open' : ''}`}>
            {props.children}
        </div>
    </>
);

type AccordionGroupProps = React.Props<{}> & {
    style?: React.CSSProperties,
};

type AccordionGroupState = {
    open: { [key: number]: boolean },
};

export default class extends React.Component<AccordionGroupProps, AccordionGroupState> {
    state: AccordionGroupState = {
        open: {},
    };

    componentDidMount() {
        this.updateOpenAmount();
    }

    componentDidUpdate(oldProps: AccordionGroupProps) {
        if (oldProps.children !== this.props.children) {
            this.updateOpenAmount();
        }
    }

    updateOpenAmount() {
        const open = {};
        const childrenCount = React.Children.count(this.props.children);
        for (let i = 0; i < childrenCount; i++) {
            open[i] = true;
        }
        this.setState({ open });
    }

    toggleAccordion = (index: number) => () => {
        const newOpen = { 
            ...this.state.open,
            [index]: !this.state.open[index],
        };
        const allClosed = Object.values(newOpen).filter(value => !!value).length === 0;
        // If the result of closing the accordion results in all being closed
        // toggle them all!
        if (allClosed) {
            for (const key of Object.keys(this.state.open)) {
                newOpen[key] = !this.state.open[key];
            }
        }
        this.setState({
            open: newOpen,
        });
    }

    render() {
        const children = React.Children.map(this.props.children, (child: any, index) => {
            const injectedProps: AccordionInjectedProps = {
                toggle: this.toggleAccordion(index),
                isOpen: this.state.open[index],
            };
            return (React.cloneElement(child, injectedProps));
        });

        return (
            <div className="accordion-group" style={this.props.style}>
                {children}
            </div>
        );
    }
}