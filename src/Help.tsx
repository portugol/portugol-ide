import * as React from 'react';

type HelpState = {
    visible: boolean,
};

class Help extends React.Component<{}, HelpState> {
    state = {
        visible: false,
    };

    toggleVisible = () => {
        this.setState(({ visible }) => ({ visible: !visible }));
    }

    render() {
        return (
            <div style={{ backgroundColor: 'grey', height: '100%', width: this.state.visible ? '100%' : undefined }}>
                <button onClick={this.toggleVisible}>{this.state.visible ? 'Esconder' : 'Mostrar'}</button>
                <div style={{ display: this.state.visible ? 'block' : 'none' }}>
                    <h1 style={{ color: 'white' }}>Ajuda</h1>
                </div>
            </div>
        );
    }
}

export default Help;