import classNames from 'classnames';
import * as React from 'react';
import Dropzone from 'react-dropzone';
import { ActionMessageContext, ActionMessageContextType } from './ActionMessageController';
import FileEditor from './FileEditor';
import FilePreviewContainer, { FilePreviewItem } from './FilePreviewContainer';
import './FilesDropzone.css';
import FilesValidator from './FilesValidator';
import IDEContext, { IDEContextType } from './IDE/Context';

type FileDropzoneInjectedProps = { actionMessage: ActionMessageContextType, ide: IDEContextType };

type FilesDropzoneProps = {
    accept?: string | string[];
};

type FilesDropzoneState = {
    editingFileIndex: number | undefined,
    applicationFiles: File[],
};

class FilesDropzone extends React.Component<FilesDropzoneProps & FileDropzoneInjectedProps, FilesDropzoneState> {
    filesValidatorRef = React.createRef<FilesValidator>();

    state: FilesDropzoneState = {
        editingFileIndex: undefined,
        applicationFiles: [],
    };

    onDrop = (acceptedFiles: File[], rejectedFiles: File[]) => {
        if (rejectedFiles.length > 0) {
            this.props.actionMessage.showMessage('Alguns arquivos não foram aceitos.', 'DANGER');
        }

        this.filesValidatorRef.current!.validate(acceptedFiles, (files) => {
            this.props.ide.addFilesToApplication(files);
        });
    }

    editFile = (fileIndex: number) => () => {
        const file = this.props.ide.applicationFiles[fileIndex];
        if (!file.isBusy) {
            this.setState({
                editingFileIndex: fileIndex,
            });
        }
    }

    stopEditingFile = () => {
        this.setState({ editingFileIndex: undefined });
    }

    editNextFile = () => {
        if (this.state.editingFileIndex !== undefined) {
            if (this.state.editingFileIndex < this.props.ide.applicationFiles.length - 1) {
                this.setState({
                    editingFileIndex: this.state.editingFileIndex + 1,
                });
            }
        }
    }

    editPreviousFile = () => {
        if (this.state.editingFileIndex !== undefined) {
            if (this.state.editingFileIndex > 0) {
                this.setState({
                    editingFileIndex: this.state.editingFileIndex - 1,
                });
            }
        }
    }

    updateEditingFileName = (name: string) => {
        if (this.state.editingFileIndex !== undefined) {
            const file = this.props.ide.applicationFiles[this.state.editingFileIndex];
            this.props.ide.updateApplicationFileName(file.name, name);
        }
    }

    deleteEditingFile = () => {
        if (this.state.editingFileIndex !== undefined) {
            const file = this.props.ide.applicationFiles[this.state.editingFileIndex];
            this.props.ide.removeFileFromApplication(file.name);
        }
    }

    componentDidMount() {
        this.setState({ applicationFiles: this.getFilesFromProps() });
    }

    componentDidUpdate(oldProps: FilesDropzoneProps & FileDropzoneInjectedProps) {
        if (oldProps.ide.applicationFiles !== this.props.ide.applicationFiles) {
            this.setState({ applicationFiles: this.getFilesFromProps() });
        }
    }

    render() {
        const editingFile = this.state.editingFileIndex !== undefined
            ? this.props.ide.applicationFiles[this.state.editingFileIndex]
            : undefined;
        const hasPreviousFileToEdit = this.state.editingFileIndex !== undefined
            && this.state.editingFileIndex > 0;
        const hasNextFileToEdit = this.state.editingFileIndex !== undefined
            && this.state.editingFileIndex < this.props.ide.applicationFiles.length - 1;

        return (
            <>
                <FilesValidator
                    ref={this.filesValidatorRef}
                    files={this.state.applicationFiles}
                />
                <FileEditor
                    file={editingFile}
                    toggle={this.stopEditingFile}
                    next={this.editNextFile}
                    previous={this.editPreviousFile}
                    updateName={this.updateEditingFileName}
                    hasPrevious={hasPreviousFileToEdit}
                    hasNext={hasNextFileToEdit}
                    deleteFile={this.deleteEditingFile}
                />
                <Dropzone onDrop={this.onDrop} accept={this.props.accept as string}>
                    {({ getRootProps, getInputProps, isDragActive }) => {
                        return (
                            <div
                                {...getRootProps({
                                    style: { backgroundColor: 'pink', width: '100%', height: '100%' },
                                    className: classNames('dropzone', { 'dropzone--isActive': isDragActive }),
                                })}
                            >
                                <input {...getInputProps()} />
                                <div style={{
                                    overflow: 'auto',
                                    width: '100%',
                                    height: '100%',
                                    backgroundColor: 'white',
                                    cursor: 'pointer',
                                    textAlign: 'center',
                                    boxSizing: 'border-box',
                                    ...(isDragActive
                                        ? { background: 'repeating-linear-gradient(45deg,#f3f3d3,#f3f3d3 10px,#fefede 10px,#fefede 20px)' }
                                        : { background: 'repeating-linear-gradient(45deg,#f3f3f3,#f3f3f3 10px,#fefefe 10px,#fefefe 20px)', }
                                    ),
                                }}>
                                    <FilePreviewContainer>
                                        {this.props.ide.applicationFiles.map((file, index) => (
                                            <FilePreviewItem
                                                selected={index === this.state.editingFileIndex}
                                                key={file.name}
                                                file={file}
                                                isLoading={file.isBusy}
                                                onClick={this.editFile(index)}
                                            />
                                        ))}
                                    </FilePreviewContainer>
                                    <div style={{ padding: '20px', clear: 'both' }}>
                                        <i
                                            className={`fas fa-${isDragActive ? 'chevron-circle-down' : 'file-import'}`}
                                            style={{ fontSize: '40px' }}
                                        />
                                        <p style={{ fontSize: '15px' }}>
                                            {isDragActive
                                                ? 'Solte para adicionar'
                                                : 'Clique ou arraste arquivos para adicionar'}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        );
                    }}
                </Dropzone>
            </>
        );
    }

    private getFilesFromProps() {
        return this.props.ide.applicationFiles
            .map(f => f.raw || { name: f.name } as File);
    }
}

export default (props: FilesDropzoneProps) => (
    <ActionMessageContext.Consumer>
        {actionMessageContext => (
            <IDEContext.Consumer>
                {ideContext => (
                    <FilesDropzone
                        actionMessage={actionMessageContext}
                        ide={ideContext}
                        {...props}
                    />
                )}
            </IDEContext.Consumer>
        )}
    </ActionMessageContext.Consumer>
);