declare module 'worker-loader!*' {
    let e: new () => Worker;
    export default e;
}