import { css, StyleSheet } from 'aphrodite';
import * as classNames from 'classnames';
import * as React from 'react';
import * as theme from './theme';

type MenuItemProps = {
    onClick?: () => void,
    title: string,
    iconName?: string,
    iconSolid?: boolean,
    iconRight?: boolean,
    active?: boolean,
};

export const MenuItem = (props: MenuItemProps) => {
    const Icon = () => (
        props.iconName
            ? (
                <i
                    className={`${props.iconSolid === false ? 'far' : 'fas'} fa-${props.iconName} ${css(styles.menuItemIcon)}`}
                    style={{
                        marginLeft: props.iconRight ? '10px' : 0,
                        marginRight: props.iconRight ? 0 : '10px',
                    }}
                />
            )
            : null
    );
    return (
        <div
            className={css(styles.menuItem, props.active && styles.activeMenuItem)}
            onClick={props.onClick}
        >
            {!props.iconRight && (<Icon />)}
            <span className={css(styles.menuItemTitle)} style={{ textAlign: props.iconRight ? 'right' : 'left' }}>
                {props.title}
            </span>
            {props.iconRight && (<Icon />)}
        </div>
    );
};

type MenuProps = {
    className?: string,
    children?: React.ReactNode,
};

const Menu = (props: MenuProps) => (
    <div className={classNames(css(styles.menu), props.className)}>
        {props.children}
    </div>
);

const styles = StyleSheet.create({
    menu: {
        width: '250px',
        backgroundColor: theme.backgroundColor.dark,
        paddingTop: '10px',
        paddingBottom: '10px',
        boxSizing: 'border-box',
    },
    menuItemIcon: {
        width: '24px',
        fontSize: '20px',
        textAlign: 'center',
        color: theme.foregroundColor.dark,
    },
    menuItemTitle: {
        flex: 1,
    },
    menuItem: {
        display: 'flex',
        alignItems: 'center',
        width: '100%',
        fontSize: '18px',
        color: theme.foregroundColor.dark,
        paddingTop: '10px',
        paddingBottom: '10px',
        paddingLeft: '20px',
        paddingRight: '20px',
        boxSizing: 'border-box',
        lineHeight: '30px',

        ':not(:disabled)': {
            cursor: 'pointer',
        },

        ':not(:disabled):hover': {
            backgroundColor: theme.backgroundColor.normal,
        },
    },
    activeMenuItem: {
        backgroundColor: theme.backgroundColor.light,
    },
});

export default Menu;