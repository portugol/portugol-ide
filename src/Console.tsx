import * as React from 'react';
import './Console.css';

type Line = { error: boolean, value: string };

type ConsoleState = {
    output: Line[],
    isReading: boolean,
    readValue: string,
};

class Console extends React.Component<{}, ConsoleState> {
    wrapperRef = React.createRef<HTMLDivElement>();
    inputRef = React.createRef<HTMLInputElement>();
    onRead: ((value: string) => void) | null = null;

    state: ConsoleState = { 
        output: [],
        isReading: false,
        readValue: '',
    };

    clear = () => {
        this.setState({
            output: [],
        });
    }

    write = (value: string, error = false) => {
        this.setState(({ output }) => {
            // TODO: make a better tabulation
            value = value.split('\\t').join('\t');
            const splitValue = value.split('\\n');
            const lastValue = (output[output.length - 1] || { value: '' }).value + splitValue[0];
            return {
                output: [
                    ...output.slice(0, output.length - 1),
                    { value: lastValue, error },
                    ...splitValue.slice(1).map(v => ({ value: v, error })),
                ],
            };
        }, () => {
            this.wrapperRef.current!.scrollTop = this.wrapperRef.current!.scrollHeight;
        });
    }

    read = async (): Promise<string> => {
        this.setState({ isReading: true }, () => {
            this.wrapperRef.current!.scrollTop = this.wrapperRef.current!.scrollHeight;
            this.inputRef.current!.focus();
        });
        return new Promise(resolve => this.onRead = resolve);
    }

    updateReadValue = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ readValue: event.target.value });
    }

    submitReadValue = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const readValue = this.state.readValue;
        this.write(readValue + '\\n');
        this.setState({
            isReading: false,
            readValue: '',
        }, () => {
            this.onRead!(readValue);
            this.onRead = null;
        });
    }

    focusOnInput = () => {
        if (this.inputRef.current) {
            this.inputRef.current.focus();
        }
    }

    render() {
        return (
            <div className="console" ref={this.wrapperRef} onClick={this.focusOnInput}>
                {this.state.output.map((line, index) => (
                    <div 
                        key={index}
                        className="console-line"
                        style={{ color: line.error ? 'red' : 'white' }}
                    >
                        {line.value}
                    </div>
                ))}
                {this.state.isReading && (
                    <form onSubmit={this.submitReadValue}>
                        <input 
                            ref={this.inputRef}
                            type="text"
                            value={this.state.readValue}
                            onChange={this.updateReadValue}
                            className="console-input"
                        />
                    </form>
                )}
            </div>
        );
    }
}

export default Console;