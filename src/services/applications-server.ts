import * as HttpStatus from 'http-status-codes';

export class InternalServerError {}
export class UnauthorizedError {}
export class IncompatibleServiceVersionError {}
export class UsernameAlreadyInUseError {}
export class UsernameDoesNotExistError {}
export class InvalidCredentialsError {}
export class ApplicationNameAlreadyInUseError {}
export class ApplicationDoesNotExistError {}
export class FileNameAlreadyInUseError {}
export class UserIsNotSelfError {}
export class IdentityNotRegisteredError {}

export type Application = {
    code: string,
    name: string,
    ownerUsername: string,
    lastUpdated: Date,
};

export type UserSelf = {
    isSelf: true,
    username: string,
    name: string,
    email: string,
    biography: string,
    showEmail: boolean,
};
export type UserOther = {
    isSelf: false,
    username: string,
    name: string,
    biography: string,
    email?: string,
};
export type User = UserSelf | UserOther;

function getHeadersWithAuthorization(tokenHash: string): { [key: string]: string } {
    return { Authorization: `Bearer ${tokenHash}` };
}

function requestJSON(body?: {}, authenticationTokenHash?: string, method: string = 'POST') {
    return {
        method,
        headers: { 
            'Content-Type': 'application/json', 
            ...(authenticationTokenHash !== undefined && getHeadersWithAuthorization(authenticationTokenHash)),
        },
        ...(body !== undefined && { body: JSON.stringify(body) }),
    };
}

class AccountEndpoint {
    private endpointUrl: string;

    constructor(rootUrl: string, private authentication: AuthenticationStore) {
        this.endpointUrl = `${rootUrl}/account`;
    }

    async create(account: { email: string, username: string, password: string }): Promise<void> {
        const url = `${this.endpointUrl}/create`;
        const response = await fetch(url, requestJSON(account));

        switch (response.status) {
            case HttpStatus.INTERNAL_SERVER_ERROR: throw new InternalServerError();
            case HttpStatus.CONFLICT: throw new UsernameAlreadyInUseError();
            case HttpStatus.OK: break;
            default: throw new IncompatibleServiceVersionError();
        }
    }

    async authenticate(credentials: { username: string, password: string }): Promise<void> {
        if (this.authentication.isAuthenticated()) {
            await this.deauthenticate().catch();
        }

        const url = `${this.endpointUrl}/authenticate`;
        const response = await fetch(url, requestJSON(credentials));

        switch (response.status) {
            case HttpStatus.INTERNAL_SERVER_ERROR: throw new InternalServerError();
            case HttpStatus.NOT_FOUND: throw new UsernameDoesNotExistError();
            case HttpStatus.UNAUTHORIZED: throw new InvalidCredentialsError();
            case HttpStatus.OK: 
                this.authentication.store(await response.json());
                break;
            default: throw new IncompatibleServiceVersionError();
        }
    }

    async authenticateWithUnivali(credentials: { personCode: string, password: string, username?: string }): Promise<void> {
        if (this.authentication.isAuthenticated()) {
            await this.deauthenticate().catch();
        }

        const url = `${this.endpointUrl}/authenticate/univali`;
        const response = await fetch(url, requestJSON(credentials));

        switch (response.status) {
            case HttpStatus.INTERNAL_SERVER_ERROR: throw new InternalServerError();
            case HttpStatus.NOT_FOUND: throw new IdentityNotRegisteredError();
            case HttpStatus.UNAUTHORIZED: throw new InvalidCredentialsError();
            case HttpStatus.OK:
                this.authentication.store(await response.json());
                break;
            default: throw new IncompatibleServiceVersionError();
        }
    }

    async deauthenticate(): Promise<void> {
        if (!this.authentication.isAuthenticated()) throw new UnauthorizedError();
        
        const url = `${this.endpointUrl}/deauthenticate`;
        const response = await fetch(url, requestJSON(undefined, this.authentication.getTokenHash()));

        switch (response.status) {
            case HttpStatus.UNAUTHORIZED: 
                this.authentication.invalidate();
                throw new UnauthorizedError();
            case HttpStatus.INTERNAL_SERVER_ERROR: throw new InternalServerError();
            case HttpStatus.OK: 
                this.authentication.invalidate();
                break;
            default: throw new IncompatibleServiceVersionError();
        }
    }
}

class ApplicationsFilesEndpoint {
    private endpointUrl: string;

    constructor(applicationUrl: string, private authentication: AuthenticationStore) {
        this.endpointUrl = `${applicationUrl}/files`;
    }

    async list(username?: string): Promise<Array<{ name: string }> | null> {
        let url = `${this.endpointUrl}`;
        if (username !== undefined) {
            url = `${url}?username=${username}`;
        }

        const response = await fetch(url, requestJSON(undefined, this.authentication.getTokenHash()!, 'GET'));

        switch (response.status) {
            case HttpStatus.UNAUTHORIZED:
                if (username === undefined) {
                    // if no username was provided, the server tries to find the application whose owner is the requester
                    // case the response was unauthorized, that means that the authentication token is not valid anymore
                    // in this case we just invalidate it, so in the next request, it can be checked before the request
                    this.authentication.invalidate();
                }
                throw new UnauthorizedError();
            case HttpStatus.INTERNAL_SERVER_ERROR: throw new InternalServerError();
            case HttpStatus.NOT_FOUND: return null;
            case HttpStatus.OK: return await response.json();
            default: throw new IncompatibleServiceVersionError();
        }
    }

    async upload(file: File, name: string): Promise<void> {
        if (!this.authentication.isAuthenticated()) throw new UnauthorizedError();

        const url = `${this.endpointUrl}/${name}`;
        const form = new FormData();
        form.append('file', file);
        const response = await fetch(url, {
            headers: getHeadersWithAuthorization(this.authentication.getTokenHash()!),
            method: 'POST',
            body: form,
        });

        switch (response.status) {
            case HttpStatus.UNAUTHORIZED: 
                this.authentication.invalidate();
                throw new UnauthorizedError();
            case HttpStatus.INTERNAL_SERVER_ERROR: throw new InternalServerError();
            case HttpStatus.CONFLICT: throw new FileNameAlreadyInUseError();
            case HttpStatus.OK: break;
            default: throw new IncompatibleServiceVersionError();
        }
    }

    async remove(filesNames: string[]): Promise<void> {
        if (!this.authentication.isAuthenticated()) throw new UnauthorizedError();

        const url = this.endpointUrl;
        const response = await fetch(url, requestJSON({ filesNames }, this.authentication.getTokenHash()!, 'DELETE'));

        switch (response.status) {
            case HttpStatus.UNAUTHORIZED: 
                this.authentication.invalidate();
                throw new UnauthorizedError();
            case HttpStatus.INTERNAL_SERVER_ERROR: throw new InternalServerError();
            case HttpStatus.OK: break;
            default: throw new IncompatibleServiceVersionError();
        }
    }

    async rename(changes: Array<[string, string]>): Promise<void> {
        if (!this.authentication.isAuthenticated()) throw new UnauthorizedError();

        const url = `${this.endpointUrl}/rename`;
        const response = await fetch(url, requestJSON({ changes }, this.authentication.getTokenHash()!, 'POST'));

        switch (response.status) {
            case HttpStatus.UNAUTHORIZED: 
                this.authentication.invalidate();
                throw new UnauthorizedError();
            case HttpStatus.INTERNAL_SERVER_ERROR: throw new InternalServerError();
            case HttpStatus.OK: break;
            default: throw new IncompatibleServiceVersionError();
        }
    }

    async download(fileName: string): Promise<File | null> {
        if (!this.authentication.isAuthenticated()) throw new UnauthorizedError();

        const url = `${this.endpointUrl}/${fileName}`;
        const response = await fetch(url, requestJSON(undefined, this.authentication.getTokenHash()!, 'GET'));

        switch (response.status) {
            case HttpStatus.UNAUTHORIZED: 
                this.authentication.invalidate();
                throw new UnauthorizedError();
            case HttpStatus.INTERNAL_SERVER_ERROR: throw new InternalServerError();
            case HttpStatus.NOT_FOUND: return null;
            case HttpStatus.OK: 
                const blob = await response.blob();
                const file = new File([blob], fileName, { type: response.headers.get('Content-Type')! });
                return file;
            default: throw new IncompatibleServiceVersionError();
        }
    }
}

class ApplicationsEndpoint {
    private endpointUrl: string;

    constructor(rootUrl: string, private authentication: AuthenticationStore) {
        this.endpointUrl = `${rootUrl}/applications`;
    }

    files(applicationName: string): ApplicationsFilesEndpoint {
        return new ApplicationsFilesEndpoint(`${this.endpointUrl}/${applicationName}`, this.authentication);
    }

    async create(application: { name: string, code: string }): Promise<void> {
        if (!this.authentication.isAuthenticated()) throw new UnauthorizedError();

        const url = this.endpointUrl;
        const response = await fetch(url, requestJSON(application, this.authentication.getTokenHash()));

        switch (response.status) {
            case HttpStatus.UNAUTHORIZED: 
                this.authentication.invalidate();
                throw new UnauthorizedError();
            case HttpStatus.INTERNAL_SERVER_ERROR: throw new InternalServerError();
            case HttpStatus.CONFLICT: throw new ApplicationNameAlreadyInUseError();
            case HttpStatus.OK: break;
            default: throw new IncompatibleServiceVersionError();
        }
    }

    async update(name: string, changes: { name?: string, code?: string }): Promise<void> {
        if (!this.authentication.isAuthenticated()) throw new UnauthorizedError();
        
        const url = `${this.endpointUrl}/${name}`;
        const response = await fetch(url, requestJSON(changes, this.authentication.getTokenHash(), 'PATCH'));

        switch (response.status) {
            case HttpStatus.UNAUTHORIZED: 
                this.authentication.invalidate();
                throw new UnauthorizedError();
            case HttpStatus.INTERNAL_SERVER_ERROR: throw new InternalServerError();
            case HttpStatus.NOT_FOUND: throw new ApplicationDoesNotExistError();
            case HttpStatus.CONFLICT: throw new ApplicationNameAlreadyInUseError();
            case HttpStatus.OK: break;
            default: throw new IncompatibleServiceVersionError();
        }
    }

    async delete(name: string): Promise<void> {
        if (!this.authentication.isAuthenticated()) throw new UnauthorizedError();

        const url = `${this.endpointUrl}/${name}`;
        const response = await fetch(url, requestJSON(undefined, this.authentication.getTokenHash(), 'DELETE'));

        switch (response.status) {
            case HttpStatus.UNAUTHORIZED: 
                this.authentication.invalidate();
                throw new UnauthorizedError();
            case HttpStatus.INTERNAL_SERVER_ERROR: throw new InternalServerError();
            case HttpStatus.NOT_FOUND: throw new ApplicationDoesNotExistError();
            case HttpStatus.OK: break;
            default: throw new IncompatibleServiceVersionError();
        }
    }

    async getAll(username?: string): Promise<Application[]> {
        if (!username && !this.authentication.isAuthenticated()) throw new UnauthorizedError();

        let url = this.endpointUrl;
        if (username) {
            url = `${url}?username=${username}`;
        }
        const response = await fetch(url, requestJSON(undefined, this.authentication.getTokenHash(), 'GET'));

        switch (response.status) {
            case HttpStatus.UNAUTHORIZED: 
                this.authentication.invalidate();
                throw new UnauthorizedError();
            case HttpStatus.INTERNAL_SERVER_ERROR: throw new InternalServerError();
            case HttpStatus.OK: 
                const data = await response.json();
                data.lastUpdated = new Date(data.lastUpdated);
                return data;
            default: throw new IncompatibleServiceVersionError();
        }
    }

    async getOne(name: string, username?: string): Promise<Application & { filesNames: string[] } | null> {
        if (!username && !this.authentication.isAuthenticated()) throw new UnauthorizedError();

        let url = `${this.endpointUrl}/${name}`;
        if (username) {
            url = `${url}?username=${username}`;
        }
        const response = await fetch(url, requestJSON(undefined, this.authentication.getTokenHash(), 'GET'));

        switch (response.status) {
            case HttpStatus.UNAUTHORIZED: 
                this.authentication.invalidate();
                throw new UnauthorizedError();
            case HttpStatus.INTERNAL_SERVER_ERROR: throw new InternalServerError();
            case HttpStatus.NOT_FOUND: return null;
            case HttpStatus.OK: 
                const data = await response.json();
                data.lastUpdated = new Date(data.lastUpdated);
                return data;
            default: throw new IncompatibleServiceVersionError();
        }
    }
}

type UserChanges = {
    name?: string,
    email?: string,
    biography?: string,
    showEmail?: boolean,
};

class UsersEndpoint {
    private readonly endpointUrl: string;

    constructor(rootUrl: string, private authentication: AuthenticationStore) {
        this.endpointUrl = `${rootUrl}/users`;
    }

    async getOne(username: string): Promise<User | null> {
        const url = `${this.endpointUrl}/${username}`;
        const response = await fetch(url, requestJSON(undefined, this.authentication.getTokenHash(), 'GET'));

        switch (response.status) {
            case HttpStatus.INTERNAL_SERVER_ERROR: throw new InternalServerError();
            case HttpStatus.NOT_FOUND: return null;
            case HttpStatus.OK: return await response.json();
            default: throw new IncompatibleServiceVersionError();
        }
    }

    async update(username: string, changes: UserChanges) {
        if (!this.authentication.isAuthenticated()) throw new UnauthorizedError();

        const url = `${this.endpointUrl}/${username}`;
        const response = await fetch(url, requestJSON(changes, this.authentication.getTokenHash(), 'PATCH'));

        switch (response.status) {
            case HttpStatus.UNAUTHORIZED: 
                this.authentication.invalidate();
                throw new UnauthorizedError();
            case HttpStatus.INTERNAL_SERVER_ERROR: throw new InternalServerError();
            case HttpStatus.FORBIDDEN: throw new UserIsNotSelfError();
            case HttpStatus.NOT_FOUND: break;
            case HttpStatus.OK: break;
            default: throw new IncompatibleServiceVersionError();
        }
    }
}

class AuthenticationStore {
    private storageKey = 'authentication-token';

    invalidate() {
        localStorage.removeItem(this.storageKey);
    }

    store(token: { creationDate: Date, hash: string, userUsername: string }) {
        localStorage.setItem(this.storageKey, JSON.stringify(token));
    }

    isAuthenticated(): boolean {
        return localStorage.getItem(this.storageKey) !== null;
    }

    getTokenHash(): string | undefined {
        const stringValue = localStorage.getItem(this.storageKey);
        if (stringValue === null) return undefined;
        return JSON.parse(stringValue).hash;
    }

    getUsername(): string | undefined {
        const stringValue = localStorage.getItem(this.storageKey);
        if (stringValue === null) return undefined;
        return JSON.parse(stringValue).userUsername;
    }
}

export default class ApplicationsServer {
    users: UsersEndpoint;
    account: AccountEndpoint;
    applications: ApplicationsEndpoint;
    authentication = new AuthenticationStore();

    constructor() {
        const rootUrl = 'http://localhost:8080/api';
        this.account = new AccountEndpoint(rootUrl, this.authentication);
        this.applications = new ApplicationsEndpoint(rootUrl, this.authentication);
        this.users = new UsersEndpoint(rootUrl, this.authentication);
    }
}