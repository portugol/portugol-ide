import * as React from 'react';
import Heading from '../Heading';
import NavigationButton from '../NavigationButton';
import { ModalContext } from './';
// tslint:disable-next-line:no-var-requires
const styles = require('./Heading.css');

type HeadingProps = React.PropsWithChildren<{}>;

export default (props: HeadingProps) => (
    <ModalContext.Consumer>
        {({ toggle, canToggle }) => (
            <Heading>
                <NavigationButton
                    type="button"
                    className={styles.closeButton}
                    disabled={!canToggle}
                    onClick={toggle}
                >
                    <i className="fas fa-times" />
                </NavigationButton>
                {props.children}
            </Heading>
        )}
    </ModalContext.Consumer>
);