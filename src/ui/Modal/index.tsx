import classNames from 'classnames';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import OnKeyUp from '../../helpers/OnKeyUp';
import Backdrop from '../Backdrop';
// tslint:disable-next-line:no-var-requires
const styles = require('./index.css');

type ModalProps = React.PropsWithChildren<{
    toggle: () => void,
    isOpen: boolean,
    className?: string,
}>;

type ModalDefaultProps = {
    canToggle: boolean,
};

const ESC_KEYCODE = 27;

type ModalContextType = {
    toggle: () => void,
    canToggle: boolean,
};

export const ModalContext = React.createContext<ModalContextType>({
    toggle: () => undefined,
    canToggle: true,
});

const Modal: React.FC<ModalProps & ModalDefaultProps> = props => {
    const toggle = () => {
        if (props.canToggle) {
            props.toggle();
        }
    };

    const cancelClickPropagationToBackdrop = (event: React.MouseEvent) => {
        event.stopPropagation();
    };

    return !props.isOpen
        ? null
        : ReactDOM.createPortal((
            <ModalContext.Provider value={{ toggle, canToggle: props.canToggle }}>
                <OnKeyUp keyCode={ESC_KEYCODE} do={toggle}>
                    <Backdrop onClick={toggle}>
                        <div 
                            onClick={cancelClickPropagationToBackdrop}
                            className={classNames(styles.modal, props.className)}
                        >
                            {props.children}
                        </div>
                    </Backdrop>
                </OnKeyUp>
            </ModalContext.Provider>
        ), document.body);
};

const defaultProps: ModalDefaultProps = {
    canToggle: true,
};

Modal.defaultProps = defaultProps;

export default Modal as React.FC<ModalProps & Partial<ModalDefaultProps>>;