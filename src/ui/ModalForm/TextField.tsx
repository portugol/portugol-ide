import * as React from 'react';
import Input from '../Input';
import InputWithMessage from '../InputWithMessage';
import { ModalFormContext } from './';

type TextFieldProps = {
    name: string,
    placeholder?: string,
    isSecure?: boolean,
    validate?: (value: string, values: { [key: string]: string }) => string | null,
};

type TextFieldInjectedProps = {
    setError: (field: string) => (error: boolean) => void,
    setValue: (field: string) => (value: string) => void,
    getValue: (field: string) => string,
    getValues: () => { [key: string]: string },
    register: (field: TextField) => void,
    unregister: (field: TextField) => void,
    disabled: boolean,
};

type TextFieldState = {
    pristine: boolean,
    error: string | null,
};

export class TextField extends React.Component<TextFieldProps & TextFieldInjectedProps> {
    state: TextFieldState = {
        pristine: true,
        error: null,
    };

    componentDidMount() {
        this.props.register(this);
    }

    componentWillUnmount() {
        this.props.unregister(this);
    }

    onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = event.target.value;
        const updateValue = () => this.props.setValue(this.props.name)(newValue);
        if (this.state.pristine) {
            this.setState({ pristine: false }, updateValue);
        } else {
            updateValue();
        }
    }

    updateError() {
        // if the field has not being touched yet, do not add error
        if (this.state.pristine) { return; }

        let error: string | null = null;
        if (this.props.validate) {
            error = this.props.validate(
                this.props.getValue(this.props.name), 
                this.props.getValues(),
            );
        }
        if (error !== this.state.error) {
            this.props.setError(this.props.name)(error !== null);
            this.setState({ error });
        }
    }

    touch = () => {
        this.setState({ pristine: false });
    }

    render() {
        const value = this.props.getValue(this.props.name);

        return (
            <InputWithMessage
                error={this.state.error}
                disabled={this.props.disabled}
            >
                {({ inputClassName }) => (
                    <Input
                        className={inputClassName}
                        type={this.props.isSecure ? 'password' : 'text'}
                        placeholder={this.props.placeholder}
                        onChange={this.onChange}
                        value={value}
                        disabled={this.props.disabled}
                    />
                )}
            </InputWithMessage>
        );
    }
}

export default (props: TextFieldProps) => (
    <ModalFormContext.Consumer>
        {({ getValue, getValues, register, setError, setValue, unregister, disabled }) => (
            <TextField
                {...props}
                setError={setError}
                setValue={setValue}
                getValue={getValue}
                getValues={getValues}
                register={register}
                unregister={unregister}
                disabled={disabled}
            />
        )}
    </ModalFormContext.Consumer>
);