import classNames from 'classnames';
import * as React from 'react';
import Modal from '../Modal';
import Heading from '../Modal/Heading';
import { TextField } from './TextField';
// tslint:disable-next-line no-var-requires
const styles = require('./index.css');

type ModalFormProps = React.PropsWithChildren<{
    className?: string,
    title: string,
    isOpen: boolean,
    disabled: boolean,
    toggle: () => void,
    onSubmit: (values: { [key: string]: string }) => void,
}>;

type ModalFormState = {
    values: { [key: string]: string },
    errors: { [key: string]: boolean },
};

type ModalFormContextType = {
    setError: (field: string) => (error: boolean) => void,
    setValue: (field: string) => (value: string) => void,
    getValue: (field: string) => string,
    getValues: () => { [key: string]: string },
    register: (field: TextField) => void,
    unregister: (field: TextField) => void,
    disabled: boolean,
    hasError: boolean,
};

export const ModalFormContext = React.createContext<ModalFormContextType>({
    setError: () => () => undefined,
    setValue: () => () => undefined,
    getValue: () => '',
    getValues: () => ({}),
    register: () => undefined,
    unregister: () => undefined,
    disabled: false,
    hasError: false,
});

export default class ModalForm extends React.Component<ModalFormProps, ModalFormState> {
    state: ModalFormState = {
        values: {},
        errors: {},
    };

    private fields: TextField[] = [];

    setError = (field: string) => (error: boolean) => {
        this.setState(({ errors }) => ({
            errors: {
                ...errors,
                [field]: error,
            },
        }));
    }

    setValue = (fieldName: string) => (value: string) => {
        this.setState(({ values }) => ({
            values: {
                ...values,
                [fieldName]: value,
            },
        }), () => {
            // after updating the value of a field, it is needed to ask every field to update their errors
            // cause sometimes, one field validation depends on the value of other
            for (const field of this.fields) {
                field.updateError();
            }
        });
    }

    getValue = (field: string) => {
        return this.state.values[field] || '';
    }

    register = (field: TextField) => {
        const fieldName = field.props.name;
        if (Object.keys(this.state).includes(fieldName)) {
            // tslint:disable-next-line:no-console
            console.error(`There is more than one field named "${fieldName}".`);
        } else {
            this.setValue(fieldName)('');
            this.setError(fieldName)(false);
        }
        this.fields.push(field);
    }

    unregister = (field: TextField) => {
        const fieldName = field.props.name;
        this.fields.splice(this.fields.indexOf(field), 1);
        if (this.fields.findIndex(f => f.props.name === fieldName) === -1) {
            const values = { ...this.state.values };
            const errors = { ...this.state.errors };
            delete values[fieldName];
            delete errors[fieldName];
            this.setState({ values, errors });
        }
    }

    onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        for (const field of this.fields) {
            field.touch();
        }

        // wait for the touch to happen ;)
        this.setState({}, () => {
            for (const field of this.fields) {
                field.updateError();
            }
    
            // wait for the update error to finish ;)
            this.setState({}, () => {
                if (this.hasError()) return;
        
                this.props.onSubmit(this.state.values);
            });
        });
    }

    hasError(): boolean {
        for (const field of Object.keys(this.state.errors)) {
            if (this.state.errors[field]) { return true; }
        }
        return false;
    }

    getValues = () => this.state.values;

    render() {
        const hasError = this.hasError();
        const { setError, setValue, getValue, getValues, register, unregister } = this;
        const { disabled } = this.props;
        return (
            <ModalFormContext.Provider
                value={{
                    setError,
                    setValue,
                    getValue,
                    getValues,
                    register,
                    unregister,
                    disabled,
                    hasError,
                }}
            >
                <Modal
                    className={classNames(styles.modalForm, this.props.className)}
                    isOpen={this.props.isOpen}
                    toggle={this.props.toggle}
                    canToggle={!this.props.disabled}
                >
                    <Heading>
                        {this.props.title}
                    </Heading>
                    <form onSubmit={this.onSubmit}>
                        {this.props.children}
                    </form>
                </Modal>
            </ModalFormContext.Provider>
        );
    }
}