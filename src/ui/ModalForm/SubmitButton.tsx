import * as React from 'react';
import Button from '../Button';
import { ModalFormContext } from './';
// tslint:disable-next-line no-var-requires
const styles = require('./SubmitButton.css');

type SubmitButtonProps = React.PropsWithChildren<{}>;

export default (props: SubmitButtonProps) => (
    <ModalFormContext.Consumer>
        {({ disabled, hasError }) => (
            <Button
                className={styles.button}
                type="submit"
                disabled={hasError || disabled}
            >
                {props.children}
            </Button>
        )}
    </ModalFormContext.Consumer>
);