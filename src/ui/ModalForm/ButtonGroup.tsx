import * as React from 'react';
// tslint:disable-next-line no-var-requires
const styles = require('./ButtonGroup.css');

type ButtonGroupProps = React.PropsWithChildren<{}>;

export default (props: ButtonGroupProps) => (
    <div className={styles.buttonGroup}>
        {props.children}
    </div>
);