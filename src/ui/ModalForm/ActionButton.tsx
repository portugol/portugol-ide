import * as React from 'react';
import Button from '../Button';
import { ModalFormContext } from './';
// tslint:disable-next-line no-var-requires
const styles = require('./ActionButton.css');

type ActionButtonProps = React.PropsWithChildren<{
    action: () => void,
}>;

export default (props: ActionButtonProps) => (
    <ModalFormContext.Consumer>
        {({ disabled }) => (
            <Button
                className={styles.button}
                type="button"
                onClick={props.action}
                disabled={disabled}
            >
                {props.children}
            </Button>
        )}
    </ModalFormContext.Consumer>
);