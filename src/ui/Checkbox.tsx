import classNames from 'classnames';
import * as React from 'react';
// tslint:disable-next-line no-var-requires
const styles = require('./Checkbox.css');

type CheckboxProps = React.InputHTMLAttributes<HTMLInputElement>;

export default React.forwardRef<HTMLInputElement, CheckboxProps>((props, ref) => (
    <label className={classNames(styles.container, props.className, { disabled: props.disabled })}>
        <input
            {...props}
            type="checkbox"
            className={styles.checkbox}
            ref={ref}
        />
        <i className={classNames('fas fa-check', styles.checkmark)} />
    </label>
));