import classNames from 'classnames';
import * as React from 'react';
// tslint:disable-next-line no-var-requires
const styles = require('./Button.css');

type ButtonProps = React.ButtonHTMLAttributes<HTMLButtonElement>;

export default React.forwardRef<HTMLButtonElement, ButtonProps>((props, ref) => (
    <button 
        {...props} 
        className={classNames(styles.button, props.className)}
        ref={ref}
    />
));