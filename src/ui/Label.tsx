import classNames from 'classnames';
import * as React from 'react';
// tslint:disable-next-line no-var-requires
const styles = require('./Label.css');

type LabelStyles = React.HTMLAttributes<HTMLLabelElement>;

export default React.forwardRef<HTMLLabelElement, LabelStyles>((props, ref) => (
    <label 
        {...props}
        className={classNames(styles.label, props.className)}
        ref={ref}
    />
));