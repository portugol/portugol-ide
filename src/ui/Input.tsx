import classNames from 'classnames';
import * as React from 'react';
// tslint:disable-next-line no-var-requires
const styles = require('./Input.css');

type InputProps = React.InputHTMLAttributes<HTMLInputElement>;

export default React.forwardRef<HTMLInputElement, InputProps>((props, ref) => (
    <input
        {...props}
        className={classNames(styles.input, props.className)}
        ref={ref}
    />
));