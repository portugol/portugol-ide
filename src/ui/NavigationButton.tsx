import classNames from 'classnames';
import * as React from 'react';
// tslint:disable-next-line no-var-requires
const styles = require('./NavigationButton.css');

type NavigationButtonProps = React.ButtonHTMLAttributes<HTMLButtonElement>;

export default React.forwardRef<HTMLButtonElement, NavigationButtonProps>((props, ref) => (
    <button
        {...props}
        className={classNames(styles.navigationButton, props.className)}
        ref={ref}
    />
));