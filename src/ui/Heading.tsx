import classNames from 'classnames';
import * as React from 'react';
// tslint:disable-next-line no-var-requires
const styles = require('./Heading.css');

type HeadingStyles = React.HTMLAttributes<HTMLHeadingElement>;

export default React.forwardRef<HTMLHeadingElement, HeadingStyles>((props, ref) => (
    <h1 
        {...props}
        className={classNames(styles.heading, props.className)}
        ref={ref}
    />
));