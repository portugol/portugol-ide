import classNames from 'classnames';
import * as React from 'react';
// tslint:disable-next-line no-var-requires
const styles = require('./TextArea.css');

type TextAreaProps = React.TextareaHTMLAttributes<HTMLTextAreaElement>;

export default React.forwardRef<HTMLTextAreaElement, TextAreaProps>((props, ref) => (
    <textarea
        {...props}
        className={classNames(styles.textArea, props.className)}
        ref={ref}
    />
));