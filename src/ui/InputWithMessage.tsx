import classNames from 'classnames';
import * as React from 'react';
// tslint:disable-next-line no-var-requires
const styles = require('./InputWithMessage.css');

type InputWithMessageProps = {
    children: (injectedProps: { inputClassName: string }) => React.Props<{}>['children'],
    error: string | null,
    disabled?: boolean,
    className?: string,
};

export default (props: InputWithMessageProps) => (
    <div className={classNames(
        styles.inputWithMessage, 
        { 
            'has-error': props.error !== null,
            'disabled': props.disabled === true,
        },
        props.className,
    )}>
        {props.children({ inputClassName: styles.input })}
        <div className={styles.errorMessage}>
            {props.error}
        </div>
    </div>
);