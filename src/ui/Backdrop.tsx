import classNames from 'classnames';
import * as React from 'react';
// tslint:disable-next-line no-var-requires
const styles = require('./Backdrop.css');

type BackdropProps = React.HTMLAttributes<HTMLDivElement>;

export default React.forwardRef<HTMLDivElement, BackdropProps>((props, ref) => (
    <div
        {...props}
        className={classNames(styles.backdrop, props.className)}
        ref={ref}
    />
));