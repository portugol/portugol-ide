import * as monaco from 'monaco-editor';
import * as React from 'react';
import { install as installPortugolClient } from './portugol/server/client';

type EditorProps = {
    width: number,
    height: number,
    onChange: (value: string) => void,
};

export class Editor extends React.Component<EditorProps> {
    editorContainer = React.createRef<HTMLDivElement>();
    editor: monaco.editor.IStandaloneCodeEditor;
    disposables: monaco.IDisposable[] = [];

    getValue = () => this.editor.getValue();

    getPosition = () => this.editor.getPosition();

    componentDidMount() {
        let disposable: monaco.IDisposable;

        this.editor = monaco.editor.create(this.editorContainer.current!, { autoClosingBrackets: true, matchBrackets: true, fontSize: 20 });
        disposable = installPortugolClient(this.editor);
        this.disposables.push(disposable);
        disposable = this.editor.onDidChangeModelContent(event => {
            this.props.onChange(this.editor.getValue());
        });
        this.disposables.push(disposable);
    }

    componentDidUpdate(oldProps: EditorProps) {
        if (oldProps.width !== this.props.width || oldProps.height !== this.props.height) {
            this.editor.layout();
        }   
    }

    componentWillUnmount() {
        this.disposables.forEach(d => d.dispose());
    }

    render() {
        return (
            <div 
                ref={this.editorContainer}
                style={{ 
                    height: `${this.props.height}px`,
                    position: 'absolute',
                    width: `${this.props.width}px`, 
                }}
            />
        );
    }
}

import { ResizeObserver } from 'resize-observer';

type AutoSizingEditorProps = {
    render: (params: { width: number, height: number }) => React.ReactNode;
};

type AutoSizingEditorState = {
    width: number,
    height: number,
};

export class FullSize extends React.Component<AutoSizingEditorProps, AutoSizingEditorState> {
    resizingWrapper = React.createRef<HTMLDivElement>();
    state = {
        height: 0,
        width: 0,
    };
    resizeObserver: ResizeObserver;

    handleWrapperResize = () => {
        this.setState({
            height: this.resizingWrapper.current!.clientHeight,
            width: this.resizingWrapper.current!.clientWidth,
        });
    }

    componentDidMount() {
        this.resizeObserver = new ResizeObserver(this.handleWrapperResize);
        this.resizeObserver.observe(this.resizingWrapper.current!);
        this.handleWrapperResize();
    }

    componentWillUnmount() {
        this.resizeObserver.unobserve(this.resizingWrapper.current!);
    }

    render() {
        return (
            <div ref={this.resizingWrapper} style={{ height: '100%', width: '100%' }}>
                <div style={{ position: 'relative' }}>
                    {this.props.render({ width: this.state.width, height: this.state.height })}
                </div>
            </div>
        );
    }
}