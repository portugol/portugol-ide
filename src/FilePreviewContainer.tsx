import classNames from 'classnames';
import * as React from 'react';
import AudioPlayer from './AudioPlayer';
import './FilePreviewContainer.css';
import FontInliner from './FontInliner';

export function isFileAudio(file: File): boolean {
    const fileNameSplit = file.name.split('.');
    const extension = fileNameSplit[fileNameSplit.length - 1].toLowerCase();
    return extension === 'mp3' || extension === 'wav' || extension === 'ogg';
}

export function isFileImage(file: File): boolean {
    const fileNameSplit = file.name.split('.');
    const extension = fileNameSplit[fileNameSplit.length - 1].toLowerCase();
    return extension === 'png' || extension === 'jpg' || extension === 'jpeg' || extension === 'gif';
}

export function isFileFont(file: File): boolean {
    const fileNameSplit = file.name.split('.');
    const extension = fileNameSplit[fileNameSplit.length - 1].toLowerCase();
    return extension === 'ttf' || extension === 'otf' || extension === 'woff';
}

type FilePreviewWrapperProps = React.Props<{}> & {
    selected: boolean,
    tracked: boolean,
    title: string,
    onClick: () => void,
    className: string,
    style?: React.CSSProperties,
};

class FilePreviewWrapper extends React.Component<FilePreviewWrapperProps> {
    onClick = (event: React.MouseEvent<HTMLDivElement>) => {
        event.stopPropagation();
        this.props.onClick();
    }

    render() {
        return (
            <div
                onClick={this.onClick}
                className={classNames('file-preview-item', this.props.className, {
                    'selected': this.props.selected,
                    'untracked': !this.props.tracked,
                })}
                title={this.props.title}
                style={this.props.style}
            >
                {this.props.children}
            </div>
        );
    }
}

type FilePreviewOverlayProps = {
    isLoading: boolean,
    isUploaded: boolean,
    isTracked: boolean,
};

const FilePreviewOverlay = (props: FilePreviewOverlayProps) => {
    const iconType = props.isUploaded
        ? 'UPLOADED'
        : props.isTracked
            ? 'TRACKED'
            : 'UNTRACKED';
    return (
        <div className={classNames('overlay', {
            'is-loading': props.isLoading,
        })}>
            <i className="fas fa-spinner fa-pulse loading-indicator" />
            {!props.isLoading && iconType !== 'TRACKED' && (
                <i className={classNames('fas status-icon', {
                    'fa-cloud': iconType === 'UPLOADED',
                    'fa-code': iconType === 'UNTRACKED'
                })} />
            )}
        </div>
    );
};

type FilePreviewContainerProps = React.Props<{}>;

export type PreviewableFile = {
    raw?: File,
    name: string,
    dataUrl?: string,
    tracked: boolean,
    uploaded: boolean,
};

class FilePreviewFont extends React.Component<FilePreviewItemProps> {
    render() {
        return (
            <FontInliner fontUrl={this.props.file && this.props.file.dataUrl || null}>
                {({ fontName }) => (
                    <FilePreviewWrapper
                        onClick={this.props.onClick}
                        selected={this.props.selected}
                        title={this.props.file.name}
                        className="is-font"
                        style={{ fontFamily: fontName === null ? '' : fontName }}
                        tracked={this.props.file.tracked}
                    >
                        <div className="font-preview">
                            0aA
                        </div>
                        <FilePreviewOverlay
                            isUploaded={this.props.file.uploaded}
                            isTracked={this.props.file.tracked}
                            isLoading={this.props.isLoading}
                        />
                    </FilePreviewWrapper>
                )}
            </FontInliner>
        );
    }
}

class FilePreviewDefault extends React.Component<FilePreviewItemProps> {
    render() {
        const splitFileName = this.props.file.name.split('.');
        const fileExtension = splitFileName[splitFileName.length - 1];

        return (
            <FilePreviewWrapper
                onClick={this.props.onClick}
                selected={this.props.selected}
                title={this.props.file.name}
                className="is-default"
                tracked={this.props.file.tracked}
            >
                <span className="file-extension">
                    .{fileExtension}
                </span>
                <FilePreviewOverlay
                    isUploaded={this.props.file.uploaded}
                    isTracked={this.props.file.tracked}
                    isLoading={this.props.isLoading}
                />
            </FilePreviewWrapper >
        );
    }
}

class FilePreviewItemImage extends React.Component<FilePreviewItemProps> {
    render() {
        return (
            <FilePreviewWrapper
                onClick={this.props.onClick}
                selected={this.props.selected}
                title={this.props.file.name}
                tracked={this.props.file.tracked}
                className="is-image"
                style={this.props.file.dataUrl === undefined
                    ? undefined
                    : { backgroundImage: `url(${this.props.file.dataUrl})` }
                }
            >
                <FilePreviewOverlay
                    isUploaded={this.props.file.uploaded}
                    isTracked={this.props.file.tracked}
                    isLoading={this.props.isLoading}
                />
            </FilePreviewWrapper>
        );
    }
}

class FilePreviewItemAudio extends React.Component<FilePreviewItemProps> {
    playOrStopAudio = (playOrStop: () => void) => (event: React.MouseEvent<HTMLButtonElement>) => {
        event.stopPropagation();
        playOrStop();
    }

    stopIfPlayingAndCallOnClick = (isPlaying: boolean, playOrStop: () => void) => () => {
        if (isPlaying) {
            playOrStop();
        }
        this.props.onClick();
    }

    render() {
        return (
            <AudioPlayer audioUrl={this.props.file && this.props.file.dataUrl || null}>
                {({ isPlaying, playOrStop }) => (
                    <FilePreviewWrapper
                        onClick={this.stopIfPlayingAndCallOnClick(isPlaying, playOrStop)}
                        selected={this.props.selected}
                        title={this.props.file.name}
                        tracked={this.props.file.tracked}
                        className="is-audio"
                    >
                        <FilePreviewOverlay
                            isUploaded={this.props.file.uploaded}
                            isTracked={this.props.file.tracked}
                            isLoading={this.props.isLoading}
                        />
                        <button
                            className="play-button"
                            onClick={this.playOrStopAudio(playOrStop)}
                        >
                            {isPlaying
                                ? <i className="fas fa-stop" />
                                : <i className="fas fa-play" />}
                        </button>
                    </FilePreviewWrapper>
                )}
            </AudioPlayer>
        );
    }
}

type FilePreviewItemProps = {
    file: PreviewableFile,
    isLoading: boolean,
    onClick: () => void,
    selected: boolean,
};

export class FilePreviewItem extends React.Component<FilePreviewItemProps> {
    onClick = (event: React.MouseEvent<HTMLDivElement>) => {
        event.stopPropagation();
        this.props.onClick();
    }

    render() {
        if (this.props.file.raw) {
            if (isFileImage(this.props.file.raw)) {
                return <FilePreviewItemImage {...this.props} />;
            } else if (isFileAudio(this.props.file.raw)) {
                return <FilePreviewItemAudio {...this.props} />;
            } else if (isFileFont(this.props.file.raw)) {
                return <FilePreviewFont {...this.props} />;
            }
        }
        return <FilePreviewDefault {...this.props} />;
    }
}

export default (props: FilePreviewContainerProps) => (
    <div className="file-preview-container">
        {props.children}
    </div>
);