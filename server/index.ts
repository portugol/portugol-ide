import * as cors from 'cors';
import * as express from 'express';
import * as path from 'path';
import * as swaggerUI from 'swagger-ui-express';
import * as db from './db';
import * as swaggerDocument from './documentation.json';
import accountRouter from './routers/account';
import applicationsRouter from './routers/applications';
import usersRouter from './routers/users';

const api = express.Router();
api.use(express.json());
api.use(cors());

const customSwaggerOptions = {
    explorer: true,
    swaggerOptions: {
        // TODO: this should be working
        authAction: {
            default: {
                name: 'default',
                schema: {
                    type: 'apiKey',
                    in: 'header',
                    name: 'Authorization',
                    description: ''
                },
                value: 'Bearer <my own default token>'
            }
        }
    }
};
api.use('/documentation', swaggerUI.serve, swaggerUI.setup(swaggerDocument, customSwaggerOptions));

api.use('/account', accountRouter);
api.use('/users', usersRouter);
api.use('/applications', applicationsRouter);

const app = express();
app.use('/api', api);
app.use(express.static(path.join(__dirname, '../build/')));
app.get('*', (request, response) => response.sendFile(path.join(__dirname, '../build/index.html')));

async function main() {
    try {
        await db.connect();
    } catch (error) {
        // tslint:disable-next-line no-console
        console.error(error);
        // tslint:disable-next-line no-console
        console.error('ocorreu um erro ao tentar se conectar com o banco de dados');
        process.exit(1);
    }

    // tslint:disable-next-line no-console
    console.log('conectado ao banco de dados com sucesso');

    const port = process.env.PORT || 8080;

    app.listen(port, () => {
        // tslint:disable-next-line no-console
        console.log(`escutando na porta ${port}`);
    });
}

main();