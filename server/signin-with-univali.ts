import * as FormData from 'form-data';
import { JSDOM } from 'jsdom';
import nodeFetch from 'node-fetch';
// tslint:disable-next-line no-var-requires
const fetch = require('fetch-cookie/node-fetch')(nodeFetch) as typeof nodeFetch;

const intranetUrl = 'https://intranet.univali.br';

export default function signInWithUnivali(personCode: string, password: string) {
    const nameSelector = '#subMenu > div.box > div > div:nth-child(3) > div.tituloservicos';
    const emailSelector = '#subMenu > div.box > div > div:nth-child(3) > div:nth-child(3) > div:nth-child(2) > a';

    const form = new FormData() as any;
    form.append('ca_usuario', personCode);
    form.append('ca_senha', password);
    return fetch(`${intranetUrl}/login.php`, {
        method: 'POST',
        body: form
    }).then(() => {
        return fetch(`${intranetUrl}/intranet/PortalAluno/`, {
            method: 'GET'
        }).then(response => response.text());
    }).then(html => {
        const dom = new JSDOM(html);
        const document = dom.window.document;
        const nameElement = document
            .querySelector(nameSelector)!;
        const emailElement = document
            .querySelector(emailSelector)!;
        return {
            name: nameElement.innerHTML,
            email: emailElement.innerHTML,
        };
    });
}