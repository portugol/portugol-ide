import * as fs from 'fs';
import * as path from 'path';

const uploadFolder = path.join(__dirname, 'upload');

function ensureFolderExists(folder: string, cb: (error: NodeJS.ErrnoException | null) => void) {
    fs.mkdir(folder, { recursive: true }, (err) => {
        if (err) {
            if (err.code === 'EEXIST') cb(null);
            else cb(err);
        } else cb(null);
    });
}

export function createFilePath(username: string, application: string, fileName?: string): string {
    return fileName
        ? path.join(uploadFolder, username, application, fileName)
        : path.join(uploadFolder, username, application);
}

export async function existsFile(name: string, username: string, application: string): Promise<boolean> {
    const filePath = createFilePath(username, application, name);
    return new Promise((resolve) => fs.exists(filePath, resolve));
}

export async function addFile(origin: string, name: string, username: string, application: string) {
    const filePath = createFilePath(username, application, name);
    const folder = path.dirname(filePath);

    return new Promise((resolve, reject) => {
        ensureFolderExists(folder, (ensureExistsError) => {
            if (ensureExistsError) { reject(ensureExistsError); }
            else {
                fs.copyFile(origin, filePath, (copyError) => {
                    if (copyError) { reject(copyError); }
                    else {
                        resolve(undefined);
                    }
                });
            }
        });
    });
}

export async function removeFile(name: string, username: string, application: string) {
    const filePath = createFilePath(username, application, name);
    const folder = path.dirname(filePath);

    return new Promise((resolve, reject) => {
        ensureFolderExists(folder, (ensureExistsError) => {
            if (ensureExistsError) { reject(ensureExistsError); }
            else {
                fs.unlink(filePath, (unlinkError) => {
                    if (unlinkError) {
                        reject(unlinkError);
                    } else {
                        resolve(undefined);
                    }
                });
            }
        });
    });
}

export async function renameFile(oldName: string, newName: string, username: string, application: string) {
    const oldFilePath = createFilePath(username, application, oldName);
    const newFilePath = createFilePath(username, application, newName);
    const folder = path.dirname(oldFilePath);

    return new Promise((resolve, reject) => {
        ensureFolderExists(folder, (ensureExistsError) => {
            if (ensureExistsError) { reject(ensureExistsError); }
            else {
                fs.rename(oldFilePath, newFilePath, (renameError) => {
                    if (renameError) { reject(renameError); }
                    else {
                        resolve(undefined);
                    }
                });
            }
        });
    });
}

export async function getFilesNames(username: string, application: string): Promise<string[]> {
    const folder = createFilePath(username, application);

    return new Promise((resolve, reject) => {
        ensureFolderExists(folder, (ensureExistsError) => {
            if (ensureExistsError) { reject(ensureExistsError); }
            else {
                fs.readdir(folder, (readError, filesNames) => {
                    if (readError) { reject(readError); }
                    else {
                        resolve(filesNames);
                    }
                });
            }
        });
    });
}

export async function getFile(name: string, username: string, application: string): Promise<Buffer> {
    const folder = createFilePath(username, application, name);

    return new Promise((resolve, reject) => {
        ensureFolderExists(folder, (ensureExistsError) => {
            if (ensureExistsError) { reject(ensureExistsError); }
            else {
                fs.readFile(folder, (readError, buffer) => {
                    if (readError) { reject(readError); }
                    else {
                        resolve(buffer);
                    }
                });
            }
        });
    });
}

export async function removeApplicationFolder(applicationName: string, username: string) {
    throw Error('not implemented');
}

export async function renameApplicationFolder(oldName: string, newName: string, username: string) {
    throw Error('not implemented');
}