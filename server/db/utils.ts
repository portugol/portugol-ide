export function stripUnderscoreId<T extends {}>(value: T): T;
export function stripUnderscoreId<T extends {}>(value: T | null): T | null;
export function stripUnderscoreId<T extends {}>(value: T[]): T[];
export function stripUnderscoreId<T extends {}>(value: T[] | null): T[] | null;
export function stripUnderscoreId<T extends {}>(value: T | T[] | null): T | T[] | null {
    if (value === null) return null;
    if (Array.isArray(value)) return value.map(stripUnderscoreId as any);
    return { ...value, _id: undefined };
}