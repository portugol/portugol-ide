import * as uuid from 'uuid';
import { AuthenticationToken, authenticationTokens, institutionsIdentities, User, users } from './collections';
import { IdentityAlreadyInUseError, IdentityNotRegisteredError, InvalidPasswordError, UserDoesNotExistError, UsernameAlreadyBeingUsedError } from './errors';
import { stripUnderscoreId } from './utils';

export async function authenticate(username: string, password: string): Promise<AuthenticationToken> {
    const user = await users!.findOne({ username });

    if (!user) {
        throw new UserDoesNotExistError();
    }

    if (user.password !== password) {
        throw new InvalidPasswordError();
    }

    const authenticationToken: AuthenticationToken = {
        creationDate: new Date(),
        userUsername: username,
        hash: uuid(),
    };
    await authenticationTokens!.insertOne(authenticationToken);

    return stripUnderscoreId(authenticationToken);
}

export async function authenticateWithUnivali(personCode: string): Promise<AuthenticationToken> {
    const identity = await institutionsIdentities!.findOne({ personCode });

    if (!identity) {
        throw new IdentityNotRegisteredError();
    }

    const authenticationToken: AuthenticationToken = {
        creationDate: new Date(),
        userUsername: identity.userUsername,
        hash: uuid(),
    };
    await authenticationTokens!.insertOne(authenticationToken);

    return stripUnderscoreId(authenticationToken);
}

export async function registerWithUnivali(personCode: string, email: string, username: string, name: string): Promise<User> {
    let identity = await institutionsIdentities!.findOne({ personCode });

    if (identity) {
        throw new IdentityAlreadyInUseError();
    }

    let user = await users!.findOne({ username });

    if (user) {
        throw new UsernameAlreadyBeingUsedError();
    }

    user = {
        biography: '',
        name,
        email,
        showEmail: false,
        username,
        // TODO: save password somewhere else
        password: 'something impossible kopSOKPSaokpskpAS12333',
    };

    users!.insertOne(user);

    identity = {
        institution: 'UNIVALI',
        personCode,
        userUsername: username,
    };

    institutionsIdentities!.insertOne(identity);

    return stripUnderscoreId(user);
}

export async function deauthenticate(authenticationTokenHash: string): Promise<void> {
    await authenticationTokens!.deleteOne({ hash: authenticationTokenHash });
}

export async function hasUnivaliAccount(personCode: string): Promise<boolean> {
    const identity = await institutionsIdentities!.findOne({ personCode });
    return identity !== null;
}