export class UsernameAlreadyBeingUsedError {}
export class UserDoesNotExistError {}
export class InvalidPasswordError {}
export class ApplicationNameAlreadyBeingUsedError {}
export class ApplicationDoesNotExistError {}
export class IdentityNotRegisteredError {}
export class IdentityAlreadyInUseError {}