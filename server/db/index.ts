import { FilterQuery, MongoClient } from 'mongodb';
import { 
    Application, 
    applications, 
    authenticationTokens,
    setCollections,
    User,
    users,
} from './collections';
import { ApplicationDoesNotExistError, ApplicationNameAlreadyBeingUsedError, UsernameAlreadyBeingUsedError } from './errors';
import { stripUnderscoreId } from './utils';

let client: MongoClient | undefined;

export async function connect(): Promise<void> {
    if (client) return;

    const url = 'mongodb://localhost:27017/portugol-web';

    client = await MongoClient.connect(url, { useNewUrlParser: true });

    setCollections(client);
}

export async function disconnect(): Promise<void> {
    if (!client) return;

    await client.close();
    client = undefined;
}

export async function createUser(email: string, username: string, password: string): Promise<User> {
    const isUsernameBeingUsed = await users!.findOne({ username });

    if (isUsernameBeingUsed) {
        throw new UsernameAlreadyBeingUsedError();
    }

    const user: User = { 
        email,
        username,
        password,
        name: '',
        biography: '',
        showEmail: false,
    };
    await users!.insertOne(user);

    return stripUnderscoreId(user);
}



export async function findUserByAuthenticationTokenHash(authenticationTokenHash: string): Promise<User | undefined> {
    const authenticationToken = await authenticationTokens!.findOne({ hash: authenticationTokenHash });

    if (!authenticationToken) return undefined;

    const user = await users!.findOne({ username: authenticationToken.userUsername });

    if (!user) {
        // tslint:disable-next-line no-console
        console.error(`Usuário com nome "${authenticationToken.userUsername}" autenticou porém não existe.`);
        return undefined;
    }

    return stripUnderscoreId(user);
}

export async function createApplication(userUsername: string, applicationName: string, applicationCode: string): Promise<Application> {
    const existingApplication = await applications!.findOne({ ownerUsername: userUsername, name: applicationName });

    if (existingApplication) throw new ApplicationNameAlreadyBeingUsedError();

    const application: Application = {
        ownerUsername: userUsername,
        name: applicationName,
        code: applicationCode,
        lastUpdated: new Date(),
    };
    await applications!.insertOne(application);

    return stripUnderscoreId(application);
}

export async function removeApplication(userUsername: string, applicationName: string): Promise<Application> {
    const query: FilterQuery<Application> = { ownerUsername: userUsername, name: applicationName };
    const application = await applications!.findOne(query);

    if (!application) {
        throw new ApplicationDoesNotExistError();
    }

    await applications!.deleteOne(query);

    return stripUnderscoreId(application);
}

export async function getApplications(userUsername: string): Promise<Application[]> {
    const sort: Partial<Record<keyof Application, number>> = { lastUpdated: -1 };
    return stripUnderscoreId(await applications!.find({ ownerUsername: userUsername }).sort(sort).toArray());
}

export async function getApplication(userUsername: string, applicationName: string): Promise<Application | null> {
    const application = await applications!.findOne({ ownerUsername: userUsername, name: applicationName });
    return stripUnderscoreId(application);
}

export async function updateApplication(userUsername: string, applicationName: string, changes: Partial<Pick<Application, 'name' | 'code'>>) {
    const query: FilterQuery<Application> = { ownerUsername: userUsername, name: applicationName };
    const application = await applications!.findOne(query);

    if (!application) throw new ApplicationDoesNotExistError();

    if (changes.name !== undefined && changes.name !== applicationName) {
        const existingApplication = await applications!.findOne({ ownerUsername: userUsername, name: changes.name });

        if (existingApplication) throw new ApplicationNameAlreadyBeingUsedError();
    }

    await applications!.replaceOne({ ownerUsername: userUsername, name: applicationName }, { ...application, ...changes, lastUpdated: new Date() });
}

export async function findUserByUsername(username: string): Promise<User | null> {
    const query: FilterQuery<User> = { username };
    const user = await users!.findOne(query);

    if (!user) return null;

    return stripUnderscoreId(user);
}

export async function updateUserByUsername<T extends keyof User>(username: string, changes: Pick<User, T>) {
    const query: FilterQuery<User> = { username };
    await users!.updateOne(query, { $set: changes });
}
