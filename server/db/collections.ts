import { Collection, MongoClient } from 'mongodb';

export type Application = {
    ownerUsername: string,
    name: string,
    code: string,
    lastUpdated: Date,
};

export type User = {
    showEmail: boolean,
    name: string,
    biography: string,
    email: string,
    username: string,
    password: string,
};

export type AuthenticationToken = {
    creationDate: Date,
    hash: string,
    userUsername: string,
};

export type InstitutionIdentity = UnivaliIdentity;

export type UnivaliIdentity = {
    institution: 'UNIVALI',
    personCode: string,
    userUsername: string,
};

export let applications: Collection<Application> | undefined;
export let users: Collection<User> | undefined;
export let authenticationTokens: Collection<AuthenticationToken> | undefined;
export let institutionsIdentities: Collection<InstitutionIdentity> | undefined;

export function setCollections(client: MongoClient) {
    const db = client.db();

    applications = db.collection('applications');
    users = db.collection('users');
    authenticationTokens = db.collection('authentication-tokens');
    institutionsIdentities = db.collection('instituitions-identities');
}