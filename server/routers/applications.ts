
import { Router } from 'express';
import * as fs from 'fs';
import * as HttpStatus from 'http-status-codes';
import * as Joi from 'joi';
import * as multer from 'multer';
import * as path from 'path';
import * as db from '../db';
import { Application } from '../db/collections';
import { ApplicationDoesNotExistError, ApplicationNameAlreadyBeingUsedError } from '../db/errors';
import { 
    addFile,
    existsFile,
    getFile,
    getFilesNames,
    removeApplicationFolder,
    removeFile,
    renameApplicationFolder,
    renameFile,
} from '../file-system';
import { loadUser } from '../load-user';

const upload = multer({ dest: path.join(__dirname, '../upload-temp') });

const router = Router();

router.post('/', loadUser(true), async (request, response) => {
    type BodySchema = {
        name: string,
        code: string,
    };
    const bodySchema = Joi.object().keys({
        name: Joi.string().required(),
        code: Joi.string().required().allow(''),
    });

    let body: BodySchema;

    try {
        body = await Joi.validate<BodySchema>(request.body, bodySchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    try {
        await db.createApplication(request.user!.username, body.name, body.code);
    } catch (error) {
        if (error instanceof ApplicationNameAlreadyBeingUsedError) {
            return response.status(HttpStatus.CONFLICT).json();
        }
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    return response.json();
});

router.patch('/:name', loadUser(true), async (request, response) => {
    type ParamsSchema = {
        name: string,
    };
    const paramsSchema = Joi.object().keys({
        name: Joi.string().required(),
    });
    type BodySchema = {
        name?: string,
        code?: string,
    };
    const bodySchema = Joi.object().keys({
        name: Joi.string().optional(),
        code: Joi.string().optional(),
    });

    let params: ParamsSchema;
    let body: BodySchema;

    try {
        params = await Joi.validate<ParamsSchema>(request.params, paramsSchema);
        body = await Joi.validate<BodySchema>(request.body, bodySchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    try {
        await db.updateApplication(request.user!.username, params.name, body);

        // update application folder name if it has changed
        if (body.name !== undefined && body.name !== params.name) {
            await renameApplicationFolder(params.name, body.name, request.user!.username);
        }
    } catch (error) {
        if (error instanceof ApplicationDoesNotExistError) {
            return response.status(HttpStatus.NOT_FOUND).json();
        }
        if (error instanceof ApplicationNameAlreadyBeingUsedError) {
            return response.status(HttpStatus.CONFLICT).json();
        }
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    return response.json();
});

router.delete('/:name', loadUser(true), async (request, response) => {
    type ParamsSchema = {
        name: string,
    };
    const paramsSchema = Joi.object().keys({
        name: Joi.string().required(),
    });

    let params: ParamsSchema;

    try {
        params = await Joi.validate<ParamsSchema>(request.params, paramsSchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    try {
        await db.removeApplication(request.user!.username, params.name);
        await removeApplicationFolder(params.name, request.user!.username);
    } catch (error) {
        if (error instanceof ApplicationDoesNotExistError) {
            return response.status(HttpStatus.NOT_FOUND).json();
        }
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    return response.json();
});

router.get('/', loadUser(false), async (request, response) => {
    type QuerySchema = {
        username?: string,
    };
    const querySchema = Joi.object().keys({
        username: Joi.string().optional(),
    });

    let query: QuerySchema;

    try {
        query = await Joi.validate<QuerySchema>(request.query, querySchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    if (!query.username && !request.user) {
        return response.status(HttpStatus.UNAUTHORIZED).json();
    }

    let applications: Application[];

    try {
        applications = await db.getApplications(query.username || request.user!.username);
    } catch (error) {
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    return response.json(applications);
});

router.get('/:name', loadUser(false), async (request, response) => {
    type ParamsSchema = {
        name: string,
    };
    const paramsSchema = Joi.object().keys({
        name: Joi.string().required(),
    });

    type QuerySchema = {
        username?: string,
    };
    const querySchema = Joi.object().keys({
        username: Joi.string().optional(),
    });

    let params: ParamsSchema;
    let query: QuerySchema;

    try {
        params = await Joi.validate<ParamsSchema>(request.params, paramsSchema);
        query = await Joi.validate<QuerySchema>(request.query, querySchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    if (!query.username && !request.user) {
        return response.status(HttpStatus.UNAUTHORIZED).json();
    }

    let application: Application & { filesNames?: string[] } | null;

    try {
        application = await db.getApplication(query.username || request.user!.username, params.name);
        if (application) {
            application.filesNames = await getFilesNames(application.ownerUsername, application.name);
        }
    } catch (error) {
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    if (!application) {
        return response.status(HttpStatus.NOT_FOUND).json();
    }

    return response.json(application);
});

// TODO: ALL THESE FILE OPERATIONS THE USER SHOULD BE THE OWNER OF THE PROJECT :D
router.post('/:name/files/rename', loadUser(true), async (request, response) => {
    type ParamsSchema = {
        name: string,
    };
    const paramsSchema = Joi.object().keys({
        name: Joi.string().required(),
    });
    type BodySchema = {
        changes: Array<[string, string]>,
    };
    const bodySchema = {
        changes: Joi.array().items(Joi.array().items(Joi.string()).length(2)),
    };

    let params: ParamsSchema;
    let body: BodySchema;

    try {
        params = await Joi.validate<ParamsSchema>(request.params, paramsSchema);
        body = await Joi.validate<BodySchema>(request.body, bodySchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    try {
        await Promise.all(body.changes.map(([oldName, newName]) => {
            return renameFile(oldName, newName, request.user!.username, params.name);
        }));
    } catch (error) {
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    return response.json();
});

router.post('/:name/files/:fileName', loadUser(true), upload.single('file'), async (request, response) => {
    type ParamsSchema = {
        name: string,
        fileName: string,
    };
    const paramsSchema = Joi.object().keys({
        name: Joi.string().required(),
        fileName: Joi.string().required(),
    });

    let params: ParamsSchema;

    try {
        params = await Joi.validate<ParamsSchema>(request.params, paramsSchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    if (await existsFile(params.fileName, request.user!.username, params.name)) {
        return response.status(HttpStatus.CONFLICT).json();
    }

    try {
        await addFile(
            request.file.path,
            params.fileName,
            request.user!.username, params.name,
        );
    } catch (error) {
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    fs.unlink(request.file.path, (error) => {
        if (error) {
            // tslint:disable-next-line
            console.error('Could not remove temporary file.', error);
        }
    });

    return response.json();
});

router.get('/:name/files', loadUser(false), async (request, response) => {
    type ParamsSchema = {
        name: string,
    };
    const paramsSchema = Joi.object().keys({
        name: Joi.string().required(),
    });

    type QuerySchema = {
        username?: string,
    };
    const querySchema = Joi.object().keys({
        username: Joi.string().optional(),
    });

    let params: ParamsSchema;
    let query: QuerySchema;

    try {
        params = await Joi.validate<ParamsSchema>(request.params, paramsSchema);
        query = await Joi.validate<QuerySchema>(request.query, querySchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    if (!query.username && !request.user) {
        return response.status(HttpStatus.UNAUTHORIZED).json();
    }

    let filesNames: string[];

    try {
        const application = await db.getApplication(query.username || request.user!.username, params.name);

        if (!application) {
            return response.status(HttpStatus.NOT_FOUND).json();
        }

        filesNames = await getFilesNames(application.ownerUsername, application.name);
    } catch (error) {
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    const files = filesNames.map(name => ({ name }));

    return response.json(files);
});

router.delete('/:name/files', loadUser(true), async (request, response) => {
    type ParamsSchema = {
        name: string,
    };
    const paramsSchema = Joi.object().keys({
        name: Joi.string().required(),
    });
    type BodySchema = {
        filesNames: string[],
    };
    const bodySchema = Joi.object().keys({
        filesNames: Joi.array().items(Joi.string()),
    });

    let params: ParamsSchema;
    let body: BodySchema;

    try {
        params = await Joi.validate<ParamsSchema>(request.params, paramsSchema);
        body = await Joi.validate<BodySchema>(request.body, bodySchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    try {
        await Promise.all(body.filesNames.map(async (fileName) => {
            if (await existsFile(fileName, request.user!.username, params.name)) {
                await removeFile(fileName, request.user!.username, params.name);
            }
        }));
    } catch (error) {
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    return response.json();
});

router.get('/:name/files/:fileName', loadUser(true), async (request, response) => {
    type ParamsSchema = {
        name: string,
        fileName: string,
    };
    const paramsSchema = Joi.object().keys({
        name: Joi.string().required(),
        fileName: Joi.string().required(),
    });

    let params: ParamsSchema;

    try {
        params = await Joi.validate<ParamsSchema>(request.params, paramsSchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).send();
    }

    if (!await existsFile(params.fileName, request.user!.username, params.name)) {
        return response.status(HttpStatus.NOT_FOUND).send();
    }

    let file: Buffer;

    try {
        file = await getFile(params.fileName, request.user!.username, params.name);
    } catch (error) {
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    return response.send(file);
});

export default router;