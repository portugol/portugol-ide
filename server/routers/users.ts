import { Router } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as Joi from 'joi';
import * as db from '../db';
import { User as DBUSer } from '../db/collections';
import { loadUser } from '../load-user';

export type UserSelf = {
    isSelf: true,
    username: string,
    name: string,
    email: string,
    biography: string,
    showEmail: boolean,
};
export type UserOther = {
    isSelf: false,
    username: string,
    name: string,
    biography: string,
    email?: string,
};
export type User = UserSelf | UserOther;

const router = Router();

router.get('/:username', loadUser(false), async (request, response) => {
    type ParamsSchema = {
        username: string,
    };
    const paramsSchema = Joi.object().keys({
        username: Joi.string().required(),
    });

    let params: ParamsSchema;

    try {
        params = await Joi.validate<ParamsSchema>(request.params, paramsSchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    let user: DBUSer | null;

    try {
        user = await db.findUserByUsername(params.username);
    } catch (error) {
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    if (user === null) {
        return response.status(HttpStatus.NOT_FOUND).json();
    }

    let responseUser: User;

    if (request.user && request.user.username === user.username) {
        responseUser ={ 
            isSelf: true,
            biography: user.biography,
            email: user.email,
            name: user.name,
            username: user.username,
            showEmail: user.showEmail,
        };
    } else {
        responseUser = {
            isSelf: false,
            biography: user.biography,
            email: user.showEmail ? user.email : undefined,
            name: user.name,
            username: user.username,
        };
    }

    return response.json(responseUser);
});

router.patch('/:username', loadUser(true), async (request, response) => {
    type ParamsSchema = {
        username: string,
    };
    const paramsSchema = Joi.object().keys({
        username: Joi.string().required(),
    });

    type BodySchema = {
        email?: string,
        name?: string,
        biography?: string,
        showEmail?: boolean,
    };
    const bodySchema = Joi.object().keys({
        email: Joi.string().optional(),
        name: Joi.string().optional(),
        biography: Joi.string().optional(),
        showEmail: Joi.boolean().optional(),
    });

    let params: ParamsSchema;
    let body: BodySchema;

    try {
        params = await Joi.validate<ParamsSchema>(request.params, paramsSchema);
        body = await Joi.validate<BodySchema>(request.body, bodySchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    if (params.username !== request.user!.username) {
        return response.status(HttpStatus.FORBIDDEN).json();
    }

    try {
        const changes = {};
        for (const key of Object.keys(body)) {
            if (body[key] !== undefined) {
                changes[key] = body[key];
            }
        }
        await db.updateUserByUsername(params.username, changes);
    } catch (error) {
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    return response.json();
});

export default router;