import { Router } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as Joi from 'joi';
import { createUser } from '../db';
import { authenticate, authenticateWithUnivali, deauthenticate, hasUnivaliAccount, registerWithUnivali } from '../db/authentication';
import { AuthenticationToken } from '../db/collections';
import { IdentityAlreadyInUseError, InvalidPasswordError, UserDoesNotExistError, UsernameAlreadyBeingUsedError } from '../db/errors';
import { loadUser } from '../load-user';
import signInWithUnivali from '../signin-with-univali';

const router = Router();

router.post('/create', async (request, response) => {
    type BodySchema = {
        username: string,
        password: string,
        email: string,
    };
    const bodySchema = Joi.object().keys({
        username: Joi.string().required(),
        password: Joi.string().required(),
        email: Joi.string().required(),
    });

    let body: BodySchema;

    try {
        body = await Joi.validate<BodySchema>(request.body, bodySchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    try {
        await createUser(body.email, body.username, body.password);
    } catch (error) {
        if (error instanceof UsernameAlreadyBeingUsedError) {
            return response.status(HttpStatus.CONFLICT).json();
        }
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    return response.json();
});

router.post('/authenticate', async (request, response) => {
    type BodySchema = {
        username: string,
        password: string,
    };
    const bodySchema = Joi.object().keys({
        username: Joi.string().required(),
        password: Joi.string().required(),
    });

    let body: BodySchema;

    try {
        body = await Joi.validate<BodySchema>(request.body, bodySchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    let authenticationToken: AuthenticationToken;

    try {
        authenticationToken = await authenticate(body.username, body.password);
    } catch (error) {
        if (error instanceof UserDoesNotExistError) {
            return response.status(HttpStatus.NOT_FOUND).json();
        }
        if (error instanceof InvalidPasswordError) {
            return response.status(HttpStatus.UNAUTHORIZED).json();
        }
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    return response.json(authenticationToken);
});

router.post('/authenticate/univali', async (request, response) => {
    type BodySchema = {
        username?: string,
        personCode: string,
        password: string,
    };
    const bodySchema = Joi.object().keys({
        username: Joi.string().optional(),
        personCode: Joi.string().required(),
        password: Joi.string().required(),
    });


    let body: BodySchema;

    try {
        body = await Joi.validate<BodySchema>(request.body, bodySchema);
    } catch (error) {
        return response.status(HttpStatus.BAD_REQUEST).json();
    }

    let univaliUser: { email: string, name: string };

    try {
        univaliUser = await signInWithUnivali(body.personCode, body.password);
    } catch (error) {
        return response.status(HttpStatus.UNAUTHORIZED).json();
    }

    let authenticationToken: AuthenticationToken;

    if (await hasUnivaliAccount(body.personCode)) {
        // AUTHENTICATE
        authenticationToken = await authenticateWithUnivali(body.personCode);
    } else {
        // REGISTER
        if (!body.username) {
            return response.status(HttpStatus.NOT_FOUND).json();
        }
        try {
            await registerWithUnivali(
                body.personCode, 
                univaliUser.email, 
                body.username, 
                univaliUser.name,
            );
        } catch (error) {
            if (error instanceof IdentityAlreadyInUseError || error instanceof UsernameAlreadyBeingUsedError) {
                return response.status(HttpStatus.CONFLICT).json();
            }
            return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
        }
        authenticationToken = await authenticateWithUnivali(body.personCode);
    }

    return response.json(authenticationToken);
});

router.post('/deauthenticate', loadUser(true), async (request, response) => {
    try {
        await deauthenticate(request.user!.authenticationTokenHash);
    } catch (error) {
        return response.status(HttpStatus.INTERNAL_SERVER_ERROR).json();
    }

    return response.json();
});

export default router;